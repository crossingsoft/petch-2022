﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Principal;
using System.Web;
using System.Web.Mvc;
using PanasonicProject.ViewModels;

namespace PanasonicProject.Infrastructure.Security
{
    public class IPECTHPrincipal : System.Security.Principal.IPrincipal
    {
        public bool IsInRole(string role)
        {
            return true;
            //if (!this.Identity.IsAuthenticated || string.IsNullOrEmpty(role))
            //{
            //    return false;
            //}
            //role = role.Trim();
            //if (this.Roles == null)
            //{
            //    var orgGroupMember = DependencyResolver.Current.GetService<OrgGroupMemberService>();
            //    this.Roles = orgGroupMember.GetRoles(this.Identity.Name).ToArray();
            //}
            //return this.Roles.Contains(role);
        }

        public IIdentity Identity { get; private set; }

        public IPECTHPrincipal(IIdentity identity)
        {
            this.Identity = identity;
        }       

        public string EmployeeId { get; set; }

        public string FullName { get; set; }
        public string Branch { get; set; }
        public string[] Roles { get; set; }
        public List<AuthorizeViewModel> AuthorizeSystem { get; set; }
    }

    public class PECTHPrincipalSerializeModel
    {
        public string EmployeeId { get; set; }

        public string Username { get; set; }
        public string Branch { get; set; }
        public string FullName { get; set; }
        public string[] Roles { get; set; }
        public List<AuthorizeViewModel> AuthorizeSystem { get; set; }
    }
}