﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web;
using CrossingSoft.MVC.Infrastructure.Security;
using PanasonicProject.Models;
using PanasonicProject.Models.Enums;
using PanasonicProject.Repositories;
using PanasonicProject.Services.Abstracts;
using PanasonicProject.ViewModels.Report;

namespace PanasonicProject.Services
{
    public interface IApplicationFormService
    {
        ApplicationForm GetAppLine(long appId);
        //ApplicationForm Create(ApplicationForm appModel);
        IEnumerable<ApplicationForm> GetListApplicationForm(BaseCriteriaReport criteria);
    }
    public class ApplicationFormService : BaseService<ApplicationForm>, IApplicationFormService
    {
        private readonly BasicRepository<ApplicationForm> _repository;

        public ApplicationFormService(BasicRepository<ApplicationForm> repository)
            : base(repository)
        {
            _repository = repository;
        }
        public ApplicationForm GetAppLine(long appId)
        {
            return Load(m => m.ApplId == appId);
        }
        public ApplicationForm GetAppLine(long? appId)
        {
            return Load(m => m.ApplId == appId);
        }
        //public ApplicationForm Create(ApplicationForm appModel)
        //{
        //    return Create(appModel);
        //}
        public ApplicationForm UpdateAppLine(ApplicationForm appModel)
        {
            return Update(appModel);
        }
        public bool UpdateWorkflowApp(long? id, string status, string documentstatus, string processInstance)
        {
            if (id == null || id == 0)
            {
                return false;
            }
            var appmodel = GetAppLine(id);
            var _status = appmodel.Status.ToUpper();
            if (_status != Resource.DocumentStatus.Draft.ToUpper() && _status != Resource.DocumentStatus.OnProcess.ToUpper() && _status != Resource.DocumentStatus.Completed.ToUpper())
            {
                return false;
            }

            appmodel.Status = status;
            appmodel.DocumentStatus = documentstatus;
            appmodel.ProcessInstance = processInstance;
            UpdateAppLine(appmodel);
            SaveChanges();
            return true;

        }
        public long GetNumLastRow()
        {
            long id = 1;
            var lastOrDefault = GetAll().LastOrDefault();
            if (lastOrDefault != null)
            {
                id = lastOrDefault.ApplId + 1;
            }
            return id;
        }

        public IEnumerable<ApplicationForm> GetListApplicationForm(BaseCriteriaReport criteria)
        {
            if (criteria.EndDate == null)
                criteria.EndDate = DateTime.Now;
            if (criteria.StartDate == null)
                criteria.StartDate = DateTime.Now.AddYears(-10);

            if (string.IsNullOrEmpty(criteria.DocumentNumber))
            {
                var models = _repository.Find(m => m.Status != Resource.DocumentStatus.Draft && m.CreatedDate >= criteria.StartDate && m.CreatedDate <= criteria.EndDate);

                if (!string.IsNullOrEmpty(criteria.CreateBy))
                {
                    models = models.Where(m => m.CreatedDisplayName.Contains(criteria.CreateBy));
                }
                if (!string.IsNullOrEmpty(criteria.DepartmentId))
                {
                    models = models.Where(m => m.DepartmentId.Contains(criteria.DepartmentId));
                }
                //if (!string.IsNullOrEmpty(criteria.SectionId))
                //{
                //    models = models.Where(m => m.SectionId == criteria.SectionId);
                //}
                return models.ToListSafe();
            }
            else
            {
                //var models = _repository.Find(m => m.Status == Resource.DocumentStatus.Approved && m.CreatedDate >= criteria.StartDate && m.CreatedDate <= criteria.EndDate && m.ApplNo.Contains(criteria.DocumentNumber));
                var models = _repository.Find(m => m.Status == Resource.DocumentStatus.Approved && m.CreatedDate >= criteria.StartDate && m.CreatedDate <= criteria.EndDate && m.IntApplId.Contains(criteria.DocumentNumber));
                if (!string.IsNullOrEmpty(criteria.CreateBy))
                {

                    models = models.Where(m => m.CreatedDisplayName.Contains(criteria.CreateBy));
                }

                return models.ToListSafe();
            }
        }

    }
}