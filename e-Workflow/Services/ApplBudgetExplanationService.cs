﻿using System.Collections.Generic;
using System.Linq;
using PanasonicProject.Models;
using PanasonicProject.Repositories;
using PanasonicProject.Services.Abstracts;

namespace PanasonicProject.Services
{
    public interface IApplBudgetExplanationService
    {
        
    }
    public class ApplBudgetExplanationService : BaseService<ApplBudgetExplanation>, IApplBudgetExplanationService
    {
        private readonly BasicRepository<ApplBudgetExplanation> _repository;

        public ApplBudgetExplanationService(BasicRepository<ApplBudgetExplanation> repository)
            : base(repository)
        {
            _repository = repository;
        }
       
    }
}