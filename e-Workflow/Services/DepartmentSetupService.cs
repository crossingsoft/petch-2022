﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using PanasonicProject.Models;
using PanasonicProject.Repositories;
using PanasonicProject.Services.Abstracts;

namespace PanasonicProject.Services
{
    public interface IDepartmentSetup
    {
        IEnumerable<DepartmentSetup> GetDepartmentSetupLineByUser(string user);
        DepartmentSetup GetDepartmentSetupLineByDepartment(string department);
    }
    public class DepartmentSetupService : BaseService<DepartmentSetup>, IDepartmentSetup
    {
        private readonly BasicRepository<DepartmentSetup> _repository;

        public DepartmentSetupService(BasicRepository<DepartmentSetup> repository)
            : base(repository)
        {
            _repository = repository;
        }

        public IEnumerable<DepartmentSetup> GetDepartmentSetupLineByUser(string user)
        {
            return GetAll(m => m.UserId == user);
        }
        public IEnumerable<DepartmentSetup> GetDepartmentSetupLineByUser(string user, bool active)
        {
            return GetAll(m => m.UserId == user && m.IsActive == active);
        }
        public DepartmentSetup GetDepartmentSetupLineByDepartment(string department)
        {
            return Load(m => m.DepartmentId == department);
        }
        public IEnumerable<IGrouping<string, DepartmentSetup>> GetReceiptSetupGroupByDepartmentByUserReceived(string userid)
        {
            return GetAll(m => m.UserId == userid && m.IsActive == true && m.Type != "Unlock").GroupBy(m => m.DepartmentId);
        }
        public IEnumerable<IGrouping<string, DepartmentSetup>> GetReceiptSetupGroupByDepartmentByUserUnlock(string userid)
        {
            return GetAll(m => m.UserId == userid && m.IsActive == true && m.Type != "Received").GroupBy(m => m.DepartmentId);
        }
    }
}