﻿using System.Collections.Generic;
using System.Linq;
using PanasonicProject.Models;
using PanasonicProject.Repositories;
using PanasonicProject.Services.Abstracts;

namespace PanasonicProject.Services
{
    public interface IBusinessPlanLineService
    {
        BusinessPlanLine GetBPLine(string id);
        BusinessPlanLine GetBPLine(string businessPlanType, string fisicalyear, string sectionid, string period);
        IEnumerable<BusinessPlanLine> GetBPLineByBPType(string year);
        IEnumerable<BusinessPlanLine> GetBPLineByFisicalYear(string typename);
        IEnumerable<BusinessPlanLine> GetBPLineByBPGroup(string BPGroupid);
    }
    public class BusinessPlanLineService : BaseService<BusinessPlanLine>, IBusinessPlanLineService
    {
        private readonly BasicRepository<BusinessPlanLine> _repository;

        public BusinessPlanLineService(BasicRepository<BusinessPlanLine> repository) 
            : base(repository)
        {
            _repository = repository;
        }
        public BusinessPlanLine GetBPLine(string id)
        {
            return Load(m => m.BusinessPlanId == id);
        }

        public BusinessPlanLine GetBPLine(string businessPlanType, string fisicalyearId, string sectionid, string period)
        {
            return Load(m => m.BusinessPlanType == businessPlanType && m.FisicalYearId == fisicalyearId && m.SectionId == sectionid && m.Period == period);
        }

        public IEnumerable<BusinessPlanLine> GetBPLineByBPType(string typename)
        {
            return GetAll(m => m.BusinessPlanType == typename);
        }

        public IEnumerable<BusinessPlanLine> GetBPLineByFisicalYear(string year)
        {
            return GetAll(m => m.FisicalYearId == year);
        }

        public IEnumerable<BusinessPlanLine> GetBPLineByBPGroup(string BPGroupid)
        {
            return GetAll(m => m.BPGroup == BPGroupid);
        }
        public IEnumerable<BusinessPlanLine> GetBPLineByBPSection(string fiscalyear, string section, string bptype)
        {
            return GetAll(m => m.SectionId == section 
                && m.BusinessPlanType == bptype 
                && m.FisicalYearId == fiscalyear);
        }
    }
}