﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Configuration;
using CrossingSoft.MVC.Repositories;
using CrossingSoft.MVC.Services;
using PanasonicProject.ViewModels;
using SourceCode.Hosting.Client.BaseAPI;
using SourceCode.Workflow.Client;
using WorklistItem = PanasonicProject.Models.WorklistItem;

namespace PanasonicProject.Services
{
    public class WorkflowService : BaseWorkflowService
    {
        public Dictionary<string, string> ProcessList = new Dictionary<string, string>();
        private string _securityLabel = WebConfigurationManager.AppSettings["SecurityLabel"];
        private string _k2Server = WebConfigurationManager.AppSettings["K2Server"];
        private int _hostPort = Int32.Parse(WebConfigurationManager.AppSettings["HostPort"]);
        private int _wfPort = Int32.Parse(WebConfigurationManager.AppSettings["WFPort"]);
        private string _domain = WebConfigurationManager.AppSettings["Domain"];
        private string _userID = WebConfigurationManager.AppSettings["UserID"];
        private string _password = WebConfigurationManager.AppSettings["Password"];
        private SCConnectionStringBuilder AdminSCBuilder { get; set; }
        private readonly ADUserTableService adUserTableService;
        private readonly IWorkflowRepository _repository;

        public WorkflowService(IWorkflowRepository repository, ADUserTableService adUserTableService)
            : base(repository)
        {
            _repository = repository;
            this.adUserTableService = adUserTableService;

            //ProcessList.Add("RFP", "Request for price");
            //ProcessList.Add("PR", "Purchase Requisition");
            //ProcessList.Add("PP", "Purchase Process");
            //ProcessList.Add("PO", "Purchase Order");
            //ProcessList.Add("GR", "Goods Receipt");
            //ProcessList.Add("GI", "Goods Issue");

            this.AdminSCBuilder = AdminSCConnectionStringBuilder();
        }

        public SCConnectionStringBuilder AdminSCConnectionStringBuilder()
        {
            SCConnectionStringBuilder scBuilder = new SCConnectionStringBuilder();

            scBuilder.Authenticate = true;
            scBuilder.Host = _k2Server;
            scBuilder.Integrated = true;
            scBuilder.IsPrimaryLogin = true;
            scBuilder.Port = Convert.ToUInt32(_hostPort);
            scBuilder.SecurityLabelName = _securityLabel;
            scBuilder.WindowsDomain = _domain;
            scBuilder.UserID = _userID;
            scBuilder.Password = _password;

            return scBuilder;
        }

        public string FQN(string name)
        {
            if (!name.StartsWith(_securityLabel + ":"))
                name = _securityLabel + ":" + name;
            return name;
        }

       
        public List<string> GetDestination(string sn)
        {
            var worklistItem = GetWorkListItem(sn);
            var destinationList = new List<string>();
            foreach (SourceCode.Workflow.Client.Destination destination in worklistItem.DelegatedUsers)
            {
                destinationList.Add(destination.Name);
            }

            return destinationList;
        }

        public DataFields GetDataFields(string sn)
        {
            var worklistItem = GetWorkListItem(sn);
            return worklistItem.ProcessInstance.DataFields;
        }

        public WorklistViewModel MapperWorklist(Worklist worklist)
        {
            try
            {
                var userDisabled = "";
                var worklistViewModel = new WorklistViewModel();
                var worklistItems = new List<WorklistItem>();
                var viewFlowUrl = _repository.ViewFlowURL();

                var list = new List<WorklistItem>();
                foreach (SourceCode.Workflow.Client.WorklistItem worklistItem in worklist)
                {
                    var getUser = adUserTableService.GetUser(worklistItem.ProcessInstance.Originator.Name);

                    
                    if (getUser == null)
                    {
                        userDisabled = userDisabled+" : " + worklistItem.ProcessInstance.Originator.Name;
                        //throw new Exception(worklistItem.ProcessInstance.Originator.Name+" "+);
                        //continue;
                    } 

                    var displayname = getUser== null? worklistItem.ProcessInstance.Originator.Name+" (disabled)" :  getUser.DisplayName;
                    if (worklistItem.ProcessInstance.FullName.Contains(ConfigurationManager.AppSettings["PathPECTHWorkflow"]) || worklistItem.ProcessInstance.FullName.Contains("K2_eRequestSheet\\workflow"))
                        list.Add(new WorklistItem
                        {
                            ActivityName = worklistItem.ActivityInstanceDestination.Name,
                            Data = worklistItem.Data,
                            DescProcessName = worklistItem.ProcessInstance.Description,
                            DueStatus = worklistItem.CalculateDueStatus(),
                            Folio = worklistItem.ProcessInstance.Folio,
                            ProcessInstanceId = worklistItem.ProcessInstance.ID,
                            ProcessName = worklistItem.ProcessName(),
                            SN = worklistItem.SerialNumber,
                            ActivityStartDate = worklistItem.ActivityInstanceDestination.StartDate,
                            ProcessStartDate = worklistItem.ProcessInstance.StartDate,
                            Status = worklistItem.Status.ToString(),
                            Title = worklistItem.GetTitle(),
                            CreateBy = displayname,
                            ViewFlow = _repository.ViewFlowURL(worklistItem.ProcessInstance.ID.ToString()),
                            EnableRelease = (worklistItem.Status.ToString() == "Open"),
                            Subject = worklistItem.ProcessInstance.DataFields["DFTitle"].Value.ToString()
                        });
                }
                worklistItems.AddRange(list.OrderByDescending(m => m.ActivityStartDate));
                worklistViewModel.WorklistItems = worklistItems;
                //throw new Exception(userDisabled);
                return worklistViewModel;
            }
            catch (Exception exception)
            {
                throw new Exception(exception.Message);
            }
        }

        public WorklistViewModel InitWorklistViewModel(string searchQuery = null)
        {
            SourceCode.Workflow.Client.Worklist worklist = this.GetWorklist(InitWorklistCriteria(searchQuery));
            return this.MapperWorklist(worklist);
        }

        public WorklistViewModel InitWorklistViewModel(string searchQuery, string processName)
        {
            if (processName.Equals("All", StringComparison.InvariantCultureIgnoreCase))
                return InitWorklistViewModel(searchQuery);

            Worklist worklist = this.GetWorklist(InitWorklistCriteria(searchQuery));

            return this.MapperWorklist(worklist);

            // Comment by Nattawat => Move Sub function
            //var worklistItems = new List<WorklistItem>();
            //var viewFlowUrl = _repository.ViewFlowURL();


            //var list = from SourceCode.Workflow.Client.WorklistItem worklistItem in worklist
            //           where worklistItem.ProcessInstance.Name == processName
            //           select new WorklistItem
            //           {
            //               ActivityName = worklistItem.ActivityInstanceDestination.Name,
            //               Data = worklistItem.Data,

            //               DueStatus = worklistItem.CalculateDueStatus(),
            //               Folio = worklistItem.ProcessInstance.Folio,
            //               ProcessInstanceId = worklistItem.ProcessInstance.ID,
            //               ProcessName = worklistItem.ProcessName(),
            //               SN = worklistItem.SerialNumber,
            //               ActivityStartDate = worklistItem.ActivityInstanceDestination.StartDate,
            //               ProcessStartDate = worklistItem.ProcessInstance.StartDate,
            //               Status = worklistItem.Status.ToString(),
            //               Title = worklistItem.GetTitle(),
            //               CreateBy = adUserTableService.GetUser(worklistItem.ProcessInstance.Originator.Name).DisplayName,
            //               ViewFlow = _repository.ViewFlowURL(worklistItem.ProcessInstance.ID.ToString()),
            //               Subject = worklistItem.ProcessInstance.DataFields["DFTitle"].Value.ToString() 
            //           };
            //worklistItems.AddRange(list.OrderByDescending(m => m.ActivityStartDate));
            //worklistViewModel.WorklistItems = worklistItems;
            //return worklistViewModel;
        }

        public WorklistViewModel InitWorklistViewModel(string searchQuery, string processName, string status)
        {
            if (status.Equals("All", StringComparison.InvariantCultureIgnoreCase))
                return InitWorklistViewModel(searchQuery, processName);

           
            SourceCode.Workflow.Client.Worklist worklist = this.GetWorklist(InitWorklistCriteria(searchQuery));

            return MapperWorklist(worklist);
            //Comment by Nattawat => Move Sub function
            //var worklistItems = new List<WorklistItem>();
            //var viewFlowUrl = _repository.ViewFlowURL();
            //var worklistViewModel = new WorklistViewModel();


            //var list = from SourceCode.Workflow.Client.WorklistItem worklistItem in worklist
            //           where worklistItem.ProcessInstance.Name == processName

            //           select new WorklistItem
            //           {
            //               ActivityName = worklistItem.ActivityInstanceDestination.Description,
            //               Data = worklistItem.Data,
            //               DueStatus = worklistItem.CalculateDueStatus(),
            //               Folio = worklistItem.ProcessInstance.Folio,
            //               ProcessInstanceId = worklistItem.ProcessInstance.ID,
            //               ProcessName = worklistItem.ProcessName(),
            //               SN = worklistItem.SerialNumber,
            //               ActivityStartDate = worklistItem.ActivityInstanceDestination.StartDate,
            //               ProcessStartDate = worklistItem.ProcessInstance.StartDate,
            //               Status = worklistItem.Status.ToString(),
            //               Title = worklistItem.GetTitle(),
            //               CreateBy = adUserTableService.GetUser(worklistItem.ProcessInstance.Originator.Name).DisplayName,
            //               ViewFlow = _repository.ViewFlowURL(worklistItem.ProcessInstance.ID.ToString()),
            //               Subject = worklistItem.ProcessInstance.DataFields["DFTitle"].Value.ToString()                          
            //           };
            //worklistItems.AddRange(list.OrderByDescending(m => m.ActivityStartDate));
            //worklistViewModel.WorklistItems = worklistItems;
            //return worklistViewModel;
        }

        public WorklistCriteria InitWorklistCriteria(string searchQuery)
        {
            var processPath = ConfigurationManager.AppSettings["PathPECTHWorkflow"];

            var worklistCriteria = new WorklistCriteria();
            searchQuery = "%" + searchQuery + "%";
            worklistCriteria.AddFilterField(WCLogical.StartBracket, WCField.ProcessFolio, WCCompare.Like, searchQuery);
            worklistCriteria.AddFilterField(WCLogical.Or, WCField.ProcessDescription, WCCompare.Like, searchQuery);
            worklistCriteria.AddFilterField(WCLogical.Or, WCField.ProcessName, WCCompare.Like, searchQuery);
            worklistCriteria.AddFilterField(WCLogical.Or, WCField.ActivityName, WCCompare.Like, searchQuery);
            worklistCriteria.AddFilterField(WCLogical.Or, WCField.ActivityDescription, WCCompare.Like, searchQuery);
            //worklistCriteria.AddFilterField(WCLogical.Or, WCField.ProcessStatus, WCCompare.Like, searchQuery);

            //worklistCriteria.AddFilterField(WCLogical.Or, WCField.ProcessStartDate, WCCompare.Like, searchQuery);
            //worklistCriteria.AddFilterField(WCLogical.Or, WCField.ActivityStartDate, WCCompare.Like, searchQuery);

            worklistCriteria.AddFilterField(WCLogical.Or, WCField.WorklistItemOwner, "Me", WCCompare.Equal, WCWorklistItemOwner.Me);
            worklistCriteria.AddFilterField(WCLogical.Or, WCField.WorklistItemOwner, "Other", WCCompare.Equal, WCWorklistItemOwner.Other);

            //worklistCriteria.AddFilterField(WCLogical.AndBracket, WCField.ProcessFullName, WCCompare.Like, processPath);
            return worklistCriteria;
        }

        
        

        public int GetSumPendingTask(string processName)
        {
            SourceCode.Workflow.Client.Worklist worklist = this.GetWorklist(InitWorklistCriteria(""));
            var list = from SourceCode.Workflow.Client.WorklistItem worklistItem in worklist
                       where worklistItem.ProcessInstance.Name == processName
                       select worklistItem;
            return list.Count();
        }

        public void Delegate(string SN, string toUserFQN)
        {
            Delegate(SN, HttpContext.Current.User.Identity.Name, toUserFQN);
        }

        public void Delegate(string SN, string userFQN, string toUserFQN)
        {
            userFQN = FQN(userFQN);
            toUserFQN = FQN(toUserFQN);

            SourceCode.Workflow.Client.WorklistItem wli = GetWorkListItem(SN);

            //code to delegate
            Destination _Destination = new Destination();
            //Allow actions to delegated user
            foreach (SourceCode.Workflow.Client.Action _action in wli.Actions)
            {
                _Destination.AllowedActions.Add(_action.Name);
            }
            _Destination.DestinationType = SourceCode.Workflow.Client.DestinationType.User;
            _Destination.Name = toUserFQN;
            wli.Delegate(_Destination);
        }


        public bool FindWorkListItem(string SN)
        {
            try
            {
                var wli = GetWorkListItem(SN);
                return true;
            }
            catch (Exception)
            {
                return false;
            }

        }

        public void GetWorklistItem(string SN, string userFQN, string toUserFQN)
        {
            userFQN = FQN(userFQN);
            toUserFQN = FQN(toUserFQN);

            SourceCode.Workflow.Client.WorklistItem wli = GetWorkListItem(SN);

        }

        public WorklistItem GetMyWorkListItem(string serialNumber)
        {

            var worklistCriteria = new WorklistCriteria();
            worklistCriteria.AddFilterField(WCField.SerialNumber, WCCompare.Equal,
                                                   serialNumber);
            var worklist = _repository.GetWorkList(worklistCriteria, false);
            var list = from SourceCode.Workflow.Client.WorklistItem worklistItem in worklist
                       select ToMyWorklistItem(worklistItem);

            return list.FirstOrDefault();

        }

        public bool IsHasWorklistItem(string sn)
        {
            try
            {
                var item = _repository.GetWorkListItem(sn);
                if (item == null) return false;
                else return true;
            }
            catch (Exception exception)
            {
                throw new Exception(exception.Message);
            }
            
        }

        private WorklistItem ToMyWorklistItem(SourceCode.Workflow.Client.WorklistItem worklistItem)
        {
            var displayname = adUserTableService.GetUser(worklistItem.ProcessInstance.Originator.Name).DisplayName;
            var item = new WorklistItem()
            {
                ActivityName = worklistItem.ActivityInstanceDestination.Name,
                Data = worklistItem.Data,
             
                DueStatus = worklistItem.CalculateDueStatus(),
                Folio = worklistItem.ProcessInstance.Folio,
                ProcessInstanceId = worklistItem.ProcessInstance.ID,
                ProcessName = worklistItem.ProcessInstance.Description,
                SN = worklistItem.SerialNumber,
                ActivityStartDate  = worklistItem.ActivityInstanceDestination.StartDate,
                ProcessStartDate = worklistItem.ProcessInstance.StartDate,
                Status = worklistItem.Status.ToString(),
                Title = worklistItem.GetTitle(),
                CreateBy = displayname,
                ViewFlow = _repository.ViewFlowURL(worklistItem.ProcessInstance.ID.ToString()),
                DescProcessName =  worklistItem.ProcessInstance.Description
            };

            return item;
        }


        public bool Allocate(string serialNumber)
        {
            try
            {
                var item = this.GetWorkListItem(serialNumber, false);
                if (!string.IsNullOrEmpty(item.AllocatedUser))
                    return false;

                GetWorkListItem(serialNumber, true);
                return true;

            }
            catch (Exception exception)
            {
                return false;
            }
        }

        public void Release(string serialNumber)
        {
            try
            {
                var item = this.GetWorkListItem(serialNumber, false);

                if (item == null)
                    throw new Exception(string.Format("Failed to release for Serial Number:{0}. Invalid worklist item.", serialNumber));

                item.Release();
            }
            catch (Exception exception)
            {
                throw new Exception(string.Format("Failed to release for Serial Number:{0}", serialNumber), exception);
            }
        }

        //public void UnDelegate(string SN)
        //{
        //    UnDelegate(SN, HttpContext.Current.User.Identity.Name);
        //}


        //public void UnDelegate(string SN, string toUserFQN)
        //{
        //        toUserFQN = FQN(toUserFQN);

        //        SourceCode.Workflow.Client.WorklistItem wli = GetWorkListItem(SN);
        //        wli.Release();
        //        var _Destination = wli.DelegatedUsers[toUserFQN];
        //        foreach (SourceCode.Workflow.Client.Action _action in wli.Actions)
        //        {
        //            _Destination.AllowedActions.Remove(_action.Name);

        //        }
        //        wli.Delegate(_Destination);
        //}
        

        
    }

    public static class WorklistItemExtensions
    {
        public static DateTime DueDate(this SourceCode.Workflow.Client.WorklistItem worklistItem)
        {
            return worklistItem.ActivityInstanceDestination.StartDate.AddSeconds(
                worklistItem.ActivityInstanceDestination.ExpectedDuration);
        }

        public static string CalculateDueStatus(this SourceCode.Workflow.Client.WorklistItem worklistItem)
        {
            //Green: current time < start + expected duration
            //Yellow: start + expected duration <= current time <start + expected duration + 24 hours
            //Red: current time > start + expected duration + 24 hours

            // If no ExpectedDuration set, leave it as on-time
            //if (worklistItem.ActivityInstanceDestination.ExpectedDuration == 0)
            //    return "ontime";

            var now = DateTime.Now;
            var dueDate = worklistItem.DueDate();
            if (now < dueDate)
                return "ontime";
            else if (dueDate <= now && now < dueDate.AddHours(24))
                return "due";
            else
                return "overdue";
        }


        public static string GetTitle(this SourceCode.Workflow.Client.WorklistItem worklistItem)
        {
            return worklistItem.ProcessInstance.Folio;
            
        }

        public static string ProcessName(this SourceCode.Workflow.Client.WorklistItem worklistItem)
        {
            return !string.IsNullOrEmpty(worklistItem.ProcessInstance.Description)
                       ? worklistItem.ProcessInstance.Description
                       : worklistItem.ProcessInstance.Name;
        }



        //public static string ViewFlow(this SourceCode.Workflow.Client.WorklistItem worklistItem, string baseUrl)
        //{
        //    return  worklistItem.ProcessInstance.ID);
        //}
    }
}