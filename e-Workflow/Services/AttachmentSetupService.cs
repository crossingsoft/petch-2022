﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using CrossingSoft.MVC.Services;
using PanasonicProject.Models;
using PanasonicProject.Repositories;
using PanasonicProject.Services;

namespace PanasonicProject.Services
{
    public interface IAttachmentSetupService
    {
        IEnumerable<AttachmentSetup> FilterActive();
    }
    public class AttachmentSetupService : BaseService<AttachmentSetup>, IAttachmentSetupService
    {
        private readonly BasicRepository<AttachmentSetup> _repository;

        public AttachmentSetupService(BasicRepository<AttachmentSetup> repository)
            : base(repository)
        {
            _repository = repository;
        }

        public IEnumerable<AttachmentSetup> FilterActive()
        {
            return GetAll(m => m.IsActive == true);
        }
    }
}