﻿using System.Collections.Generic;
using System.Linq;
using PanasonicProject.Models;
using PanasonicProject.Repositories;
using PanasonicProject.Services.Abstracts;

namespace PanasonicProject.Services
{
    public interface IApplFormBudgetService
    {
        ApplFormBudgetTable GetAppBudgetLine(long id);
        IEnumerable<ApplFormBudgetTable> GetAppBudgetLineByApp(long appId);
        IEnumerable<ApplFormBudgetTable> GetAppBudgetLineByBPNum(string bpNum);
        //ApplFormBudgetTable Create(ApplFormBudgetTable appModel);

    }
    public class ApplFormBudgetService : BaseService<ApplFormBudgetTable>, IApplFormBudgetService
    {
        private readonly BasicRepository<ApplFormBudgetTable> _repository;

        public ApplFormBudgetService(BasicRepository<ApplFormBudgetTable> repository)
            : base(repository)
        {
            _repository = repository;
        }
        public ApplFormBudgetTable GetAppBudgetLine(long id)
        {
            return Load(m => m.ID == id);
        }
        public IEnumerable<ApplFormBudgetTable> GetAppBudgetLineByApp(long appId)
        {
            return GetAll(m => m.ApplId == appId).ToListSafe();
        }
        public IEnumerable<ApplFormBudgetTable> GetAppBudgetLineByBPNum(string bpNum)
        {
            return GetAll(m => m.BPNum == bpNum).ToListSafe();
        }

        //public ApplFormBudgetTable Create(ApplFormBudgetTable appModel)
        //{
        //    return _repository.Create(appModel);
        //}
    }
}