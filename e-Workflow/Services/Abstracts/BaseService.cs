﻿using System.Collections.Generic;
using CrossingSoft.MVC.Repositories;
using PanasonicProject.Models;

namespace PanasonicProject.Services.Abstracts
{
    public abstract class BaseService<T> : CrossingSoft.MVC.Services.BaseService<T> where T : class, new()
    {
        protected BaseService(BaseRepository<T> repository)
            : base(repository)
        {
        }

        public string ApplicationParamNotfound(string appParam)
        {
            return string.Format("Applicaiton Parameter {0}  not found",appParam);
        }

        public string SysParamNotfound(string sysParam)
        {
            return string.Format("System Parameter {0}  not found", sysParam);
        }
    }
}