﻿using System.Collections.Generic;
using PanasonicProject.Services.Interfaces;
using PanasonicProject.ViewModels.Interfaces;

namespace PanasonicProject.Services.Abstracts
{
    public abstract class BaseReportService<TTable, TLine> : IReportService<TTable, TLine>
        where TTable : class, ITableViewModel, new()
        where TLine : class, ILineViewModel, new()
    {
        public IEnumerable<TTable> DefaultTable()
        {
            return new List<TTable>();
        }

        public IEnumerable<TLine> DefaultLine()
        {
            return new List<TLine>();
        }
        public virtual void RemoveAll()
        {
            new List<TTable>();
            new List<TLine>();
        }
    }
}