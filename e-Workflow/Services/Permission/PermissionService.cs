﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using PanasonicProject.Models;
using PanasonicProject.Models.Permission;
using PanasonicProject.Repositories;
using PanasonicProject.Services.Abstracts;

namespace PanasonicProject.Services.Permission
{
    public interface IPermissionService
    {

    }

    public class PermissionService : BaseService<Models.Permission.PermissionTable>, IPermissionService
    {
        private readonly BasicRepository<Models.Permission.PermissionTable> _repository;
        public PermissionService(BasicRepository<Models.Permission.PermissionTable> repository)
            : base(repository)
        {
            _repository = repository;
        }
    }
}