﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using PanasonicProject.Models;
using PanasonicProject.Models.Permission;
using PanasonicProject.Repositories;
using PanasonicProject.Services.Abstracts;

namespace PanasonicProject.Services.Permission
{
    public interface IPermissionMemberService
    {

    }

    public class PermissionMemberService : BaseService<PermissionMember>, IPermissionMemberService
    {
        private readonly BasicRepository<Models.Permission.PermissionMember> _repository;
        public PermissionMemberService(BasicRepository<Models.Permission.PermissionMember> repository)
            : base(repository)
        {
            _repository = repository;
        }
    }
}