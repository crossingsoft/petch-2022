﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using CrossingSoft.MVC.Services;
using PanasonicProject.Models;
using PanasonicProject.Repositories;

namespace PanasonicProject.Services
{
    public interface ISectionTableService
    {
        SectionTable GetSectionLine(long id);
    }
    public class SectionTableService : BaseService<SectionTable>, ISectionTableService
    {
        private readonly BasicRepository<SectionTable> _repository;

        public SectionTableService(BasicRepository<SectionTable> repository)
            : base(repository)
        {
            _repository = repository;
        }

        public SectionTable GetSectionLine(long id)
        {
            return Load(m => m.SectionId == id.ToString());
        }
        public SectionTable GetSectionLineByDivisionUser(long id, string divison)
        {
            var sectionModel = Load(m => m.SectionId == id.ToString() && m.Division == divison);
            return sectionModel ?? null;
        }
        public IEnumerable<SectionTable> GetSectionLineByDivision(string divison)
        {
            var sectionModel = GetAll(m => m.Division == divison);
            return sectionModel ?? null;
        }
        public IEnumerable<SectionTable> GetSectionLineByDepartment(string department)
        {
            var sectionModel = GetAll(m => m.Department == department);
            return sectionModel;
        }
        public IEnumerable<SectionTable> GetSectionLine(long section, string divison)
        {
            var sectionModel = GetAll(m => m.Division == divison && m.SectionId == section.ToString());
            return sectionModel ?? null;
        }
        public IEnumerable<SectionTable> GetSectionLine(string department, string divison)
        {
            var sectionModel = GetAll(m => m.Division == divison && m.Department == department);
            return sectionModel ?? null;
        }
        public List<SectionTable> GetListSectionFilter(long? sectionid)
        {
            if (sectionid == null)
            {
                return _repository.GetAll().ToListSafe();
            }
            else
            {
                return _repository.Find(m => m.SectionId == sectionid.ToString()).ToListSafe();
            }

        }
    }
}