﻿using System.Collections.Generic;
using System.Linq;
using PanasonicProject.Models;
using PanasonicProject.Repositories;
using PanasonicProject.Services.Abstracts;

namespace PanasonicProject.Services
{
    public class ApprovalStepService : BaseService<ApprovalStepTable>
    {
        private readonly BasicRepository<ApprovalStepTable> _repository;

        public ApprovalStepService(BasicRepository<ApprovalStepTable> repository)
            : base(repository)
        {
            _repository = repository;
        }
        public IEnumerable<ApprovalStepTable> GetpurchLineList(long approvId)
        {
            return GetAll(m => m.ApproID == approvId).ToListSafe();
        }
        public decimal? GetMaxAmount(string state, string workflowtype)
        {
            decimal? maxAmount = 0;
            var approvalStepTable = GetAll(m => m.ApproStateName == state && m.WorkflowType == workflowtype).FirstOrDefault();
            if (approvalStepTable != null)
            {
                maxAmount = approvalStepTable.MaxAmount;
            }
            return maxAmount;
        }
    }
}