﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using CrossingSoft.MVC.Services;
using PanasonicProject.Models;
using PanasonicProject.Repositories;

namespace PanasonicProject.Services
{
    public interface IDivisionTableService
    {
        Division GetDivisionLine(string id);
    }
    public class DivisionTableService : BaseService<Division>, IDivisionTableService
    {
        private readonly BasicRepository<Division> _repository;

        public DivisionTableService(BasicRepository<Division> repository)
            : base(repository)
        {
            _repository = repository;
        }

        public Division GetDivisionLine(string id)
        {
            return Load(m => m.ID == id);
        }
        public List<Division> GetListDivisionFilter(string divisionname)
        {
            if (string.IsNullOrEmpty(divisionname))
            {
                return _repository.GetAll().ToListSafe();
            }
            else
            {
                return _repository.Find(m => m.ID.Contains(divisionname)).ToListSafe();
            }

        }
    }
}