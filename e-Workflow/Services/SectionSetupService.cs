﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using CrossingSoft.MVC.Services;
using PanasonicProject.Models;
using PanasonicProject.Repositories;
using PanasonicProject.Services;

namespace PanasonicProject.Services
{
    public interface ISectionSetupService
    {
        IEnumerable<SectionSetup> GetSectionSetupLineByUser(string user);
    }
    public class SectionSetupService : BaseService<SectionSetup>, ISectionSetupService
    {
        private readonly BasicRepository<SectionSetup> _repository;

        public SectionSetupService(BasicRepository<SectionSetup> repository)
            : base(repository)
        {
            _repository = repository;
        }

        public IEnumerable<SectionSetup> GetSectionSetupLineByUser(string user)
        {
            return GetAll(m => m.UserId == user);
        }
        public SectionSetup GetSectionSetupLineBySecion(long sectionId)
        {
            return Load(m => m.SectionId == sectionId);
        }
    }
}