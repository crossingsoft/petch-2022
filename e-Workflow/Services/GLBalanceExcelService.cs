﻿using PanasonicProject.Models;
using PanasonicProject.Repositories;
using PanasonicProject.Services.Abstracts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PanasonicProject.Services
{
    public interface IGLBalanceExcelService
    {
    }
    public class GLBalanceExcelService : BaseService<GLBalanceExcel>, IGLBalanceExcelService
    {
        private readonly BasicRepository<GLBalanceExcel> _repository;

        public GLBalanceExcelService(BasicRepository<GLBalanceExcel> repository)
            : base(repository)
        {
            _repository = repository;
        }
    }
}