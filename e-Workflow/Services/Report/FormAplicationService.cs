﻿using System.Collections.Generic;
using PanasonicProject.Models;
using PanasonicProject.ViewModels.Report;

namespace PanasonicProject.Services.Report
{
    public interface IFormAplicationService
    {
        IEnumerable<FormAplicationViewModel.Table> GetHeader(ApplicationForm header);
        IEnumerable<FormAplicationViewModel.Line> GetBody(IEnumerable<ApplFormBudgetTable> table);
    }

    public class FormAplicationService : Abstracts.BaseReportService<FormAplicationViewModel.Table, FormAplicationViewModel.Line>, IFormAplicationService
    {

        public IEnumerable<FormAplicationViewModel.Table> GetHeader(ApplicationForm header)
        {
            var formTable = new List<FormAplicationViewModel.Table>();
            var formLine = new FormAplicationViewModel.Table();
            formTable.Add(formLine);
            return formTable;
        }

        public IEnumerable<FormAplicationViewModel.Line> GetBody(IEnumerable<ApplFormBudgetTable> table)
        {
            var formTable = new List<FormAplicationViewModel.Line>();
            var formLine = new FormAplicationViewModel.Line();
            formTable.Add(formLine);
            return formTable;
        }
    }
}