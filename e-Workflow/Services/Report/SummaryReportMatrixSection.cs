﻿using PanasonicProject.ViewModels;

namespace PanasonicProject.Services.Report
{
    public interface ISummaryReportMatrixSection
    {
    }
    public class SummaryReportMatrixSection : Abstracts.BaseReportService<SummaryReportMatrixSectionTableViewModel, SummaryReportMatrixSectionLineViewModel>, ISummaryReportMatrixSection
    {

    }
}