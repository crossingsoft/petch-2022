﻿using System.Collections.Generic;
using PanasonicProject.ViewModels;

namespace PanasonicProject.Services.Report
{
    public interface ISummaryReportService
    {
        IEnumerable<SummaryReportTableViewModel> GetHeader();
        IEnumerable<SummaryReportTableViewModel> GetBody();
    }
    public class SummaryReportService : Abstracts.BaseReportService<SummaryReportTableViewModel, SummaryReportLineViewModel>, ISummaryReportService
    {
        public IEnumerable<SummaryReportTableViewModel> GetHeader()
        {
            throw new System.NotImplementedException();
        }

        public IEnumerable<SummaryReportTableViewModel> GetBody()
        {
            throw new System.NotImplementedException();
        }
    }
}