﻿using System.Collections.Generic;
using PanasonicProject.Models;
using PanasonicProject.ViewModels;

namespace PanasonicProject.Services.Report
{
    public interface ISummaryReportBPTypeService
    {
        IEnumerable<SummaryReportBPTypeViewModel.Table> GetHeader();
        IEnumerable<SummaryReportBPTypeViewModel.Line> GetBodyExpense(List<BusinessPlanLine> table );
        IEnumerable<SummaryReportBPTypeViewModel.Line> GetBodyRepair(List<BusinessPlanLine> table);
        IEnumerable<SummaryReportBPTypeViewModel.Line> GetBodyFixedAsset(List<BusinessPlanLine> table);
    }
    public class SummaryReportBPTypeService : Abstracts.BaseReportService<SummaryReportBPTypeViewModel.Table, SummaryReportBPTypeViewModel.Line>, ISummaryReportBPTypeService
    {
        public IEnumerable<SummaryReportBPTypeViewModel.Table> GetHeader()
        {
            throw new System.NotImplementedException();
        }

        public IEnumerable<SummaryReportBPTypeViewModel.Line> GetBodyExpense(List<BusinessPlanLine> table)
        {
            throw new System.NotImplementedException();
        }

        public IEnumerable<SummaryReportBPTypeViewModel.Line> GetBodyRepair(List<BusinessPlanLine> table)
        {
            throw new System.NotImplementedException();
        }

        public IEnumerable<SummaryReportBPTypeViewModel.Line> GetBodyFixedAsset(List<BusinessPlanLine> table)
        {
            throw new System.NotImplementedException();
        }
    }
}