﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using CrossingSoft.MVC.Services;
using PanasonicProject.Models;
using PanasonicProject.Repositories;

namespace PanasonicProject.Services
{
    public interface IBPAvalTableService
    {
    }
    public class BPAvalTableService : BaseService<BPAvalTable>, IBPAvalTableService
    {
        private readonly BasicRepository<BPAvalTable> _repository;

        public BPAvalTableService(BasicRepository<BPAvalTable> repository)
            : base(repository)
        {
            _repository = repository;
        }
    }

}