﻿using System.Collections.Generic;
using System.Linq;
using CrossingSoft.MVC.Repositories;
using CrossingSoft.MVC.Services;
using iTextSharp.text;
using PanasonicProject.Models;
using PanasonicProject.Repositories;

namespace PanasonicProject.Services
{
    public interface IMST_SubRequestReportNameService
    {
    }
    public class MST_SubRequestReportNameService : BaseService<MST_SubRequestReportName>, IMST_SubRequestReportNameService
    {
        private readonly BasicRepository<MST_SubRequestReportName> _repository;

        public MST_SubRequestReportNameService(BasicRepository<MST_SubRequestReportName> repository)
            : base(repository)
        {
            _repository = repository;
        }

        public List<MST_SubRequestReportName> GetListSubReportName(string requestToId)
        {
            return _repository.Find(m => m.RequestToID == requestToId).ToListSafe();
        }
    }
}