﻿using System.Collections.Generic;
using System.Linq;
using PanasonicProject.Models;
using PanasonicProject.Repositories;
using PanasonicProject.Services.Abstracts;

namespace PanasonicProject.Services
{
    public interface IDepartmentTableService
    {
        DepartmentTable GetDepartmentLine(string id);
    }

    public class DepartmentTableService : BaseService<DepartmentTable>, IDepartmentTableService
    {
        private readonly BasicRepository<DepartmentTable> _repository;

        public DepartmentTableService(BasicRepository<DepartmentTable> repository)
            : base(repository)
        {
            _repository = repository;
        }
        public DepartmentTable GetDepartmentLine(string id)
        {
            return Load(m => m.DepartmentId == id);
        }
        public List<DepartmentTable> GetDepartmentLine(string id,string divisionid)
        {
            return GetAll(m => m.DepartmentId == id && m.DivisionId == divisionid).ToListSafe();
        }

        public List<DepartmentTable> GetDepartmentLineByDivision(string divisionid)
        {
            return GetAll(m => m.DivisionId == divisionid && m.DivisionId == divisionid).ToListSafe();
        }
        public List<DepartmentTable> GetListDepartmentFilter(string departmentname)
        {
            if (string.IsNullOrEmpty(departmentname))
            {
                return _repository.GetAll().ToListSafe();
            }
            else
            {
                return _repository.Find(m => m.DepartmentId.Contains(departmentname)).ToListSafe();
            }

        }
    }
}