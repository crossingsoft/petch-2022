﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web;
using PanasonicProject.Models;
using PanasonicProject.Models.Enums;
using PanasonicProject.Repositories;
using PanasonicProject.Services.Abstracts;

namespace PanasonicProject.Services
{
    public interface IApplicationApprovalHistoryService
    {
    }
    public class ApplicationApprovalHistoryService : BaseService<ApplicationApprovalHistory>, IApplicationApprovalHistoryService
    {
        private readonly BasicRepository<ApplicationApprovalHistory> _repository;
        private readonly ApplicationFormService _applicationFormService;

        public ApplicationApprovalHistoryService(BasicRepository<ApplicationApprovalHistory> repository, ApplicationFormService applicationFormService)
            : base(repository)
        {
            _repository = repository;
            _applicationFormService = applicationFormService;
        }

        public ApplicationApprovalHistory GetApplicationApprovalHistoryLine(long id)
        {
            return Load(m => m.ID == id);
        }
        public ApplicationApprovalHistory GetApplicationApprovalHistoryLine(long? id)
        {
            return Load(m => m.ID == id);
        }
        public IEnumerable<ApplicationApprovalHistory> GetApplicationApprovalHistoryLineList(long appid)
        {
            return GetAll(m => m.PurchId == appid);
        }
        public ApplicationApprovalHistory CreateApplicationApprovalHistoryLin(ApplicationApprovalHistory appModel)
        {
            return Create(appModel);
        }
        public bool RefPOCreate(long id, string action, DateTime currentdate, string comment, string actionby, 
            string activity)
        {
            string _comment = null, _displayname = null;

            if(comment != null)
            {
               var array = comment.Split(';');
               foreach (var t in array)
               {

                   if (t.Contains("displayname"))
                   {
                       _displayname = t.Replace("displayname","");
                   }
                   else
                   {
                       _comment += t;
                   }
               }
            }
            if (id == null || id == 0)
            {
                return false;
            }
            var appmodel = _applicationFormService.GetAppLine(id);
            var _status = appmodel.Status.ToUpper();
            if (_status != Resource.DocumentStatus.Draft.ToUpper() && _status != Resource.DocumentStatus.OnProcess.ToUpper() && _status != Resource.DocumentStatus.Completed.ToUpper())
            {
                return false;
            }
            var hismodel = GetApplicationApprovalHistoryLineList(id);
            if (!hismodel.Any())
            {
                var apphisfirst = new ApplicationApprovalHistory
                {
                    ID = GetNumLastRow(),
                    PurchId = id,
                    Action = "Submit",
                    ActionDate = currentdate,
                    Comment = _comment,
                    Actioner = actionby,
                    Activity = "Create Document",
                    ActionByDisplayName = _displayname
                };
                CreateApplicationApprovalHistoryLin(apphisfirst);
                SaveChanges();

            }
            var apphis = new ApplicationApprovalHistory
            {
                ID = GetNumLastRow(),
                PurchId = id,
                Action = action,
                ActionDate = currentdate,
                Comment = _comment,
                Actioner = actionby,
                Activity = activity,
                ActionByDisplayName = _displayname
            };
            //if (activity == "Completed")
            //{
            //    appmodel.Status = "Completed";
            //}
            //else if (activity == "Rejected")
            //{
            //    appmodel.Status = "Rejected";
            //}
            //else
            //{
            //    appmodel.Status = "On Process";
            //}
            //appmodel.DocumentStatus = activity;
            //_applicationFormService.UpdateAppLine(appmodel);
            //_applicationFormService.SaveChanges();
            CreateApplicationApprovalHistoryLin(apphis);
            SaveChanges();
            return true;
        }
        public long GetNumLastRow()
        {
            long id = 1;
            var lastOrDefault = GetAll().LastOrDefault();
            if (lastOrDefault != null)
            {
                id = lastOrDefault.ID + 1;
            }
            return id;
        }
    }
}