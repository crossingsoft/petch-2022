﻿using System.Collections.Generic;
using PanasonicProject.ViewModels.Interfaces;

namespace PanasonicProject.Services.Interfaces
{
    public interface IReportService<out TDataSourceTable, out TDataSourceLine>
        where TDataSourceTable : class, ITableViewModel, new()
        where TDataSourceLine : class, ILineViewModel, new()
    {
        IEnumerable<TDataSourceTable> DefaultTable();
        IEnumerable<TDataSourceLine> DefaultLine();
    }
}