﻿using System.Collections.Generic;
using System.Linq;
using CrossingSoft.MVC.Repositories;
using CrossingSoft.MVC.Services;
using iTextSharp.text;
using PanasonicProject.Models;
using PanasonicProject.Repositories;

namespace PanasonicProject.Services
{
    public interface IADUserTableService
    {
    }
    public class ADUserTableService : BaseService<ADUserTable> ,IADUserTableService
    {
        private readonly BasicRepository<ADUserTable> _repository;

        public ADUserTableService(BasicRepository<ADUserTable> repository)
            : base(repository)
        {
            _repository = repository;
        }

        public ADUserTable GetUser(string username)
        {
            return GetAll(m => m.Name == username).FirstOrDefault();
        }
        public List<ADUserTable> GetUserDetail()
        {
            return GetAll().ToList();
        }
        public List<ADUserTable> GetListUserFilter(string username)
        {
            if (string.IsNullOrEmpty(username))
            {
                return _repository.GetAll().ToListSafe();
            }
            else
            {
                return _repository.Find(m => m.UserName.Contains(username)).ToListSafe();
            }
            
        }
    }
}