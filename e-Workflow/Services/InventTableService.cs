﻿using System.Collections.Generic;
using PanasonicProject.Models;
using PanasonicProject.Repositories;
using PanasonicProject.Services.Abstracts;

namespace PanasonicProject.Services
{
    public class InventTableService : BaseService<InventTable>
    {
        private readonly BasicRepository<InventTable> _repository;

        public InventTableService(BasicRepository<InventTable> repository)
            : base(repository)
        {
            _repository = repository;
        }

        public IEnumerable<InventTable> GetList()
        {
            return GetAll();
        }
    }
}