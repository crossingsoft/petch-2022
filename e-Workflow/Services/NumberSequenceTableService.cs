﻿using System;
using System.Globalization;
using PanasonicProject.Models;
using PanasonicProject.Repositories;
using PanasonicProject.Services.Abstracts;
namespace PanasonicProject.Services
{
    public class NumberSequenceTableService : BaseService<NumberSequenceTable>
    {
        private readonly BasicRepository<NumberSequenceTable> _repository;


        public NumberSequenceTableService(BasicRepository<NumberSequenceTable> repository
            )
            : base(repository)
        {
            _repository = repository;

        }

        public NumberSequenceTable GetNumberSequenceTable(string module, string month, string year)
        {
            return Load(m => m.Module == module && m.Month == month && m.Year == year);
        }

        public void Used(NumberSequenceTable numberSequenceTable)
        {
            if (numberSequenceTable == null) return;
            // Advance next num by 1
            numberSequenceTable.NextNumber += 1;
            this.Update(numberSequenceTable);
            // Don forget to call SaveChanges manually.
        }



        public string GetNewNumberDoc(string module)
        {
            string year = DateTime.Now.ToString("yy");
            string month = DateTime.Now.ToString("MM");
            return GetNewNumberDoc(module, month, year);
        }
        public string GetNewNumberAcc(string module)
        {
            string year = DateTime.Now.ToString("yy");
            string month = DateTime.Now.ToString("MM");
            return GetNewNumberAcc(module, month, year);
        }
        public string GetNewNumberAcc(string module, string month, string year)
        {
            string numberSequenceNumber = string.Empty;

            var numberSequence = GetNumberSequenceTable(module, month, year);

            if (numberSequence == null)
            {
                long currMonth = Convert.ToInt32(month), currYear = Convert.ToInt32(year);

                var lastData = GetAll(m => m.Module == module).ToListSafe().FindLast(m => m.Module == module);

                string oldMonth = month, oldYear = year;

                var pMonth = lastData.Month;
                var pyear = lastData.Year;
                var oldNumberSequenceOnly = GetNumberSequenceTable(module, pMonth, pyear);

                long nextNumber = 0;
                if (currMonth == 1 || lastData.Month == "12")
                {
                    nextNumber = 1;
                }

                else
                {
                    nextNumber = oldNumberSequenceOnly.NextNumber + 1;
                }

                var newNumberSequence = new NumberSequenceTable()
                {
                    Module = module,
                    Month = month,
                    Year = year,
                    NextNumber = nextNumber,
                    Format = "####"
                };

                var createNewNumberSequence = Create(newNumberSequence);
                SaveChanges();

                var newNo = NewNo(createNewNumberSequence);
                numberSequenceNumber = JoinNumberAcc(createNewNumberSequence, newNo);

                Used(createNewNumberSequence);
            }
            else
            {
                var newNo = NewNo(numberSequence);
                numberSequenceNumber = JoinNumberAcc(numberSequence, newNo);
                Used(numberSequence);
            }
            SaveChanges();
            return numberSequenceNumber;
        }
        public string GetNewNumberDoc(string module, string month, string year)
        {
            string numberSequenceNumber = string.Empty;
            year = DateTime.Now.ToString("yy");

            var numberSequence = GetNumberSequenceTable(module, month, year);

            if (numberSequence == null)
            {
                //var previousMonth = DateTime.Now.AddMonths(-1);
                //var pMonth = previousMonth.Month.ToString();
                //var pyear = previousMonth.ToString("yy");
                //var oldNumberSequenceOnly = GetNumberSequenceTable(module, pMonth, pyear);
                //var nextNumber = oldNumberSequenceOnly.NextNumber + 1;
                var nextNumber = 1;
                var newNumberSequence = new NumberSequenceTable()
                {
                    Module = module,
                    Month = month,
                    Year = year,
                    NextNumber = nextNumber,
                    Format = "####"
                };

                var createNewNumberSequence = Create(newNumberSequence);
                SaveChanges();

                var newNo = NewNo(createNewNumberSequence);
                numberSequenceNumber = JoinNumberDoc(createNewNumberSequence, newNo);

                Used(createNewNumberSequence);
            }
            else
            {
                var newNo = NewNo(numberSequence);
                numberSequenceNumber = JoinNumberDoc(numberSequence, newNo);
                Used(numberSequence);
            }
            SaveChanges();
            return numberSequenceNumber;
        }

        public string JoinNumberDoc(NumberSequenceTable numberSequence, string newNumber)
        {
            return string.Format("{0}{1}{2}{3}", numberSequence.Module, numberSequence.Year, numberSequence.Month, newNumber);
        }
        public string JoinNumberAcc(NumberSequenceTable numberSequence, string newNumber)
        {
            return string.Format("{0}{1}{2}{3}", numberSequence.Module, numberSequence.Year, "-", newNumber);
        }
        public string NewNo(NumberSequenceTable numberSequenceTable)
        {
            var format = numberSequenceTable.Format;

            int wcStart = format.IndexOf('#');
            int wcEnd = format.LastIndexOf('#');
            int wcLength = (wcEnd - wcStart) + 1;

            long nextNum = numberSequenceTable.NextNumber;
            var paddedNum = nextNum.ToString(CultureInfo.InvariantCulture).PadLeft(wcLength, '0');
            var no = format.Replace("#".PadLeft(wcLength, '#'), paddedNum);

            return no;
        }

    }
}