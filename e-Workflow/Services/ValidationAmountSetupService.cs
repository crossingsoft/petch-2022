﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using CrossingSoft.MVC.Services;
using PanasonicProject.Models;
using PanasonicProject.Repositories;

namespace PanasonicProject.Services
{
    public interface IValidationAmountSetupService
    {
    }
    public class ValidationAmountSetupService: BaseService<ValidationAmountSetup>, IValidationAmountSetupService
    {
        private readonly BasicRepository<ValidationAmountSetup> _repository;

        public ValidationAmountSetupService(BasicRepository<ValidationAmountSetup> repository)
            : base(repository)
        {
            _repository = repository;
        }
        public ValidationAmountSetup GetValidationAmountSetupLine(long id)
        {
            return Load(m => m.Id == id);
        }
        public IEnumerable<ValidationAmountSetup> GetValidationAmountSetupLineByWorkflowType(string workflowType)
        {
            return GetAll(m => m.WorkflowType == workflowType);
        }
    }
}