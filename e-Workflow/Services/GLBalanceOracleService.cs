﻿using PanasonicProject.Models;
using PanasonicProject.Repositories;
using PanasonicProject.Services.Abstracts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PanasonicProject.Services
{
    public interface IGLBalanceOracleService
    {
    }
    public class GLBalanceOracleService : BaseService<GLBalanceOracle>, IGLBalanceOracleService
    {
        private readonly BasicRepository<GLBalanceOracle> _repository;

        public GLBalanceOracleService(BasicRepository<GLBalanceOracle> repository) 
            : base(repository)
        {
            _repository = repository;
        }
    }
}