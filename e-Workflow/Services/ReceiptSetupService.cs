﻿using System.Collections.Generic;
using System.Linq;
using PanasonicProject.Models;
using PanasonicProject.Repositories;
using PanasonicProject.Services.Abstracts;

namespace PanasonicProject.Services
{
    public class ReceiptSetupService : BaseService<ReceiptSetup>
    {
        private readonly BasicRepository<ReceiptSetup> _repository;

        public ReceiptSetupService(BasicRepository<ReceiptSetup> repository)
            : base(repository)
        {
            _repository = repository;
        }
        public IEnumerable<ReceiptSetup> GetReceiptSetupByUser(string userid)
        {
            return GetAll(m => m.UserId == userid);
        }
        public IEnumerable<IGrouping<string, ReceiptSetup>> GetReceiptSetupGroupByDepartmentByUser(string userid)
        {
            return GetAll(m => m.UserId == userid).GroupBy(m => m.DepartmentId);
        }
        public ReceiptSetup GetReceiptSetup(long id)
        {
            return Load(m => m.ReceiptSetupId == id);
        }

    }
}