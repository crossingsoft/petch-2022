﻿using System;
using System.Collections.Generic;
using System.Linq;
using PanasonicProject.Models;
using PanasonicProject.Repositories;
using PanasonicProject.Services.Abstracts;
namespace PanasonicProject.Services
{
    public class GRNLineService : BaseService<GRNLine>
    {
        private readonly BasicRepository<GRNLine> _repository;
        private readonly PurchLineService _purchLineService;

        public GRNLineService(BasicRepository<GRNLine> repository,
            PurchLineService purchLineService)
            : base(repository)
        {
            _repository = repository;
            _purchLineService = purchLineService;
        }

        public IEnumerable<GRNLine> GetGRNLineList(long grnTableId)
        {
            return GetAll(m => m.ParentId == grnTableId).ToListSafe();
        }
        public IEnumerable<GRNLine> GetGRNLineList(long grnTableId, string items)
        {
            return GetAll(m => m.ParentId == grnTableId && m.Items == items).ToListSafe();
        }
        public void SetLineNumber(long grnTableId)
        {
            var grnLineList = GetGRNLineList(grnTableId);
            long no = 1;
            foreach (var grnLine in grnLineList)
            {
                grnLine.LineNum = no++;
                UpdateGRNLine(grnLine);
            }
            SaveChanges();
        }

        public GRNLine GetGRNLine(long grnLineId)
        {
            return Load(m => m.Id == grnLineId);
        }
        public GRNLine CreateGRNLine(GRNLine GRNLine)
        {
            return Create(GRNLine);
        }

        public GRNLine UpdateGRNLine(GRNLine GRNLine)
        {
            return Update(GRNLine);
        }
        public void DeleteGRNLine(long GRNLineId)
        {
            Delete(m => m.Id == GRNLineId);
        }
    }
}