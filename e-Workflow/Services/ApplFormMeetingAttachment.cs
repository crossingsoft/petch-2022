﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web;
using PanasonicProject.Models;
using PanasonicProject.Models.Enums;
using PanasonicProject.Repositories;
using PanasonicProject.Services.Abstracts;

namespace PanasonicProject.Services
{
    public interface IApplFormMeetingAttachment
    {
    }
    public class ApplFormMeetingAttachment : BaseService<ApplFormAttachmentTable>, IApplFormMeetingAttachment
    {
        private readonly BasicRepository<ApplFormAttachmentTable> _repository;

        public ApplFormMeetingAttachment(BasicRepository<ApplFormAttachmentTable> repository)
            : base(repository)
        {
            _repository = repository;

        }

        public IEnumerable<ApplFormAttachmentTable> GetApplFormAttachmentTables(long id)
        {
            return GetAll(m => m.AppId == id);
        }
    }
}