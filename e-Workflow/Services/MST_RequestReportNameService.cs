﻿using System.Collections.Generic;
using System.Linq;
using CrossingSoft.MVC.Repositories;
using CrossingSoft.MVC.Services;
using iTextSharp.text;
using PanasonicProject.Models;
using PanasonicProject.Repositories;

namespace PanasonicProject.Services
{
    public interface IMST_RequestReportNameService
    {
    }
    public class MST_RequestReportNameService : BaseService<MST_RequestReportName>, IMST_RequestReportNameService
    {
        private readonly BasicRepository<MST_RequestReportName> _repository;

        public MST_RequestReportNameService(BasicRepository<MST_RequestReportName> repository)
            : base(repository)
        {
            _repository = repository;
        }

        public List<MST_RequestReportName> GetAllRequestReportName()
        {
            var name = _repository.GetAll().ToListSafe();
            return _repository.GetAll().ToListSafe();
        }
    }
}