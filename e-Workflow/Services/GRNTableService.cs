﻿using System.Collections.Generic;
using System.Linq;
using PanasonicProject.Models;
using PanasonicProject.Repositories;
using PanasonicProject.Services.Abstracts;

namespace PanasonicProject.Services
{
    public interface IGRNTableService
    {
        GRNTable GetGRNLine(long grnId);
    }
    public class GRNTableService : BaseService<GRNTable>, IGRNTableService
    {
        private readonly BasicRepository<GRNTable> _repository;
        private readonly GRNLineService _grnLineService;
        private readonly PurchLineService _purchLineService;
        private readonly PurchTableService _purchTableService;

        public GRNTableService(BasicRepository<GRNTable> repository, GRNLineService grnLineService, PurchLineService purchLineService, PurchTableService purchTableService)
            : base(repository)
        {
            _repository = repository;
            _grnLineService = grnLineService;
            _purchLineService = purchLineService;
            _purchTableService = purchTableService;
        }

        public GRNTable GetGRNLine(long grnId)
        {
            return Load(m => m.GRNId == grnId);
        }

        public GRNTable UpdateGRNLine(GRNTable GRNLine)
        {
            return Update(GRNLine);
        }
        public IEnumerable<GRNTable> GetGRNLineByPO(long poId)
        {
            return GetAll(m => m.PurchId == poId);
        }
        public IEnumerable<GRNTable> GetGRNLineByPO(long? poId)
        {
            return GetAll(m => m.PurchId == poId);
        }
        public void UpdateGRNLine(long grnId)
        {

            var grnlineModel = _grnLineService.GetGRNLineList(grnId);
            var poId = GetGRNLine(grnId).PurchId;

            var poModel = _purchTableService.GetPOLine(poId);
            var isReceipt = true;
            string valerror = null;
            //if (grnlineModel.Count() == 1)
            //{
            //    foreach (var grnline in grnlineModel)
            //    {
            //        var totalqtyRec = (decimal?) 0.00;
            //        var grnmodel = GetGRNLineByPO(poId);
            //        foreach (var grn in grnmodel)
            //        {
            //            var grnlinemodel = _grnLineService.GetGRNLineList(grn.GRNId)
            //                .Where(m => m.Items == grnline.Items);
            //            var i = 0;
            //            foreach (var grnlineEx in grnlinemodel)
            //            {
            //                if (i == 0)
            //                {
            //                    totalqtyRec = 0;
            //                }
            //                else
            //                {
            //                    totalqtyRec += grnlineEx.Qty;
            //                }
            //                i++;
            //            }
            //        }
            //        var qtyAcc = (decimal?) 0;
            //        decimal? qtyRec = grnline.Qty ?? 0;

            //        qtyAcc = grnline.POlineQty - (qtyRec + totalqtyRec);

            //        grnline.QTYRec = qtyRec + totalqtyRec;
            //        grnline.QTYAccrual = qtyAcc;
            //        _grnLineService.UpdateGRNLine(grnline);


            //        var qtyPO = grnline.POlineQty;
            //        if (qtyAcc > 0)
            //            isReceipt = false;
            //    }
            //}

            //else
            //{
                foreach (var grnline in grnlineModel)
                {
                    valerror += "1";
                    var totalqtyRec = (decimal?)0.00;
                    var grnmodel = GetGRNLineByPO(poId);
                    foreach (var grn in grnmodel)
                    {
                        valerror += "2";
                        var grnlinemodel = _grnLineService.GetGRNLineList(grn.GRNId)
                            .Where(m => m.Items == grnline.Items && m.LineNum == grnline.LineNum);
                        foreach (var grnlineEx in grnlinemodel)
                        {
                            var gg = totalqtyRec.ToString();
                            valerror += "3 " + gg + " ";
                            totalqtyRec += grnlineEx.Qty;
                        }
                    }
                    valerror += "before check" + totalqtyRec;
                    var qtyAcc = (decimal?)0;
                    decimal? qtyRec = grnline.Qty ?? 0;

                    if (totalqtyRec != 0)
                    {
                        qtyAcc = grnline.POlineQty - (totalqtyRec);
                    }
                    valerror += "4";
                    grnline.QTYRec =  totalqtyRec;
                    valerror += "5" + totalqtyRec;
                    grnline.QTYAccrual = qtyAcc;
                    valerror += "6" + qtyAcc;
                   //grnline.FiscalYear = valerror;
                    _grnLineService.UpdateGRNLine(grnline);


                    var qtyPO = grnline.POlineQty;
                    if (qtyAcc > 0)
                        isReceipt = false;
                }
            //}
            _grnLineService.SaveChanges();
            var grnTableModel = new PurchTable();
            var status = (string)null;
            if (isReceipt)
            {
                status = "Received";
            }
            else
            {
                status = "Partial received";
            }
            poModel.ReceiptStatus = status;
            _purchTableService.UpdatePOLine(poModel);
            _purchTableService.SaveChanges();
        }

        public void GRAccess()
        {

        }
    }
}