﻿using System.Collections.Generic;
using System.Linq;
using PanasonicProject.Models;
using PanasonicProject.Repositories;
using PanasonicProject.Services.Abstracts;
using PanasonicProject.ViewModels.Report;

namespace PanasonicProject.Services
{
    public interface IPurchLineService
    {
        IEnumerable<PurchLine> GetAppBudgetLineByBPNum(string bpNum);
    }
    public class PurchLineService : BaseService<PurchLine>, IPurchLineService
    {
        private readonly BasicRepository<PurchLine> _repository;

        public PurchLineService(BasicRepository<PurchLine> repository)
            : base(repository)
        {
            _repository = repository;
        }

        public IEnumerable<PurchLine> GetpurchLineList(long purchTableId)
        {
            return GetAll(m => m.PurchId == purchTableId).ToListSafe();
        }
        public IEnumerable<PurchLine> GetpurchLineListByVendor(long purchTableId, string vendorname)
        {
            return GetAll(m => m.PurchId == purchTableId && m.VendName == vendorname).ToListSafe();
        }
        public IEnumerable<PurchLine> GetpurchLineList(long? purchTableId)
        {
            return GetAll(m => m.PurchId == purchTableId).ToListSafe();
        }
        public IEnumerable<PurchLine> GetpurchLineListbySection(string sectionId)
        {
            return GetAll(m => m.SectionId == sectionId).ToListSafe();
        }
        public List<IGrouping<long?, PurchLine>> GetpurchLineListGroup(long purchTableId)
        {
            return GetAll(m => m.PurchId == purchTableId).GroupBy(m =>m.ApplId).ToListSafe();
        }
        public void SetLineNumber(long purchTableId)
        {
            var purchLineList = GetpurchLineList(purchTableId);
            long no = 1;
            foreach (var purchLine in purchLineList)
            {
                purchLine.LineNum = no++;
                UpdatePurchLine(purchLine);
            }
            SaveChanges();
        }
        public PurchLine GetPurchLine(long purchLineId)
        {
            return Load(m => m.PurchLineId == purchLineId);
        }
        public PurchLine GetPurchLine(long? purchLineId)
        {
            return Load(m => m.PurchLineId == purchLineId);
        }
        public PurchLine CreatePurchLine(PurchLine purchLine)
        {
            return Create(purchLine);
        }

        public PurchLine UpdatePurchLine(PurchLine purchLine)
        {
            return Update(purchLine);
        }
        public void DeletePurchLine(long purchLineId)
        {
            Delete(m => m.PurchId == purchLineId);
        }
        public IEnumerable<PurchLine> GetAppBudgetLineByBPNum(string bpNum)
        {
            return GetAll(m => m.BPNum == bpNum);
        }

        public IEnumerable<PurchLine> GetListPOLineForm(BaseCriteriaReport criteria)
        {
            var models = _repository.Find(m => m.ReqDate.Value.Date >= criteria.StartDate && m.ReqDate.Value.Date <= criteria.EndDate).ToListSafe();

            //if (!string.IsNullOrEmpty(criteria.CreateBy))
            //{

            //}

            return models;
        }

    }
}