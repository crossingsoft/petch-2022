﻿using System.Collections.Generic;
using System.Linq;
using CrossingSoft.MVC.Repositories;
using CrossingSoft.MVC.Services;
using iTextSharp.text;
using PanasonicProject.Models;
using PanasonicProject.Repositories;

namespace PanasonicProject.Services
{
    public interface ISB_GoodReceiveNote_DService
    {
    }
    public class SB_GoodReceiveNote_DService : BaseService<SB_GoodReceiveNote_D>, ISB_GoodReceiveNote_DService
    {
        private readonly BasicRepository<SB_GoodReceiveNote_D> _repository;

        public SB_GoodReceiveNote_DService(BasicRepository<SB_GoodReceiveNote_D> repository)
            : base(repository)
        {
            _repository = repository;
        }
        public void UpdateGRNLineAccess(SB_GoodReceiveNote_D model)
        {
            _repository.Update(model);
        }
        public SB_GoodReceiveNote_D LoadGRNLineAccess(string receive_No, long? runningNumber, long? seq)
        {
            return _repository.Load(m => m.ReceiveNo == receive_No && m.RunningNumber == runningNumber && m.Seq == seq);
        }
    }
}