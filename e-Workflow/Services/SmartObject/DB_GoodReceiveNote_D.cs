﻿using System.Collections.Generic;
using System.Linq;
using CrossingSoft.MVC.Repositories;
using CrossingSoft.MVC.Services;
using iTextSharp.text;
using PanasonicProject.Models;
using PanasonicProject.Repositories;

namespace PanasonicProject.Services
{
    public interface IDB_GoodReceiveNote_DService
    {
    }
    public class DB_GoodReceiveNote_DService : BaseService<DB_GoodReceiveNote_D>, IDB_GoodReceiveNote_DService
    {
        private readonly BasicRepository<DB_GoodReceiveNote_D> _repository;

        public DB_GoodReceiveNote_DService(BasicRepository<DB_GoodReceiveNote_D> repository)
            : base(repository)
        {
            _repository = repository;
        }

        public void UpdateGRNLineAccess(DB_GoodReceiveNote_D model)
        {
            _repository.Update(model);
        }
        public DB_GoodReceiveNote_D LoadGRNLineAccess(string receiveNo, long? runningNumber, long? seq)
        {
            return _repository.Load(m => m.ReceiveNo == receiveNo && m.RunningNumber == runningNumber && m.Seq == seq);
        }

    }

}