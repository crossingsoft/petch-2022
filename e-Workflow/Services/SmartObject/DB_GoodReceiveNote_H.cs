﻿using System.Collections.Generic;
using System.Linq;
using CrossingSoft.MVC.Repositories;
using CrossingSoft.MVC.Services;
using iTextSharp.text;
using PanasonicProject.Models;
using PanasonicProject.Repositories;

namespace PanasonicProject.Services
{
    public interface IDB_GoodReceiveNote_HService
    {
    }
    public class DB_GoodReceiveNote_HService : BaseService<DB_GoodReceiveNote_H>, IDB_GoodReceiveNote_HService
    {
        private readonly BasicRepository<DB_GoodReceiveNote_H> _repository;

        public DB_GoodReceiveNote_HService(BasicRepository<DB_GoodReceiveNote_H> repository)
            : base(repository)
        {
            _repository = repository;
        }
        public DB_GoodReceiveNote_H LoadGRNAccess(long? RunningNumber)
        {
            return _repository.Load(m => m.RunningNumber == RunningNumber);
        }
    }
}