﻿using System.Collections.Generic;
using System.Linq;
using CrossingSoft.MVC.Repositories;
using CrossingSoft.MVC.Services;
using iTextSharp.text;
using PanasonicProject.Models;
using PanasonicProject.Repositories;

namespace PanasonicProject.Services
{
    public interface ISB_GoodReceiveNote_HService
    {
    }
    public class SB_GoodReceiveNote_HService : BaseService<SB_GoodReceiveNote_H>, ISB_GoodReceiveNote_HService
    {
        private readonly BasicRepository<SB_GoodReceiveNote_H> _repository;

        public SB_GoodReceiveNote_HService(BasicRepository<SB_GoodReceiveNote_H> repository)
            : base(repository)
        {
            _repository = repository;
        }
        public SB_GoodReceiveNote_H LoadGRNAccess(long? RunningNumber)
        {
            return _repository.Load(m => m.RunningNumber == RunningNumber);
        }
    }
}