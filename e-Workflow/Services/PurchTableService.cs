﻿using System;
using System.Collections.Generic;
using System.Linq;
using PanasonicProject.Models;
using PanasonicProject.Repositories;
using PanasonicProject.Services.Abstracts;
using PanasonicProject.ViewModels.Report;

namespace PanasonicProject.Services
{
    public interface IPurchTableService
    {
        PurchTable GetPOLine(long id);
        PurchTable GetPOLine(long? id);
        PurchTable UpdatePOLine(PurchTable purchTable);
    }
    public class PurchTableService : BaseService<PurchTable>, IPurchTableService
    {
        private readonly BasicRepository<PurchTable> _repository;


        public PurchTableService(BasicRepository<PurchTable> repository) 
            : base(repository)
        {
            _repository = repository;
        }

        public PurchTable GetPOLine(long id)
        {
            return Load(m => m.PurchId == id);
        }
        public PurchTable GetPOLine(long? id)
        {
            return Load(m => m.PurchId == id);
        }
        public IEnumerable<PurchTable> GetPOLineList(long id)
        {
            return GetAll(m => m.PurchId == id);
        }
        public IEnumerable<PurchTable> GetPOLineListByApp(long id)
        {
            return GetAll(m => m.ApplId == id);
        }
        public IEnumerable<PurchTable> GetPOLineListByAppCompleted(long id)
        {
            return GetAll(m => m.ApplId == id && m.POStatus == Resource.DocumentStatus.Completed);
        }
        public PurchTable UpdatePOLine(PurchTable purchTable)
        {
            return Update(purchTable);
        }

        public IEnumerable<PurchTable> GetPOFormCriteraiReport(BaseCriteriaReport criteria)
        {
            if (criteria.EndDate == null)
                criteria.EndDate = DateTime.Now;
            if (criteria.StartDate == null)
                criteria.StartDate = DateTime.Now.AddYears(-10);

            if (string.IsNullOrEmpty(criteria.DocumentNumber))
            {
                var modelHeads = _repository.Find(m => m.POStatus != Resource.DocumentStatus.Draft && m.CreatedDate >= criteria.StartDate && m.CreatedDate <= criteria.EndDate);


                if (!string.IsNullOrEmpty(criteria.CreateBy))
                {
                    modelHeads = modelHeads.Where(m => m.CreatedDisplayName.Contains(criteria.CreateBy));
                }
                if (!string.IsNullOrEmpty(criteria.DivisionId))
                {
                    modelHeads = modelHeads.Where(m => m.FactoryId.Contains(criteria.DivisionId));
                }
                if (!string.IsNullOrEmpty(criteria.DepartmentId))
                {
                    modelHeads = modelHeads.Where(m => m.DepartmentId.Contains(criteria.DepartmentId));
                }
                //if (!string.IsNullOrEmpty(criteria.SectionId))
                //{
                //    modelHeads = modelHeads.Where(m => m.SectionId.Contains(criteria.SectionId));
                //}
                return modelHeads.ToListSafe();
            }
            else
            {
                //var modelHeads = _repository.Find(m => m.POStatus == Resource.DocumentStatus.Approved && m.CreatedDate >= criteria.StartDate && m.CreatedDate <= criteria.EndDate && m.PurchNum.Contains(criteria.DocumentNumber));
                var modelHeads = _repository.Find(m => m.POStatus == Resource.DocumentStatus.Approved && m.CreatedDate >= criteria.StartDate && m.CreatedDate <= criteria.EndDate && m.ExtPurchNum.Contains(criteria.DocumentNumber));
                if (!string.IsNullOrEmpty(criteria.CreateBy))
                {
                    modelHeads = modelHeads.Where(m => m.CreatedDisplayName.Contains(criteria.CreateBy));
                }
                return modelHeads.ToListSafe();
            }

        }

    }
}