﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Org.BouncyCastle.Bcpg;

namespace PanasonicProject.Models
{
    [Table("Receipt_Setup")]
    public class ReceiptSetup
    {
        [Key]
        public long ReceiptSetupId { get; set; }
        public string DepartmentId { get; set; }
        public string UserId { get; set; }
    }
}