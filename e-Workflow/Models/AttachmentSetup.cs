﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Org.BouncyCastle.Bcpg;

namespace PanasonicProject.Models
{
    [Table("AtttachmentSetup")]
    public class AttachmentSetup
    {
        [Key]
        public long? ID { get; set; }
        public string Type { get; set; }
        public decimal? MaxAmount { get; set; }
        public bool? IsActive { get; set; }
        public DateTime? CreatedBy { get; set; }
        public DateTime? CreatedDate { get; set; }
    }
}