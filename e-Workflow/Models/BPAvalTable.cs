﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace PanasonicProject.Models
{
    [Table("BPAvalTable")]
    public class BPAvalTable
    {
            [Key]
            public string ID { get; set; }
            public string FisicalYear { get; set; }
            public string Department { get; set; }
            public string Section { get; set; }
            public long? Subsection { get; set; }
            public string BPType { get; set; }
            public decimal? BPAmount { get; set; }
            public decimal? Application { get; set; }
            public decimal? PO { get; set; }
            public decimal? GR { get; set; }
            public decimal? AvalBPAmount { get; set; }
            public decimal? NetBPAmount { get; set; }
    }
}