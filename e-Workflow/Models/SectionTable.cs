﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace PanasonicProject.Models
{
    [Table("SectionTable")]
    public class SectionTable
    {
        [Key]
        public string SectionId { get; set; }
        public string SectionName { get; set; }
        public string Department { get; set; }
        public string Division { get; set; }
        public string Requester { get; set; }
        public string AccountingRev { get; set; }
        public string Manager { get; set; }
        public string GM { get; set; }
        public string AccountingGen { get; set; }
        public bool eWorkflow { get; set; }
    }
}