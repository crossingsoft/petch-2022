﻿using System;
using System.ComponentModel.DataAnnotations;

namespace PanasonicProject.Models
{
    public class WorklistItem
    {
        public int ProcessInstanceId { get; set; }
        public string SN { get; set; }
        public string Data { get; set; }
        public string ViewFlow { get; set; }
        public string Folio { get; set; }
        public string ProcessName { get; set; }
        public string ActivityName { get; set; }
        public DateTime ProcessStartDate { get; set; }
        public DateTime ActivityStartDate { get; set; }
        [DataType(DataType.Date)]
        //public DateTime DueDate { get; set; }
        public string Status { get; set; }
        public string DueStatus { get; set; }
        public string Title { get; set; }
        public string CreateBy { get; set; }
        public string WorkListItemUrl { get; set; }
        public string Subject { get; set; }
        public string DescProcessName { get; set; }
        public bool EnableRelease { get; set; }
    }
}