﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace PanasonicProject.Models
{
    [Table("BPTable")]
    public class BusinessPlanLine
    {
        [Key]
        public string BusinessPlanId { get; set; }
        public string BusinessPlanType { get; set; }
        public string FisicalYearId { get; set; }
        public string SectionId { get; set; }
        public string RefNumber { get; set; }
        public string Description { get; set; }
        public string Period { get; set; }
        public decimal? BPAmount { get; set; }
        public string BPGroup { get; set; }
        public string BPTYPE { get; set; }
        public string WorkflowType { get; set; }
        public string ACTIVE { get; set; }
        public string PeriodIDOperation { get; set; }
        public long? LifeYear { get; set; }
        public string FixAssetPurposeID { get; set; }
        public string CreateUser { get; set; }
        public DateTime? CreateDate { get; set; }
        public decimal? BPActualAvailable { get; set; }
        public decimal? BPActualAmount { get; set; }
    }
}