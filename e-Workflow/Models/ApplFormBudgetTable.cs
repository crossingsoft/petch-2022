﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace PanasonicProject.Models
{
    [Table("ApplFormBudgetTable")]
    public class ApplFormBudgetTable
    {
        [Key]
        public long ID { get; set; }
        public Nullable<long> ApplId { get; set; }
        public string BPNum { get; set; }
        public string BudgetType { get; set; }
        public string RefNumber { get; set; }
        public string FiscalYear { get; set; }
        public string Month { get; set; }
        public string SectionId { get; set; }
        public Nullable<decimal> BPAmount { get; set; }
        public Nullable<decimal> BPAvailableAmount { get; set; }
        public Nullable<decimal> BPReserve { get; set; }
        public string WorkflowType { get; set; }
        public string BpType { get; set; }

        public string Description { get; set; }
        public string BPGroupDescription { get; set; }
        public decimal? GRBudgetUsed { get; set; }
        public decimal? GRBudgetRemain { get; set; }
    }
}