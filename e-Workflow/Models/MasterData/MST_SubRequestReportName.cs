﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace PanasonicProject.Models
{
    [Table("MST_SubRequestReportName")]
    public class MST_SubRequestReportName
    {
        [Key]
        public string SubRequestToID { get; set; }
        public string RequestToID { get; set; }
        public string Name { get; set; }
        public string CreatedBy { get; set; }
        public DateTime? CreatedDate { get; set; }
        public string ModifiedBy { get; set; }
        public DateTime? ModifiedDate { get; set; }

    }
}