﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace PanasonicProject.Models
{
    [Table("ApplFormAttachmentTable")]
    public class ApplFormAttachmentTable
    {
        [Key]
        public long ID { get; set; }
        public long? AppId { get; set; }
        public long? RowNumber { get; set; }
        public string Type { get; set; }
        public string Attachmentfile { get; set; }
        public string Remarks { get; set; }
        public string CreatedBy { get; set; }
        public string CreatedByDisplayName { get; set; }
        public DateTime? CreatedDate { get; set; }
        public long? TypeId { get; set; }
    }
}