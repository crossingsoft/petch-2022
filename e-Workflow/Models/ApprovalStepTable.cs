﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace PanasonicProject.Models
{
    [Table("ApprovalStepTable")]
    public class ApprovalStepTable
    {
        [Key]
        public long ApproID { get; set; }
        public string ApproStateName { get; set; }
        public string WorkflowType { get; set; }
        public string Position { get; set; }
        public string Level { get; set; }
        public decimal? MaxAmount { get; set; }
        public string CreateBy { get; set; }
        public DateTime? CreateDate { get; set; }
    }
}