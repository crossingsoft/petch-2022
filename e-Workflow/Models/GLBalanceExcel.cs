﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace PanasonicProject.Models
{
    [Table("GLBalanceExcel")]
    public class GLBalanceExcel
    {
        [Key]
        public long ID { get; set; }
        public string YearId { get; set; }
        public string ProjNo { get; set; }
        public string SectionCode { get; set; }
        public string Item { get; set; }
        public decimal? ActaulAmount { get; set; }
        public DateTime? CreateDate { get; set; }
    }
}