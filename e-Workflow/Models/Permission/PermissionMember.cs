﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace PanasonicProject.Models.Permission
{
    [Table("EWorkflow_PermissionMember")]
    public class PermissionMember
    {
        [Key]
        public long PermissionMemberId { get; set; }
        public string PermissionId { get; set; }
        public string Username { get; set; }
        public string FQN { get; set; }
        public DateTime? CreatedDate { get; set; }
        public string CreatedBy { get; set; }
        public DateTime? ModifiedDate { get; set; }
        public string ModifiedBy { get; set; }
    }
}