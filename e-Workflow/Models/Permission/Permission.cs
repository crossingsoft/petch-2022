﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace PanasonicProject.Models.Permission
{
    [Table("EWorkflow_PermissionTable")]
    public class PermissionTable
    {
        [Key]
        public string PermissionId { get; set; }
        public string Module { get; set; }
        public string PermissionType { get; set; }
        public DateTime? CreatedDate { get; set; }
        public string CreatedBy { get; set; }
        public DateTime? ModifiedDate { get; set; }
        public string ModifiedBy { get; set; }
    }
}