﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace PanasonicProject.Models
{
    [Table("SectionSetup")]
    public class SectionSetup
    {
        [Key]
        public long SecSetypID { get; set; }
        public string UserId { get; set; }
        public long SectionId { get; set; }
    }
}