﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace PanasonicProject.Models
{
    [Table("Division")]
    public class Division
    {
        [Key]
        public string ID { get; set; }
        public string DivisionName { get; set; }
    }
}