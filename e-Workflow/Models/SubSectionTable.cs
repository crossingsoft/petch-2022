﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace PanasonicProject.Models
{
    [Table("SubSectionTable")]
    public class SubSectionTable
    {
        [Key]
        public string SubSectionId { get; set; }
        public string SubSectionName { get; set; }
        public long? SectionId { get; set; }
    }
}