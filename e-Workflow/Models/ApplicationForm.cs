﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace PanasonicProject.Models
{
    [Table("ApplicationForm")]
    public class ApplicationForm
    {
        [Key]
        public long ApplId { get; set; }
        public string ApplNo { get; set; }
        public string Subject { get; set; }
        public string SectionId { get; set; }
        public string CreatedName { get; set; }
        public Nullable<System.DateTime> CreatedDate { get; set; }
        public Nullable<System.DateTime> ReqDate { get; set; }
        public string RespPerson { get; set; }
        public string Purpose { get; set; }
        public string BudgetExp { get; set; }
        public string ActionPeriod { get; set; }
        public string FactoryAdv { get; set; }
        public Nullable<decimal> BudgetAmount { get; set; }
        public Nullable<decimal> BudgetRemain { get; set; }
        public string BudgetType { get; set; }
        public Nullable<decimal> BPAmount { get; set; }
        public Nullable<bool> AirPolution { get; set; }
        public Nullable<bool> WaterPolution { get; set; }
        public Nullable<bool> WasteMng { get; set; }
        public Nullable<bool> ChemMng { get; set; }
        public Nullable<bool> LawRegulation { get; set; }
        public Nullable<bool> Bolier { get; set; }
        public Nullable<bool> Safety { get; set; }
        public Nullable<bool> Other { get; set; }
        public Nullable<bool> RegController { get; set; }
        public Nullable<bool> Controller { get; set; }
        public string IntApplId { get; set; }
        public Nullable<System.DateTime> IntDateRecord { get; set; }
        public string RefApplId { get; set; }
        public string Status { get; set; }
        public string SectionName { get; set; }
        public string DocumentStatus { get; set; }
        public string ProcessInstance { get; set; }
        public string CurrencyCode { get; set; }
        public Nullable<decimal> CurrencyRate { get; set; }
        public Nullable<decimal> BudgetExplanationTotal { get; set; }
        public Nullable<decimal> BudgetExplanationTotalTH { get; set; }
        public string DepartmentId { get; set; }
        public string DepartmentName { get; set; }
        public Nullable<decimal> BudgetAvalabel { get; set; }
        public string RemarkCancel { get; set; }
        public string CreatedDisplayName { get; set; }

        public string ActiveBy { get; set; }
        public bool? IsDeductBudget { get; set; }

        public bool? IsCancelling { get; set; }

        public DateTime? CancelledDate { get; set; }
        public DateTime? CancelDate { get; set; }

        public string RequesterBy { get; set; }
        public DateTime? RequesterDate { get; set; }
        public DateTime? SubmitedDate { get; set; }
        public DateTime? FinishedDate { get; set; }
        public string RequesterDisplayName { get; set; }
       // public bool? IsReserved { get; set; }
        public DateTime? ModifiedBy { get; set; }
        public DateTime? ModifiedDate { get; set; }
        public string RefAppId { get; set; }
        public string RefAppNo { get; set; }
        public bool? IsClosed { get; set; }


        //
        public string DivisionId { get; set; }
        public string ApplTypeSelect { get; set; }
    }
}