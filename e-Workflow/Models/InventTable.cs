﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace PanasonicProject.Models
{
    [Table("InventTable")]
    public class InventTable
    {
        [Key]
        public string ItemId { get; set; }
        public string Name { get; set; }
        public string Unit { get; set; }
        public decimal? Price { get; set; }
        public string DepartmentOwn { get; set; }

    }
}