﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace PanasonicProject.Models
{
    [Table("NumberSequenceSetup")]
    public class NumberSequenceSetup
    {

        [Key]
        public string Module { get; set; }
        public string Description { get; set; }

    }
}