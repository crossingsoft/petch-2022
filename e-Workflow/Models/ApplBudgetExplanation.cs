﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace PanasonicProject.Models
{
    [Table("ApplBudgetExplanation")]
    public class ApplBudgetExplanation
    {
        [Key]
        public long ID { get; set; }
        public Nullable<long> ApplId { get; set; }
        public string Description { get; set; }
        public Nullable<decimal> UnitPrice { get; set; }
        public Nullable<decimal> Qty { get; set; }
        public Nullable<decimal> LineAmount { get; set; }
        public Nullable<decimal> LineAmountTH { get; set; }
    }
}