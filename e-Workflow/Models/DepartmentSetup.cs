﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;


namespace PanasonicProject.Models
{
    [Table("DepartmentSetup")]
    public class DepartmentSetup
    {
        [Key]
        public long Id { get; set; }
        public string UserId { get; set; }
        public string DepartmentId { get; set; }
        public string Type { get; set; }
        public bool? IsActive { get; set; }
    }
}