﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace PanasonicProject.Models
{
    [Table("NumberSequenceTable")]
    public class NumberSequenceTable
    {

        [Key, Column(Order = 0)]
        public string Module { get; set; }
        [Key, Column(Order = 1)]
        public string Month { get; set; }
        [Key, Column(Order = 2)]
        public string Year { get; set; }
        public long NextNumber { get; set; }
        public string Format { get; set; }
        

    }
}