﻿using System.ComponentModel.DataAnnotations;
using PanasonicProject.Models.Interfaces;

namespace PanasonicProject.Models
{
  //[SmartObject("ADService2_ADUser", GetList = "GetUsers", Load = "GetUserDetails")]
   [SmartObject("AD_User_PECTH", GetList = "GetUsers", Load = "GetUserDetails")]
    public class ADUserTable : ISMOModel
    {
        public string UserName { get; set; }
        public string Name { get; set; }
        public string DisplayName { get; set; }
        public string Email { get; set; }
        public string Description { get; set; }
        public string Manager { get; set; }
        public string PrimaryGroup { get; set; }
        public string PrimaryOU { get; set; }
        public string Domain { get; set; }
        public string SipAccount { get; set; }
        public string DistinguishedName { get; set; }
        public string TelephoneNumber { get; set; }
        public string Moblie { get; set; }
        public string HomePage { get; set; }
        public string FaxNumber { get; set; }
        public string HomePhone { get; set; }
        public string IPPhone { get; set; }
        public string StreetAddress { get; set; }
        public string City { get; set; }
        public string Country { get; set; }
        public string State { get; set; }
        public string Department { get; set; }
        public string Company { get; set; }
        public string Title { get; set; }
        public string Office { get; set; }
    }
}