﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace PanasonicProject.Models
{
    [Table("GRNLine")]
    public class GRNLine
    {
        [Key]
        public long Id { get; set; }
        public long ParentId { get; set; }
        public long? LineNum { get; set; }
        public string Items { get; set; }
        public string ItemName { get; set; }
        public decimal? Qty { get; set; }
        public decimal? QTYRec { get; set; }
        public decimal? QTYAccrual { get; set; }
        public decimal? UnitPrice { get; set; }
        public decimal? POlineQty { get; set; }
        public string Unit { get; set; }
        public string PurchId { get; set; }
        public string ReceiveNo { get; set; }
        public long? PurchLineId { get; set; }
        public string BPNum { get; set; }
        public string BudgetType { get; set; }
        public string RefNumber { get; set; }
        public string FiscalYear { get; set; }
        public string Month { get; set; }
        public string BpType { get; set; }
        public string Description { get; set; }
        public string BPGroup { get; set; }
        public string BPGroupDescription { get; set; }
        public decimal? POUnitPrice { get; set; }
        public decimal? LineAmount { get; set; }
        public decimal? LineAmountTH { get; set; }
        public decimal? GRLineAmount { get; set; }
        public decimal? GRLineAmountTH { get; set; }
        public string ItemNameDescription { get; set; }
        public string ItemCode { get; set; }
        public string SectionId { get; set; }
        public long? POId { get; set; }
        public string SubSection { get; set; }
        public decimal? GRAmountRemain { get; set; }
        public string Source { get; set; }
        public string WorkflowType { get; set; }
        public decimal? QtyValidation { get; set; }
    }
}