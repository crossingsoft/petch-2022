﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace PanasonicProject.Models
{
    [Table("GLBalanceOracle")]
    public class GLBalanceOracle
    {
        [Key]
        public long ID { get; set; }
        public string PERIOD_NAME { get; set; }
        public string DEPARTMENT_CODE { get; set; }
        public string DEPARTMENT_DESCRIPTION { get; set; }
        public string ACCOUNT_CODE { get; set; }
        public string ACCOUNT_DESCRIPTION { get; set; }
        public decimal? BEGINNING_BALANCE { get; set; }
        public decimal? PERIOD_DEBIT { get; set; }
        public decimal? PERIOD_CREDIT { get; set; }
        public decimal? PERIOD_ACTIVITY { get; set; }
        public decimal? ENDING_BALANCE { get; set; }
        public DateTime? DATESTAMP { get; set; }
        public DateTime? PERIOD_DATE { get; set; }
        
    }
}