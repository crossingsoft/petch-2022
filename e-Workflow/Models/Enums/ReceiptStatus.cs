﻿using System.ComponentModel;

namespace PanasonicProject.Models.Enums
{
    public static class ReceiptStatusId
    {
        public const int OpenOrder = 0;
        public const int PartialReceipt = 1;
        public const int Received = 2;
        public const int Closed = 3;
    }
    public enum ReceiptStatus
    {
        Draft = ReceiptStatusId.OpenOrder,
        [Description("Partial Receipt")]
        Active = ReceiptStatusId.PartialReceipt,
        Received = ReceiptStatusId.Received,
        Closed = ReceiptStatusId.Closed
    }
}