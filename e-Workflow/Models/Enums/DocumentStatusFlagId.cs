﻿using System.ComponentModel;

namespace PanasonicProject.Models.Enums
{
    public static class DocumentStatusFlagId
    {
        public const int Draft = 0;
        public const int Active = 1;
        public const int InProgress = 2;
        public const int Cancelled = 6;
        public const int Recalled = 7;
        public const int Deleted = 8;
        public const int Completed = 9;
        public const int Approved = 10;
    }
    public enum DocumentStatusFlag
    {
        Draft = DocumentStatusFlagId.Draft,
        Active = DocumentStatusFlagId.Active,
        [Description("In Progress")]
        InProgress = DocumentStatusFlagId.InProgress,
        Cancelled = DocumentStatusFlagId.Cancelled,
        Recalled = DocumentStatusFlagId.Recalled,
        Deleted = DocumentStatusFlagId.Deleted,
        Completed = DocumentStatusFlagId.Completed,
        Approved = DocumentStatusFlagId.Approved
    }
    public enum DocumentStatusFlagSearch
    {
        Draft = DocumentStatusFlagId.Draft,
        Active = DocumentStatusFlagId.Active, // 1 or 2 or 7
        Deleted = DocumentStatusFlagId.Deleted,
        Completed = DocumentStatusFlagId.Completed // 6 or 9
    }
}