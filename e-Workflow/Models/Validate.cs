﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PanasonicProject.Models
{
    public class Validate
    {
        public bool Success { get; set; }
        public string MessageError { get; set; }
    }
}