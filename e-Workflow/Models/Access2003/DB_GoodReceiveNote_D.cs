﻿using System.ComponentModel.DataAnnotations;
using PanasonicProject.Models.Interfaces;

namespace PanasonicProject.Models
{
    [SmartObject("DB_GoodReceiveNote_D", Create = "Create")]
    //[SmartObject("DB_GoodReceiveNote_D", Create = "Create")]
    //[SmartObject("AD_User_PECTH", GetList = "GetUsers", Load = "GetUserDetails")]
    public class DB_GoodReceiveNote_D : ISMOModel
    {
        public string InvoiceNo { get; set; }
        public string PartNumber { get; set; }
        public string PoNumber { get; set; }
        public decimal? Qty { get; set; }
        public string ReceiveNo { get; set; }
        public long? Seq { get; set; }
        public decimal? UnitPrice { get; set; }
        public string Unit { get; set; }
        public long? RunningNumber { get; set; }

        //public string paramPo_No { get; set; }
        //public string paramReceive_No { get; set; }
        //public long? paramRunning { get; set; }
        //public string paramUnit_No { get; set; }
        //public string paramPartNumber { get; set; }
        //public string paramQty { get; set; }
        //public long? paramSeq { get; set; }
        //public string paramUnitPrice { get; set; }
        //method Getline
        //public string Receive_No { get; set; }
        //public long? Running { get; set; }

    }
}