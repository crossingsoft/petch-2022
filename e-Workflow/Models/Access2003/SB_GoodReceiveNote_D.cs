﻿using System;
using System.ComponentModel.DataAnnotations;
using PanasonicProject.Models.Interfaces;

namespace PanasonicProject.Models
{
    [SmartObject("SB_GoodReceiveNote_D", Create = "Create")]
    //[SmartObject("AD_User_PECTH", GetList = "GetUsers", Load = "GetUserDetails")]
    public class SB_GoodReceiveNote_D : ISMOModel
    {
        public string InvoiceNo { get; set; }
        public string PartNumber { get; set; }
        public string PoNumber { get; set; }
        public decimal? Qty { get; set; }
        public string ReceiveNo { get; set; }
        public long? Seq { get; set; }
        public decimal? UnitPrice { get; set; }
        public string Unit { get; set; }
        public long? RunningNumber { get; set; }
    }
}