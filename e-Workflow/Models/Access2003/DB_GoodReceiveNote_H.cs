﻿using System.ComponentModel.DataAnnotations;
using PanasonicProject.Models.Interfaces;

namespace PanasonicProject.Models
{
    [SmartObject("DB_GoodReceiveNote_H", Create = "Create", GetList = "Get_List")]
    //[SmartObject("DB_GoodReceiveNote_H", Create = "Create", GetList = "Get_List", Save = "Save")]
    //[SmartObject("AD_User_PECTH", GetList = "GetUsers", Load = "GetUserDetails")]
    public class DB_GoodReceiveNote_H : ISMOModel
    {
        public string CFlag { get; set; }
        public string DFlag { get; set; }
        public string CreateDate { get; set; }
        public string InvoiceDate { get; set; }
        public string InvoiceNo { get; set; }
        public string ItemClassNo { get; set; }
        public string ReceiveDate { get; set; }
        public string ReceiveNo { get; set; }
        public string ReceiverNo { get; set; }
        public string Remark { get; set; }
        public string SFlag { get; set; }
        public string SupplierNo { get; set; }
        public long? RunningNumber { get; set; }
    }
}