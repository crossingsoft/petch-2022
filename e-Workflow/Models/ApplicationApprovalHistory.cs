﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace PanasonicProject.Models
{
    [Table("ApplicationApprovalHistory")]
    public class ApplicationApprovalHistory
    {
        [Key]
        public long ID { get; set; }
        public long? PurchId { get; set; }
        public string Action { get; set; }
        public DateTime? ActionDate { get; set; }
        public string Comment { get; set; }
        public string Actioner { get; set; }
        public string Activity { get; set; }
        public string ActionByDisplayName { get; set; }
    }
}