﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace PanasonicProject.Models
{
    [Table("BudgetType")]
    public class BudgetType
    {
        [Key]
        public string BudgetTypeId { get; set; }
    }
}