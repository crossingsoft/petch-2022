﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace PanasonicProject.Models
{
    [Table("PurchLine")]
    public class PurchLine
    {
        [Key]
        public long PurchLineId { get; set; }
        public long PurchId { get; set; }
        public long? LineNum { get; set; }
        public string Items { get; set; }
        public string ItemName { get; set; }
        public decimal? Qty { get; set; }
        public string Unit { get; set; }
        public decimal? UnitPrice { get; set; }
        public decimal? LineAmount { get; set; }
        public decimal? RecQty { get; set; }
        public decimal? RecUnitPrice { get; set; }
        public decimal? RecLineAmount { get; set; }
        public string Remark { get; set; }
        public string VendId { get; set; }
        public string VendName { get; set; }
        public DateTime? ReqDate { get; set; }
        public string BPNum { get; set; }
        public string SectionId { get; set; }
        public long? ApplId { get; set; }
        public string Account { get; set; }
        public string WorkflowType { get; set; }
        public long? APPBudgetId { get; set; }

        public decimal? BPAvailable { get; set; }
        public string DeliveryLocation { get; set; }
        public string CurrencyId { get; set; }
        public string CurrencyFullName { get; set; }
        public string BPGroupDescription { get; set; }
        public string Description { get; set; }

        public string ItemNameDescription { get; set; }
        public string VendorNumber { get; set; }
        public string ItemCode { get; set; }

        public decimal? LineAmountTH { get; set; }
        public DateTime? DeliveryDate { get; set; }
        public decimal? CurrencyRate { get; set; }
        public string SubSection { get; set; }
        public string RefNumber { get; set; }
        public string BPGroup { get; set; }
        public string Month { get; set; }

        public bool? RefAPP { get; set; }
        public decimal? GRBudgetUsed { get; set; }
        public decimal? GRBudgetRemain { get; set; }


        //Add-on Project e-Request Sheets
        public decimal? PRDetailId { get; set; }
        public string PRDocumentNumber { get; set; }
        public decimal ReturnQtyToPR { get; set; }
    }
}