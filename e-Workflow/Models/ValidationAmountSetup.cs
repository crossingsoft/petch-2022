﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace PanasonicProject.Models
{
    [Table("ValidationAmountSetup")]
    public class ValidationAmountSetup
    {
        [Key]
        public long Id { get; set; }
        public string WorkflowType { get; set; }
        public string Name { get; set; }
        public decimal? TotalAmountStart { get; set; }
        public decimal? TotalAmountEnd { get; set; }
        public bool? RefAPP { get; set; }
        public bool? RefPO { get; set; }
        public string CreatedBy { get; set; }
        public DateTime? CreatedDate { get; set; }
        public string ModifiedBy { get; set; }
        public DateTime? ModifiedDate { get; set; }
    }
}