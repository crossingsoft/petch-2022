﻿
using System.Data.Entity;

namespace PanasonicProject.Models.Contexts
{
    public class MainContext : DbContext
    {
        public MainContext(string connectionString)
            : base(connectionString)
        {

        }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            //Configure default schema
            modelBuilder.HasDefaultSchema("SmartBoxData");
        }
        //check
        public DbSet<BPAvalTable> BPAvalTable { get; set; }
        //master
        public DbSet<SubSectionTable> SubSectionTable { get; set; }
        public DbSet<Division> Division { get; set; }
        public DbSet<DepartmentTable> DepartmentTable { get; set; }
        public DbSet<SectionSetup> SectionSetup { get; set; }
        public DbSet<SectionTable> SectionTable { get; set; }
        public DbSet<DepartmentSetup> DepartmentSetup { get; set; }
        public DbSet<MST_RequestReportName> MST_RequestReportName { get; set; }
        public DbSet<MST_SubRequestReportName> MST_SubRequestReportName { get; set; }
        public DbSet<ValidationAmountSetup> ValidationAmountSetup { get; set; }
        public DbSet<ReceiptSetup> ReceiptSetup { get; set; }
        public DbSet<AttachmentSetup> AttachmentSetups { get; set; }

        //datasource
        public DbSet<InventTable> InventTable { get; set; }

        public DbSet<PurchTable> PurchTable { get; set; }
        public DbSet<PurchLine> PurchLine { get; set; }
        public DbSet<GRNTable> GRNTable { get; set; }
        public DbSet<GRNLine> GRNLine { get; set; }
        public DbSet<BusinessPlanLine> BusinessPlanLine { get; set; }

        public DbSet<NumberSequenceTable> NumberSequenceTable { get; set; }
        public DbSet<ApprovalStepTable> ApprovalStepTable { get; set; }

        public DbSet<ApplicationForm> ApplicationForm { get; set; }
        public DbSet<ApplFormBudgetTable> ApplFormBudgetTable { get; set; }
        public DbSet<ApplicationApprovalHistory> ApplicationApprovalHistory { get; set; } 
        public DbSet<ApplFormBudgetTablePOTemp> ApplFormBudgetTablePOTemp { get; set; }
        public DbSet<ApplBudgetExplanation> ApplBudgetExplanation { get; set; }
        public DbSet<ApplFormAttachmentTable> ApplFormAttachmentTables { get; set; }
        public DbSet<GLBalanceOracle> GLBalanceOracle { get; set; }
        public DbSet<GLBalanceExcel> GLBalanceExcel { get; set; }

        //Permission
        public DbSet<Permission.PermissionTable> PermissionTables { get; set; }
        public DbSet<Permission.PermissionMember> PermissionMembers { get; set; }

    }
}