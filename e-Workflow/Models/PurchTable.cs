﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace PanasonicProject.Models
{
    [Table("PurchTable")]
    public class PurchTable
    {
        [Key]
        public long PurchId { get; set; }
        public string PurchNum { get; set; }
        public string SectionId { get; set; }
        public string FactoryId { get; set; }
        public Nullable<System.DateTime> CreatedDate { get; set; }
        public string CreatedBy { get; set; }
        public Nullable<System.DateTime> PurchDate { get; set; }
        public string ModifiedBy { get; set; }
        public Nullable<System.DateTime> ModifiedDate { get; set; }
        public string Purpose { get; set; }
        public string RequestedBy { get; set; }
        public string VendId { get; set; }
        public string VendName { get; set; }
        public string VendAddress { get; set; }
        public string DeliveryTo { get; set; }
        public string DeliveryAddress { get; set; }
        public string PaymTerms { get; set; }
        public string POStatus { get; set; }
        public string GRStatus { get; set; }
        public Nullable<long> ApplId { get; set; }
        public Nullable<decimal> TotalAmount { get; set; }
        public string RequesterName { get; set; }
        public string ExtPurchNum { get; set; }
        public Nullable<System.DateTime> ExtPurchDate { get; set; }
        public string SectionName { get; set; }
        public string FactoryName { get; set; }
        public Nullable<long> ProcessInstance { get; set; }
        public string DocumentStatus { get; set; }
        public string WorkflowType { get; set; }
        public string AppSubject { get; set; }
        public string ReceiptStatus { get; set; }
        public Nullable<long> APPBudgetId { get; set; }
        public Nullable<bool> IsImport { get; set; }
        public string CurrencyCode { get; set; }
        public Nullable<decimal> CurrencyRate { get; set; }
        public Nullable<decimal> TotalAmountTH { get; set; }
        public Nullable<System.DateTime> DeliveryDate { get; set; }
        public string DepartmentId { get; set; }
        public string DepartmentName { get; set; }
        public string Shipment { get; set; }
        public string Factory { get; set; }
        public string Transportatior { get; set; }
        public string CaseMark { get; set; }
        public string CreatedDisplayName { get; set; }

        public string ActiveBy { get; set; }
        public bool? IsDeductBudget { get; set; }

        public bool? IsCancel { get; set; }
        public string RemarkCancel { get; set; }
        public decimal? AppAvailable { get; set; }
        public string BudgetType { get; set; }
        public string VendorNumber { get; set; }
        public DateTime? CancelledDate { get; set; }
        public DateTime? CancelDate { get; set; }

        public string RequesterBy { get; set; }
        public DateTime? RequesterDate { get; set; }
        public DateTime? SubmitedDate { get; set; }
        public DateTime? FinishedDate { get; set; }
        public string RequesterDisplayName { get; set; }
        // public bool? IsReserved { get; set; }
        public string ApplNo { get; set; }
        public string Orderer { get; set; }

        //Add-on Project e-Request Sheets
        public string PurchReqNum { get; set; }
        public bool FromRequestSheet { get; set; }
    }
}