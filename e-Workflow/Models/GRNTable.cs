﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace PanasonicProject.Models
{
    [Table("GRNTable")]
    public class GRNTable
    {
        [Key]
        public long GRNId { get; set; }
        public string GRNExtId { get; set; }
        public string PurchNumber { get; set; }
        public long? PurchId { get; set; }
        public DateTime? GRNDate { get; set; }
        public DateTime? GRNExtDate { get; set; }
        public string CreatedBy { get; set; }
        public string VendId { get; set; }
        public string VendName { get; set; }
        public string Status { get; set; }
        public string ItemClassNo { get; set; }
        public string InvoiceNo { get; set; }
        public DateTime? InvoiceDate { get; set; }
        public string Remark { get; set; }
        public DateTime? CreateDate { get; set; }
        public long? GRNExtInterface { get; set; }
        public string ReceiptStatus { get; set; }
        public string VendNumberShoirt { get; set; }
        public string DivisionId { get; set; }
        public string CreatedDisplayName { get; set; }
        public string ReceiveNo { get; set; }
        public string CurrencyCode { get; set; }
        public decimal? CurrencyRate { get; set; }
        public string CurrencyFullName { get; set; }
        public decimal? POOverBudget { get; set; }
        public decimal? POTotalAmount { get; set; }
        public decimal? POTotalAmountTH { get; set; }
        public DateTime? FinishedDate { get; set; }
        public string DepartmentId { get; set; }

        public decimal? BudgetPercentage { get; set; }
        public string ReceiveNumber { get; set; }
        public decimal? POOriginalTotalAmountTH { get; set; }
        public bool? IsUnlock { get; set; }
        public string PurchNumberAcc { get; set; }

    }
}
