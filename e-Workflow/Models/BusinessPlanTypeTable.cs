﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace PanasonicProject.Models
{
    [Table("BusinessPlanTypeTable")]
    public class BusinessPlanTypeTable
    {
        [Key]
        public long ID { get; set; }
        public string BusinessPlanType { get; set; }
        public long? FisicalYearId { get; set; }
        public long? SectionId { get; set; }
        public long? ACCodeOrProjectNO { get; set; }
        public string Description { get; set; }
        public long? Period { get; set; }
        public decimal? Amount { get; set; }
        public string BPGroup { get; set; }
        public string BPType { get; set; }
        public bool? Active { get; set; }
        public string PeriodIdOperation { get; set; }
        public long? LifeYear { get; set; }
        public string FixAssetPurposeId { get; set; }
        public string CreateUser { get; set; }
        public DateTime? CreateDate { get; set; }
    }
}