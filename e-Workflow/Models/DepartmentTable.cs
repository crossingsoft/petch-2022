﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace PanasonicProject.Models
{
    [Table("Department_2")]
    public class DepartmentTable
    {
        [Key]
        public string DepartmentId { get; set; }
        public string Name { get; set; }
        public bool? eWorkflow { get; set; }
        public string Manager { get; set; }
        public string GM { get; set; }
        public string DivisionId { get; set; }
    }
}