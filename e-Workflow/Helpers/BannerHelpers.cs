﻿using System.Web.Mvc;
using System.Web.Mvc.Html;

namespace System
{
    public static class BannerHelpers
    {
        public static MvcHtmlString GetBanner(this HtmlHelper htmlHelper,string module)
        {
            //var div = new TagBuilder("div");
            //var img = new TagBuilder("img");
            //const string url = @"~/Images/";
            //var src = UrlHelper.GenerateContentUrl(url + "Banner.png",
            //                                              htmlHelper.ViewContext.HttpContext);
            //img.Attributes["src"] = src;
            //img.Attributes["style"] = String.Format("width:{0}%; height:{1}%;margin-top: {2}px;", 100, 100, 32);

            //div.AddCssClass("shadow-rightleft");
            //div.InnerHtml = img.ToString();
            //return new MvcHtmlString(div.ToString());

            const string url = @"~/Images/";
            var src = UrlHelper.GenerateContentUrl(url + "Banner.png",
                                                          htmlHelper.ViewContext.HttpContext);
            var div = new TagBuilder("div");
            var p = "<p>" + module + "</p>";
            //var p = "<p>Application for doing business</p>";
            var img = new TagBuilder("img");

            img.Attributes["src"] = src;
            img.Attributes["style"] = String.Format("width:{0}%; height:{1}%;margin-top: {2}px;", 100, 100, 32);

            div.AddCssClass("iframe-shadow-rightleft padded-multiline image-wrapper");
            div.InnerHtml = img.ToString();
            div.InnerHtml = p;
            return new MvcHtmlString(div.ToString());
        }
        public static MvcHtmlString GetOriginalBanner(this HtmlHelper htmlHelper)
        {
            var div = new TagBuilder("div");
            var img = new TagBuilder("img");
            const string url = @"~/Images/";
            var src = UrlHelper.GenerateContentUrl(url + "Banner.png",
                                                          htmlHelper.ViewContext.HttpContext);
            img.Attributes["src"] = src;
            img.Attributes["style"] = String.Format("width:{0}%; height:{1}%;margin-top: {2}px;", 100, 100, 32);

            div.AddCssClass("shadow-rightleft");
            div.InnerHtml = img.ToString();
            return new MvcHtmlString(div.ToString());
        }

        public static MvcHtmlString DateTime(this HtmlHelper htmlHelper,string id)
        {
            var input = new TagBuilder("input");
            input.AddCssClass("datepicker");
            input.Attributes.Add("ID", id);
            return new MvcHtmlString(input.ToString());
        }
    }
}