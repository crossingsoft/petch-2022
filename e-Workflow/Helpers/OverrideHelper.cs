﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PanasonicProject
{
    public static class OverrideHelper
    {
        public static List<TSource> ToListSafe<TSource>(this IEnumerable<TSource> source)
        {
            try
            {

                //return source.ToList();
                //if (!source.Any())
                if(source == null)
                {
                    
                    return new List<TSource>();
                }
                else
                {
                    return source.ToList();
                }
            }
            catch (Exception exception)
            {
                throw new Exception();
            }
        }
    }
}