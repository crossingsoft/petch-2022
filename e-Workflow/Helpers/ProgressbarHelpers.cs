﻿using System;
using System.Web.Mvc;


namespace PanasonicProject.Helpers
{
    public static class ProgressbarHelpers
    {
        public static MvcHtmlString GetProgressbar(this HtmlHelper htmlHelper, string paramActivity)
        {
            var div = new TagBuilder("div");
            var img = new TagBuilder("img");
            const string url = @"~/Images/Progressbar/";
            const string defaultImage = "creating";
            if (paramActivity == null)
            {
                paramActivity = defaultImage;
            }
            var src = UrlHelper.GenerateContentUrl(url + paramActivity+".png",
                                                          htmlHelper.ViewContext.HttpContext);
            img.Attributes["src"] = src;
            img.Attributes["style"] = String.Format("width:{0}%; height:{1}%; background-color:{2}; padding: 0px 25px 0px 25px;", 100, 100, "white");

            div.AddCssClass("progress-shadow-rightleft");
            div.InnerHtml = img.ToString();
            return new MvcHtmlString(div.ToString());
        }
    }
}