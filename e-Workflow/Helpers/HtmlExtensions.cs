﻿using System;
using System.Security.Policy;
using System.Web.Mvc;

namespace PanasonicProject.Helpers
{
    public static class HtmlExtensions
    {
        public static MvcHtmlString BtnReleaseWorklist(this HtmlHelper htmlHelper, bool enabled, string urlAction, bool newTab)
        {
            var a = new TagBuilder("a");
            if (enabled)
            {
                a.Attributes["href"] = urlAction;
                if (newTab)
                {
                    a.Attributes["target"] = "_blank";
                }
                a.AddCssClass("btn btn-danger");
                a.InnerHtml = "Release";
            }
            return new MvcHtmlString(a.ToString());
        }

        public static MvcHtmlString BtnOpenWorklist(this HtmlHelper htmlHelper, bool enabled, string urlAction,bool newTab=false)
        {
            var a = new TagBuilder("a");
            if (enabled)
            {
                a.Attributes["href"] = urlAction;

                a.AddCssClass("btn btn-primary");
                a.InnerHtml = "Open";
            }
            return new MvcHtmlString(a.ToString());
        }
    }
}