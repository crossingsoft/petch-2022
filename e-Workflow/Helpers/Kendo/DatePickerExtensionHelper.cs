﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using System.Web.Mvc.Html;
using System.Web.Routing;
using Kendo.Mvc.UI.Fluent;

namespace PanasonicProject.Helpers.Kendo
{
    public static class DatePickerExtensionHelper
    {
        public static DatePickerBuilder DatePickerStartFor<TModel>(
            this WidgetFactory<TModel> widgetFactory,
            Expression<Func<TModel, DateTime?>> startExpression,
            Expression<Func<TModel, DateTime?>> endExpression)
        {
            return widgetFactory.DatePickerFor(startExpression)
                .Format("dd-MMM-yyyy")
                .Events(e => e.Change("pecth.datetimepicker.validateOnChange"))
                .HtmlAttributes(new RouteValueDictionary
                                {
                                    {"data-compare-id", widgetFactory.HtmlHelper.IdFor(endExpression)},
                                    {"data-role-date", "start"},
                                    {"placeholder","From"}
                                });
        }

        public static DatePickerBuilder DatePickerEndFor<TModel>(
            this WidgetFactory<TModel> widgetFactory,
            Expression<Func<TModel, DateTime?>> endExpression,
            Expression<Func<TModel, DateTime?>> startExpression)
        {
            return widgetFactory.DatePickerFor(endExpression)
                .Format("dd-MMM-yyyy")
                .Events(e => e.Change("pecth.datetimepicker.validateOnChange"))
                .HtmlAttributes(new RouteValueDictionary
                                {
                                    {"data-compare-id", widgetFactory.HtmlHelper.IdFor(startExpression)},
                                    {"data-role-date", "end"},
                                     {"placeholder","To"}
                                });
        }
    }
}