﻿
using System.Web.Configuration;
using System.Web.Mvc;
using System.Web.Routing;

namespace System
{
    public static class URLSmartformsHelpers
    {

        public static MvcHtmlString GetSmartForm(this HtmlHelper htmlHelper, string formName,
            RouteValueDictionary htmlAttributesDictionary = null)
        {

            bool runTimeMode = Convert.ToBoolean(WebConfigurationManager.AppSettings["SmartformsRuntimeMode"]);
            string host = WebConfigurationManager.AppSettings["SmartformsHost"];
            string port = WebConfigurationManager.AppSettings["SmartformsPort"];
            string formCategories = WebConfigurationManager.AppSettings["SmartformsCategories"];
           

            string smartFormsMode = string.Empty;
            string url = string.Empty;
            if (runTimeMode)
            {
                smartFormsMode = "Runtime";
                //url = "https";
                url = "http";
            }
            else
            {
                smartFormsMode = "Designer";
                url = "https";
            }
            string urlPath;
            if (runTimeMode)
            {
               //  urlPath = string.Format("{0}://{1}/{2}/Runtime/{3}/{4}", url, host, smartFormsMode, formCategories, formName);

                urlPath = string.Format("{0}://{1}:{2}/{3}/Runtime/{4}/{5}", url, host, port, smartFormsMode, formCategories, formName);
            }
            else
            {
                urlPath = string.Format("{0}://{1}/{2}/Runtime/{3}/{4}", url, host, smartFormsMode, formCategories, formName);

            }
          

            var iframe = new TagBuilder("iframe");

            
            iframe.MergeAttributes(htmlAttributesDictionary);
            if (!iframe.Attributes.ContainsKey("width"))
                iframe.Attributes["width"] = "100%";

            if (!iframe.Attributes.ContainsKey("id"))
                iframe.Attributes["id"] = formName;

          

            iframe.Attributes["src"] = urlPath;
            return MvcHtmlString.Create(iframe.ToString());
        }

        public static MvcHtmlString GetSmartFormWithURL(this HtmlHelper htmlHelper, string url,
            RouteValueDictionary htmlAttributesDictionary = null)
        {

            var iframe = new TagBuilder("iframe");


            iframe.MergeAttributes(htmlAttributesDictionary);
            if (!iframe.Attributes.ContainsKey("width"))
                iframe.Attributes["width"] = "100%";

            iframe.Attributes["src"] = url;
            return MvcHtmlString.Create(iframe.ToString());
        }

        public static MvcHtmlString GetSmartForm(this HtmlHelper htmlHelper, string formName, string module, long? param, RouteValueDictionary htmlAttributesDictionary = null)
        {

            bool runTimeMode = Convert.ToBoolean(WebConfigurationManager.AppSettings["SmartformsRuntimeMode"]);
            string host = WebConfigurationManager.AppSettings["SmartformsHost"];
            string port = WebConfigurationManager.AppSettings["SmartformsPort"];
            string formCategories = WebConfigurationManager.AppSettings["SmartformsCategories"];


            string smartFormsMode = string.Empty;
            string url = string.Empty;
            if (runTimeMode)
            {
                smartFormsMode = "Runtime";
                //url = "https";
                url = "http";
            }
            else
            {
                smartFormsMode = "Designer";
                url = "https";
            }
            string urlPath;
            if (runTimeMode)
            {
                //urlPath = string.Format("{0}://{1}/{2}/Runtime/{3}/{4}/?{5}={6}", url, host, smartFormsMode, formCategories, formName, module, param);

                urlPath = string.Format("{0}://{1}:{2}/{3}/Runtime/{4}/{5}/?{6}={7}", url, host, port, smartFormsMode, formCategories, formName, module, param);

            }
            else
            {
                urlPath = string.Format("{0}://{1}/{2}/Runtime/{3}/{4}/?{5}={6}", url, host, smartFormsMode, formCategories, formName, module, param);

            }


            var iframe = new TagBuilder("iframe");


            iframe.MergeAttributes(htmlAttributesDictionary);
            if (!iframe.Attributes.ContainsKey("width"))
                iframe.Attributes["width"] = "100%";

            if (!iframe.Attributes.ContainsKey("id"))
                iframe.Attributes["id"] = formName;



            iframe.Attributes["src"] = urlPath;
            return MvcHtmlString.Create(iframe.ToString());
        }
    }
}