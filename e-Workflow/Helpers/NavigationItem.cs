﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using PanasonicProject.Resource;

namespace MK_Project.Helpers
{
    public static class NavigationItemService
    {
        private static readonly object sync = new object();
        private static bool _ready = false;
        private static List<NavigationItem> _navigationItems;
        public static List<NavigationItem> NavigationItems
        {
            get
            {
                //if (_navigationItems != null)
                //    return _navigationItems;

                if (!_ready)
                {
                    lock (sync)
                    {
                        if (!_ready)
                        {
                            var user = HttpContext.Current.User as PanasonicProject.Infrastructure.Security.IPECTHPrincipal;

                            var input = new List<NavigationItem>();

                            // 0 = Link text
                            // 1 = Action name
                            // 2 = Controller
                            //var workspace = new NavigationItem("Workspace", "#", null);
                            //input.Add(workspace);
                            //input.Add(new NavigationItem(workspace, "My Workspace", "Index", "Workspace"));
                            //input.Add(new NavigationItem(workspace, "Purchaser", "Purchaser", "Workspace"));
                            //input.Add(new NavigationItem(workspace, "Budget Pool", "Budget", "Workspace"));
                            //var home = new NavigationItem("Home", "WorkspaceMenu", "Home", "glyphicon glyphicon-home");
                            //input.Add(home);

                            var workspace = new NavigationItem("My Worklist", "index", "Workspace");
                            input.Add(workspace);

                            var newDoc = new NavigationItem("New Document", "#", null);
                            input.Add(newDoc);
                            input.Add(new NavigationItem(newDoc, "Request Sheets", "RequestSheet", "Home"));
                            input.Add(new NavigationItem(newDoc, "Application for doing business", "AppBussMenu", "Home"));
                            input.Add(new NavigationItem(newDoc, "Purchase Order", "PurchaseOrderMenu", "Home"));
                            input.Add(new NavigationItem(newDoc, "Goods Receipt", "GoodReceiptNoteMenu", "Home"));

                            var inquiries = new NavigationItem("Inquiries", "#", null);
                            input.Add(inquiries);
                            input.Add(new NavigationItem(inquiries, "My Request Sheets", "RequestSheetInquiry", "Home"));
                            input.Add(new NavigationItem(inquiries, "My Application for doing business", "APPInquiriesMenu", "Home"));
                            input.Add(new NavigationItem(inquiries, "My Purchase Order", "POInquiriesMenu", "Home"));
                            input.Add(new NavigationItem(inquiries, "My Goods Receipt", "GRInquiriesMenu", "Home"));

                            var distributor = new NavigationItem("Distributor", "Distributor", "Home");
                            input.Add(distributor);

                            var buyer = new NavigationItem("Buyer", "#", null);
                            input.Add(buyer);
                            input.Add(new NavigationItem(buyer, "Draft Purchase Order", "DraftPurchaseOrder", "Home"));
                            input.Add(new NavigationItem(buyer, "Draft Application for doing business", "DraftApplication", "Home"));

                            //manager, gm
                            if (
                                user.AuthorizeSystem != null && user.AuthorizeSystem.Exists(
                                    m =>
                                        m.Module == PermissionResource.Module_App &&
                                        m.Type == PermissionResource.Type_Related))
                            {
                                input.Add(new NavigationItem(inquiries, "My Related Application for doing business",
                                    "RelatedAPPInquiriesMenu", "Home"));
                            }
                            if (
                                user.AuthorizeSystem != null && user.AuthorizeSystem.Exists(
                                    m =>
                                        m.Module == PermissionResource.Module_PO &&
                                        m.Type == PermissionResource.Type_Related))
                            {
                                input.Add(new NavigationItem(inquiries, "My Related Purchase Order",
                                    "RelatedPOInquiriesMenu", "Home"));
                            }

                            //md, fd
                            if (
                                user.AuthorizeSystem != null && user.AuthorizeSystem.Exists(
                                    m =>
                                        m.Module == PermissionResource.Module_App &&
                                        m.Type == PermissionResource.Type_ALL))
                            {
                                input.Add(new NavigationItem(inquiries, "All Application for doing business",
                                    "AllAPPInquiriesMenu", "Home"));
                            }
                            if (
                                user.AuthorizeSystem != null && user.AuthorizeSystem.Exists(
                                    m =>
                                        m.Module == PermissionResource.Module_PO &&
                                        m.Type == PermissionResource.Type_ALL))
                            {
                                input.Add(new NavigationItem(inquiries, "All Purchase Order",
                                    "AllPOInquiriesMenu", "Home"));
                            }

                            //team
                            if (
                                user.AuthorizeSystem != null && user.AuthorizeSystem.Exists(
                                    m =>
                                        m.Module == PermissionResource.Module_App &&
                                        m.Type == PermissionResource.Type_Team))
                            {
                                input.Add(new NavigationItem(inquiries, "Team Application for doing business",
                                    "TeamAPPInquiriesMenu", "Home"));
                            }
                            if (
                                user.AuthorizeSystem != null && user.AuthorizeSystem.Exists(
                                    m =>
                                        m.Module == PermissionResource.Module_PO &&
                                        m.Type == PermissionResource.Type_Team))
                            {
                                input.Add(new NavigationItem(inquiries, "Team Purchase Order",
                                    "TeamPOInquiriesMenu", "Home"));
                            }

                            //acc assign no
                            if (
                                user.AuthorizeSystem != null && user.AuthorizeSystem.Exists(
                                    m =>
                                        m.Module == PermissionResource.Module_AccAssign &&
                                        m.Type == PermissionResource.Type_ALL))
                            {
                                input.Add(new NavigationItem(inquiries, "Assign No. (Application)", "AllAPPInquiriesMenuACC",
                                    "Home"));
                            }
                            if (user.AuthorizeSystem != null && user.AuthorizeSystem.Exists(
                                    m =>
                                        m.Module == PermissionResource.Module_AccAssign &&
                                        m.Type == PermissionResource.Type_ALL))
                            {
                                input.Add(new NavigationItem(inquiries, "Assign No. (Purchase Order)", "AllPOInquiriesMenuACC",
                                    "Home"));
                            }

                            //acc assign bp
                            if (
                                user.AuthorizeSystem != null && user.AuthorizeSystem.Exists(
                                    m =>
                                        m.Module == PermissionResource.Module_AccAssignBP &&
                                        m.Type == PermissionResource.Type_ALL))
                            {
                                input.Add(new NavigationItem(inquiries, "Update BP (Application)", "AllAPPInquiriesMenuACCUpdate",
                                    "Home"));
                            }
                            if (user.AuthorizeSystem != null && user.AuthorizeSystem.Exists(
                                    m =>
                                        m.Module == PermissionResource.Module_AccAssignBP &&
                                        m.Type == PermissionResource.Type_ALL))
                            {
                                input.Add(new NavigationItem(inquiries, "Update BP (Purchase Order)", "AllPOInquiriesMenuACCUpdate",
                                    "Home"));
                            }

                            //gr unlock
                            if (user.AuthorizeSystem != null && user.AuthorizeSystem.Exists(
                                    m =>
                                        m.Module == PermissionResource.Module_UnlockGR &&
                                        m.Type == PermissionResource.Type_ALL))
                            {
                                input.Add(new NavigationItem(inquiries, "Update Goods Receipt", "AllGRInquiriesUnlock",
                                    "Home"));
                            }

                            var reports = new NavigationItem("Reports", "#", null);
                            //if (
                            //    user.AuthorizeSystem != null && user.AuthorizeSystem.Exists(
                            //        m =>
                            //            m.Module == PermissionResource.Report_AppPO &&
                            //            m.Type == PermissionResource.Type_ALL))
                            //{

                            //}
                            if (
    user.AuthorizeSystem != null && user.AuthorizeSystem.Exists(
        m =>
            m.Module == PermissionResource.Report_AppPO &&
            m.Type == PermissionResource.Type_ALL))
                            {
                                input.Add(new NavigationItem(reports, "Application & Purchase order",
                                    "SummaryReportsView", "Reports"));
                            }
                            if (
                                user.AuthorizeSystem != null && user.AuthorizeSystem.Exists(
                                    m =>
                                        m.Module == PermissionResource.Report_BP &&
                                        m.Type == PermissionResource.Type_ALL))
                            {
                                input.Add(new NavigationItem(reports, "BP Visualization", "BPVisualization", "Reports"));
                                input.Add(new NavigationItem(reports, "BP Monitoring", "BPMonitoring", "Reports"));
                            }
                            if (
                                user.AuthorizeSystem != null && user.AuthorizeSystem.Exists(
                                    m =>
                                        m.Module == PermissionResource.Report_AllExpenseandInvestment &&
                                        m.Type == PermissionResource.Type_ALL))
                            {
                                input.Add(new NavigationItem(reports, "Summary Expense and Investment",
                                    "ExpenseRepairAndFixedAsset", "Reports", new { target = "_blank" }));
                            }
                            if (
                                user.AuthorizeSystem != null && user.AuthorizeSystem.Exists(
                                    m =>
                                        m.Module == PermissionResource.Report_ExpenseandInvestment &&
                                        m.Type == PermissionResource.Type_ALL))
                            {
                                input.Add(new NavigationItem(reports, "Summary Fixed Assets (Investment)", "FixedAssets",
                                    "Reports", new { target = "_blank" }));
                                input.Add(new NavigationItem(reports, "Summary Indirect (Expense)", "Expense", "Reports",
                                    new { target = "_blank" }));
                                input.Add(new NavigationItem(reports, "Summary Repairing (Expense)", "Repair", "Reports",
                                    new { target = "_blank" }));
                                input.Add(new NavigationItem(reports, "Summary Others (Expense)", "Others", "Reports",
                                    new { target = "_blank" }));
                            }
                            if (
                                user.AuthorizeSystem != null && user.AuthorizeSystem.Exists(
                                    m =>
                                        m.Module == PermissionResource.Report_GR &&
                                        m.Type == PermissionResource.Type_ALL))
                            {
                                input.Add(new NavigationItem(reports, "Goods Receipt", "GRN", "Reports",
                                    new { target = "_blank" }));
                                input.Add(new NavigationItem(reports, "Goods Receipt & Purchase History", "GRNHistory",
                                    "Reports", new { target = "_blank" }));
                            }
                            if (
                                user.AuthorizeSystem != null && user.AuthorizeSystem.Exists(
                                    m =>
                                        m.Module == PermissionResource.Report_Budget &&
                                        m.Type == PermissionResource.Type_ALL))
                            {
                                input.Add(new NavigationItem(reports, "Compare Business Plan & Actual",
                                    "CompareBPAmountAndActualAmount", "Reports", new { target = "_blank" }));
                                input.Add(new NavigationItem(reports, "Summary Expense and Investment by Section",
                                    "SummaryInvestmentbySection", "Reports", new { target = "_blank" }));
                            }
                            if (
                                user.AuthorizeSystem != null && user.AuthorizeSystem.Exists(
                                    m =>
                                        m.Module == PermissionResource.Report_Item &&
                                        m.Type == PermissionResource.Type_ALL))
                            {
                                input.Add(new NavigationItem(reports, "Item Consumption Analysis",
                                    "ItemConsumptionAnalysis", "Reports", new { target = "_blank" }));
                            }
                            //input.Add(new NavigationItem(reports, "Summary Roles user & Section name", "RolesUserReport", "Reports"));

                            input.Add(reports);
                            //input.Add(new NavigationItem(reports, "Line of Authorization", "LineofAuthorization", "Home"));
                            //input.Add(new NavigationItem(reports, "Line of Authorization", "LineofAuthorization", "Home"));
                            //input.Add(new NavigationItem(reports, "Line of Authorization", "LineofAuthorization", "Home"));
                            //input.Add(new NavigationItem(reports, "Line of Authorization", "LineofAuthorization", "Home"));
                            //input.Add(new NavigationItem(reports, "Line of Authorization", "LineofAuthorization", "Home"));
                            //input.Add(new NavigationItem(reports, "Line of Authorization", "LineofAuthorization", "Home"));
                            //input.Add(new NavigationItem(reports, "Line of Authorization", "LineofAuthorization", "Home"));
                            if (user.AuthorizeSystem != null && user.AuthorizeSystem.Exists(m => m.Module == PermissionResource.Module_Admin && m.Type == PermissionResource.Type_ALL))
                            {
                                var masterData = new NavigationItem("Master Data", "#", null);
                                input.Add(masterData);
                                input.Add(new NavigationItem(masterData, "Line of Authorization", "LineofAuthorization", "Home"));
                                input.Add(new NavigationItem(masterData, "System Parameters", "SystemParameter", "Home"));
                                input.Add(new NavigationItem(masterData, "Number Sequence", "NumberSequence", "Home"));
                                //input.Add(new NavigationItem(masterData, "BP Available", "BPAval", "Home"));

                                //input.Add(new NavigationItem(masterData, "Business Plan", "BusinessPlan", "Home"));
                                input.Add(new NavigationItem(masterData, "Division", "Division", "Home"));
                                input.Add(new NavigationItem(masterData, "Department", "Department", "Home"));
                                input.Add(new NavigationItem(masterData, "Department Setup", "DepartmentSetup", "Home"));

                                input.Add(new NavigationItem(masterData, "Section", "Section", "Home"));
                                input.Add(new NavigationItem(masterData, "Sub Section", "SubSection", "Home"));

                                input.Add(new NavigationItem(masterData, "Vendor", "Vendor", "Home"));
                                input.Add(new NavigationItem(masterData, "Material Master", "MaterialMaster", "Home"));
                                input.Add(new NavigationItem(masterData, "Currency Setup", "Currency", "Home"));
                                input.Add(new NavigationItem(masterData, "Matrix Accounting", "ValidationAmountSetup", "Home"));

                                input.Add(new NavigationItem(masterData, "Permission Table", "PermissionTable", "Home"));
                                input.Add(new NavigationItem(masterData, "Permission Member", "PermissionMember", "Home"));
                                input.Add(new NavigationItem(masterData, "Employee Manager", "EmployeeTable", "Home"));
                                input.Add(new NavigationItem(masterData, "Printer Setup", "PrinterSetup", "Home"));
                                input.Add(new NavigationItem(masterData, "Investment Table (Import Excel)", "ImportExcel", "Home"));
                                //input.Add(new NavigationItem(masterData, "Received Setup", "ReceivedSetup", "Home"));
                            }

                            _navigationItems = input;
                            _ready = false;
                        }
                    }
                }
                return _navigationItems;
            }
        }
    }

    public sealed class NavigationItem
    {
        public string LinkText { get; set; }
        public string ActionName { get; set; }
        public string ParamName { get; set; }
        public string ControllerName { get; set; }
        public object HtmlAttributes { get; set; }
        public NavigationItem Parent { get; set; }
        public Guid Guid { get; set; }

        public string IconBootstrap { get; set; }

        public NavigationItem(string linkText)
        {
            this.LinkText = linkText;
            this.ActionName = "#";
            this.ControllerName = null;
            this.HtmlAttributes = null;
            this.Guid = Guid.NewGuid();
        }

        public NavigationItem(NavigationItem parent, string linkText)
        {
            this.Parent = parent;
            this.LinkText = linkText;
            this.ActionName = "#";
            this.ControllerName = null;
            this.HtmlAttributes = null;
            this.Guid = Guid.NewGuid();
        }

        public NavigationItem(string linkText, string actionName, string controllerName, object htmlAttributes = null)
        {
            this.LinkText = linkText;
            this.ActionName = actionName;
            this.ControllerName = controllerName;
            this.HtmlAttributes = htmlAttributes;
            this.Guid = Guid.NewGuid();
        }

        public NavigationItem(string linkText, string actionName, string controllerName, string iconBoostrap, object htmlAttributes = null)
        {
            this.LinkText = linkText;
            this.ActionName = actionName;
            this.ControllerName = controllerName;
            this.IconBootstrap = iconBoostrap;
            this.HtmlAttributes = htmlAttributes;
            this.Guid = Guid.NewGuid();
        }

        public NavigationItem(NavigationItem parent, string linkText, string actionName, string controllerName, object htmlAttributes = null)
        {
            this.Parent = parent;
            this.LinkText = linkText;
            this.ActionName = actionName;
            this.ControllerName = controllerName;
            this.HtmlAttributes = htmlAttributes;
            this.Guid = Guid.NewGuid();
        }
        public NavigationItem(NavigationItem parent, string linkText, string actionName, string paramName, string controllerName, object htmlAttributes = null)
        {
            this.Parent = parent;
            this.LinkText = linkText;
            this.ActionName = actionName;
            this.ParamName = paramName;
            this.ControllerName = controllerName;
            this.HtmlAttributes = htmlAttributes;
            this.Guid = Guid.NewGuid();
        }
    }
}