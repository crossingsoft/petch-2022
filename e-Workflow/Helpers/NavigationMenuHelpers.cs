﻿using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Mvc.Html;
using System.Web.Routing;
using MK_Project.Helpers;

namespace System
{
    public static class NavigationMenuHelpers
    {
        /// 
        /// adds the active class if the link's action & controller matches current request
        /// 
        private static MvcHtmlString MenuActionLink(this HtmlHelper htmlHelper,
            string linkText, string actionName, string controllerName,
            object routeValues = null, object htmlAttributes = null)
        {
            var htmlAttributesDictionary =
                HtmlHelper.AnonymousObjectToHtmlAttributes(htmlAttributes);

            if (actionName == "#")
            {
                var a = new TagBuilder("a");
                a.MergeAttributes(htmlAttributesDictionary);
                a.MergeAttribute("href", actionName);
                a.InnerHtml = !string.IsNullOrEmpty(linkText)
                                  ? HttpUtility.HtmlEncode(linkText)
                                  : string.Empty;
                a.InnerHtml = a.InnerHtml.Replace("[br]", "<br>");
                return MvcHtmlString.Create(a.ToString(TagRenderMode.Normal));
            }

            //if (((string)htmlHelper.ViewContext.RouteData.Values["controller"])
            //        .Equals(controllerName, StringComparison.OrdinalIgnoreCase) &&
            //    ((string)htmlHelper.ViewContext.RouteData.Values["action"])
            //        .Equals(actionName, StringComparison.OrdinalIgnoreCase))
            //if (((string)htmlHelper.ViewContext.RouteData.Values["controller"])
            //    .Equals(controllerName, StringComparison.InvariantCultureIgnoreCase))
            //{
            //    if (htmlAttributesDictionary.ContainsKey("class"))
            //    {
            //        if (!htmlAttributesDictionary["class"].ToString().EndsWith(" "))
            //            htmlAttributesDictionary["class"] += " ";
            //        htmlAttributesDictionary["class"] += activeClassName;
            //    }
            //    else
            //    {
            //        htmlAttributesDictionary.Add("class", activeClassName);//
            //    }
            //}

            return htmlHelper.ActionLink(linkText, actionName, controllerName,
                                            new RouteValueDictionary(routeValues),
                                            htmlAttributesDictionary);
        }

        /// 
        /// adds the active class if the link's path matches current request
        /// 
        private static MvcHtmlString MenuActionLink(this HtmlHelper htmlHelper,
            string linkText, string path, object htmlAttributes = null)
        {
            var htmlAttributesDictionary =
                HtmlHelper.AnonymousObjectToHtmlAttributes(htmlAttributes);

            if (path == "#")
            {
                var a = new TagBuilder("a");
                a.MergeAttributes(htmlAttributesDictionary);
                a.MergeAttribute("href", path);
                a.InnerHtml = !string.IsNullOrEmpty(linkText)
                                  ? HttpUtility.HtmlEncode(linkText)
                                  : string.Empty;
                return MvcHtmlString.Create(a.ToString(TagRenderMode.Normal));
            }

            //if (HttpContext.Current.Request.Path
            //    .Equals(path, StringComparison.OrdinalIgnoreCase))
            //{
            //    // careful in case class already exists
            //    htmlAttributesDictionary["class"] += " " + activeClassName;
            //}
            var tagBuilder = new TagBuilder("a")
            {
                InnerHtml = !string.IsNullOrEmpty(linkText)
                                ? HttpUtility.HtmlEncode(linkText)
                                : string.Empty
            };
            tagBuilder.MergeAttributes(htmlAttributesDictionary);
            tagBuilder.MergeAttribute("href", path);
            return MvcHtmlString.Create(tagBuilder.ToString(TagRenderMode.Normal));
        }

        /// 
        /// adds the active class if the link's action & controller matches current request
        /// 
        private static TagBuilder MenuActionLink(this HtmlHelper htmlHelper,
            TagBuilder parentTag,
            string linkText, string actionName, string controllerName, string iconBoostrap,
            object routeValues = null, object htmlAttributes = null)
        {
            var htmlAttributesDictionary =
                HtmlHelper.AnonymousObjectToHtmlAttributes(htmlAttributes);

            return MenuActionLink(htmlHelper, parentTag, linkText, actionName, controllerName, iconBoostrap, routeValues,
                                  htmlAttributesDictionary);
        }

        private static TagBuilder MenuActionLink(this HtmlHelper htmlHelper,
            TagBuilder parentTag,
            string linkText, string actionName, string controllerName, string iconBoostrap,
            object routeValues = null, RouteValueDictionary htmlAttributesDictionary = null)
        {
            if (actionName == "#")
            {
                var a = new TagBuilder("a");
                a.MergeAttributes(htmlAttributesDictionary);
                a.MergeAttribute("href", actionName);
                if (!string.IsNullOrEmpty(iconBoostrap))
                {
                    var p = new TagBuilder("p");
                    p.AddCssClass(iconBoostrap);
                    a.InnerHtml = p.ToString();

                    a.InnerHtml += !string.IsNullOrEmpty(linkText.Replace("[br]", "<br>"))
                   ? linkText : string.Empty;
                }
                else
                {
                    a.InnerHtml = !string.IsNullOrEmpty(linkText)
                   ? linkText.Replace("[br]", "<br>") : string.Empty;

                }

                parentTag.InnerHtml = a.ToString(TagRenderMode.Normal);
                return parentTag;
            }

            if (!string.IsNullOrEmpty(iconBoostrap))
            {
                var p = new TagBuilder("p");
                p.AddCssClass(iconBoostrap);
                var link = htmlHelper.ActionLink("[replaceme] " + linkText, actionName, controllerName,
                    new RouteValueDictionary(routeValues),
                    htmlAttributesDictionary).ToHtmlString();

                parentTag.InnerHtml = link.Replace("[replaceme]", p.ToString()).Replace("[br]", "<br>");
                return parentTag;



            }


            parentTag.InnerHtml = htmlHelper.ActionLink(linkText, actionName, controllerName,
                                                        new RouteValueDictionary(routeValues),
                                                        htmlAttributesDictionary).ToString().Replace("[br]", "<br>");

            return parentTag;
        }

        private static ConcurrentDictionary<string, object> _concurrentDictionary = new ConcurrentDictionary<string, object>();
        public static MvcHtmlString NavigationMenuContent(this HtmlHelper htmlHelper, object htmlAttributes = null, object htmlStyle = null)
        {
            var sessionKey = "RenderedMenu";
            var user = HttpContext.Current.User.Identity;
            string menuContentStr = null;
            //if (HttpContext.Current.Session[sessionKey] != null)
            if (1 > 2)
            {
                menuContentStr = (string)HttpContext.Current.Session[sessionKey];
            }
            else
            {
                object sync = _concurrentDictionary.GetOrAdd(HttpContext.Current.User.Identity.Name, key => new object());
                lock (sync)
                {

                    var input = OnlyAuthorizedItems();
                    // The main ul

                    var main = new TagBuilder("ul");
                    main.MergeAttributes(new RouteValueDictionary(htmlAttributes));

                    // First level ul
                    foreach (var item in input.Where(m => m.Parent == null))
                    {
                        var hasChild = input.Any(m => m.Parent == item);
                        TagBuilder firstLevelLi;
                        if (hasChild)
                        {
                            var htmlAttributesDictionary =
                                HtmlHelper.AnonymousObjectToHtmlAttributes(item.HtmlAttributes);

                            if (htmlAttributesDictionary.ContainsKey("class"))
                                htmlAttributesDictionary["class"] += "dropdown-toggle";
                            else
                                htmlAttributesDictionary.Add("class", "dropdown-toggle");

                            if (htmlAttributesDictionary.ContainsKey("data-toggle"))
                                htmlAttributesDictionary["data-toggle"] += "dropdown";
                            else
                                htmlAttributesDictionary.Add("data-toggle", "dropdown");

                            var li = new TagBuilder("li");
                            li.AddCssClass("dropdown");
                            firstLevelLi =
                                htmlHelper.MenuActionLink(li, item.LinkText + @" <b class=""caret""></b>",
                                                          item.ActionName,
                                                          item.ControllerName,
                                                          null,
                                                          null,
                                                          htmlAttributesDictionary);
                        }
                        else
                        {
                            if (!string.IsNullOrEmpty(item.IconBootstrap))
                            {
                                firstLevelLi =
                                    htmlHelper.MenuActionLink(new TagBuilder("li"), item.LinkText, item.ActionName,
                                        item.ControllerName, item.IconBootstrap,
                                        null,
                                        item.HtmlAttributes);
                            }
                            else
                            {
                                firstLevelLi =
                               htmlHelper.MenuActionLink(new TagBuilder("li"), item.LinkText, item.ActionName,
                                                         item.ControllerName,
                                                         null,
                                                         null,
                                                         item.HtmlAttributes);
                            }

                        }

                        // Second level
                        if (hasChild)
                        {
                            var secondLevelUl = new TagBuilder("ul");
                            secondLevelUl.AddCssClass("dropdown-menu");

                            NavigationItem item1 = item;
                            foreach (var secondItem in input.Where(m => m.Parent == item1))
                            {
                                var secondLevelLi = new TagBuilder("li");
                                secondLevelLi.InnerHtml =
                                    htmlHelper.MenuActionLink(secondItem.LinkText, secondItem.ActionName,
                                                              secondItem.ControllerName,
                                                              null,
                                                              secondItem.HtmlAttributes).ToHtmlString().Replace("[br]", "<br>");
                                // Third level
                                if (input.Any(m => m.Parent == secondItem))
                                {
                                    var thirdLevelUl = new TagBuilder("ul");

                                    NavigationItem item2 = secondItem;
                                    foreach (var thirdItem in input.Where(m => m.Parent == item2))
                                    {
                                        var thirdLevelLi = new TagBuilder("li");
                                        thirdLevelLi.InnerHtml =
                                            htmlHelper.MenuActionLink(thirdItem.LinkText, thirdItem.ActionName,
                                                                      thirdItem.ControllerName,
                                                                      null,
                                                                      thirdItem.HtmlAttributes).ToHtmlString().Replace("[br]", "<br>");

                                        thirdLevelUl.InnerHtml += thirdLevelLi.ToString(TagRenderMode.Normal);
                                    }
                                    secondLevelLi.InnerHtml += thirdLevelUl.ToString(TagRenderMode.Normal);
                                }
                                secondLevelUl.InnerHtml += secondLevelLi.ToString(TagRenderMode.Normal);

                                //var divider = new TagBuilder("li");
                                //divider.AddCssClass("divider");
                                //secondLevelUl.InnerHtml += divider.ToString(TagRenderMode.Normal);
                            }
                            firstLevelLi.InnerHtml += secondLevelUl.ToString(TagRenderMode.Normal);
                        }

                        main.InnerHtml += firstLevelLi.ToString(TagRenderMode.Normal);
                    }
                    menuContentStr = main.ToString(TagRenderMode.Normal);
                    HttpContext.Current.Session[sessionKey] = menuContentStr;
                }
            }
            return MvcHtmlString.Create(menuContentStr);
        }

        private static IEnumerable<NavigationItem> OnlyAuthorizedItems()
        {
            var input = NavigationItemService.NavigationItems;
            //if (HttpContext.Current.Session["PermList"] != null)
            //{
            //    var array = HttpContext.Current.Session["PermList"] as string[];
            //    return input.Where(m => array.Contains(m.Guid.ToString()));
            //}

            var output = new List<NavigationItem>();
            // var permissionService = DependencyResolver.Current.GetService<MK_Project.Services.PermissionService>();
            foreach (var item in input.Where(m => m.Parent == null))
            {
                var parent = item;
                if (parent.ActionName != "#")
                {
                    //if (permissionService.IsAuthorized(parent, HttpContext.Current.User))
                    if (true == true)
                    {
                        output.Add(parent);
                    }
                    else
                    {
                        continue;
                    }
                }
                else
                {
                    var children = input.Where(m => m.Parent == parent);

                    foreach (var item1 in children)
                    {
                        var child = item1;
                        //if (permissionService.IsAuthorized(child, HttpContext.Current.User))
                            output.Add(child);
                        //permList.Add(child.Guid.ToString(), permissionService.IsAuthorized(child, HttpContext.Current.User));
                    }
                    var hasChild = output.Any(m => m.Parent == parent);
                    TagBuilder firstLevelLi;
                    if (hasChild)
                    {
                        output.Add(parent);
                    }
                }
            }
            HttpContext.Current.Session["PermList"] = output.Select(m => m.Guid.ToString()).ToArray();
            return output;
        }
    }
}