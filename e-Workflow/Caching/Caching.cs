using System;
using System.Runtime.Caching;
using System.Threading;

namespace System
{
    class Caching
    {
        private static MemoryCache _cache = MemoryCache.Default;

        public static void AddToCache<T>(string key, T obj, CacheItemPolicy policy = null)
        {
            if (policy == null)
            {
                policy = new CacheItemPolicy();
                    policy.AbsoluteExpiration =
                DateTimeOffset.Now.AddHours(6);
            }
            _cache.AddOrGetExisting(key, obj, policy);
        }

        public static T GetFromCache<T>(string key)
        {
            return (T)_cache[key];
        }

        public static T GetFromCache<T>(string key, Func<T> valueFactory, CacheItemPolicy policy = null)
        {
            var newValue = new Lazy<T>(valueFactory, LazyThreadSafetyMode.PublicationOnly);
            var value = (Lazy<T>)_cache.AddOrGetExisting(key, newValue, policy);
            return (value ?? newValue).Value; // Lazy<T> handles the locking itself
        }

        public static void RemoveFromCache(string key)
        {
            _cache.Remove(key);
        }

        public static void ClearAll()
        {
            foreach (var cache in _cache)
            {
                _cache.Remove(cache.Key);
            }
        }
    }
}
/*
public CurrencyUnit GetCurrencyUnit(string currencyCode)
        {
            return Caching.GetFromCache("CurrencyUnit" + "-" + currencyCode,
                                           () => DoGetCurrencyUnit(currencyCode));
        }

        public CurrencyUnit DoGetCurrencyUnit(string currencyCode)
        {
            var currencyUnit = Load(m => m.CurrencyCode == currencyCode);
            return currencyUnit;
        }
*/