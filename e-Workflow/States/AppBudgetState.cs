﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Microsoft.Ajax.Utilities;
using PanasonicProject.Models;
using PanasonicProject.Models.Enums;
using PanasonicProject.Services;
using PanasonicProject.ViewModels;

namespace PanasonicProject.States
{
    public interface IAppBudgetState
    {
        decimal? GetBPAmountApplFormBudget(string bpNumber);
        decimal? GetBPAmountPurchLine(string bpNumber);
    }
    public class AppBudgetState : IAppBudgetState
    {
        private readonly ApplFormBudgetService _applFormBudgetService;
        private readonly ApplicationFormService _applicationFormService;
        private readonly PurchLineService _purchLineService;
        private readonly PurchTableService _purchTableService;
        private readonly GRNTableService _grnTableService;

        public AppBudgetState(ApplFormBudgetService applFormBudgetService, PurchLineService purchLineService, ApplicationFormService applicationFormService, PurchTableService purchTableService, GRNTableService grnTableService)
        {
            _applFormBudgetService = applFormBudgetService;
            _purchLineService = purchLineService;
            _applicationFormService = applicationFormService;
            _purchTableService = purchTableService;
            _grnTableService = grnTableService;
        }

        public decimal? GetBPAmountApplFormBudget(string bpNumber)
        {
            var bpUsed = (decimal?)0.00;
            var appBudgetModel = _applFormBudgetService.GetAppBudgetLineByBPNum(bpNumber);
            foreach (var appline in appBudgetModel)
            {
                var appModel = _applicationFormService.GetAppLine(appline.ApplId);
                if (appModel == null)
                {
                    continue;
                }
                var status = appModel.Status;
                if (appModel.IsCancelling == true)
                {
                    if (appline.GRBudgetUsed == null || appline.GRBudgetUsed == 0)
                    {
                        bpUsed += (appline.BPReserve != null) ? +appline.BPReserve : +0;
                    }
                    else
                    {
                        bpUsed += (appline.GRBudgetUsed != null) ? +appline.GRBudgetUsed : +0;
                    }
                }
                else
                {
                    if (appline.GRBudgetUsed == null || appline.GRBudgetUsed == 0)
                    {
                        bpUsed += (status == DocumentStatusFlag.Approved.ToString() && appline.BPReserve != null && appModel.IsCancelling != true) ? +appline.BPReserve : +0;
                    }
                    else if (appline.GRBudgetUsed != null && appModel.IsClosed == true)
                    {
                        bpUsed += (status == DocumentStatusFlag.Approved.ToString() && appline.GRBudgetUsed != null && appModel.IsCancelling != true) ? +appline.GRBudgetUsed : +0;
                    }
                    else if (appline.GRBudgetUsed != null && appModel.IsClosed != true)
                    {
                        bpUsed += (status == DocumentStatusFlag.Approved.ToString() && appline.GRBudgetUsed != null && appModel.IsCancelling != true) ? +appline.BPReserve : +0;
                    }
                    //else
                    //{
                    //    bpUsed += (status == DocumentStatusFlag.Approved.ToString() && appline.GRBudgetUsed != null && appModel.IsCancelling != true) ? +appline.GRBudgetUsed : +0;
                    //}
                }
            }
            return bpUsed;
        }

        public StateValidate GetArrBPAmountApplFormBudget(string bpNumber)
        {
            var stateValidate = new StateValidate();
            try
            {
                var bpUsed = (decimal?)0.00;

                stateValidate.StateName = "init";
                var app = _applicationFormService.GetAll(m => m.Status == DocumentStatusFlag.Approved.ToString()).ToList();
                var appBudgetModels = from line in _applFormBudgetService.GetAppBudgetLineByBPNum(bpNumber).ToList()
                                      join modelHead in app.ToList()
                                          on line.ApplId equals modelHead.ApplId
                                      select new ApplFormBudgetTable { BPReserve = line.BPReserve };

                stateValidate.StateName = "afterLoad";
                var applFormBudgetTables = appBudgetModels as ApplFormBudgetTable[] ?? appBudgetModels.ToArray();
                applFormBudgetTables.ToList();
                stateValidate.StateName = "afterConvert";
                if (applFormBudgetTables.Any())
                {
                    bpUsed = applFormBudgetTables.Sum(m => m.BPReserve);
                }
                stateValidate.StateName = "afterSUm";
                stateValidate.Value = bpUsed;
                return stateValidate;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.ToString() + " validationstate " + stateValidate);
            }
        }

        public StateValidate GetArrBPAmountApplFormBudgetJoinPO(string bpNumber)
        {
            var stateValidate = new StateValidate();
            try
            {
                var bpUsed = (decimal?)0.00;

                stateValidate.StateName = "init";
                var app = _applicationFormService.GetAll(m => m.Status == DocumentStatusFlag.Approved.ToString()).ToList();
                var po = _purchTableService.GetAll(m => m.POStatus == DocumentStatusFlag.Approved.ToString()).ToList();
                var appBudgetModels = from line in _applFormBudgetService.GetAppBudgetLineByBPNum(bpNumber).ToList()
                                      join modelHead in app.ToList()
                                          on line.ApplId equals modelHead.ApplId

                                      select new ApplFormBudgetTable { BPReserve = line.BPReserve };

                stateValidate.StateName = "afterLoad";
                var applFormBudgetTables = appBudgetModels as ApplFormBudgetTable[] ?? appBudgetModels.ToArray();
                applFormBudgetTables.ToList();
                stateValidate.StateName = "afterConvert";
                if (applFormBudgetTables.Any())
                {
                    bpUsed = applFormBudgetTables.Sum(m => m.BPReserve);
                }
                stateValidate.StateName = "afterSUm";
                stateValidate.Value = bpUsed;
                return stateValidate;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.ToString() + " validationstate " + stateValidate);
            }
        }

        public decimal? GetBPAmountPurchLine(string bpNumber)
        {
            var bpUsed = (decimal?)0.00;
            var polineModel = _purchLineService.GetAppBudgetLineByBPNum(bpNumber);
          
                foreach (var poline in polineModel)
                {
                    var poModel = _purchTableService.GetPOLine(poline.PurchId);
                    var status = poModel.POStatus;
                    if (poline.ApplId != null) continue;
                    if (poModel.IsCancel == true)
                    {
                        if (poline.GRBudgetUsed == null || poline.GRBudgetUsed == 0)
                        {
                            bpUsed += (poline.LineAmountTH != null) ? +poline.LineAmountTH : +0;
                        }
                        else
                        {
                            bpUsed += (poline.GRBudgetUsed != null) ? +poline.GRBudgetUsed : +0;
                        }
                    }
                    else
                    {
                        if (poline.GRBudgetUsed == null || poline.GRBudgetUsed == 0)
                        {
                            bpUsed += (status == DocumentStatusFlag.Approved.ToString() && poline.LineAmountTH != null)
                                ? +poline.LineAmountTH
                                : +0;
                        }
                        else
                        {
                            bpUsed += (status == DocumentStatusFlag.Approved.ToString() && poline.GRBudgetUsed != null)
                                ? +poline.GRBudgetUsed
                                : +0;
                        }
                    }
                }
            
            return bpUsed;
        }

        public decimal? GetArrBPAmountPurchLine(string bpNumber)
        {
            var bpUsed = (decimal?)0.00;


            var po = _purchTableService.GetAll(m => m.POStatus == DocumentStatusFlag.Approved.ToString()).ToList();
            var polinemodel = from line in _purchLineService.GetAppBudgetLineByBPNum(bpNumber).ToList()
                              join modelHead in po.ToList()
                                      on line.PurchId equals modelHead.PurchId
                                  select new ApplFormBudgetTable { BPReserve = line.LineAmount };

            var applFormBudgetTables = polinemodel as ApplFormBudgetTable[] ?? polinemodel.ToArray();
            applFormBudgetTables.ToList();
            if (applFormBudgetTables.Any())
            {
                bpUsed = applFormBudgetTables.Sum(m => m.BPReserve);
            }
            return bpUsed;
        }
    }
}