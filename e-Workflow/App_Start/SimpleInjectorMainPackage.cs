﻿using System.Configuration;
using System.Data.Entity;
using System.Linq;
using System.Web.Configuration;
using CrossingSoft.MVC.Infrastructure.K2;
using CrossingSoft.MVC.Infrastructure.Security;
using CrossingSoft.MVC.Infrastructure.Storage;
using CrossingSoft.MVC.Infrastructure.Storage.EF;
using CrossingSoft.MVC.Infrastructure.Storage.K2;
using CrossingSoft.MVC.Repositories;
using PanasonicProject.Connections;
using PanasonicProject.Models.Contexts;
using PanasonicProject.Repositories;
using SimpleInjector;
using SimpleInjector.Extensions;
using SimpleInjector.Integration.Web;

namespace PanasonicProject
{
    public class SimpleInjectorMainPackage : SimpleInjector.Packaging.IPackage
    {
        public void RegisterServices(Container container)
        {
            var webLifestyle = new WebRequestLifestyle();

            //container.Register<DbContext>(() => new MainContext(WebConfigurationManager.AppSettings["DBConnectionString"]), webLifestyle);

            container.RegisterWithContext<DbContext>
                (context => new MainContext(WebConfigurationManager.AppSettings["DBConnectionString"]));

            //container.RegisterWithContext<DbContext>
            //    (context => new AccessContext(WebConfigurationManager.AppSettings["PBTHDBConnectionString"]));

            container.RegisterOpenGeneric(typeof(ISession<>), typeof(SmartObjectSession<>), webLifestyle,
                c => c.ServiceType.GetGenericArguments().Single().GetInterfaces().Any(m => m.Name == typeof(Models.Interfaces.ISMOModel).Name));
            container.RegisterOpenGeneric(typeof(ISession<>), typeof(EFSession<>), webLifestyle, c => !c.Handled);

            container.RegisterOpenGeneric(typeof(BaseRepository<>), typeof(BasicRepository<>), webLifestyle, c => !c.Handled);

            container.RegisterPerWebRequest<IHostConnection>(HostConnectionInjection.Inject);
            container.RegisterPerWebRequest<IWorkflowConnection>(WorkflowConnectionInjection.Inject);
            container.RegisterPerWebRequest<IWorkflowRepository, WorkflowRepository>();
            //container.RegisterPerWebRequest<IPermissionService, PermissionService>();
        }
    }
}