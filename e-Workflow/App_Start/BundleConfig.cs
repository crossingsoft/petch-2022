﻿using System.Web.Optimization;

namespace PanasonicProject
{
    public class BundleConfig
    {
        // For more information on Bundling, visit http://go.microsoft.com/fwlink/?LinkId=254725
        public static void RegisterBundles(BundleCollection bundles)
        {

            bundles.Add(new StyleBundle("~/Content/css").Include(
                             "~/Content/css/bootstrap.css",
                             "~/Content/css/Site.css",
                             "~/Content/css/jquery-ui.css",
                             "~/Content/css/bootstrap-datepicker.css",
                             "~/Content/kendo/css/kendo.common.min.css",
                             "~/Content/kendo/css/kendo.metro.min.css"));


            bundles.Add(new ScriptBundle("~/bundles/bootstrap").Include(
                "~/Content/js/affix.js",
                 "~/Content/js/alert.js",
                  "~/Content/js/carousel.js",
                   "~/Content/js/collapse.js",
                    "~/Content/js/dropdown.js",
                     "~/Content/js/modal.js",
                     "~/Content/js/tooltip.js",
                      "~/Content/js/popover.js",
                       "~/Content/js/scrollspy.js",
                        "~/Content/js/tab.js",
                          "~/Content/js/transition.js",
                          "~/Content/js/bootstrap-toast.js",
                          "~/Scripts/bootstrap-datepicker.js"
                ));

            bundles.Add(new ScriptBundle("~/bundles/PECTH").Include(
                "~/Content/pecth/pecth.js",
                "~/Content/pecth/pecth.reports.js",
                "~/Content/pecth/pecth.datetimepicker.js"
                ));

            bundles.Add(new ScriptBundle("~/bundles/kendo").Include(
                "~/Scripts/kendo/JS/kendo.all.min.js",
                "~/Scripts/kendo/JS/kendo.aspnetmvc.min.js",
                "~/Scripts/kendo.modernizr.custom.js",
                "~/Scripts/kendo/JS/jszip.min.js"
                ));




            bundles.Add(new ScriptBundle("~/bundles/jquery").Include(
                        "~/Scripts/jquery-{version}.js",
                        "~/Scripts/jquery.blockUI.js",
                         "~/Content/js/panasonic.js"));

            bundles.Add(new ScriptBundle("~/bundles/iframeResizer").Include(
                        "~/Scripts/iframeResizer.min.js"));

            bundles.Add(new ScriptBundle("~/bundles/jqueryui").Include(
                        "~/Scripts/jquery-ui-{version}.js"));

            bundles.Add(new ScriptBundle("~/bundles/jqueryval").Include(
                        "~/Scripts/jquery.unobtrusive*",
                        "~/Scripts/jquery.validate*"));

            // Use the development version of Modernizr to develop with and learn from. Then, when you're
            // ready for production, use the build tool at http://modernizr.com to pick only the tests you need.
            bundles.Add(new ScriptBundle("~/bundles/modernizr").Include(
                        "~/Scripts/modernizr-*"));



            bundles.Add(new StyleBundle("~/Content/themes/base/css").Include(
                        "~/Content/themes/base/jquery.ui.core.css",
                        "~/Content/themes/base/jquery.ui.resizable.css",
                        "~/Content/themes/base/jquery.ui.selectable.css",
                        "~/Content/themes/base/jquery.ui.accordion.css",
                        "~/Content/themes/base/jquery.ui.autocomplete.css",
                        "~/Content/themes/base/jquery.ui.button.css",
                        "~/Content/themes/base/jquery.ui.dialog.css",
                        "~/Content/themes/base/jquery.ui.slider.css",
                        "~/Content/themes/base/jquery.ui.tabs.css",
                        "~/Content/themes/base/jquery.ui.datepicker.css",
                        "~/Content/themes/base/jquery.ui.progressbar.css",
                        "~/Content/themes/base/jquery.ui.theme.css"));
        }
    }
}