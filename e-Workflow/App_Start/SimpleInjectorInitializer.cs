﻿using System.Reflection;
using System.Web.Mvc;
using PanasonicProject;
using SimpleInjector;
using SimpleInjector.Integration.Web.Mvc;

[assembly: WebActivatorEx.PostApplicationStartMethod(typeof(SimpleInjectorInitializer), "Initialize")]

namespace PanasonicProject
{
    public static class SimpleInjectorInitializer
    {
        /// <summary>Initialize the container and register it as MVC3 Dependency Resolver.</summary>
        public static void Initialize()
        {
            // Did you know the container can diagnose your configuration? Go to: https://bit.ly/YE8OJj.
            var container = new Container();

            InitializeContainer(container);

            container.RegisterMvcControllers(Assembly.GetExecutingAssembly());

            DependencyResolver.SetResolver(new SimpleInjectorDependencyResolver(container));

            container.Verify();
        }

        private static void InitializeContainer(Container container)
        {
            var package = new SimpleInjectorMainPackage();
            package.RegisterServices(container);
        }
    }
}