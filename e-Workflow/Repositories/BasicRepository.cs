﻿using CrossingSoft.MVC.Infrastructure.Storage;
using CrossingSoft.MVC.Repositories;

namespace PanasonicProject.Repositories
{
    public class BasicRepository<T> : BaseRepository<T> where T : class, new()
    {
        public BasicRepository(ISession<T> session)
            : base(session)
        {
        }
    }
}