﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web;

namespace System
{
    public static class DateTimeExtension
    {
        
        public static DateTime GetEndCurrentDate(this DateTime date)
        {
            return date.AddDays(1).AddSeconds(-1);
        }

        public static DateTime GetCurrentDate(this DateTime date)
        {
            return date.Date;
        }

        public static DateTime GetYesterdayDate(this DateTime date)
        {
            return date.AddDays(-1);
        }

        public static string DateString(this DateTime? dateTime, bool haveTime = false)
        {
            if (dateTime == null)
                return string.Empty;


            if (!haveTime) return String.Format("{0:dd/MM/yyyy}", dateTime.Value);

            return String.Format("{0:g}", dateTime.Value);
        }

        public static string DateString(this DateTime dateTime, bool haveTime = false)
        {

            if (!haveTime) return String.Format("{0:dd/MM/yyyy}", dateTime);

            return String.Format("{0:g}", dateTime);
        }

        public static string ToSAPFormat(this DateTime? dateTime)
        {
            if (dateTime == null) return string.Empty;
            return ToSAPFormat(dateTime.Value);
        }

        public static string ToSSRSFormat(this DateTime? dateTime)
        {
            if (dateTime == null) return string.Empty;
            //2015-07-11
            return dateTime.Value.ToString("yyyy-MM-dd");
        }

        public static string ToSAPFormat(this DateTime dateTime)
        {

            return dateTime.ToString("yyyyMMdd");
        }



        public static string ThaiDateString(this DateTime? dateTime, bool mini = false)
        {
            if (dateTime == null)
                return string.Empty;

            return ThaiDateString(dateTime.Value, mini);
        }

        public static string ThaiDateString(this DateTime dateTime, bool mini = false)
        {

            var ci = new CultureInfo("th-TH");

            if (mini) return dateTime.ToString("d MMM yy", ci);
            var year = dateTime.Year;
            return dateTime.ToString("วันdddd ที่ d MMMM ", ci) + year;
        }
        public static string ToProjectDate(this DateTime dateTime, bool mini = false)
        {
            return dateTime.ToString("dd-MMM-yyyy ") + dateTime.ToShortTimeString();
        }
    }
}