﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace System
{
    public static class BooleanExtension
    {
        public static bool GetIsFood(this string countingMatType)
        {
            return (!string.IsNullOrEmpty(countingMatType) && countingMatType.Equals("Food"));
        }
    }
}