﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace System
{
    public static class URLSmartformsExtension
    {
        public static string GetURLSmartForms(this string url, string formName, bool runTimeMode = true,string host="localhost")
        {
            var urlPath = string.Format("https://{0}/{1}/Runtime/Form/{2}",host,(runTimeMode)?"Runtime":"Designer",formName);
            return urlPath;
        }

        public static string GetURLSmartForms(this string url, string formName, string host = "localhost")
        {
            return GetURLSmartForms(url, formName, false, host);
        }
    }
}