﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace System
{
    public static class SAPExtension
    {
        public static string GetSearchQuery(this string searchQuery)
        {
            if (string.IsNullOrEmpty(searchQuery)) return "";

            return string.Format("{0}*", searchQuery);
        }

        public static string ToItemNOFormat(this long itemNo)
        {
            return itemNo.ToString().PadLeft(10, '0');
        }
        public static string ToItemNOFormat(this string itemNo)
        {
            return itemNo.PadLeft(10, '0');
        }
    }
}