﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MK_Project.Extension
{
    public class SerializeHeaderAttribute : Attribute
    {
        public string Type { get; set; }
        public string Method { get; set; }

    }
    public class SerializePropertiesAttribute : Attribute
    {

    }
}