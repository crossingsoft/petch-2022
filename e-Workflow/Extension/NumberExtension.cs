﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace System
{
    public static class NumberExtension
    {
        public static string ConvertToThaiStatus(this long status)
        {
            string statusConvert;
            switch (status)
            {
                case 0:
                    statusConvert = "รอส่ง"; break;
                case 1:
                    statusConvert = "อยู่ระหว่างดำเนินการ"; break;
                case 2:
                    statusConvert = "ส่งแล้ว"; break;
                case 3:
                    statusConvert = "ยกเลิก"; break;
                case 4:
                    statusConvert = "ไม่อนุมัติ"; break;
                default:
                    statusConvert = "";
                    break;
            }

            return statusConvert;
        }
    }
}