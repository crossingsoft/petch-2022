﻿using System.Configuration;
using System.Linq;
using System.Web.Mvc;
using Antlr.Runtime.Misc;
using PanasonicProject.Facades;
using PanasonicProject.Resource;
using PanasonicProject.ViewModels.Report;

namespace PanasonicProject.Controllers
{
    public class ReportsController : PanasonicProject.Controllers.Abtracts.BaseController
    {
        private readonly ReportFacade _facade;

        public ReportsController(ReportFacade facade)
        {
            _facade = facade;
        }

        public void Index()
        {
             
        }

        public ActionResult SummaryReportsView()
        {
            var model = _facade.InitSummaryReportsViewModel();
            return View("SummaryReports", model);
        }

        public ActionResult RolesUserReport()
        {
            return View("RolesUserReport");
        }

        [HttpPost]
        public ActionResult SearchReports(SummaryReportsViewModel baseCriteriaReport)
        {
            string url = "";

            //var model = _facade.InitSummaryReportsViewModel();
            if (baseCriteriaReport.Criteria.ReportName == ReportName.ExpRepFix && (baseCriteriaReport.Criteria.SubReportName == ReportName.ExpRepFix_Expense || baseCriteriaReport.Criteria.SubReportName == ReportName.ExpRepFix_FixedAsset) || baseCriteriaReport.Criteria.SubReportName == ReportName.ExpRepFix_Repair)
            {
                url = ConfigurationManager.AppSettings["SQLHost"] + "%2fSummaryExpenseRepairFixedSubReport" + _facade.InitBaseParameter(baseCriteriaReport.Criteria) + "&SubReportName=" + baseCriteriaReport.Criteria.SubReportName + "&rs:Command=Render";
            }
            else if (baseCriteriaReport.Criteria.ReportName == ReportName.ExpRepFix && baseCriteriaReport.Criteria.SubReportName == ReportName.ExpRepFix_Summary)
            {
                url = ConfigurationManager.AppSettings["SQLHost"] + "%2fSummaryExpenseRepairFixedReport" + _facade.InitBaseParameter(baseCriteriaReport.Criteria) + "&rs:Command=Render";
            }
            else if (baseCriteriaReport.Criteria.ReportName == ReportName.GR)
            {
                url = ConfigurationManager.AppSettings["SQLHost"] + "%2fGRForm" + _facade.InitBaseParameter(baseCriteriaReport.Criteria) + "&rs:Command=Render";
            }

            if (string.IsNullOrEmpty(url)) 
                return PartialView("Error_PartialView", "System not has report. please check criteria again.");
            else
                return PartialView("ReportViewer",url);
        }

        [HttpGet]
        public ActionResult BPVisualization()
        {
            string url = ConfigurationManager.AppSettings["SSRSItemHost"] + "%2fSSRS_BPVisualization";
            return Redirect(url);
        }

        [HttpGet]
        public ActionResult BPMonitoring()
        {
            string url = ConfigurationManager.AppSettings["SSRSItemHost"] + "%2fSSRS_BPMonitoring";
            return Redirect(url);
        }

        [HttpGet]
        public ActionResult ExpenseRepairAndFixedAsset()
        {
            string url = ConfigurationManager.AppSettings["SQLHost"] + "%2fSSRS_SummaryExpenseRepairFixedReport";
            return Redirect(url);
        }

        [HttpGet]
        public ActionResult Expense()
        {
            string url = ConfigurationManager.AppSettings["SSRSItemHostPECTH"] + "&SubReportName=Indirect";
            return Redirect(url);
        }

        [HttpGet]
        public ActionResult Repair()
        {
            string url = ConfigurationManager.AppSettings["SSRSItemHostPECTH"] + "&SubReportName=Repairing";
            return Redirect(url);
        }

        [HttpGet]
        public ActionResult Others()
        {
            string url = ConfigurationManager.AppSettings["SSRSItemHostPECTH"] + "&SubReportName=Others";
            return Redirect(url);
        }

        [HttpGet]
        public ActionResult FixedAssets()
        {
            string url = ConfigurationManager.AppSettings["SSRSItemHostPECTH"] + "&SubReportName=Fixed Assets";
            return Redirect(url);
        }

        public ActionResult CompareBPAmountAndActualAmount()
        {
            string url = ConfigurationManager.AppSettings["SSRSItemHost"] + "%2fSSRS_CompareBPAmountAndActualAmount";
            return Redirect(url);
        }

        public ActionResult SummaryInvestmentbySection()
        {
            string url = ConfigurationManager.AppSettings["SSRSItemHost"] + "%2fSSRS_SummaryInvestmentbySection";
            return Redirect(url);
        }

        [HttpGet]
        public ActionResult GRN()
        {
            string url = ConfigurationManager.AppSettings["SSRSItemHost"] + "%2fSSRS_GRForm";
            return Redirect(url);
        }

        [HttpGet]
        public ActionResult GRNHistory()
        {
            string url = ConfigurationManager.AppSettings["SSRSItemHost"] + "%2fSSRS_GRReport";
            return Redirect(url);
        }

        [HttpGet]
        public ActionResult ItemConsumptionAnalysis()
        {
            string url = ConfigurationManager.AppSettings["SSRSItemHost"] + "%2fSSRS_ItemConsumptionAnalysisChartReport_L1";
            return Redirect(url);
        }


        [HttpPost]
        public ActionResult SearchReportPDF(SummaryReportsViewModel baseCriteriaReport)
        {
            if (baseCriteriaReport.Criteria.ReportName == ReportName.PO_App)
            {
                var bytes = _facade.GetListPOAndFriendsFormReport(baseCriteriaReport.Criteria);
                if (!bytes.Any())
                {
                    baseCriteriaReport.IsErrorMessage = true;
                    baseCriteriaReport.ErrorMessage = PECTHResource.ReportNotFound;
                }
                else
                {
                    MultiFilePdf(bytes, "Purchase order");
                }
                
            }
            else if (baseCriteriaReport.Criteria.ReportName == ReportName.App)
            {
                var bytes = _facade.GetListApplicationFormReport(baseCriteriaReport.Criteria);
                if (!bytes.Any())
                {
                    baseCriteriaReport.IsErrorMessage = true;
                    baseCriteriaReport.ErrorMessage = PECTHResource.ReportNotFound;
                }
                else
                {
                    MultiFilePdf(bytes, "Application");
                }
            }
            else if (baseCriteriaReport.Criteria.ReportName == ReportName.PO && baseCriteriaReport.Criteria.PrePintForm.HasValue)
            {
                var bytes = _facade.GetListPOFormReport(baseCriteriaReport.Criteria);
                if (!bytes.Any())
                {
                    baseCriteriaReport.IsErrorMessage = true;
                    baseCriteriaReport.ErrorMessage = PECTHResource.ReportNotFound;
                }
                else
                {
                    MultiFilePdf(bytes, "Purchase order");
                }
            }
            else if (baseCriteriaReport.Criteria.ReportName == ReportName.PO &&
                     !baseCriteriaReport.Criteria.PrePintForm.HasValue)
            {
                var bytes = _facade.GetListPOFormReport(baseCriteriaReport.Criteria);
                if (!bytes.Any())
                {
                    baseCriteriaReport.IsErrorMessage = true;
                    baseCriteriaReport.ErrorMessage = PECTHResource.ReportNotFound;
                }
                else
                {
                    MultiFilePdf(bytes, "Purchase order");
                }
            }
            else
            {
                baseCriteriaReport.IsErrorMessage = true;
                baseCriteriaReport.ErrorMessage = PECTHResource.ReportNotFound;
            }

            baseCriteriaReport.Criteria = _facade.InitBaseCriteriaReport();
            return View("SummaryReports", baseCriteriaReport);
            //return RedirectToAction("SummaryReportsView", "Reports");
        }

        public JsonResult SearchUser(string text)
        {
            var datas = _facade.GetListUserFilter(text);
            return Json(datas, JsonRequestBehavior.AllowGet);
        }
        public JsonResult SearchSection(long? number)
        {
            var datas = _facade.GetListSectionFilter(number);
            return Json(datas, JsonRequestBehavior.AllowGet);
        }
        public JsonResult SearchDivision(string text)
        {
            var datas = _facade.GetListDivisionFilter(text);
            return Json(datas, JsonRequestBehavior.AllowGet);
        }
        public JsonResult SearchDepartment(string text)
        {
            var datas = _facade.GetListDepartmentFilter(text);
            return Json(datas, JsonRequestBehavior.AllowGet);
        }
        public JsonResult GetAllRequestReportName()
        {
            return Json(_facade.GetAllRequestReportName(), JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetAllSubRequestReportName()
        {
            var requestToId = "ExpRepFix";
            return Json(_facade.GetListSubRequestReportName(requestToId), JsonRequestBehavior.AllowGet);
        }


        //Print local memo
        public void PrintPOAndAppViewer(long? primaryId)
        {
            if (primaryId != null)
            {
                var report = _facade.GetListPOAndFriendsFormReport(primaryId.Value);
                MultiFilePdf(report,"PO : "+primaryId);
            }
            
        }

        public void PrintPOViewer(long? primaryId, bool isPrePrint)
        {
            if (primaryId != null)
            {
                var report = _facade.GetListPOFormReport(primaryId.Value, isPrePrint);
                MultiFilePdf(report, "PO : " + primaryId);
            }

        }

        public void PrintPOLocalViewer(long? primaryId, bool isPrePrint,bool isModel600E)
        {
            if (primaryId != null)
            {
                var report = _facade.GetListPOLocalFormReport(primaryId.Value, isPrePrint, isModel600E);
                MultiFilePdf(report, "PO : " + primaryId);
            }

        }

        public void PrintApplicationViewer(long? primaryId, bool isPrePrint,bool isModel600E)
        {
            if (primaryId != null)
            {
                var report = _facade.GetListApplicationFormReport(primaryId.Value, isPrePrint, isModel600E);
                MultiFilePdf(report,"App : "+primaryId);
                //CreatePDF(report[0], "App : " + primaryId);
            }
        }

        [HttpGet]
        public ActionResult TestPrinted()
        {
            return View();
        }

    }
}