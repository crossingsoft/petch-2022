﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using PagedList;
using PanasonicProject.Controllers.Abtracts;
using PanasonicProject.Facades;
using PanasonicProject.Services;

namespace PanasonicProject.Controllers
{
    public class WorkspaceController : BaseController
    {
        private readonly WorkflowService _workflowService;
        private readonly WorkspaceFacade _workspaceFacade; 

        public WorkspaceController(WorkflowService workflowService, WorkspaceFacade workspaceFacade)
        {
            _workflowService = workflowService;
            _workspaceFacade = workspaceFacade;
        }

        [HttpGet]
        public ActionResult Index(string processId = "All", string status = "All", string searchQuery = null, int pageNumber=1)
        {
            try
            {
                ViewBag.SearchQuery = searchQuery;
                int pageSize = 10;
                var model = _workflowService.InitWorklistViewModel(searchQuery, processId, status);

                ViewBag.ProcessId = processId;
                if (Request.IsAjaxRequest())
                {
                    return PartialView("_List", model.WorklistItems.ToPagedList(pageNumber, pageSize));
                }
                return View(model.WorklistItems.ToPagedList(pageNumber, pageSize));
            }
            catch (Exception exception)
            {
                throw new Exception(exception.Message);
            }
           
        }

        [HttpGet]
        public ActionResult OpenWorklistItem(string sn)
        {
            try
            {
                if (_workflowService.IsHasWorklistItem(sn))
                {
                    var workListItem = _workflowService.GetMyWorkListItem(sn);
                    var model = InitBaseSmartFormViewModel(workListItem);

                    try
                    {
                        var dataFields = _workflowService.GetDataFields(sn);
                        var prId = _workflowService.GetDataField(dataFields, "RequestHeaderID");
                        if(prId != null)
                        {
                            var facade = DependencyResolver.Current.GetService<PurchaseOrderFacade>();
                            var id = Convert.ToInt32(prId);
                            var prTable = facade.GetPRTable(id);
                            if (prTable == null)
                            {
                                model.ActivityName = "RequestSheet/0";
                            }
                            else
                            {
                                model.ActivityName = "RequestSheet/" + prTable.CurrentStep.ToString();
                            }
                            
                        }

                    }
                    catch (Exception ex)
                    {

                    }

                    _workspaceFacade.UpdateActiveBy(sn);

                    return View("WorklistItem", model);
                }
                else
                {
                    return View("Index");
                }
                
            }
            catch (Exception exception)
            {
                return View("Index");
            }
            
        }

        [HttpGet]
        public ActionResult Release(string sn)
        {
            try
            {
                _workflowService.Release(sn);
                _workspaceFacade.ClearActiveBy(sn);
                return RedirectToAction("Index", "Workspace");
            }
            catch (Exception exception)
            {
                throw new Exception(exception.Message);
            }
        }

        //[HttpGet]
        //public JsonResult Index(string processId = "All", string status = "All", string searchQuery = null)
        //{
        //    //return null;

        //    var model = _workflowService.InitWorklistViewModel(searchQuery, processId, status);

        //    ViewBag.ProcessId = processId;
        //    if (Request.IsAjaxRequest())
        //    {
        //        // return PartialView("_List", model);
        //    }
        //   // return View(model);
        //    return Json(model, JsonRequestBehavior.AllowGet);
        //}
    }
}