﻿using System.Web.Mvc;
using iTextSharp.text.pdf.qrcode;
using PanasonicProject.Controllers.Abtracts;
using PanasonicProject.Facades;
using PanasonicProject.Models;
using PanasonicProject.Resource;
using PanasonicProject.Services;
using PanasonicProject.ViewModels;
using System.Web;
using iTextSharp.text.pdf.parser;
using System.IO;
using System;
using System.Web.Configuration;
using PanasonicProject.Models.eRequestSheet;

namespace PanasonicProject.Controllers
{
    public class HomeController : BaseController
    {
        private readonly InventTableService _inventTableService;
        private readonly PermissionFacade _permissionFacade;
        private readonly FormAppFacade _facade;

        public HomeController(InventTableService inventTableService, PermissionFacade permissionFacade, FormAppFacade facade)
        {
            _inventTableService = inventTableService;
            _permissionFacade = permissionFacade;
            _facade = facade;
        }

        public ActionResult ImportExcel()
        {
            return View("ImportExcel");
        }

        public ActionResult ReadFileImportExcel(HttpPostedFileBase file)
        {
            try
            {
                var root = WebConfigurationManager.AppSettings["Upload_ImportPath"];
                var folder = "Upload";
                string pathString = System.IO.Path.Combine(root, folder);

                if (!Directory.Exists(pathString))
                {
                    Directory.CreateDirectory(pathString);
                }
                string fileName = file.FileName;
                pathString = System.IO.Path.Combine(pathString, fileName);
                //if (!System.IO.File.Exists(pathString))
                {
                    byte[] data;
                    using (Stream inputStream = file.InputStream)
                    {
                        MemoryStream memoryStream = inputStream as MemoryStream;
                        if (memoryStream == null)
                        {
                            memoryStream = new MemoryStream();
                            inputStream.CopyTo(memoryStream);
                        }
                        data = memoryStream.ToArray();
                        System.IO.File.WriteAllBytes(pathString, data);
                    }
                    _facade.ReadData(pathString);
                }

               // return RedirectToAction("Index", "Workspace");
                return Content("<script language='javascript' type='text/javascript'>alert('Import Excel Completed'); window.location.href ='/home/importexcel';</script>");
            }
            catch (Exception exception)
            {
                throw new Exception(exception.Message);
            }

        }
        
        public JsonResult GetListInvent()
        {
            var inventList = _inventTableService.GetList();
            return Json(inventList, JsonRequestBehavior.AllowGet);
        }

        public ActionResult Index()
        {
            return RedirectToAction("Index", "Workspace");
        }
        public ActionResult WorkspaceMenu()
        {
           
            return View("Workspace");
        }

        public ActionResult PurchaseOrderMenu(long? parampurchId)
        {
            var purchTableService = DependencyResolver.Current.GetService<PurchTableService>();
            var status = (string)null;
            if (parampurchId != null)
            {
                status = purchTableService.GetPOLine(parampurchId).DocumentStatus;
            }

            ParamViewModel.ParamId = parampurchId;
            ParamViewModel.Activity = status;
            return View("PurchaseOrder");
        }

        public ActionResult PurchaseOrderDetail(long parampurchId)
        {
            var purchTableService = DependencyResolver.Current.GetService<PurchTableService>();
            var status = purchTableService.GetPOLine(parampurchId).DocumentStatus;

            ParamViewModel.ParamId = parampurchId;
            ParamViewModel.Activity = status;
            return View("PurchaseOrderDetail");
        }
        
        public ActionResult PurchaseOrderCancel(long parampurchId)
        {
            var purchTableService = DependencyResolver.Current.GetService<PurchTableService>();
            var status = purchTableService.GetPOLine(parampurchId).DocumentStatus;

            ParamViewModel.ParamId = parampurchId;
            ParamViewModel.Activity = status;
            return View("PurchaseOrderDetailCancel");
        }
        public ActionResult PurchaseOrderUpdateStatus(long parampurchId)
        {
            var purchTableService = DependencyResolver.Current.GetService<PurchTableService>();
            var status = purchTableService.GetPOLine(parampurchId).DocumentStatus;

            ParamViewModel.ParamId = parampurchId;
            ParamViewModel.Activity = status;
            return View("PurchTableUpdateStatus");
        }
        public ActionResult GoodReceiptNoteMenu()
        {
            return View("GoodReceiptNote");
        }
        public ActionResult AppBussMenu(long? paramapplid)
        {
            var appTableService = DependencyResolver.Current.GetService<ApplicationFormService>();
            var status = (string) null;
            if (paramapplid != null)
            {
                status = appTableService.GetAppLine(paramapplid).DocumentStatus;
            }

            ParamViewModel.ParamId = paramapplid;
            ParamViewModel.Activity = status;
            return View("AppBuss");
        }
        public ActionResult AppBussDetail(long paramapplid)
        {
            var appTableService = DependencyResolver.Current.GetService<ApplicationFormService>();
            var status = appTableService.GetAppLine(paramapplid).DocumentStatus;

            ParamViewModel.ParamId = paramapplid;
            ParamViewModel.Activity = status;
            return View("AppBussDetail");
        }
        public ActionResult AppBussCancel(long paramapplid)
        {
            var appTableService = DependencyResolver.Current.GetService<ApplicationFormService>();
            var status = appTableService.GetAppLine(paramapplid).DocumentStatus;

            ParamViewModel.ParamId = paramapplid;
            ParamViewModel.Activity = status;
            return View("AppBussDetailCancel");
        }
        public ActionResult AppBussClosed(long paramapplid)
        {
            var appTableService = DependencyResolver.Current.GetService<ApplicationFormService>();
            var status = appTableService.GetAppLine(paramapplid).DocumentStatus;
            ParamViewModel.ParamId = paramapplid;
            ParamViewModel.Activity = status;
            return View("AppBussDetailClosed");
        }
        //inq    
        public ActionResult APPInquiriesMenu()
        {
            return View("APPInquiry");
        }
        public ActionResult POInquiriesMenu()
        {
            return View("POInquiry");
        }
        public ActionResult GRInquiriesMenu()
        {
            return View("GRInquiry");
        }

        public ActionResult RelatedAPPInquiriesMenu()
        {
            if (!_permissionFacade.IsAuth(PermissionResource.Module_App, PermissionResource.Type_Related,
                HttpContext.User.Identity.Name))
            {
                return View("UnAuthorize");
            }

            return View("RelatedAPPInquiry");
        }
        public ActionResult RelatedPOInquiriesMenu()
        {
            if (!_permissionFacade.IsAuth(PermissionResource.Module_PO, PermissionResource.Type_Related,
                HttpContext.User.Identity.Name))
            {
                return View("UnAuthorize");
            }

            return View("RelatedPOInquiry");
        }

        public ActionResult TeamAPPInquiriesMenu()
        {
            if (!_permissionFacade.IsAuth(PermissionResource.Module_App, PermissionResource.Type_Team,
                HttpContext.User.Identity.Name))
            {
                return View("UnAuthorize");
            }

            return View("TeamAPPInquiry");
        }
        public ActionResult TeamPOInquiriesMenu()
        {
            if (!_permissionFacade.IsAuth(PermissionResource.Module_PO, PermissionResource.Type_Team,
                HttpContext.User.Identity.Name))
            {
                return View("UnAuthorize");
            }

            return View("TeamPOInquiry");
        }

        public ActionResult AllAPPInquiriesMenu()
        {
            if (!_permissionFacade.IsAuth(PermissionResource.Module_App, PermissionResource.Type_ALL,
                HttpContext.User.Identity.Name))
            {
                return View("UnAuthorize");
            }

            return View("AllAPPInquiry");
        }
        public ActionResult AllPOInquiriesMenu()
        {
            if (!_permissionFacade.IsAuth(PermissionResource.Module_PO, PermissionResource.Type_ALL,
                HttpContext.User.Identity.Name))
            {
                return View("UnAuthorize");
            }

            return View("AllPOInquiry");
        }


        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }


        public ActionResult GRMenu(long? paramgrtableid)
        {
            ParamViewModel.ParamId = paramgrtableid;
            return View("GRNMenu");
        }
        public ActionResult GRDetail(long paramgrtableid)
        {
            ParamViewModel.ParamId = paramgrtableid;
            return View("GRNDetail");
        }
        public ActionResult GRUpdate(long paramgrtableid)
        {
            ParamViewModel.ParamId = paramgrtableid;
            return View("GRNUpdate");
        }
        public ActionResult GRACCUpdate(long paramgrtableid)
        {
            ParamViewModel.ParamId = paramgrtableid;
            return View("GRNACCUpdate");
        }
        //acc assign
        public ActionResult AppBussAcc(long paramapplid)
        {
            ParamViewModel.ParamId = paramapplid;
            return View("AppBussACCAssign");
        }
        public ActionResult POAcc(long parampurchid)
        {
            ParamViewModel.ParamId = parampurchid;
            return View("PurchaseACCAssign");
        }

        //bp assign
        public ActionResult AppBussAccBP(long paramapplid)
        {
            ParamViewModel.ParamId = paramapplid;
            return View("AppBussACCAssignBP");
        }
        public ActionResult POAccBP(long parampurchid)
        {
            ParamViewModel.ParamId = parampurchid;
            return View("PurchaseACCAssignBP");
        }

        //gr unlock
        public ActionResult GRUnlock(long paramgrtableid)
        {
            ParamViewModel.ParamId = paramgrtableid;
            return View("GRNUnlock");
        }

        //master
        public ActionResult SystemParameter()
        {
            if (!_permissionFacade.IsAuth(PermissionResource.Module_Admin, PermissionResource.Type_ALL,
                HttpContext.User.Identity.Name))
            {
                return View("UnAuthorize");
            }

            return View("SystemParameter");
        }
        public ActionResult LineofAuthorization()
        {
            if (!_permissionFacade.IsAuth(PermissionResource.Module_Admin, PermissionResource.Type_ALL,
                HttpContext.User.Identity.Name))
            {
                return View("UnAuthorize");
            }

            return View("ApprovalStep");
        }
        public ActionResult BusinessPlan()
        {
            if (!_permissionFacade.IsAuth(PermissionResource.Module_Admin, PermissionResource.Type_ALL,
                HttpContext.User.Identity.Name))
            {
                return View("UnAuthorize");
            }

            return View("BP");
        }
        public ActionResult NumberSequence()
        {
            if (!_permissionFacade.IsAuth(PermissionResource.Module_Admin, PermissionResource.Type_ALL,
                HttpContext.User.Identity.Name))
            {
                return View("UnAuthorize");
            }

            return View("NumberSequence");
        }
        public ActionResult Employee()
        {
            if (!_permissionFacade.IsAuth(PermissionResource.Module_Admin, PermissionResource.Type_ALL,
                HttpContext.User.Identity.Name))
            {
                return View("UnAuthorize");
            }

            return View("EmployeeTable");
        }
        public ActionResult Division()
        {
            if (!_permissionFacade.IsAuth(PermissionResource.Module_Admin, PermissionResource.Type_ALL,
                HttpContext.User.Identity.Name))
            {
                return View("UnAuthorize");
            }

            return View("DivisionTable");
        }
        public ActionResult Department()
        {
            if (!_permissionFacade.IsAuth(PermissionResource.Module_Admin, PermissionResource.Type_ALL,
                HttpContext.User.Identity.Name))
            {
                return View("UnAuthorize");
            }

            return View("DepartmentTable");
        }
        public ActionResult DepartmentSetup()
        {
            if (!_permissionFacade.IsAuth(PermissionResource.Module_Admin, PermissionResource.Type_ALL,
                HttpContext.User.Identity.Name))
            {
                return View("UnAuthorize");
            }

            return View("DepartmentSetup");
        }
        public ActionResult Section()
        {
            if (!_permissionFacade.IsAuth(PermissionResource.Module_Admin, PermissionResource.Type_ALL,
                HttpContext.User.Identity.Name))
            {
                return View("UnAuthorize");
            }

            return View("SectionTable");
        }
        public ActionResult SubSection()
        {
            if (!_permissionFacade.IsAuth(PermissionResource.Module_Admin, PermissionResource.Type_ALL,
                HttpContext.User.Identity.Name))
            {
                return View("UnAuthorize");
            }

            return View("SubSectionTable");
        }
        public ActionResult Vendor()
        {
            if (!_permissionFacade.IsAuth(PermissionResource.Module_Admin, PermissionResource.Type_ALL,
                HttpContext.User.Identity.Name))
            {
                return View("UnAuthorize");
            }

            return View("VendorTable");
        }
        public ActionResult MaterialMaster()
        {
            if (!_permissionFacade.IsAuth(PermissionResource.Module_Admin, PermissionResource.Type_ALL,
                HttpContext.User.Identity.Name))
            {
                return View("UnAuthorize");
            }

            return View("MaterialTable");
        }
        public ActionResult BPAval()
        {
            if (!_permissionFacade.IsAuth(PermissionResource.Module_Admin, PermissionResource.Type_ALL,
                HttpContext.User.Identity.Name))
            {
                return View("UnAuthorize");
            }

            return View("BPAval");
        }
        public ActionResult Currency()
        {
            if (!_permissionFacade.IsAuth(PermissionResource.Module_Admin, PermissionResource.Type_ALL,
                HttpContext.User.Identity.Name))
            {
                return View("UnAuthorize");
            }

            return View("CurrencyTable");
        }
        public ActionResult ValidationAmountSetup()
        {
            if (!_permissionFacade.IsAuth(PermissionResource.Module_Admin, PermissionResource.Type_ALL,
                HttpContext.User.Identity.Name))
            {
                return View("UnAuthorize");
            }

            return View("ValidationAmountSetup");
        }

        public ActionResult PermissionTable()
        {
            if (!_permissionFacade.IsAuth(PermissionResource.Module_Admin, PermissionResource.Type_ALL,
                HttpContext.User.Identity.Name))
            {
                return View("UnAuthorize");
            }

            return View("EWorkflow_PermissionTable");
        }
        public ActionResult PermissionMember()
        {
            if (!_permissionFacade.IsAuth(PermissionResource.Module_Admin, PermissionResource.Type_ALL,
                HttpContext.User.Identity.Name))
            {
                return View("UnAuthorize");
            }

            return View("EWorkflow_PermissionMember");
        }
        public ActionResult EmployeeTable()
        {
            if (!_permissionFacade.IsAuth(PermissionResource.Module_Admin, PermissionResource.Type_ALL,
                HttpContext.User.Identity.Name))
            {
                return View("UnAuthorize");
            }

            return View("EWorkflow_EmployeeTable");
        }
        public ActionResult PrinterSetup()
        {
            if (!_permissionFacade.IsAuth(PermissionResource.Module_Admin, PermissionResource.Type_ALL,
                HttpContext.User.Identity.Name))
            {
                return View("UnAuthorize");
            }

            return View("EWorkflow_PrinterSetup");
        }
        public ActionResult ReceivedSetup()
        {
            if (!_permissionFacade.IsAuth(PermissionResource.Module_Admin, PermissionResource.Type_ALL,
                HttpContext.User.Identity.Name))
            {
                return View("UnAuthorize");
            }

            return View("ReceiptSetup");
        }

        public ActionResult AllAPPInquiriesMenuACC()
        {
            if (!_permissionFacade.IsAuth(PermissionResource.Module_AccAssign, PermissionResource.Type_ALL,
                HttpContext.User.Identity.Name))
            {
                return View("UnAuthorize");
            }

            return View("AllAPPInquiryACC");
        }
        public ActionResult AllPOInquiriesMenuACC()
        {
            if (!_permissionFacade.IsAuth(PermissionResource.Module_AccAssign, PermissionResource.Type_ALL,
                HttpContext.User.Identity.Name))
            {
                return View("UnAuthorize");
            }

            return View("AllPOInquiryFormACC");
        }
        public ActionResult AllAPPInquiriesMenuACCUpdate()
        {
            if (!_permissionFacade.IsAuth(PermissionResource.Module_AccAssignBP, PermissionResource.Type_ALL,
                HttpContext.User.Identity.Name))
            {
                return View("UnAuthorize");
            }

            return View("AllAPPInquiryACCUpdate");
        }
        public ActionResult AllPOInquiriesMenuACCUpdate()
        {
            if (!_permissionFacade.IsAuth(PermissionResource.Module_AccAssignBP, PermissionResource.Type_ALL,
                HttpContext.User.Identity.Name))
            {
                return View("UnAuthorize");
            }

            return View("AllPOInquiryFormACCUpdate");
        }
        public ActionResult AllGRInquiriesUnlock()
        {
            if (!_permissionFacade.IsAuth(PermissionResource.Module_UnlockGR, PermissionResource.Type_ALL,
                HttpContext.User.Identity.Name))
            {
                return View("UnAuthorize");
            }

            return View("AllGRInquiryUnlock");
        }



        //Request Sheets
        public ActionResult RequestSheet(long? id)
        {
            var facade = DependencyResolver.Current.GetService<PurchaseOrderFacade>();
            var prTable = facade.GetPRTable(id);

            if(prTable == null)
            {
                ParamViewModel.ParamId = 0;
                ParamViewModel.Activity = "RequestSheet/0";
                return View("RequestSheet");
            }
            else
            {
                ParamViewModel.ParamId = id;
                ParamViewModel.Activity = "RequestSheet/" + prTable.CurrentStep.ToString();
                return View("RequestSheet");
            }
        }

        public ActionResult RequestSheetInquiry()
        {
            return View("RequestSheetInquiry");
        }

        public ActionResult Distributor()
        {
            return View("Distributor");
        }

        public ActionResult DraftPurchaseOrder()
        {
            return View("DraftPurchaseOrder");
        }

        public ActionResult DraftApplication()
        {
            return View("DraftApplication");
        }
    }
}