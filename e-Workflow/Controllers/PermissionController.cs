﻿using System;
using System.Configuration;
using System.Linq;
using System.Web.Mvc;
using Antlr.Runtime.Misc;
using Kendo.Mvc.Extensions;
using Kendo.Mvc.UI;
using PanasonicProject.Facades;
using PanasonicProject.Resource;
using PanasonicProject.ViewModels.Report;

namespace PanasonicProject.Controllers
{
    public class PermissionController : PanasonicProject.Controllers.Abtracts.BaseController
    {
        private readonly PermissionFacade _facade;

        public PermissionController(PermissionFacade facade)
        {
            _facade = facade;
        }

        public ActionResult Index()
        {
            return View("Permission");
        }


        public ActionResult GetPermissionTable([DataSourceRequest]DataSourceRequest request)
        {
            try
            {
                var models = _facade.GetPermissionTable();
                return Json(models.ToDataSourceResult(request), JsonRequestBehavior.AllowGet);
            }
            catch (Exception exception)
            {
                throw new Exception(exception.Message);
            }
        }
    }
}