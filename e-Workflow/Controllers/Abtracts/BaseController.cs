﻿using System;
using System.Collections.Generic;
using System.Data.Entity.Validation;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Mvc;
using CrossingSoft.MVC.Infrastructure.Notification;
using iTextSharp.text;
using iTextSharp.text.pdf;
using iTextSharp.text.pdf.qrcode;
using Microsoft.Reporting.WebForms;
using PanasonicProject.Models;
using PanasonicProject.Services.Abstracts;
using PanasonicProject.ViewModels.Abstracts;

namespace PanasonicProject.Controllers.Abtracts
{
    public abstract class BaseController : Controller
    {

        protected ActionResult HandleException(Exception exception, string errorMessage = null)
        {
            if (Request.IsAjaxRequest())
            {
                return HandleExceptionPartial(exception, errorMessage);
            }

            this.ShowMessage(MessageType.Danger, FormatMessage(exception, errorMessage), true);
            return View("Error");

        }

        protected PartialViewResult HandleExceptionPartial(Exception exception, string errorMessage = null)
        {
            this.ShowMessage(MessageType.Danger, FormatMessage(exception, errorMessage), true);
            return PartialView("_Error");
        }

        protected static string FormatMessage(Exception exception, string errorMessage)
        {

            var msg = new StringBuilder();
            if (!string.IsNullOrEmpty(errorMessage))
                msg.AppendLine(errorMessage);
            if (!string.IsNullOrEmpty(exception.Message))
                msg.Append(exception.Message);
            msg.Replace(Environment.NewLine, "<br/>");

            return msg.ToString();
        }
        protected string HandleDbEntityValidationException(DbEntityValidationException dbEx, string separator = "<br/>")
        {
            var list = new List<string>();
            foreach (var validationErrors in dbEx.EntityValidationErrors)
            {
                foreach (var validationError in validationErrors.ValidationErrors)
                {
                    ModelState.AddModelError(string.Empty, validationError.ErrorMessage);
                    list.Add(validationError.ErrorMessage);
                }
            }
            return String.Join(separator, list);
        }

        protected ActionResult Report(string reportName, HttpRequestBase httpRequestBase, ReportDataSource[] reportDataSources, string fileNameStr, string format = "PDF", int page = 0)
        {
            if (format == null || format.Equals("web", StringComparison.InvariantCultureIgnoreCase))
            {
                var url = string.Format(@"~/Reports/{0}.aspx", reportName);
                if (httpRequestBase.RequestContext.RouteData.Values.ContainsKey("id"))
                {
                    var id = httpRequestBase.RequestContext.RouteData.Values["id"];
                    url += string.Format(@"?{0}={1}", "id", id);
                }



                var allKey = httpRequestBase.QueryString.AllKeys;
                if (allKey.Any())
                {
                    url += "?";
                }

                for (int i = 0; i < httpRequestBase.QueryString.Count; i++)
                {
                    url += string.Format("{0}={1}", allKey[i], httpRequestBase.QueryString[i]);
                    if (i != httpRequestBase.QueryString.Count - 1)
                        url += "&";
                }
                Response.Redirect(url);
                return null;
            }
            else
            {

                var zipping = false;

                var localReport = new LocalReport();
                localReport.ReportPath = Server.MapPath(string.Format(@"~/Reports/{0}.rdlc", reportName));

                foreach (var reportDataSource in reportDataSources)
                {
                    localReport.DataSources.Add(reportDataSource);
                }

                string reportType;
                string outputFormat;
                if (format.Equals("jpeg", StringComparison.InvariantCultureIgnoreCase))
                {
                    reportType = "image";
                    outputFormat = "jpeg";
                    if (page == 0)
                    {
                        zipping = true;
                    }
                }
                else if (format.Equals("png", StringComparison.InvariantCultureIgnoreCase))
                {
                    reportType = "image";
                    outputFormat = "png";
                    if (page == 0)
                    {
                        zipping = true;
                    }
                }
                else if (format.Equals("tiff", StringComparison.InvariantCultureIgnoreCase))
                {
                    reportType = "image";
                    outputFormat = "tiff";
                    page = 0;
                }
                else if (format.Equals("excel", StringComparison.InvariantCultureIgnoreCase))
                {
                    reportType = "Excel";
                    outputFormat = "xls";
                    page = 0;
                }
                else if (format.Equals("word", StringComparison.InvariantCultureIgnoreCase))
                {
                    reportType = "Word";
                    outputFormat = "doc";
                    page = 0;
                }
                else
                {
                    reportType = "PDF";
                    outputFormat = "PDF";
                    page = 0;
                }
                string mimeType = string.Empty;
                string encoding;
                string fileNameExtension = null;
                if (zipping == false)
                {
                    string deviceInfo = string.Format(
                        "<DeviceInfo>" +
                        "  <OutputFormat>{0}</OutputFormat>" +
                        "  <StartPage>{1}</StartPage>" +
                        "</DeviceInfo>", outputFormat, page);

                    Warning[] warnings;
                    string[] streams;
                    byte[] renderedBytes;

                    //Render the report
                    renderedBytes = localReport.Render(
                        reportType,
                        deviceInfo,
                        out mimeType,
                        out encoding,
                        out fileNameExtension,
                        out streams,
                        out warnings);


                    string fileName = string.Format("{0}.{1}", fileNameStr,
                                                    fileNameExtension);
                    Response.AppendHeader("Content-Disposition", string.Format("inline; filename={0}", fileName));
                    return File(renderedBytes, mimeType);
                }
                else
                {
                    var files = new List<byte[]>();
                    for (int i = 1; i <= 99; i++)
                    {
                        string deviceInfo = string.Format(
                        "<DeviceInfo>" +
                        "  <OutputFormat>{0}</OutputFormat>" +
                        "  <StartPage>{1}</StartPage>" +
                        "</DeviceInfo>", outputFormat, i);

                        Warning[] warnings;
                        string[] streams;
                        byte[] renderedBytes;

                        //Render the report
                        renderedBytes = localReport.Render(
                            reportType,
                            deviceInfo,
                            out mimeType,
                            out encoding,
                            out fileNameExtension,
                            out streams,
                            out warnings);
                        if (renderedBytes.Length == 0)
                            break;

                        files.Add(renderedBytes);
                    }
                    if (files.Any())
                    {
                        if (files.Count == 1)
                        {
                            string fileName = string.Format("{0}.{1}", fileNameStr,
                                                    fileNameExtension);
                            Response.AppendHeader("Content-Disposition",
                                                          string.Format("inline; filename={0}", fileName));
                            return File(files[0], mimeType);
                        }
                        //else
                        //{
                        //    using (var zip = new Ionic.Zip.ZipFile())
                        //    {
                        //        var guid = Guid.NewGuid();

                        //        if (!System.IO.Directory.Exists(Upload.UploadHandler.TemporaryStorageRoot))
                        //            System.IO.Directory.CreateDirectory(Upload.UploadHandler.TemporaryStorageRoot);

                        //        var filePath = UploadHandler.TemporaryStorageRoot + guid + "/";

                        //        if (!System.IO.Directory.Exists(filePath))
                        //            System.IO.Directory.CreateDirectory(filePath);

                        //        int i = 1;
                        //        foreach (var file in files)
                        //        {
                        //            string fileName = string.Format("{0}_{1}.{2}", fileNameStr, i, outputFormat);
                        //            System.IO.File.WriteAllBytes(filePath + fileName, file);

                        //            if (System.IO.File.Exists(filePath + fileName))
                        //            {
                        //                zip.AddFile(filePath + fileName, string.Empty);
                        //            }
                        //            i++;
                        //        }
                        //        string zipName = string.Format("{0}.zip", fileNameStr);
                        //        zip.Save(filePath + zipName);
                        //        if (!System.IO.Directory.Exists(filePath + zipName))
                        //        {
                        //            Response.AppendHeader("Content-Disposition",
                        //                                  string.Format("attachment; filename={0}", zipName));
                        //            return File(filePath + zipName, "application/octet-stream");
                        //        }
                        //    }
                        //}
                    }
                    return null;
                }
            }
        }

        protected ActionResult Report(string reportName, HttpRequestBase httpRequestBase, ReportDataSource reportDataSource, string fileNameStr, string format = "PDF", int page = 0)
        {
            return Report(reportName, httpRequestBase,
                          new[] { reportDataSource }, fileNameStr,
                          format, page);

        }

        //private void Print(byte[] renderedBytes)
        //{
        //    //const string printerName =
        //    //   "Microsoft Office Document Image Writer";
        //    const string printerName =
        //       "Microsoft XPS Document Writer";
        //    var printDoc = new PrintDocument();
        //    printDoc.PrinterSettings.PrinterName = printerName;
        //    if (!printDoc.PrinterSettings.IsValid)
        //    {
        //        string msg = String.Format(
        //           "Can't find printer \"{0}\".", printerName);
        //        throw new Exception(string.Format("Print error: {0}", msg));
        //    }
        //    //printDoc.PrintPage += new PrintPageEventHandler(PrintPage);

        //    printDoc.PrintPage += (sender, args) => PrintPage(sender, args, renderedBytes);
        //    printDoc.Print();
        //}
        //// Handler for PrintPageEvents
        //private void PrintPage(object sender, PrintPageEventArgs ev, byte[] renderedBytes)
        //{
        //    MemoryStream m_streams = new MemoryStream(renderedBytes);
        //    for (int i = 0; i < 1; i++)
        //    {
        //        Metafile pageImage = new
        //            Metafile(m_streams);
        //        ev.Graphics.DrawImage(pageImage, ev.PageBounds);
        //        ev.HasMorePages = (false);
        //    }

        //}

        //protected void Excel<TReport>(string excelName, IEnumerable<TReport> data) where TReport : class, new()
        //{
        //    string attachment = "attachment; filename=" + excelName + ".csv";
        //    Response.ClearContent();
        //    Response.AddHeader("content-disposition", attachment);
        //    Response.Charset = Encoding.UTF8.ToString();
        //    Response.ContentType = "application/vnd.ms-excel";
        //    Response.ContentEncoding = Encoding.UTF8;
        //    Response.BinaryWrite(System.Text.Encoding.UTF8.GetPreamble());
        //    string tab = string.Empty;
        //    var type = typeof(TReport);
        //    foreach (var t in type.GetProperties())
        //    {
        //        Response.Write(tab + t.Name);
        //        tab = ",";
        //    }
        //    Response.Write("\n");

        //    foreach (var row in data)
        //    {
        //        tab = string.Empty;
        //        foreach (var property in row.GetType().GetProperties())
        //        {
        //            Response.Write(tab + property.GetValue(row, null));
        //            tab = ",";
        //        }
        //        Response.Write("\n");
        //    }
        //    Response.End();
        //}
        //protected static string FormatMessage(Exception exception, string errorMessage)
        //{

        //    var msg = new StringBuilder();
        //    if (!string.IsNullOrEmpty(errorMessage))
        //        msg.AppendLine(errorMessage);
        //    if (!string.IsNullOrEmpty(exception.Message))
        //        msg.Append(exception.Message);
        //    msg.Replace(Environment.NewLine, "<br/>");

        //    return msg.ToString();
        //}
        //protected ActionResult HandleException(Exception exception, string errorMessage = null)
        //{
        //    if (Request.IsAjaxRequest())
        //    {
        //        return HandleExceptionPartial(exception, errorMessage);
        //    }

        //    this.ShowMessage(MessageType.Danger, FormatMessage(exception, errorMessage), true);
        //    return View("Error");

        //}

        //protected PartialViewResult HandleExceptionPartial(Exception exception, string errorMessage = null)
        //{
        //    this.ShowMessage(MessageType.Danger, FormatMessage(exception, errorMessage), true);
        //    return PartialView("_Error");
        //}

        public void CreatePDF(byte[] bytes,string fileName)
        {
            Response.Clear();
            Response.ContentType = "application/pdf";
            Response.AppendHeader("Content-Disposition", "inline;filename=" + fileName + ".pdf");
            Response.BufferOutput = true;
            Response.BinaryWrite(bytes);
            Response.End();
        }

        public BaseSmartFormViewModel InitBaseSmartFormViewModel(WorklistItem worklistItem)
        {
            BaseSmartFormViewModel model = new BaseSmartFormViewModel();
            model.Url = worklistItem.Data;
            model.DescProcessName = worklistItem.DescProcessName;
            model.ActivityName = worklistItem.ActivityName;

            var data = worklistItem.Data;
            return model;
        }

        public void MultiFilePdf(List<byte[]> filesBytes,string filename)
        {//Stamp
            if (filesBytes.Any())
            {                
                MemoryStream ms = new MemoryStream();
                Document pdfDoc = new Document();
                
                PdfWriter writer = PdfWriter.GetInstance(pdfDoc, ms);
                pdfDoc.Open();
                pdfDoc.SetPageSize(PageSize.A4);
                PdfContentByte cb = writer.DirectContent;
                PdfImportedPage page;
                PdfReader reader;
                foreach (byte[] p in filesBytes)
                {
                    reader = new PdfReader(p);
                    int pages = reader.NumberOfPages;

                    // loop over document pages
                    for (int i = 1; i <= pages; i++)
                    {
                        pdfDoc.SetPageSize(PageSize.A4);
                        pdfDoc.NewPage();
                        page = writer.GetImportedPage(reader, i);
                        cb.AddTemplate(page, 0, 0);
                    }
                }
                pdfDoc.Close();
                CreatePDF(ms.ToArray(), filename);   
            }
        }
    }

    public abstract class BaseController<T> : CrossingSoft.MVC.Controllers.BaseController<T> where T : class,new()
    {
        protected BaseController(BaseService<T> service)
            : base(service)
        {
        }
        protected BaseController(int defaultPageSize, BaseService<T> service)
            : base(defaultPageSize, service)
        {
        }

        

        protected string HandleDbEntityValidationException(DbEntityValidationException dbEx, string separator = "<br/>")
        {
            var list = new List<string>();
            foreach (var validationErrors in dbEx.EntityValidationErrors)
            {
                foreach (var validationError in validationErrors.ValidationErrors)
                {
                    ModelState.AddModelError(string.Empty, validationError.ErrorMessage);
                    list.Add(validationError.ErrorMessage);
                }
            }
            return String.Join(separator, list);
        }

        protected ActionResult Report(string reportName, HttpRequestBase httpRequestBase, ReportDataSource[] reportDataSources, string fileNameStr, string format = "PDF", int page = 0)
        {
            if (format == null || format.Equals("web", StringComparison.InvariantCultureIgnoreCase))
            {
                var url = string.Format(@"~/Reports/{0}.aspx", reportName);
                if (httpRequestBase.RequestContext.RouteData.Values.ContainsKey("id"))
                {
                    var id = httpRequestBase.RequestContext.RouteData.Values["id"];
                    url += string.Format(@"?{0}={1}", "id", id);
                }



                var allKey = httpRequestBase.QueryString.AllKeys;
                if (allKey.Any())
                {
                    url += "?";
                }

                for (int i = 0; i < httpRequestBase.QueryString.Count; i++)
                {
                    url += string.Format("{0}={1}", allKey[i], httpRequestBase.QueryString[i]);
                    if (i != httpRequestBase.QueryString.Count - 1)
                        url += "&";
                }
                Response.Redirect(url);
                return null;
            }
            else
            {

                var zipping = false;

                var localReport = new LocalReport();
                localReport.ReportPath = Server.MapPath(string.Format(@"~/Reports/{0}.rdlc", reportName));

                foreach (var reportDataSource in reportDataSources)
                {
                    localReport.DataSources.Add(reportDataSource);
                }

                string reportType;
                string outputFormat;
                if (format.Equals("jpeg", StringComparison.InvariantCultureIgnoreCase))
                {
                    reportType = "image";
                    outputFormat = "jpeg";
                    if (page == 0)
                    {
                        zipping = true;
                    }
                }
                else if (format.Equals("png", StringComparison.InvariantCultureIgnoreCase))
                {
                    reportType = "image";
                    outputFormat = "png";
                    if (page == 0)
                    {
                        zipping = true;
                    }
                }
                else if (format.Equals("tiff", StringComparison.InvariantCultureIgnoreCase))
                {
                    reportType = "image";
                    outputFormat = "tiff";
                    page = 0;
                }
                else if (format.Equals("excel", StringComparison.InvariantCultureIgnoreCase))
                {
                    reportType = "Excel";
                    outputFormat = "xls";
                    page = 0;
                }
                else
                {
                    reportType = "PDF";
                    outputFormat = "PDF";
                    page = 0;
                }
                string mimeType = string.Empty;
                string encoding;
                string fileNameExtension = null;
                if (zipping == false)
                {
                    string deviceInfo = string.Format(
                        "<DeviceInfo>" +
                        "  <OutputFormat>{0}</OutputFormat>" +
                        "  <StartPage>{1}</StartPage>" +
                        "</DeviceInfo>", outputFormat, page);

                    Warning[] warnings;
                    string[] streams;
                    byte[] renderedBytes;

                    //Render the report
                    renderedBytes = localReport.Render(
                        reportType,
                        deviceInfo,
                        out mimeType,
                        out encoding,
                        out fileNameExtension,
                        out streams,
                        out warnings);


                    string fileName = string.Format("{0}.{1}", fileNameStr,
                                                    fileNameExtension);
                    Response.AppendHeader("Content-Disposition", string.Format("inline; filename={0}", fileName));
                    return File(renderedBytes, mimeType);
                }
                else
                {
                    var files = new List<byte[]>();
                    for (int i = 1; i <= 99; i++)
                    {
                        string deviceInfo = string.Format(
                        "<DeviceInfo>" +
                        "  <OutputFormat>{0}</OutputFormat>" +
                        "  <StartPage>{1}</StartPage>" +
                        "</DeviceInfo>", outputFormat, i);

                        Warning[] warnings;
                        string[] streams;
                        byte[] renderedBytes;

                        //Render the report
                        renderedBytes = localReport.Render(
                            reportType,
                            deviceInfo,
                            out mimeType,
                            out encoding,
                            out fileNameExtension,
                            out streams,
                            out warnings);
                        if (renderedBytes.Length == 0)
                            break;

                        files.Add(renderedBytes);
                    }
                    if (files.Any())
                    {
                        if (files.Count == 1)
                        {
                            string fileName = string.Format("{0}.{1}", fileNameStr,
                                                    fileNameExtension);
                            Response.AppendHeader("Content-Disposition",
                                                          string.Format("inline; filename={0}", fileName));
                            return File(files[0], mimeType);
                        }
                        //else
                        //{
                        //    using (var zip = new Ionic.Zip.ZipFile())
                        //    {
                        //        var guid = Guid.NewGuid();

                        //        if (!System.IO.Directory.Exists(Upload.UploadHandler.TemporaryStorageRoot))
                        //            System.IO.Directory.CreateDirectory(Upload.UploadHandler.TemporaryStorageRoot);

                        //        var filePath = UploadHandler.TemporaryStorageRoot + guid + "/";

                        //        if (!System.IO.Directory.Exists(filePath))
                        //            System.IO.Directory.CreateDirectory(filePath);

                        //        int i = 1;
                        //        foreach (var file in files)
                        //        {
                        //            string fileName = string.Format("{0}_{1}.{2}", fileNameStr, i, outputFormat);
                        //            System.IO.File.WriteAllBytes(filePath + fileName, file);

                        //            if (System.IO.File.Exists(filePath + fileName))
                        //            {
                        //                zip.AddFile(filePath + fileName, string.Empty);
                        //            }
                        //            i++;
                        //        }
                        //        string zipName = string.Format("{0}.zip", fileNameStr);
                        //        zip.Save(filePath + zipName);
                        //        if (!System.IO.Directory.Exists(filePath + zipName))
                        //        {
                        //            Response.AppendHeader("Content-Disposition",
                        //                                  string.Format("attachment; filename={0}", zipName));
                        //            return File(filePath + zipName, "application/octet-stream");
                        //        }
                        //    }
                        //}
                    }
                    return null;
                }
            }
        }

        protected ActionResult Report(string reportName, HttpRequestBase httpRequestBase, ReportDataSource reportDataSource, string fileNameStr, string format = "PDF", int page = 0)
        {
            return Report(reportName, httpRequestBase,
                          new[] { reportDataSource }, fileNameStr,
                          format, page);

        }
    }
}