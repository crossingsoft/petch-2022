﻿using PanasonicProject.Facades;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Services;

namespace PanasonicProject.WebServices
{
    /// <summary>
    /// Summary description for RequestSheetWebService
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    // [System.Web.Script.Services.ScriptService]
    public class RequestSheetWebService : System.Web.Services.WebService
    {

        [WebMethod]
        public string ConvertRequestSheetToPurchOrder(int id)
        {
            var facade = DependencyResolver.Current.GetService<PurchaseOrderFacade>();
            return facade.ConvertRequestSheetToPurchOrder(id);
        }

        [WebMethod]
        public string ConvertRequestSheetToApplication(string id, string sectionId, string departmentId, string username, string divisionId, string budgetType)
        {
            var facade = DependencyResolver.Current.GetService<PurchaseOrderFacade>();
            return facade.ConvertRequestSheetToApplication(id,sectionId,departmentId,username,divisionId,budgetType);
        }

        [WebMethod]
        public bool DeletePOLineWithPR(int id)
        {
            var facade = DependencyResolver.Current.GetService<PurchaseOrderFacade>();
            return facade.DeletePOLineWithPR(id);
        }

        [WebMethod]
        public bool DeletePOTableWithPR(int id)
        {
            var facade = DependencyResolver.Current.GetService<PurchaseOrderFacade>();
            return facade.DeletePOTableWithPR(id);
        }
    }
}
