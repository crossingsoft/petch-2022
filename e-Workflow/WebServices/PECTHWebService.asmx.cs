﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.Entity.Infrastructure;
using System.Data.OleDb;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Services;
using PanasonicProject.Helpers;
using PanasonicProject.Models;
using PanasonicProject.Services;
using PanasonicProject.States;
using PanasonicProject.ViewModels;
using PanasonicProject.Facades;
using SourceCode.Workflow.Client;

namespace PanasonicProject.WebServices
{
    /// <summary>
    /// Summary description for PECTHWebService
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    // [System.Web.Script.Services.ScriptService]
    public class PECTHWebService : BaseWebService
    {
        [WebMethod]
        public string GetNewNumberDoc(string module)
        {
            var numberSequenceTableService = DependencyResolver.Current.GetService<NumberSequenceTableService>();
            return numberSequenceTableService.GetNewNumberDoc(module);
        }
        [WebMethod]
        public string GetNewNumberAcc(string module)
        {
            var numberSequenceTableService = DependencyResolver.Current.GetService<NumberSequenceTableService>();
            return numberSequenceTableService.GetNewNumberAcc(module);
        }
        [WebMethod]
        public void SetLineNumberPurch(long purchTableId)
        {
            var purchLineService = DependencyResolver.Current.GetService<PurchLineService>();
            purchLineService.SetLineNumber(purchTableId);
        }

        [WebMethod]
        public void SetLineNumberGR(long grTableId)
        {
            var grnLineService = DependencyResolver.Current.GetService<GRNLineService>();
            grnLineService.SetLineNumber(grTableId);
        }

        [WebMethod]
        public Validate DuplicatePOLine(long purchTableId, long grTableId)
        {
            var grnLineService = DependencyResolver.Current.GetService<GRNFacade>();
            return grnLineService.DuplicatePOLine(purchTableId, grTableId);
        }
        [WebMethod]
        public Validate DuplicatePOLineByVend(long purchTableId, long grTableId, string vendorname)
        {
            var grnLineService = DependencyResolver.Current.GetService<GRNFacade>();
            return grnLineService.DuplicatePOLineByVend(purchTableId, grTableId, vendorname);
        }
        [WebMethod]
        public string GetWorkflowType(long purchTableId)
        {
            var purchLineService = DependencyResolver.Current.GetService<PurchLineService>();
            var poModel = purchLineService.GetpurchLineList(purchTableId).FirstOrDefault();
            string workflowType = null;
            if (poModel != null)
            {
                workflowType = poModel.WorkflowType;
            }
            return workflowType;
        }

        [WebMethod]
        public string GetAPPWorkflowType(long appTableId)
        {
            var applFormBudgetService = DependencyResolver.Current.GetService<ApplFormBudgetService>();
            var appModel = applFormBudgetService.GetAppBudgetLineByApp(appTableId).FirstOrDefault();
            string workflowType = null;
            if (appModel != null)
            {
                workflowType = appModel.WorkflowType;
            }
            return workflowType;
        }

        [WebMethod]
        public decimal? GetMaxAmount(string state, string workflowtype)
        {
            var approvalSteService = DependencyResolver.Current.GetService<ApprovalStepService>();
            return approvalSteService.GetMaxAmount(state, workflowtype);
        }

        [WebMethod]
        public decimal? GetBPRemain(string bpNumber)
        {
            var bpRemain = (decimal?)0.00;
            if (string.IsNullOrEmpty(bpNumber))
            {
                return bpRemain;
            }
            var businessPlanLineService = DependencyResolver.Current.GetService<BusinessPlanLineService>();
            var appBudgetState = DependencyResolver.Current.GetService<AppBudgetState>();

            var businessPlanLineModel = businessPlanLineService.GetBPLine(bpNumber);
            var bpCurrent = businessPlanLineModel.BPAmount;
            var bpUsed = (decimal?)0.00;

            bpUsed += appBudgetState.GetBPAmountApplFormBudget(bpNumber);
            bpUsed += appBudgetState.GetBPAmountPurchLine(bpNumber);

            bpRemain = bpCurrent - bpUsed;
            return bpRemain;
        }

        [WebMethod]
        public CheckBPViewModel GetBPRemainAndCheckBPAval(string bpNumber)
        {
            var viewmodel = new CheckBPViewModel();
            var bpRemain = (decimal?)0.00;
            var bpAval = (decimal?)0.00;
            if (string.IsNullOrEmpty(bpNumber))
            {
                viewmodel.BPRemain = bpRemain;
                viewmodel.BPAval = bpAval;
                return viewmodel;
            }
            var businessPlanLineService = DependencyResolver.Current.GetService<BusinessPlanLineService>();
            var appBudgetState = DependencyResolver.Current.GetService<AppBudgetState>();

            var businessPlanLineModel = businessPlanLineService.GetBPLine(bpNumber);
            var bpCurrent = businessPlanLineModel.BPAmount;
            var bpUsed = (decimal?)0.00;

            bpUsed += appBudgetState.GetBPAmountApplFormBudget(bpNumber);
            bpUsed += appBudgetState.GetBPAmountPurchLine(bpNumber);

            bpRemain = bpCurrent - bpUsed;
            viewmodel.BPRemain = bpRemain;
            var fisicalYearId = businessPlanLineModel.FisicalYearId;
            var sectionId = businessPlanLineModel.SectionId;
           // var resultBPAval = CheckBPAval(fisicalYearId, sectionId, businessPlanLineModel.BusinessPlanType);
           // viewmodel.BPAval = resultBPAval.AvalBPAmount;
            viewmodel.BPAval = bpCurrent - bpUsed;
            return viewmodel;
        }

        //[WebMethod]
        //public long CopyDocument(long Id, string module)
        //{
        //    return new long();
        //}
        [WebMethod]
        public List<SectionTable> GetSectionByUser(string userParam, string divisionParam)
        {
            const string notHasDivi = "X";
            var sectionModel = DependencyResolver.Current.GetService<SectionTableService>();
            var sectionSetupModel = DependencyResolver.Current.GetService<SectionSetupService>();

            var actSectionModel = sectionSetupModel.GetSectionSetupLineByUser(userParam);
            var divisionModel = sectionModel.GetSectionLineByDivision(divisionParam);
            var sectionViewModel = new List<SectionTable>();

            foreach (var acrline in actSectionModel)
            {
                var section = new SectionTable();
                SectionTable actSection;
                if (divisionParam == notHasDivi)
                {
                    actSection = sectionModel.GetSectionLine(acrline.SectionId);
                    section.SectionId = acrline.SectionId.ToString();
                    section.SectionName = actSection.SectionName;
                    section.Department = actSection.Department;
                    section.Division = actSection.Division;
                    section.Requester = actSection.Requester;
                    section.AccountingRev = actSection.AccountingRev;
                    section.Manager = actSection.Manager;
                    section.GM = actSection.GM;
                    section.AccountingGen = actSection.AccountingGen;
                    section.eWorkflow = actSection.eWorkflow;
                    sectionViewModel.Add(section);
                }
                else
                {
                    var sectionline = sectionModel.GetSectionLine(acrline.SectionId, divisionParam);
                    foreach (var line in sectionline)
                    {
                        section.SectionId = acrline.SectionId.ToString();
                        section.SectionName = line.SectionName;
                        section.Department = line.Department;
                        section.Division = line.Division;
                        section.Requester = line.Requester;
                        section.AccountingRev = line.AccountingRev;
                        section.Manager = line.Manager;
                        section.GM = line.GM;
                        section.AccountingGen = line.AccountingGen;
                        section.eWorkflow = line.eWorkflow;
                        sectionViewModel.Add(section);
                    }

                }
            }
            return sectionViewModel;
        }

        [WebMethod]
        public List<SectionTable> GetDepartmentByUser(string userParam, string divisionParam)
        {
            const string notHasDivi = "X";
            var sectionModel = DependencyResolver.Current.GetService<SectionTableService>();
            var departmentSetupModelService = DependencyResolver.Current.GetService<DepartmentSetupService>();
            var actDepartmentModel = departmentSetupModelService.GetDepartmentSetupLineByUser(userParam);


            var sectionViewModel = new List<SectionTable>();

            foreach (var acrline in actDepartmentModel)
            {

                List<SectionTable> actSectionDepart;
                if (divisionParam == notHasDivi)
                {
                    actSectionDepart = sectionModel.GetSectionLineByDepartment(acrline.DepartmentId).ToListSafe();
                    foreach (var sectionDepart in actSectionDepart)
                    {
                        var section = new SectionTable();
                        section.SectionId = sectionDepart.SectionId;
                        section.SectionName = sectionDepart.SectionName;
                        section.Department = sectionDepart.Department;
                        section.Division = sectionDepart.Division;
                        section.Requester = sectionDepart.Requester;
                        section.AccountingRev = sectionDepart.AccountingRev;
                        section.Manager = sectionDepart.Manager;
                        section.GM = sectionDepart.GM;
                        section.AccountingGen = sectionDepart.AccountingGen;
                        section.eWorkflow = sectionDepart.eWorkflow;
                        sectionViewModel.Add(section);
                    }
                }
                else
                {
                    var sectionline = sectionModel.GetSectionLine(acrline.DepartmentId, divisionParam);
                    foreach (var line in sectionline)
                    {
                        var section = new SectionTable();
                        section.SectionId = line.SectionId;
                        section.SectionName = line.SectionName;
                        section.Department = line.Department;
                        section.Division = line.Division;
                        section.Requester = line.Requester;
                        section.AccountingRev = line.AccountingRev;
                        section.Manager = line.Manager;
                        section.GM = line.GM;
                        section.AccountingGen = line.AccountingGen;
                        section.eWorkflow = line.eWorkflow;
                        sectionViewModel.Add(section);
                    }

                }
            }
            return sectionViewModel;
        }
        [WebMethod]
        public List<DepartmentTable> GetDepartment(string userParam, string divisionParam)
        {
            const string notHasDivi = "X";
            var departmentSetupModelService = DependencyResolver.Current.GetService<DepartmentSetupService>();
            var actDepartmentModel = departmentSetupModelService.GetDepartmentSetupLineByUser(userParam);
            var departmentTableService = DependencyResolver.Current.GetService<DepartmentTableService>();
            var departmentbyDivision = departmentTableService.GetDepartmentLineByDivision(divisionParam);
            var departmentTableViewModel = new List<DepartmentTable>();
            foreach (var acrline in actDepartmentModel)
            {

                var actDepartmentTable = new DepartmentTable();
                if (divisionParam == notHasDivi)
                {
                    var DepartmentTable = departmentTableService.GetDepartmentLine(acrline.DepartmentId);
                    actDepartmentTable.DepartmentId = DepartmentTable.DepartmentId;
                    actDepartmentTable.Name = DepartmentTable.Name;
                    actDepartmentTable.eWorkflow = DepartmentTable.eWorkflow;
                    departmentTableViewModel.Add(actDepartmentTable);
                }
                else
                {
                    var groupline = departmentTableService.GetDepartmentLine(acrline.DepartmentId, divisionParam);
                    foreach (var line in groupline)
                    {
                        actDepartmentTable.DepartmentId = line.DepartmentId;
                        actDepartmentTable.Name = line.Name;
                        actDepartmentTable.eWorkflow = line.eWorkflow;
                        departmentTableViewModel.Add(actDepartmentTable);
                    }
                }
            }
            return departmentTableViewModel;
        }
        [WebMethod]
        public bool AppWorkflow(long? id, string status, string documentstatus, string processInstance)
        {
            var appTableService = DependencyResolver.Current.GetService<ApplicationFormService>();
            return appTableService.UpdateWorkflowApp(id, status, documentstatus, processInstance);
        }

        [WebMethod]
        public string GenerateDocumentId(long id, string module)
        {
            const string format = "#####";
            var fileversion = "D" + module;
            var wcStart = format.IndexOf('#');
            var wcEnd = format.LastIndexOf('#');
            var wcLength = (wcEnd - wcStart) + 1;
            var nextNum = id;
            var paddedNum = nextNum.ToString(CultureInfo.InvariantCulture).PadLeft(wcLength, '0');
            var no = format.Replace("#".PadLeft(wcLength, '#'), paddedNum);
            return fileversion + no;
        }
        [WebMethod]
        public CheckBPViewModelstate GetTotalAval(long appid)
        {
            try
            {
                var service = DependencyResolver.Current.GetService<FormAppFacade>();
                return service.Gettotalaval(appid);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.ToString());
            }
        }
        [WebMethod]
        public BPAvalTable CheckBPAval(string paramFiscalyear, string paramSectionId, string paramBPtype)
        {
            var businessPlanLineService = DependencyResolver.Current.GetService<BusinessPlanLineService>();
            var applicationFormService = DependencyResolver.Current.GetService<ApplicationFormService>();
            var applFormBudgetService = DependencyResolver.Current.GetService<ApplFormBudgetService>();
            var purchTableService = DependencyResolver.Current.GetService<PurchTableService>();
            var purchLineService = DependencyResolver.Current.GetService<PurchLineService>();
            var bpModel = businessPlanLineService.GetBPLineByBPSection(paramFiscalyear, paramSectionId, paramBPtype);
            var bpavalmodel = new BPAvalTable();
            if (!bpModel.Any())
            {
                return new BPAvalTable();
            }
            var pomodel = purchTableService.GetAll(m => m.POStatus == "Approved" && m.ApplId == null);
            var polineComList = new List<PurchLine>();
            foreach (var po in pomodel)
            {
                var joinpoline = purchLineService.GetAll(m => m.PurchId == po.PurchId);
                foreach (var joinpo in joinpoline)
                {
                    polineComList.Add(joinpo);
                }
            }
            var poamount = (decimal)0.00;
            foreach (var bp in bpModel)
            {
                var polist = polineComList.Where(m => m.BPNum == bp.BusinessPlanId);
                foreach (var po in polist)
                {
                    if (po.LineAmount == null)
                    {
                        poamount += 0;
                    }
                    else
                    {
                        poamount += (decimal)po.LineAmount;
                    }
                }
            }

            var appmodel = applicationFormService.GetAll(m => m.Status == "Approved");
            var applineComList = new List<ApplFormBudgetTable>();
            foreach (var app in appmodel)
            {
                var joinappline = applFormBudgetService.GetAll(m => m.ApplId == app.ApplId);
                foreach (var joinapp in joinappline)
                {
                    applineComList.Add(joinapp);
                }
            }
            var appamount = (decimal)0.00;
            foreach (var bp in bpModel)
            {
                var applist = applineComList.Where(m => m.BPNum == bp.BusinessPlanId);
                foreach (var app in applist)
                {
                    if (app.BPReserve == null)
                    {
                        appamount += 0;
                    }
                    else
                    {
                        appamount += (decimal)app.BPReserve;
                    }
                }
            }

            var firstrow = bpModel.FirstOrDefault();
            bpavalmodel.FisicalYear = firstrow.FisicalYearId;
            bpavalmodel.Section = firstrow.SectionId;
            bpavalmodel.BPType = firstrow.BusinessPlanType;
            //bpavalmodel.BPAmount = firstrow.BPAmount;

            bpavalmodel.PO = poamount;
            bpavalmodel.Application = appamount;

            var bpamount = (decimal)0.00;

            foreach (var bp in bpModel)
            {
                if (bp.BPAmount == null)
                {
                    bpamount += 0;
                }
                else
                {
                    bpamount += (decimal)bp.BPAmount;
                }
            }
            bpavalmodel.BPAmount = bpamount;
            bpavalmodel.AvalBPAmount = bpamount - (poamount + appamount);
            return bpavalmodel;
        }

        [WebMethod]
        public BPAvalTable CheckBPAvalAvd(string paramFiscalyear, string paramSectionId, string paramBPtype)
        {
            var businessPlanLineService = DependencyResolver.Current.GetService<BusinessPlanLineService>();
            var applicationFormService = DependencyResolver.Current.GetService<ApplicationFormService>();
            var applFormBudgetService = DependencyResolver.Current.GetService<ApplFormBudgetService>();
            var purchTableService = DependencyResolver.Current.GetService<PurchTableService>();
            var purchLineService = DependencyResolver.Current.GetService<PurchLineService>();
            var bpModel = businessPlanLineService.GetBPLineByBPSection(paramFiscalyear, paramSectionId, paramBPtype);
            var bpavalmodel = new BPAvalTable();
            if (!bpModel.Any())
            {
                return new BPAvalTable();
            }


            var appBudgetState = DependencyResolver.Current.GetService<AppBudgetState>();
            decimal? poamount = null, appamount = null;
            foreach (var bpNumber in bpModel)
            {
                //var businessPlanLineModel = businessPlanLineService.GetBPLine(bpNumber.BusinessPlanId);

                appamount += appBudgetState.GetBPAmountApplFormBudget(bpNumber.BusinessPlanId);
                poamount += appBudgetState.GetBPAmountPurchLine(bpNumber.BusinessPlanId);
            }


            var firstrow = bpModel.FirstOrDefault();
            bpavalmodel.FisicalYear = firstrow.FisicalYearId;
            bpavalmodel.Section = firstrow.SectionId;
            bpavalmodel.BPType = firstrow.BusinessPlanType;
            //bpavalmodel.BPAmount = firstrow.BPAmount;

            bpavalmodel.PO = poamount;
            bpavalmodel.Application = appamount;

            var bpamount = (decimal)0.00;

            foreach (var bp in bpModel)
            {
                if (bp.BPAmount == null)
                {
                    bpamount += 0;
                }
                else
                {
                    bpamount += (decimal)bp.BPAmount;
                }
            }
            bpavalmodel.BPAmount = bpamount;
            bpavalmodel.AvalBPAmount = bpamount - (poamount + appamount);
            return bpavalmodel;
        }

        [WebMethod]
        public void GRNTableUpdate(long grnId)
        {
            var grnTableService = DependencyResolver.Current.GetService<GRNTableService>();
            grnTableService.UpdateGRNLine(grnId);
        }

        [WebMethod]
        public bool CreateHistory(long id, string action, DateTime currentdate, string comment, string actionby,
            string activity)
        {
            var applicationApprovalHistoryService =
                DependencyResolver.Current.GetService<ApplicationApprovalHistoryService>();
            return applicationApprovalHistoryService.RefPOCreate(id, action, currentdate, comment, actionby, activity);
        }

        [WebMethod]
        public bool CheckTableLine(long Id, string rowname)
        {
            var hasLine = false;
            var purchLineService = DependencyResolver.Current.GetService<PurchLineService>();
            var polineModel = purchLineService.GetpurchLineList(Id);
            if (polineModel.Any())
            {
                hasLine = true;
            }
            return hasLine;
        }

        [WebMethod]
        public Validate CancelPOLine(long Id)
        {
            //to do facade
            var purchTableService = DependencyResolver.Current.GetService<PurchTableService>();
            var grnTableService = DependencyResolver.Current.GetService<GRNTableService>();

            var isUsedPO = false;
            var result = new Validate();
            var polineModel = purchTableService.GetPOLine(Id);




            var grnModel = grnTableService.GetGRNLineByPO(Id);
            var blukNumber = new ArrayList();
            foreach (var grn in grnModel)
            {
                blukNumber.Add(grn.GRNExtId);
                if (grn.Status == "Completed")
                {
                    isUsedPO = true;
                    break;
                }
            }

            if (!isUsedPO)
            {
                polineModel.POStatus = "Cancelled";
                purchTableService.UpdatePOLine(polineModel);
                purchTableService.SaveChanges();
                result.Success = true;
                result.MessageError = String.Format("The Purchase Order number '{0}' cancelled sucessful.", polineModel.PurchNum);
            }
            else
            {
                result.Success = false;
                result.MessageError = String.Format("This Purchase order number '{0}' used from Goods Receipt.", polineModel.PurchNum);
            }
            return result;
        }
        [WebMethod]
        public Validate CancelAppLine(long Id)
        {
            //to do facade
            var appService = DependencyResolver.Current.GetService<ApplicationFormService>();
            var purchTableService = DependencyResolver.Current.GetService<PurchTableService>();

            var isUsedAPP = false;
            var result = new Validate();
            var applineModel = appService.GetAppLine(Id);
            var poModel = purchTableService.GetPOLineListByAppCompleted(Id);
            var blukNumber = new ArrayList();

            if (poModel.Any())
            {
                isUsedAPP = true;
                foreach (var po in poModel)
                {
                    blukNumber.Add(po.PurchNum);
                }
            }



            if (!isUsedAPP)
            {
                applineModel.Status = "Cancelled";
                appService.UpdateAppLine(applineModel);
                appService.SaveChanges();

                result.Success = true;
                result.MessageError = String.Format("The Purchase Order number '{0}' cancelled sucessful.", applineModel.ApplNo);
            }
            else
            {
                const string separate = ", ";
                var commaSeparated = (string)null;
                var index = 1;
                foreach (var Number in blukNumber)
                {
                    if (index < blukNumber.Count)
                    {
                        commaSeparated += Number + separate;
                    }
                    else
                    {
                        commaSeparated += Number;
                    }
                    index++;
                }
                result.Success = false;
                result.MessageError = String.Format("This Purchase order number '{0}' used from Goods Receipt number {1}.", applineModel.ApplNo, commaSeparated);
            }
            return result;
        }

        [WebMethod]
        public List<BusinessPlanAvailLine> GetAllBP()
        {
            var bpService = DependencyResolver.Current.GetService<BusinessPlanLineService>();
            var bpModel = bpService.GetAll();
            var bplineList = new List<BusinessPlanAvailLine>();
            foreach (var bp in bpModel)
            {
                var bpAvailable = GetBPRemain(bp.BusinessPlanId);
                var bpline = new BusinessPlanAvailLine
                {
                    BusinessPlanId = bp.BusinessPlanId,
                    BusinessPlanType = bp.BusinessPlanType,
                    FisicalYearId = bp.FisicalYearId,
                    SectionId = bp.SectionId,
                    RefNumber = bp.RefNumber,
                    Description = bp.BusinessPlanId,
                    Period = bp.Period,
                    BPAmount = bp.BPAmount,
                    BPAvailable = bpAvailable,
                    BPGroup = bp.BPGroup,
                    BPTYPE = bp.BPTYPE,
                    WorkflowType = bp.WorkflowType,
                    ACTIVE = bp.ACTIVE,
                    PeriodIDOperation = bp.PeriodIDOperation,
                    LifeYear = bp.LifeYear,
                    FixAssetPurposeID = bp.FixAssetPurposeID,
                    CreateUser = bp.CreateUser,
                    CreateDate = bp.CreateDate
                };

                bplineList.Add(bpline);
            }
            return bplineList;
        }

        [WebMethod]
        public long CopyPurchaseOrder(long primaryId)
        {
            var service = DependencyResolver.Current.GetService<PurchaseOrderFacade>();
            var documentnumber = GenerateDocumentId(primaryId, "P");
            return service.CopyPurchaseOrder(primaryId, documentnumber);
        }

        [WebMethod]
        public long? CopyApplication(long primaryId)
        {
            var service = DependencyResolver.Current.GetService<FormAppFacade>();
            var documentnumber = GenerateDocumentId(primaryId, "A");
            return service.CopyApplication(primaryId, documentnumber);
        }

        [WebMethod]
        public long? CopyGRN(long primaryId)
        {
            var service = DependencyResolver.Current.GetService<GRNFacade>();
            var documentnumber = GenerateDocumentId(primaryId, "G");
            return service.CopyGRN(primaryId, documentnumber);
        }

        [WebMethod]
        public Validate CheckGRNLine(long primaryId)
        {
            var service = DependencyResolver.Current.GetService<GRNFacade>();
            return service.CheckGRLine(primaryId);
        }

        [WebMethod]
        public GRNCheckOverBudget CheckOverBudget(long primaryId)
        {
            var service = DependencyResolver.Current.GetService<GRNFacade>();
            return service.CheckOverBudget(primaryId);
        }
        [WebMethod]
        public Validate CreateLocalGR(long primaryId)
        {
            try
            {
                var service = DependencyResolver.Current.GetService<GRNFacade>();
                var result = service.CreateGRTableToMsAccessAdvanceValidation(primaryId);

                if (result.Success)
                {
                    return InitValidate(true, "Create local success.");
                }
                else
                {
                    throw new Exception(result.MessageError);
                }

            }
            catch (Exception exception)
            {
                return InitValidate(false, exception.Message);
            }

        }
        [WebMethod]
        public Validate UpdateLocalGR(long primaryId)
        {
            try
            {
                var service = DependencyResolver.Current.GetService<GRNFacade>();
                var result = service.UpdateGRTableToMSAccess(primaryId);

                if (result.Success)
                {
                    return InitValidate(true, "Update local success.");
                }
                else
                {
                    throw new Exception(result.MessageError);
                }

            }
            catch (Exception exception)
            {
                return InitValidate(false, exception.Message);
            }

        }

        [WebMethod]
        public Validate CheckBPBeforeSubmit(long primaryId)
        {
            try
            {
                var service = DependencyResolver.Current.GetService<PurchaseOrderFacade>();
                return service.CheckBPBeforeSubmit(primaryId);
            }
            catch (Exception exception)
            {
                return InitValidate(false, exception.Message);
            }
        }
        [WebMethod]
        public Validate CheckPOBeforeSubmit(long primaryId)
        {
            try
            {
                var service = DependencyResolver.Current.GetService<PurchaseOrderFacade>();
                return service.CheckPOBeforeSubmit(primaryId);
            }
            catch (Exception exception)
            {
                return InitValidate(false, exception.Message);
            }
        }
        [WebMethod]
        public List<ValidationAmountSetup> GetValidationAmountSetup(string workflowtype)
        {
            try
            {
                var service = DependencyResolver.Current.GetService<PurchaseOrderFacade>();
                return service.GetValidationAmountSetup(workflowtype);
            }
            catch (Exception exception)
            {
                throw new Exception(exception.ToString());
            }

        }
        [WebMethod]
        public string GetMsgValidation(string workflowtype, decimal? totalamount, long? appid)
        {
            try
            {
                var service = DependencyResolver.Current.GetService<PurchaseOrderFacade>();
                return service.GetMsgValidation(workflowtype, totalamount, appid);
            }
            catch (Exception exception)
            {
                throw new Exception(exception.ToString());
            }
        }
        [WebMethod]
        public void UpdatePOBeforeSubmit(long primaryId)
        {
            try
            {
                var service = DependencyResolver.Current.GetService<PurchaseOrderFacade>();
                service.UpdatePOBeforeSubmit(primaryId);
            }
            catch (Exception exception)
            {
                throw new Exception(exception.ToString());
            }
        }
        [WebMethod]
        public List<ApplicationForm> RelatedApp(string username)
        {
            try
            {
                var service = DependencyResolver.Current.GetService<FormAppFacade>();
                return service.GetAppByUser(username);
            }
            catch (Exception exception)
            {
                throw new Exception(exception.ToString());
            }
        }
        [WebMethod]
        public List<PurchTable> RelatedPO(string username)
        {
            try
            {
                var service = DependencyResolver.Current.GetService<PurchaseOrderFacade>();
                return service.GetPOByUser(username);
            }
            catch (Exception exception)
            {
                throw new Exception(exception.ToString());
            }
        }
        [WebMethod]
        public List<PurchTable> TeamPO(string username)
        {
            try
            {
                var service = DependencyResolver.Current.GetService<PurchaseOrderFacade>();
                return service.GetPOByDepartmentUser(username);
            }
            catch (Exception exception)
            {
                throw new Exception(exception.ToString());
            }
        }
        [WebMethod]
        public List<ApplicationForm> TeamAPP(string username)
        {
            try
            {
                //f
                var service = DependencyResolver.Current.GetService<FormAppFacade>();
                return service.GetAppByDepartmentUser(username);
            }
            catch (Exception exception)
            {
                throw new Exception(exception.ToString());
            }
        }
        [WebMethod]
        public StateValidate StateValidateGetPOByUser(string username)
        {
            try
            {
                var service = DependencyResolver.Current.GetService<PurchaseOrderFacade>();
                return service.StateValidateGetPOByUser(username);
            }
            catch (Exception exception)
            {
                throw new Exception(exception.ToString());
            }
        }
        [WebMethod]
        public void GetReceived(long grnid)
        {
            try
            {
                var service = DependencyResolver.Current.GetService<GRNFacade>();
                service.GetReceived(grnid);
            }
            catch (Exception exception)
            {
                throw new Exception(exception.ToString());
            }
        }
        [WebMethod]
        public Validate GetReceivedValidate(long grnid)
        {
            try
            {
                var service = DependencyResolver.Current.GetService<GRNFacade>();
                return service.GetReceivedvalidate(grnid);
            }
            catch (Exception exception)
            {
                throw new Exception(exception.ToString());
            }
        }
        //[WebMethod]
        //public StateValidate GetReceived(string bpNumber)
        //{
        //    try
        //    {
        //        var service = DependencyResolver.Current.GetService<AppBudgetState>();
        //        return service.GetArrBPAmountApplFormBudget(bpNumber);
        //    }
        //    catch (Exception ex)
        //    {
        //        throw new Exception(ex.ToString());
        //    }
        //}
        [WebMethod]
        public Validate UpdateStatusClosed(long purchId)
        {
            try
            {
                var service = DependencyResolver.Current.GetService<PurchaseOrderFacade>();
                return service.UpdateStatusClosed(purchId);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.ToString());
            }
        }
        [WebMethod]
        public decimal? GetBPAmountApplFormBudget(string bpnumber)
        {
            try
            {
                var service = DependencyResolver.Current.GetService<AppBudgetState>();
                return service.GetBPAmountApplFormBudget(bpnumber);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.ToString());
            }
        }
        [WebMethod]
        public decimal? GetBPAmountPurchLine(string bpnumber)
        {
            try
            {
                var service = DependencyResolver.Current.GetService<AppBudgetState>();
                return service.GetBPAmountPurchLine(bpnumber);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.ToString());
            }
        }

        [WebMethod]
        public decimal? CheckPOAmount(long? id)
        {
            try
            {
                var service = DependencyResolver.Current.GetService<PurchaseOrderFacade>();
                return service.CheckPOAmount(id);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.ToString());
            }
        }
        [WebMethod]
        public List<PurchTable> AllowReceipt(string userid)
        {
            try
            {
                var service = DependencyResolver.Current.GetService<PurchaseOrderFacade>();
                return service.AllowReceiptByDepartment(userid);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.ToString());
            }
        }
        [WebMethod]
        public List<GRNTable> AllowUnlock(string userid)
        {
            try
            {
                var service = DependencyResolver.Current.GetService<GRNFacade>();
                return service.GetAllGR();
            }
            catch (Exception ex)
            {
                throw new Exception(ex.ToString());
            }
        }
        [WebMethod]
        public List<PurchTable> AllowReceiptByDepartment(string userid)
        {
            try
            {
                var service = DependencyResolver.Current.GetService<PurchaseOrderFacade>();
                return service.AllowReceiptByDepartment(userid);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.ToString());
            }
        }
        [WebMethod]
        public string GetDivisionByDepartment(string departmentid)
        {
            try
            {
                string department = null;
                var service = DependencyResolver.Current.GetService<DepartmentTableService>();
                var departmentModel = service.GetDepartmentLine(departmentid);
                if (departmentModel != null)
                    department = departmentModel.DivisionId;
                return department;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.ToString());
            }
        }
        [WebMethod]
        public List<ADUserTable> GetPersonInChargeByDepartment(string departmentid)
        {
            try
            {
                var service = DependencyResolver.Current.GetService<FormAppFacade>();
                return service.GetPersonInChargeByDepartment(departmentid);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.ToString());
            }
        }
        [WebMethod]
        public List<DepartmentSetup> GetPersonInChargeAll(string departmentid)
        {
            try
            {
                var service = DependencyResolver.Current.GetService<FormAppFacade>();
                return service.GetPersonInChargeByAll(departmentid).ToList();
            }
            catch (Exception ex)
            {
                throw new Exception(ex.ToString());
            }
        }
        [WebMethod]
        public Validate CloseAPP(long appid)
        {
            try
            {
                var service = DependencyResolver.Current.GetService<FormAppFacade>();
                return service.CloseApp(appid);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.ToString());
            }
        }
        [WebMethod]
        public List<string> GRNGetVendor(long appid)
        {
            try
            {
                var service = DependencyResolver.Current.GetService<GRNFacade>();
                return service.GRNGetVendor(appid);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.ToString());
            }
        }
        [WebMethod]
        public List<VendorMaster> GRNGetVendorByPO(long appid)
        {
            try
            {
                var service = DependencyResolver.Current.GetService<GRNFacade>();
                return service.GRNGetVendorByPO(appid);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.ToString());
            }
        }

        [WebMethod]
        public StateValidate ApplCheckDup(long appid)
        {
            var val = new StateValidate();
            var service = DependencyResolver.Current.GetService<FormAppFacade>();
            var appService = DependencyResolver.Current.GetService<ApplFormBudgetService>();
            var appBudgetline = appService.GetAppBudgetLineByApp(appid);
            var bpNumberLists = new List<ApplFormBudgetTable>();
            string msgbp = null;
            foreach (var dupbp in appBudgetline)
            {
                var checkbpl = bpNumberLists.Where(m => m.BPNum == dupbp.BPNum);
                if (checkbpl.Any())
                {
                    msgbp = string.Format("BP Number is duplicate");
                    val.MessageError = msgbp;
                    return val;
                }
                var bpNumber = new ApplFormBudgetTable();
                bpNumber.BPNum = dupbp.BPNum;
                bpNumberLists.Add(bpNumber);
            }
            return val;
        }

        [WebMethod]
        public StateValidate UpdateApplBudgetAndRemoveExplanation(long appid)
        {
            try
            {
                var val = new StateValidate();
                var service = DependencyResolver.Current.GetService<FormAppFacade>();
                var appService = DependencyResolver.Current.GetService<ApplFormBudgetService>();
                var appExp = service.UpdateApplBudgetAndRemoveExplanation(appid);
                var appBudgetline = appService.GetAppBudgetLineByApp(appid).ToListSafe();
                decimal firstRowBPAval = 0;
                 string currencyAval = string.Empty;
                val.StateName = appExp.MessageError;
                string msg = null;
                var savechange = true;
                var firstRowApp = appService.GetFirstOnly(m => m.ApplId == appid);
                string budgettype = "Expense";
                switch (firstRowApp.WorkflowType)
                {
                    case "FixAssets":
                        budgettype = "Investment";
                        break;
                    case "Repairing":
                        budgettype = "Expense";
                        break;
                    case "Indirect":
                        budgettype = "Expense";
                        break;
                    case "Others":
                        budgettype = "Expense";
                        break;
                }
                if (firstRowApp.BudgetType != "Others")
                {
                    foreach (var line in appBudgetline)
                    {
                        var bpViewModel = GetBPRemainAndCheckBPAval(line.BPNum);
                        if (bpViewModel != null)
                        {
                            if (line.BPReserve > bpViewModel.BPRemain)
                            {
                                firstRowBPAval = (decimal)bpViewModel.BPAval;
                                var bpRemain = (decimal)bpViewModel.BPRemain;
                                var bpAval = (decimal)bpViewModel.BPAval;
                                var bpReserve = (decimal)line.BPReserve;
                                var currency = bpRemain.ToString("n2");
                                currencyAval = firstRowBPAval.ToString("n2");
                                msg += string.Format("** BP RESERVED AMOUNT : {0} BAHT ** \r\n" +
                                    "** BP AVAILABLE AMOUNT : {1} BAHT ** \r\n" +
                                    "BP NUMBER {2} IS NOT ENOUGH \r\n\r\n"
                                    , bpReserve.ToString("n2")
                                    , currency
                                    , line.BPNum);
                                savechange = false;
                            }
                            line.BPAvailableAmount = bpViewModel.BPRemain;
                            appService.Update(line);
                        }
                    }
                }
                if (msg != null)
                {
                    var allReserve = appBudgetline.Sum(m => m.BPReserve);

                    if (allReserve > firstRowBPAval)
                    {
                        msg +=
                            string.Format(
                                "\r\nTOTAL AVAILABLE BP AMOUNT OF {0} IS NOT ENOUGH ({1} BAHT) \r\n \r\n" +
                                "PLEASE RECONSIDER !!!! \r\n\r\n", budgettype.ToUpper(), currencyAval);
                    }
                    else
                    {
                        msg +=
                            string.Format(
                                "\r\nTOTAL AVAILABLE BP AMOUNT OF {0} IS ENOUGH ({1} BAHT) \r\n \r\n" +
                                "PLEASE RECONSIDER !!!! \r\n\r\n", budgettype.ToUpper(), currencyAval);
                    }
                }
                appService.SaveChanges();

                val.MessageError = msg;
                return val;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.ToString());
            }
        }

        [WebMethod]
        public StateValidate AttachmentMeeting(long appid)
        {
            try
            {
                var service = DependencyResolver.Current.GetService<FormAppFacade>();
                return service.AttachmentMeeting(appid);
            }   
            catch (Exception ex)
            {
                throw new Exception(ex.ToString());
            }
        }

        [WebMethod]
        public StateValidate UpdatePOBudgetAndRemoveExplanation(long poid)
        {
            try
            {
                var val = new StateValidate();
                var polineService = DependencyResolver.Current.GetService<PurchLineService>();

                var poline = polineService.GetpurchLineList(poid).ToListSafe();
                string msg = null;
                var savechange = true;
                decimal firstRowBPAval = 0;
                string currencyAval = string.Empty;
                var poService = DependencyResolver.Current.GetService<PurchTableService>();
                var po = poService.Load(m => m.PurchId == poid);
                    string budgettype = "Expense";
                    switch (po.WorkflowType)
                    {
                        case "FixAssets":
                            budgettype = "Investment";
                            break;
                        case "Repairing":
                            budgettype = "Expense";
                            break;
                        case "Indirect":
                            budgettype = "Expense";
                            break;
                        case "Others":
                            budgettype = "Expense";
                            break;
                    }
                if (poline.Any())
                {
                    foreach (var line in poline)
                    {
                        var bpViewModel = GetBPRemainAndCheckBPAval(line.BPNum);
                        if (bpViewModel != null)
                        {
                            if (line.LineAmountTH > bpViewModel.BPRemain)
                            {
                                firstRowBPAval = (decimal)bpViewModel.BPAval;
                                var bpRemain = (decimal)bpViewModel.BPRemain;
                                var bpAval = (decimal)bpViewModel.BPAval;
                                var bpReserve = (decimal)line.LineAmountTH;
                                var currency = bpRemain.ToString("n2");
                                currencyAval = firstRowBPAval.ToString("n2");
                                msg += string.Format("** BP RESERVED AMOUNT : {0} BAHT ** \r\n" +
                                    "** BP AVAILABLE AMOUNT : {1} BAHT ** \r\n" +
                                    "BP NUMBER {2} IS NOT ENOUGH \r\n\r\n"
                                    , bpReserve.ToString("n2")
                                    , currency
                                    , line.BPNum);
                                savechange = false;
                            }
                            line.BPAvailable = bpViewModel.BPRemain;
                            polineService.Update(line);
                        }
                    }
                    if (msg != null)
                    {
                        var allReserve = poline.Sum(m => m.LineAmountTH);
                        if (allReserve > firstRowBPAval)
                        {
                            msg +=
                                string.Format(
                                    "\r\nTOTAL AVAILABLE BP AMOUNT OF {0} IS NOT ENOUGH ({1} BAHT) \r\n \r\n" +
                                    "PLEASE RECONSIDER !!!! \r\n", budgettype.ToUpper(), currencyAval);
                        }
                        else
                        {
                            msg +=
                                string.Format(
                                    "\r\nTOTAL AVAILABLE BP AMOUNT OF {0} IS ENOUGH ({1} BAHT) \r\n \r\n" +
                                    "PLEASE RECONSIDER !!!! \r\n", budgettype.ToUpper(), currencyAval);
                        }
                    }
                }
                polineService.SaveChanges();

                val.MessageError = msg;
                return val;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.ToString());
            }
        }

        [WebMethod]
        public void DeletedList()
        {
            var service = DependencyResolver.Current.GetService<GRNFacade>();
            service.DeletedList();
        }

        [WebMethod]
        public StateValidate CheckPOOnProcess(long poid)
        {
            var service = DependencyResolver.Current.GetService<PurchaseOrderFacade>();
           return  service.CheckPOOnProcess(poid);
        }

        [WebMethod]
        public void UpdateBPActualAvailable()
        {
            var service = DependencyResolver.Current.GetService<FormAppFacade>();
            service.UpdateBPTable();
        }
    }
}
