﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using PanasonicProject.Models;

namespace PanasonicProject.WebServices
{
    public class BaseWebService : System.Web.Services.WebService
    {
        public Validate InitValidate(bool success, string message)
        {
            var validate = new Validate();
            validate.Success = success;
            validate.MessageError = message;
            return validate;
        }
    }
}