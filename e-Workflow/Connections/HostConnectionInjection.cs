﻿using System;
using System.Web.Configuration;
using CrossingSoft.MVC.Infrastructure.K2;

namespace PanasonicProject.Connections
{
    public class HostConnectionInjection
    {
        public static HostConnection Inject()
        {
            return new HostConnection(
                WebConfigurationManager.AppSettings["K2Server"],
               Convert.ToInt32(WebConfigurationManager.AppSettings["K2HostServerPort"]),
                WebConfigurationManager.AppSettings["SecurityLabelName"],
                 WebConfigurationManager.AppSettings["K2AccountDomainName"],
                WebConfigurationManager.AppSettings["K2AccountUserName"],
                WebConfigurationManager.AppSettings["K2AccountPassword"]
                );

            //return new HostConnection(
            //     "localhost",
            //    5555,
            //    "K2",
            //    "K2",
            //    "Administrator",
            //    "P@ssw0rd"
            //    );

        }
    }
}