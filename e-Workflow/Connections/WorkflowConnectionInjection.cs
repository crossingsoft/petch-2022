﻿using System;
using System.Web;
using System.Web.Configuration;
using CrossingSoft.MVC.Infrastructure.K2;
using SourceCode.Workflow.Client;

namespace PanasonicProject.Connections
{
    public static class WorkflowConnectionInjection
    {
        public static WorkflowConnection Inject()
        {
            
              //bool impersonateActive = Convert.ToBoolean(WebConfigurationManager.AppSettings["K2ImpersonateActive"]);
              //if (impersonateActive)
              //{
              //    return new WorkflowConnection(
              //         WebConfigurationManager.AppSettings["K2Server"],
              //        Convert.ToInt32(WebConfigurationManager.AppSettings["K2WorkflowServerPort"]),
              //        WebConfigurationManager.AppSettings["SecurityLabelName"],
              //        WebConfigurationManager.AppSettings["K2AccountDomainName"],
              //        WebConfigurationManager.AppSettings["K2AccountUserName"],
              //        WebConfigurationManager.AppSettings["K2AccountPassword"],
              //        true,
              //        WebConfigurationManager.AppSettings["K2ImpersonateName"]);
              //}


            try
            {
                var currentConrext = HttpContext.Current.User.Identity;
                return new WorkflowConnection(
                       WebConfigurationManager.AppSettings["K2Server"],
                      Convert.ToInt32(WebConfigurationManager.AppSettings["K2WorkflowServerPort"]),
                       WebConfigurationManager.AppSettings["SecurityLabelName"],
                        WebConfigurationManager.AppSettings["K2AccountDomainName"],
                       WebConfigurationManager.AppSettings["K2AccountUserName"],
                       WebConfigurationManager.AppSettings["K2AccountPassword"], true,
                       currentConrext.Name
                       );
           
            }
            catch (Exception exception)
            {
                return new WorkflowConnection(
                       WebConfigurationManager.AppSettings["K2Server"],
                      Convert.ToInt32(WebConfigurationManager.AppSettings["K2WorkflowServerPort"]),
                       WebConfigurationManager.AppSettings["SecurityLabelName"],
                        WebConfigurationManager.AppSettings["K2AccountDomainName"],
                       WebConfigurationManager.AppSettings["K2AccountUserName"],
                       WebConfigurationManager.AppSettings["K2AccountPassword"]
                       );
           
            }
            
             
            //return new WorkflowConnection(
            //   "localhost",
            //    5252,
            //    "K2",
            //    "K2",
            //    "Administrator",
            //    "P@ssw0rd",
            //    false);
        }
    }
}