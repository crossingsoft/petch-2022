﻿using System;
using System.Collections.Generic;
using System.IdentityModel.Services;
using System.Linq;
using System.Security.Cryptography;
using System.Threading;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;
using System.Web.Script.Serialization;
using System.Web.Security;
using PanasonicProject.Facades;
using PanasonicProject.Infrastructure.Security;
using PanasonicProject.Services;
using PanasonicProject.Services.Permission;

namespace PanasonicProject
{
    public class MvcApplication : System.Web.HttpApplication
    {
        protected void Application_Start()
        {
            AreaRegistration.RegisterAllAreas();

            WebApiConfig.Register(GlobalConfiguration.Configuration);
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);
        }

        protected void Application_PostAuthenticateRequest(Object sender, EventArgs e)
        {
            try
            {
               
                if (HttpContext.Current.User.Identity.IsAuthenticated)
                {
                    var _permission = DependencyResolver.Current.GetService<PermissionFacade>();
                    var user = new IPECTHPrincipal(User.Identity);
                    
                    HttpCookie authCookie = Request.Cookies[User.Identity.Name];
                  
                    if (authCookie != null)
                    {
                        
                        FormsAuthenticationTicket authTicket = FormsAuthentication.Decrypt(authCookie.Value);
                        
                        var serializer = new JavaScriptSerializer();
                        
                        var pecthPrincipalSerializeModel = serializer.Deserialize<PECTHPrincipalSerializeModel>(authTicket.UserData);

                        user.EmployeeId = pecthPrincipalSerializeModel.EmployeeId;
                        user.FullName = pecthPrincipalSerializeModel.FullName;
                        user.Roles = pecthPrincipalSerializeModel.Roles;
                        user.Branch = pecthPrincipalSerializeModel.Branch;
                        user.AuthorizeSystem = _permission.GetPermissionByUser(HttpContext.Current.User.Identity.Name);
                        HttpContext.Current.User = user;
                        Thread.CurrentPrincipal = user;

                        //var _adUserService = DependencyResolver.Current.GetService<ADUserTableService>();
                        //var userModel = _adUserService.GetUser(User.Identity.Name);
                        //user.FullName = userModel.DisplayName;


                    }
                    else
                    {

                       // var _orgGroupMemberService = DependencyResolver.Current.GetService<OrgGroupMemberService>();
                         var _adUserService = DependencyResolver.Current.GetService<ADUserTableService>();
                        //user.Roles = _orgGroupMemberService.GetRoles(user.Identity.Name).ToArray();
                        HttpContext.Current.User = user;
                        Thread.CurrentPrincipal = user;
                        //var branchOwnerService = DependencyResolver.Current.GetService<BranchOwnerService>();                     
                        var PECTHPrincipalSerializeModels = new PECTHPrincipalSerializeModel();
                        var displayname = user.Identity.Name;
                        if (!String.IsNullOrEmpty(user.Identity.Name))
                        {
                            var adUser = _adUserService.GetUser(user.Identity.Name);
                            displayname = _adUserService.GetUser(user.Identity.Name).DisplayName;
                            user.FullName = displayname;
                        }
                        
                        PECTHPrincipalSerializeModels.Username = user.Identity.Name;
                        PECTHPrincipalSerializeModels.FullName = displayname;
                        PECTHPrincipalSerializeModels.Roles = user.Roles;
                        PECTHPrincipalSerializeModels.AuthorizeSystem = _permission.GetPermissionByUser(HttpContext.Current.User.Identity.Name);
                        
                        var serializer = new JavaScriptSerializer();
                        string userData = serializer.Serialize(PECTHPrincipalSerializeModels);
                        var formsAuthTicket = new FormsAuthenticationTicket(1, User.Identity.Name, DateTime.Now,
                            DateTime.Now.AddDays(1), false, userData);

                        var encryptedTicket = FormsAuthentication.Encrypt(formsAuthTicket);
                        var httpcook = new HttpCookie(User.Identity.Name, encryptedTicket);
                        Response.Cookies.Add(httpcook);

                        HttpContext.Current.User = user;


                        //Override Nattawat
                        HttpCookie authCookies = Request.Cookies[User.Identity.Name];
                        FormsAuthenticationTicket authTicket = FormsAuthentication.Decrypt(authCookies.Value);
                        var serializers = new JavaScriptSerializer();

                        var pecthPrincipalSerializeModel = serializers.Deserialize<PECTHPrincipalSerializeModel>(authTicket.UserData);

                        user.EmployeeId = pecthPrincipalSerializeModel.EmployeeId;
                        user.FullName = pecthPrincipalSerializeModel.FullName;
                        user.Roles = pecthPrincipalSerializeModel.Roles;
                        user.Branch = pecthPrincipalSerializeModel.Branch;
                        user.AuthorizeSystem = _permission.GetPermissionByUser(HttpContext.Current.User.Identity.Name);
                        HttpContext.Current.User = user;
                        Thread.CurrentPrincipal = user;
                    }


                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }


        }

    }
}
