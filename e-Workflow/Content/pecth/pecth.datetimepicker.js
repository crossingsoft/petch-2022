﻿pecth = pecth || {};
pecth.datetimepicker = (function () {
    var _blockStartDate = function (c) {
        if (c != undefined) {
            var end = $(c).data("kendoDatePicker");
            var endValue = end.value();
            var s = $("#" + $(c).data("compareId"));
            var start = s.data("kendoDatePicker");
            var newMax = new Date(2099, 11, 31);
            if (endValue != null) {
                newMax = endValue;
            } else {
                if (s.data('originalMax') != null) {
                    newMax = new Date(s.data('originalMax'));
                }
            }
            start.max(newMax);
        }
    }
    var _blockEndDate = function (c) {
        if (c != undefined) {
            var start = $(c).data("kendoDatePicker");
            var startValue = start.value();
            var e = $("#" + $(c).data("compareId"));
            var end = e.data("kendoDatePicker");
            var newMin = new Date(1900, 0, 1);
            if (startValue != null) {
                newMin = startValue;
            } else {
                if (e.data('originalMin') != null) {
                    newMin = new Date(e.data('originalMin'));
                }
            }
            end.min(newMin);
        }
    }
    var _blockDate = function (c) {
        var $c = $(c);
        if ($c.data('role-date').toLowerCase() == 'start') {
            _blockEndDate($c);
        } else {
            _blockStartDate($c);
        }
    }
    var _validateStartDate = function (start, end) {
        var startVal = start.value();
        var endVal = end.value();
        if (startVal != null && endVal != null) {
            if (startVal > endVal) {
                displayUnknownError("Date start must be less than the date end.");
                start.value('');
                return false;
            }
        }
        return true;
    }
    var _validateEndDate = function (end, start) {
        var endVal = end.value();
        var startVal = start.value();
        if (startVal != null && endVal != null) {
            if (endVal < startVal) {
                displayUnknownError("Date end must be more than the date start.");
                end.value('');
                return false;
            }
        }
        return true;
    }
    var _validate = function (c) {
        if (c != undefined) {
            var $c = $(c);
            var kendoPicker = $(c).data("kendoDatePicker");
            var comparer = $("#" + $(c).data("compareId")).data("kendoDatePicker");
            if ($c.data('role-date').toLowerCase() == 'start') {
                return _validateStartDate(kendoPicker, comparer);
            } else {
                return _validateEndDate(kendoPicker, comparer);
            }
        }
    }
    return {
        validateOnChange: function (e) {
            if (_validate(this.element)) {
                _blockDate(this.element);
            }
        },
        validateOnload: function (name) {
            if (name != undefined) { //Legacy
                if (typeof name == 'string' || name instanceof String) {
                    if (name != "") {
                        var client = $("#" + name).data("kendoDatePicker");
                        var bindClient = $("#" + $("#" + name).data("compareId")).data("kendoDatePicker");
                        var valClient = client.value();
                        if (valClient) {
                            valClient = new Date(valClient);
                            valClient.setDate(valClient.getDate());
                            bindClient.max(valClient);
                        }
                    }
                } else {
                    $(name).data('originalMin', $(name).data('kendoDatePicker').min());
                    $(name).data('originalMax', $(name).data('kendoDatePicker').max());
                    _blockDate(name);
                }
            }
        },
        disableInput: function (o) {
            var $o = $(o);
            $o.keydown(function (e) {
                var keyCode = (window.event) ? e.keyCode : e.which;
                if (keyCode == 8 || keyCode == 46) {
                    $o.val('');
                    return true;
                } else {
                    e.preventDefault();
                    return false;
                }
            });
            $o.click(function (e) {
                this.select();
            });
        }
    };
})();