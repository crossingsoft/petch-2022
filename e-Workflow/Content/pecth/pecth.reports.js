﻿pecth = pecth || {};
pecth.reports = (function () {
    return {
        searchReport: function (e) {
            var url = $(e).data("url");
            var idViews = $(e).data("resultid");
            var idframe = $(e).data("panelviewsid");

            $("#" + idframe).removeClass('displaynon');

            $.ajax({
                url: url,
                type: 'Post',
                dataType: "html",
                //data: addAntiForgeryToken($('form#formsearch').serializeObject()),
                data: $('form').serialize(),
                success: function (result) {
                    $("#" + idViews).html('');
                    $("#" + idViews).html(result);
                },
                error: function (err) {
                    alert(err);
                }
            });
        },
        clearForm: function (e) {
            var reportname = $(e).data("reportname");
            var documentnumber = $(e).data("documentnumber");
            var createdby = $(e).data("createdby");
            var startdate = $(e).data("startdate");
            var enddate = $(e).data("enddate");
            var divisionid = $(e).data("divisionid");
            var departmentid = $(e).data("departmentid");
            var sectionid = $(e).data("sectionid");

            $("#" + reportname).data("kendoComboBox").text('');
            $("#" + documentnumber).val('');
            $("#" + createdby).data("kendoComboBox").text('');
            $("#" + startdate).val('');
            $("#" + enddate).val('');
            $("#" + divisionid).data("kendoComboBox").text('');
            $("#" + departmentid).data("kendoComboBox").text('');
            $("#" + sectionid).data("kendoComboBox").text('');
        },
        alert:function() {
            alert('s');
        },
        Notification: function (id, text) {
            var noti = $("#" + id).kendoNotification().data("kendoNotification");
            noti.setOptions({ position: { top: 90, left: Math.floor($(window).width() / 2 - 110), bottom: 0, right: 0 } });
            noti.show(text, "error");
        },
        changeReportName: function (e) {
            var val = e.sender.value();
            $("#isImportDiv").addClass("displaynon");

            if (val === 'Application' || val === 'Purchase order' || val === 'Purchase order & Application') {
                $("#btnSubmitIsAjax").addClass('displaynon');
                $("#btnSubmit").removeClass('displaynon');
            } else {
                $("#btnSubmit").addClass('displaynon');
                $("#btnSubmitIsAjax").removeClass('displaynon');
            }

            if (val === "Expense, Repair and Fixed asset") {
                $("#subReport").removeClass("displaynon");

            } else {
                $("#subReport").addClass("displaynon");
            }

            if (val === 'Purchase order') {
                $("#isImportDiv").removeClass("displaynon");
            }
        },
        hidePennel: function(e) {
            var idframe = $(e).data("panelviewsid");
            $("#" + idframe).addClass('displaynon');
        },
        filterCascade: function (e) {
            alert(e);
            return false;
        }

    };
})();