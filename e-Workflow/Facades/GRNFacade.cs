﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using iTextSharp.text.pdf.qrcode;
using Microsoft.Ajax.Utilities;
using PanasonicProject.Models;
using PanasonicProject.Models.Enums;
using PanasonicProject.Resource;
using PanasonicProject.Services;
using PanasonicProject.ViewModels;

namespace PanasonicProject.Facades
{
    public interface IGRNFacade
    {

    }

    public class GRNFacade : Abstracts.BasePanasonicFacade, IGRNFacade
    {
        public readonly GRNTableService _grnTableService;
        public readonly GRNLineService _grnLineService;

        public readonly DB_GoodReceiveNote_DService _dbGoodReceiveNoteDService;
        public readonly DB_GoodReceiveNote_HService _dbGoodReceiveNoteHService;
        public readonly SB_GoodReceiveNote_DService _sbGoodReceiveNoteDService;
        public readonly SB_GoodReceiveNote_HService _sbGoodReceiveNoteHService;

        public readonly PurchTableService _PurchTableService;
        public readonly PurchLineService _PurchLineService;
        private readonly DepartmentSetupService _departmentSetupService;

        public GRNFacade(GRNTableService grnTableService, GRNLineService grnLineService, DB_GoodReceiveNote_DService dbGoodReceiveNoteDService, DB_GoodReceiveNote_HService dbGoodReceiveNoteHService, SB_GoodReceiveNote_DService sbGoodReceiveNoteDService, SB_GoodReceiveNote_HService sbGoodReceiveNoteHService, PurchTableService purchTableService, PurchLineService purchLineService, DepartmentSetupService departmentSetupService)
        {
            _grnTableService = grnTableService;
            _grnLineService = grnLineService;
            _dbGoodReceiveNoteDService = dbGoodReceiveNoteDService;
            _dbGoodReceiveNoteHService = dbGoodReceiveNoteHService;
            _sbGoodReceiveNoteDService = sbGoodReceiveNoteDService;
            _sbGoodReceiveNoteHService = sbGoodReceiveNoteHService;
            _PurchTableService = purchTableService;
            _PurchLineService = purchLineService;
            _departmentSetupService = departmentSetupService;
        }

        public long CopyGRN(long? primaryId, string documentnumber)
        {
            var modelCopyFrom = _grnTableService.Load(m => m.GRNId == primaryId);
            var modelCopyTo = new GRNTable();
            modelCopyTo = modelCopyFrom;
            modelCopyTo.GRNExtId = documentnumber;
            modelCopyTo.CreateDate = DateTime.Now;
            modelCopyTo.Status = DocumentStatus.Draft;
            modelCopyTo.IsUnlock = null;
            modelCopyTo = _grnTableService.Create(modelCopyTo);
            _grnTableService.SaveChanges();

            var modelLineCopyFrom = _grnLineService.Search(m => m.ParentId == primaryId);

            foreach (var t in modelLineCopyFrom)
            {
                var modelLineCopyTo = new GRNLine();
                t.ParentId = modelCopyTo.GRNId;
                modelLineCopyTo = t;
                _grnLineService.Create(modelLineCopyTo);
            }

            _grnLineService.SaveChanges();

            return modelCopyTo.GRNId;
        }

        //public string validation(IEnumerable<GRNLine> grLines)
        //{
        //    string msg;
        //    foreach (var grLine in grLines)
        //    {
                
        //    }
        //    return
        //}

        public Validate CreateGRTableToMsAccessAdvanceValidation(long primaryId)
        {
            try
            {
                var val = new Validate();
                const string indirect = "Indirect", direct = "Direct";
                var modelGRTable = _grnTableService.Load(m => m.GRNId == primaryId);
                var modelGRLine = _grnLineService.GetAll(m => m.ParentId == primaryId).ToListSafe();
                var modelGRLineIndirect = modelGRLine.Where(m => m.Source == indirect && m.Qty != null).ToListSafe();
                if (modelGRLine.Any())
                {

                    if (!modelGRLineIndirect.Any())
                    {
                        val.Success = true;
                        val.MessageError = "not has indirect";
                        return val;
                    }
                    if (modelGRTable.ReceiveNo == null)
                    {
                        val.Success = false;
                        val.MessageError = "The Goods Receipt must have Receive No. (Indirect)";
                        return val;
                    }
                    if (modelGRTable.ItemClassNo == null)
                    {
                        val.Success = false;
                        val.MessageError = "The Goods Receipt must have Item Class No. (Indirect)";
                        return val;
                    }
                    foreach (var grLine in modelGRLineIndirect.Where(m => m.Qty < 0))
                    {
                        val.Success = false;
                        val.MessageError +=
                            string.Format("Line Number {0} item name {1} : ไม่สามารถกรอกข้อมูลติดลบได้ \r\n", grLine.LineNum, grLine.ItemNameDescription);
                        //val.MessageError += "GR Line " + grLine.LineNum + " item name " + grLine.ItemNameDescription + " ไม่สามารถกรอกข้อมูลติดลบได้" ;
                    }
                    if (!string.IsNullOrEmpty(val.MessageError))
                    {
                        return val;
                    }
                }
                else
                {
                    return new Validate();
                }

                if (modelGRTable != null)
                {
                    if (modelGRTable.DivisionId.Trim().ToUpper() == "DB".ToUpper())
                    {
                        var dbAccessHead = new DB_GoodReceiveNote_H();
                        var number = _dbGoodReceiveNoteHService.GetAll().ToListSafe().Count() + 1;
                        dbAccessHead.ReceiveNo = modelGRTable.ReceiveNo;
                        dbAccessHead.ItemClassNo = modelGRTable.ItemClassNo;
                        dbAccessHead.ReceiveDate = (modelGRTable.GRNDate != null ? modelGRTable.GRNDate.ToString() : "");
                        dbAccessHead.SupplierNo = modelGRTable.VendNumberShoirt;
                        dbAccessHead.InvoiceNo = modelGRTable.InvoiceNo;
                        dbAccessHead.InvoiceDate = (modelGRTable.InvoiceDate != null ? modelGRTable.InvoiceDate.ToString() : "");
                        dbAccessHead.ReceiverNo = modelGRTable.CreatedDisplayName;
                        dbAccessHead.Remark = modelGRTable.Remark;
                        dbAccessHead.DFlag = "N";
                        dbAccessHead.SFlag = "N";
                        dbAccessHead.CFlag = "N";
                        dbAccessHead.RunningNumber = number;
                        dbAccessHead.CreateDate = (modelGRTable.FinishedDate != null ? modelGRTable.FinishedDate.ToString() : "");



                        foreach (var t in modelGRLineIndirect)
                        {
                            var dbAccessLine = new DB_GoodReceiveNote_D();
                            dbAccessLine.RunningNumber = dbAccessHead.RunningNumber;
                            dbAccessLine.InvoiceNo = modelGRTable.InvoiceNo;
                            dbAccessLine.PoNumber = modelGRTable.PurchNumber;
                            dbAccessLine.PartNumber = t.Items;
                            dbAccessLine.Qty = t.Qty;
                            dbAccessLine.ReceiveNo = modelGRTable.ReceiveNo;
                            dbAccessLine.Seq = t.LineNum;
                            dbAccessLine.Unit = t.Unit;
                            dbAccessLine.UnitPrice = t.UnitPrice;
                            _dbGoodReceiveNoteDService.Create(dbAccessLine);
                        }

                        _dbGoodReceiveNoteHService.Create(dbAccessHead);

                        modelGRTable.GRNExtInterface = number;

                        _grnTableService.Update(modelGRTable);
                        _grnTableService.SaveChanges();
                        _dbGoodReceiveNoteDService.SaveChanges();
                        _dbGoodReceiveNoteHService.SaveChanges();
                    }
                    else if (modelGRTable.DivisionId.Trim().ToUpper() == "SB".ToUpper())
                    {
                        var dbAccessHead = new SB_GoodReceiveNote_H();

                        dbAccessHead.ReceiveNo = modelGRTable.ReceiveNo;
                        dbAccessHead.ItemClassNo = modelGRTable.ItemClassNo;
                        dbAccessHead.ReceiveDate = (modelGRTable.GRNDate != null ? modelGRTable.GRNDate.ToString() : "");
                        dbAccessHead.SupplierNo = modelGRTable.VendNumberShoirt;
                        dbAccessHead.InvoiceNo = modelGRTable.InvoiceNo;
                        dbAccessHead.InvoiceDate = (modelGRTable.InvoiceDate != null ? modelGRTable.InvoiceDate.ToString() : "");
                        dbAccessHead.ReceiverNo = modelGRTable.CreatedDisplayName;
                        dbAccessHead.Remark = modelGRTable.Remark;
                        dbAccessHead.DFlag = "N";
                        dbAccessHead.SFlag = "N";
                        dbAccessHead.CFlag = "N";
                        dbAccessHead.RunningNumber = _dbGoodReceiveNoteHService.GetAll().ToListSafe().Count() + 1;
                        dbAccessHead.CreateDate = (modelGRTable.FinishedDate != null ? modelGRTable.FinishedDate.ToString() : "");

                        //var modelGRLine = _grnLineService.GetAll(m => m.ParentId == primaryId).ToListSafe();

                        foreach (var t in modelGRLineIndirect)
                        {
                            var dbAccessLine = new SB_GoodReceiveNote_D();
                            dbAccessLine.RunningNumber = dbAccessHead.RunningNumber;
                            dbAccessLine.InvoiceNo = modelGRTable.InvoiceNo;
                            dbAccessLine.PoNumber = modelGRTable.GRNExtId;
                            dbAccessLine.PartNumber = t.Items;
                            dbAccessLine.Qty = t.Qty;
                            dbAccessLine.ReceiveNo = modelGRTable.ReceiveNo;
                            dbAccessLine.Seq = t.LineNum;
                            dbAccessLine.Unit = t.Unit;
                            dbAccessLine.UnitPrice = t.UnitPrice;
                            _sbGoodReceiveNoteDService.Create(dbAccessLine);
                        }

                        _sbGoodReceiveNoteHService.Create(dbAccessHead);
                        modelGRTable.GRNExtInterface = dbAccessHead.RunningNumber;

                        _grnTableService.Update(modelGRTable);
                        _grnTableService.SaveChanges();
                        _sbGoodReceiveNoteDService.SaveChanges();
                        _sbGoodReceiveNoteHService.SaveChanges();

                    }
                    else
                    {
                        throw new Exception("Division ID is not value.");
                    }
                }

                return InitValidate(true, "Create to Microsoft Access Success.");
            }
            catch (Exception exception)
            {
                return InitValidate(false, exception.Message);
            }
        }


        public Validate CreateGRTableToMSAccess(long primaryId)
        {
            try
            {
                var modelGRTable = _grnTableService.Load(m => m.GRNId == primaryId);
                if (modelGRTable != null)
                {
                    if (modelGRTable.DivisionId.Trim().ToUpper() == "DB".ToUpper())
                    {
                        var dbAccessHead = new DB_GoodReceiveNote_H();

                        dbAccessHead.ReceiveNo = modelGRTable.ReceiveNo;
                        dbAccessHead.ItemClassNo = modelGRTable.ItemClassNo;
                        dbAccessHead.ReceiveDate = (modelGRTable.GRNDate != null ? modelGRTable.GRNDate.ToString() : "");
                        dbAccessHead.SupplierNo = modelGRTable.VendNumberShoirt;
                        dbAccessHead.InvoiceNo = modelGRTable.InvoiceNo;
                        dbAccessHead.InvoiceDate = (modelGRTable.InvoiceDate != null ? modelGRTable.InvoiceDate.ToString() : "");
                        dbAccessHead.ReceiverNo = modelGRTable.CreatedDisplayName;
                        dbAccessHead.Remark = modelGRTable.Remark;
                        dbAccessHead.DFlag = "N";
                        dbAccessHead.SFlag = "N";
                        dbAccessHead.CFlag = "N";
                        dbAccessHead.RunningNumber = _dbGoodReceiveNoteHService.GetAll().ToListSafe().Count() + 1;
                        dbAccessHead.CreateDate = (modelGRTable.FinishedDate != null ? modelGRTable.FinishedDate.ToString() : "");

                        var modelGRLine = _grnLineService.GetAll(m => m.ParentId == primaryId).ToListSafe();

                        foreach (var t in modelGRLine)
                        {
                            var dbAccessLine = new DB_GoodReceiveNote_D();
                            dbAccessLine.RunningNumber = dbAccessHead.RunningNumber;
                            dbAccessLine.InvoiceNo = modelGRTable.InvoiceNo;
                            dbAccessLine.PoNumber = modelGRTable.PurchNumber;
                            dbAccessLine.PartNumber = t.Items;
                            dbAccessLine.Qty = t.Qty;
                            dbAccessLine.ReceiveNo = modelGRTable.ReceiveNo;
                            dbAccessLine.Seq = t.LineNum;
                            dbAccessLine.Unit = t.Unit;
                            dbAccessLine.UnitPrice = t.UnitPrice;
                            _dbGoodReceiveNoteDService.Create(dbAccessLine);
                        }

                        _dbGoodReceiveNoteHService.Create(dbAccessHead);

                        modelGRTable.GRNExtInterface = dbAccessHead.RunningNumber;

                        _grnTableService.Update(modelGRTable);
                        _grnTableService.SaveChanges();
                        _dbGoodReceiveNoteDService.SaveChanges();
                        _dbGoodReceiveNoteHService.SaveChanges();
                    }
                    else if (modelGRTable.DivisionId.Trim().ToUpper() == "SB".ToUpper())
                    {
                        var dbAccessHead = new SB_GoodReceiveNote_H();

                        dbAccessHead.ReceiveNo = modelGRTable.ReceiveNo;
                        dbAccessHead.ItemClassNo = modelGRTable.ItemClassNo;
                        dbAccessHead.ReceiveDate = (modelGRTable.GRNDate != null ? modelGRTable.GRNDate.ToString() : "");
                        dbAccessHead.SupplierNo = modelGRTable.VendNumberShoirt;
                        dbAccessHead.InvoiceNo = modelGRTable.InvoiceNo;
                        dbAccessHead.InvoiceDate = (modelGRTable.InvoiceDate != null ? modelGRTable.InvoiceDate.ToString() : "");
                        dbAccessHead.ReceiverNo = modelGRTable.CreatedDisplayName;
                        dbAccessHead.Remark = modelGRTable.Remark;
                        dbAccessHead.DFlag = "N";
                        dbAccessHead.SFlag = "N";
                        dbAccessHead.CFlag = "N";
                        dbAccessHead.RunningNumber = _dbGoodReceiveNoteHService.GetAll().ToListSafe().Count() + 1;
                        dbAccessHead.CreateDate = (modelGRTable.FinishedDate != null ? modelGRTable.FinishedDate.ToString() : "");

                        var modelGRLine = _grnLineService.GetAll(m => m.ParentId == primaryId).ToListSafe();

                        foreach (var t in modelGRLine)
                        {
                            var dbAccessLine = new SB_GoodReceiveNote_D();
                            dbAccessLine.RunningNumber = dbAccessHead.RunningNumber;
                            dbAccessLine.InvoiceNo = modelGRTable.InvoiceNo;
                            dbAccessLine.PoNumber = modelGRTable.GRNExtId;
                            dbAccessLine.PartNumber = t.Items;
                            dbAccessLine.Qty = t.Qty;
                            dbAccessLine.ReceiveNo = modelGRTable.ReceiveNo;
                            dbAccessLine.Seq = t.LineNum;
                            dbAccessLine.Unit = t.Unit;
                            dbAccessLine.UnitPrice = t.UnitPrice;
                            _sbGoodReceiveNoteDService.Create(dbAccessLine);
                        }

                        _sbGoodReceiveNoteHService.Create(dbAccessHead);
                        modelGRTable.GRNExtInterface = dbAccessHead.RunningNumber;

                        _grnTableService.Update(modelGRTable);
                        _grnTableService.SaveChanges();
                        _sbGoodReceiveNoteDService.SaveChanges();
                        _sbGoodReceiveNoteHService.SaveChanges();

                    }
                    else
                    {
                        throw new Exception("Division ID is not value.");
                    }
                }

                return InitValidate(true, "Create to Microsoft Access Success.");
            }
            catch (Exception exception)
            {
                return InitValidate(false, exception.Message);
            }
        }
        public Validate UpdateGRTableToMSAccess(long primaryId)
        {
            try
            {
                //reset 
                var val = new Validate();
                const string indirect = "Indirect", direct = "Direct";
                var modelGRTable = _grnTableService.Load(m => m.GRNId == primaryId);
                var modelGRLine = _grnLineService.GetAll(m => m.ParentId == primaryId).ToListSafe();
                var modelGRLineIndirect = modelGRLine.Where(m => m.Source == indirect && m.Qty != null).ToListSafe();
                if (modelGRLine.Any())
                {

                    if (!modelGRLineIndirect.Any())
                    {
                        val.Success = true;
                        val.MessageError = "not has indirect";
                        return val;
                    }
                    foreach (var grLine in modelGRLineIndirect.Where(m => m.Qty != m.QtyValidation ))
                    {
                        if (grLine.Qty == 0)
                        {
                            continue;
                        }
                        val.Success = false;
                        val.MessageError +=
                            string.Format("Line Number {0} item name {1} : กรุณาใส่จำนวนรับเป็น 0 \r\n", grLine.LineNum, grLine.ItemNameDescription);
                        //val.MessageError += "GR Line " + grLine.LineNum + " item name " + grLine.ItemNameDescription + " ไม่สามารถกรอกข้อมูลติดลบได้" ;
                    }
                    if (!string.IsNullOrEmpty(val.MessageError))
                    {
                        return val;
                    }
                }
                else
                {
                    return new Validate();
                }

                if (modelGRTable != null)
                {
                    if (modelGRTable.DivisionId.Trim().ToUpper() == "DB".ToUpper())
                    {
                        //var modelGRLine = _grnLineService.GetAll(m => m.ParentId == primaryId).ToListSafe();
                        var localgr = _dbGoodReceiveNoteHService.LoadGRNAccess(modelGRTable.GRNExtInterface);
                        if (localgr == null)
                        {
                            val.Success = false;
                            val.MessageError = "localgr is null";
                        }
                        else
                        {
                            if (localgr.CFlag == "Y")
                            {
                                val.Success = true;
                                val.MessageError = "The Goods Receipt can not update in local GR";
                                return val;
                            }
                        }

                        foreach (var t in modelGRLine)
                        {
                            var localGRLine =
                                _dbGoodReceiveNoteDService.LoadGRNLineAccess(modelGRTable.ReceiveNo,
                                    modelGRTable.GRNExtInterface, t.LineNum);
                            if (localGRLine == null) continue;
                            //if (t.Qty == 0)
                            //{
                            //    t.Qty = 0;
                            //    t.QTYAccrual = t.POlineQty;
                            //    t.QTYRec = 0;
                            //    _grnLineService.Update(t);
                            //}
                            localGRLine.Qty = t.Qty;
                            localGRLine.UnitPrice = t.UnitPrice;
                            _dbGoodReceiveNoteDService.UpdateGRNLineAccess(localGRLine);
                        }

                        _dbGoodReceiveNoteDService.SaveChanges();

                    }
                    else if (modelGRTable.DivisionId.Trim().ToUpper() == "SB".ToUpper())
                    {
                        //var modelGRLine = _grnLineService.GetAll(m => m.ParentId == primaryId).ToListSafe();
                        var localgr = _sbGoodReceiveNoteHService.LoadGRNAccess(modelGRTable.GRNExtInterface);
                        if (localgr == null)
                        {
                            val.Success = false;
                            val.MessageError = "localgr is null";

                        }
                        else
                        {
                            if (localgr.CFlag == "Y")
                            {
                                val.Success = true;
                                val.MessageError = "The Goods Receipt can not update in local GR";
                                return val;
                            }
                        }
                        foreach (var t in modelGRLine)
                        {
                            var localGRLine =
                                   _sbGoodReceiveNoteDService.LoadGRNLineAccess(modelGRTable.ReceiveNo,
                                       modelGRTable.GRNExtInterface, t.LineNum);
                            if (localGRLine == null) continue;
                            //if (t.Qty == 0)
                            //{
                            //    t.Qty = 0;
                            //    t.QTYAccrual = t.POlineQty;
                            //    t.QTYRec = 0;
                            //    _grnLineService.Update(t);
                            //}
                            localGRLine.Qty = t.Qty;
                            localGRLine.UnitPrice = t.UnitPrice;
                            _sbGoodReceiveNoteDService.Update(localGRLine);
                        }

                        _sbGoodReceiveNoteDService.SaveChanges();

                    }
                    else
                    {
                        throw new Exception("Division ID is not value.");
                    }
                }

                return InitValidate(true, "Create to Microsoft Access Success.");
            }
            catch (Exception exception)
            {
                return InitValidate(false, exception.Message);
            }
        }
        public Validate UpdateACCGRTableToMSAccess(long primaryId)
        {
            try
            {
                //reset 
                var val = new Validate();
                const string indirect = "Indirect", direct = "Direct";
                var modelGRTable = _grnTableService.Load(m => m.GRNId == primaryId);
                var modelGRLine = _grnLineService.GetAll(m => m.ParentId == primaryId).ToListSafe();
                var modelGRLineIndirect = modelGRLine.Where(m => m.Source == indirect && m.Qty != null).ToListSafe();
                if (modelGRLine.Any())
                {

                    if (!modelGRLineIndirect.Any())
                    {
                        val.Success = true;
                        val.MessageError = "not has indirect";
                        return val;
                    }
                    foreach (var grLine in modelGRLineIndirect.Where(m => m.Qty != m.QtyValidation))
                    {
                        if (grLine.Qty == 0)
                        {
                            grLine.Qty = 0;
                            grLine.QTYAccrual = grLine.POlineQty;
                            grLine.QTYRec = 0;
                            _grnLineService.Update(grLine);

                            continue;
                        }
                        val.Success = false;
                        val.MessageError +=
                            string.Format("Line Number {0} item name {1} : กรุณาใส่จำนวนรับเป็น 0 \r\n", grLine.LineNum, grLine.ItemNameDescription);
                        //val.MessageError += "GR Line " + grLine.LineNum + " item name " + grLine.ItemNameDescription + " ไม่สามารถกรอกข้อมูลติดลบได้" ;
                    }
                    _grnLineService.SaveChanges();
                    if (!string.IsNullOrEmpty(val.MessageError))
                    {
                        return val;
                    }
                }
                else
                {
                    return new Validate();
                }

                if (modelGRTable != null)
                {
                    if (modelGRTable.DivisionId.Trim().ToUpper() == "DB".ToUpper())
                    {
                        //var modelGRLine = _grnLineService.GetAll(m => m.ParentId == primaryId).ToListSafe();
                        var localgr = _dbGoodReceiveNoteHService.LoadGRNAccess(modelGRTable.GRNExtInterface);
                        if (localgr.CFlag == "Y")
                        {
                            val.Success = true;
                            val.MessageError = "The Goods Receipt can not Update in local GR";
                            return val;
                        }
                        foreach (var t in modelGRLine)
                        {
                            var localGRLine =
                                _dbGoodReceiveNoteDService.LoadGRNLineAccess(modelGRTable.ReceiveNo,
                                    modelGRTable.GRNExtInterface, t.LineNum);
                            if (localGRLine == null) continue;

                            localGRLine.InvoiceNo = modelGRTable.InvoiceNo;
                            localGRLine.PoNumber = modelGRTable.PurchNumber;
                            localGRLine.ReceiveNo = modelGRTable.ReceiveNo;

                            _dbGoodReceiveNoteDService.UpdateGRNLineAccess(localGRLine);
                        }

                        _dbGoodReceiveNoteDService.SaveChanges();

                    }
                    else if (modelGRTable.DivisionId.Trim().ToUpper() == "SB".ToUpper())
                    {
                        //var modelGRLine = _grnLineService.GetAll(m => m.ParentId == primaryId).ToListSafe();
                        var localgr = _sbGoodReceiveNoteHService.LoadGRNAccess(modelGRTable.GRNExtInterface);
                        if (localgr.CFlag == "Y")
                        {
                            val.Success = true;
                            val.MessageError = "The Goods Receipt can not Update in local GR";
                            return val;
                        }
                        foreach (var t in modelGRLine)
                        {
                            var localGRLine =
                                   _sbGoodReceiveNoteDService.LoadGRNLineAccess(modelGRTable.ReceiveNo,
                                       modelGRTable.GRNExtInterface, t.LineNum);
                            if (localGRLine == null) continue;

                            localGRLine.InvoiceNo = modelGRTable.InvoiceNo;
                            localGRLine.PoNumber = modelGRTable.PurchNumber;
                            localGRLine.ReceiveNo = modelGRTable.ReceiveNo;
                            _sbGoodReceiveNoteDService.Update(localGRLine);
                        }

                        _sbGoodReceiveNoteDService.SaveChanges();

                    }

                    else
                    {
                        throw new Exception("Division ID is not value.");
                    }
                }

                return InitValidate(true, "Create to Microsoft Access Success.");
            }
            catch (Exception exception)
            {
                return InitValidate(false, exception.Message);
            }
        }
        public Validate CheckGRLine(long primaryId)
        {
            try
            {
                var grn = _grnTableService.Load(m => m.GRNId == primaryId);
                var grnline = _grnLineService.GetAll(m => m.ParentId == primaryId);
                foreach (var line in grnline)
                {
                    if (line.Qty == null)
                    {
                        _grnLineService.DeleteGRNLine(line.Id);
                    }
                    else
                    {
                        //if (line.Qty == null || line.Qty == 0)
                        //{
                        //    _grnLineService.DeleteGRNLine(line.Id);
                        //}
                        line.QtyValidation = line.Qty;
                        if (line.Source == "Indirect")
                        {
                            line.ReceiveNo = grn.ReceiveNo;
                        }
                        _grnLineService.Update(line);
                    }
                }
                _grnLineService.SaveChanges();

                return InitValidate(true, "Check GRN Line function success.");
            }
            catch (Exception exception)
            {
                return InitValidate(false, exception.Message);
            }
        }

        public GRNCheckOverBudget CheckOverBudget(long purchTableId)
        {

            try
            {
                var model = from a in _grnLineService.GetAll().ToList()
                            join b in _grnTableService.GetAll(m => m.PurchId == purchTableId).ToList() on a.ParentId equals
                                b.GRNId
                            where b.Status == Resource.DocumentStatus.Completed
                            select new GRNLine()
                            {
                                POUnitPrice = a.POUnitPrice,
                                GRLineAmount = a.GRLineAmount,
                                GRLineAmountTH = a.GRLineAmountTH,
                                UnitPrice = a.UnitPrice
                            };
                if (model != null && model.ToListSafe().Count != 0)
                {
                    var sumPOUnitPrice = model.Sum(m => m.POUnitPrice);
                    var sumLineAmount = model.Sum(m => m.GRLineAmount);
                    var sumLineAmountTH = model.Sum(m => m.GRLineAmountTH);
                    var sumUnitPrice = model.Sum(m => m.UnitPrice);

                    var grnCheckOverBudget = new GRNCheckOverBudget()
                    {
                        POUnitPrice = sumPOUnitPrice,
                        LineAmount = sumLineAmount,
                        LineAmountTH = sumLineAmountTH,
                        UnitPrice = sumUnitPrice
                    };
                    return grnCheckOverBudget;
                }
                else
                {
                    return new GRNCheckOverBudget()
                    {
                        POUnitPrice = 0,
                        LineAmount = 0,
                        LineAmountTH = 0,
                        UnitPrice = 0
                    };
                }
                //var _grnLineService.Delete(m => m.PurchLineId == purchTableId);
            }
            catch (Exception exception)
            {
                throw new Exception(exception.Message);
            }
        }

        public Validate DuplicatePOLine(long purchTableId, long grTableId)
        {
            var validate = new Validate();
            try
            {

                _grnLineService.Delete(m => m.ParentId == grTableId);
                const string indirect = "Indirect", direct = "Direct";
                var purchOrderLinelist = _PurchLineService.GetpurchLineList(purchTableId);
                int i = 1;
                foreach (var purchOrderLine in purchOrderLinelist)
                {
                    var grnLine = new GRNLine();
                    var wfType = purchOrderLine.WorkflowType;
                    grnLine.LineNum = i++;
                    grnLine.ParentId = grTableId;
                    grnLine.Items = purchOrderLine.Items;
                    grnLine.ItemCode = purchOrderLine.ItemCode;
                    grnLine.ItemName = purchOrderLine.ItemName;
                    grnLine.ItemNameDescription = purchOrderLine.ItemNameDescription;
                    grnLine.Unit = purchOrderLine.Unit;
                    grnLine.UnitPrice = purchOrderLine.UnitPrice;
                    grnLine.POlineQty = purchOrderLine.Qty;
                    grnLine.PurchLineId = purchOrderLine.PurchLineId;
                    grnLine.Qty = null;
                    //grnLine.QTYRec = this.GetSumReceiveQtyGRNLine(purchOrderLine.PurchLineId);
                    grnLine.QTYRec = this.GetSumReceiveQtyGRNLine(purchOrderLine.PurchLineId);
                    //grnLine.QTYAccrual = purchOrderLine.Qty - grnLine.QTYRec;
                    grnLine.QTYAccrual = 0;
                    //add 17-08
                    grnLine.BPNum = purchOrderLine.BPNum;
                    //grnLine.BudgetType = ;
                    grnLine.RefNumber = purchOrderLine.RefNumber;
                    //grnLine.FiscalYear = ;
                    grnLine.Month = purchOrderLine.Month;
                    //grnLine.BpType = ;
                    grnLine.Description = purchOrderLine.Description;
                    grnLine.SectionId = purchOrderLine.SectionId;
                    grnLine.BPGroup = purchOrderLine.BPGroup;
                    grnLine.BPGroupDescription = purchOrderLine.BPGroupDescription;
                    grnLine.POUnitPrice = purchOrderLine.UnitPrice;
                    grnLine.LineAmount = purchOrderLine.LineAmount;
                    grnLine.LineAmountTH = purchOrderLine.LineAmountTH;
                    grnLine.GRLineAmount = 0;
                    grnLine.GRLineAmountTH = 0;
                    grnLine.POId = purchTableId;
                    grnLine.SubSection = purchOrderLine.SubSection;
                    grnLine.WorkflowType = purchOrderLine.WorkflowType;
                    if (wfType == indirect)
                    {
                        grnLine.Source = indirect;
                    }
                    else
                    {
                        grnLine.Source = direct;
                    }
                    _grnLineService.CreateGRNLine(grnLine);
                }
                _grnLineService.SaveChanges();

                validate.Success = true;
                return validate;
            }
            catch (Exception ex)
            {
                validate.Success = false;
                validate.MessageError = ex.Message;
                return validate;
            }
        }
        public Validate DuplicatePOLineUpdate(long purchTableId, long grTableId)
        {
            var validate = new Validate();
            try
            {

                _grnLineService.Delete(m => m.ParentId == grTableId);
                const string indirect = "Indirect", direct = "Direct";
                var purchOrderLinelist = _PurchLineService.GetpurchLineList(purchTableId);
                int i = 1;
                foreach (var purchOrderLine in purchOrderLinelist)
                {
                    var grnLine = new GRNLine();
                    var wfType = purchOrderLine.WorkflowType;
                    grnLine.LineNum = i++;
                    grnLine.ParentId = grTableId;
                    grnLine.Items = purchOrderLine.Items;
                    grnLine.ItemCode = purchOrderLine.ItemCode;
                    grnLine.ItemName = purchOrderLine.ItemName;
                    grnLine.ItemNameDescription = purchOrderLine.ItemNameDescription;
                    grnLine.Unit = purchOrderLine.Unit;
                    grnLine.UnitPrice = purchOrderLine.UnitPrice;
                    grnLine.POlineQty = purchOrderLine.Qty;
                    grnLine.PurchLineId = purchOrderLine.PurchLineId;
                    grnLine.Qty = null;
                    //grnLine.QTYRec = this.GetSumReceiveQtyGRNLine(purchOrderLine.PurchLineId);
                    grnLine.QTYRec = this.GetSumReceiveQtyGRNLine(purchOrderLine.PurchLineId);
                    //grnLine.QTYAccrual = purchOrderLine.Qty - grnLine.QTYRec;
                    grnLine.QTYAccrual = 0;
                    //add 17-08
                    grnLine.BPNum = purchOrderLine.BPNum;
                    //grnLine.BudgetType = ;
                    grnLine.RefNumber = purchOrderLine.RefNumber;
                    //grnLine.FiscalYear = ;
                    grnLine.Month = purchOrderLine.Month;
                    //grnLine.BpType = ;
                    grnLine.Description = purchOrderLine.Description;
                    grnLine.SectionId = purchOrderLine.SectionId;
                    grnLine.BPGroup = purchOrderLine.BPGroup;
                    grnLine.BPGroupDescription = purchOrderLine.BPGroupDescription;
                    grnLine.POUnitPrice = purchOrderLine.UnitPrice;
                    grnLine.LineAmount = purchOrderLine.LineAmount;
                    grnLine.LineAmountTH = purchOrderLine.LineAmountTH;
                    grnLine.GRLineAmount = 0;
                    grnLine.GRLineAmountTH = 0;
                    grnLine.POId = purchTableId;
                    grnLine.SubSection = purchOrderLine.SubSection;
                    grnLine.WorkflowType = purchOrderLine.WorkflowType;
                    if (wfType == indirect)
                    {
                        grnLine.Source = indirect;
                    }
                    else
                    {
                        grnLine.Source = direct;
                    }
                    _grnLineService.CreateGRNLine(grnLine);
                }
                _grnLineService.SaveChanges();

                validate.Success = true;
                return validate;
            }
            catch (Exception ex)
            {
                validate.Success = false;
                validate.MessageError = ex.Message;
                return validate;
            }
        }
        public Validate DuplicatePOLineByVend(long purchTableId, long grTableId, string vendorname)
        {
            var validate = new Validate();
            try
            {
                _grnLineService.Delete(m => m.ParentId == grTableId);
                const string indirect = "Indirect", direct = "Direct";
                var purchOrderLinelist = _PurchLineService.GetpurchLineListByVendor(purchTableId, vendorname);
                int i = 1;
                foreach (var purchOrderLine in purchOrderLinelist)
                {
                    var grnLine = new GRNLine(); var wfType = purchOrderLine.WorkflowType;
                    grnLine.LineNum = i++;
                    grnLine.ParentId = grTableId;
                    grnLine.Items = purchOrderLine.Items;
                    grnLine.ItemCode = purchOrderLine.ItemCode;
                    grnLine.ItemName = purchOrderLine.ItemName;
                    grnLine.ItemNameDescription = purchOrderLine.ItemNameDescription;
                    grnLine.Unit = purchOrderLine.Unit;
                    grnLine.UnitPrice = purchOrderLine.UnitPrice;
                    grnLine.POlineQty = purchOrderLine.Qty;
                    grnLine.PurchLineId = purchOrderLine.PurchLineId;
                    grnLine.Qty = null;
                    //grnLine.QTYRec = this.GetSumReceiveQtyGRNLine(purchOrderLine.PurchLineId);
                    grnLine.QTYRec = this.GetSumReceiveQtyGRNLine(purchOrderLine.PurchLineId);
                    //grnLine.QTYAccrual = purchOrderLine.Qty - grnLine.QTYRec;
                    grnLine.QTYAccrual = 0;
                    //add 17-08
                    grnLine.BPNum = purchOrderLine.BPNum;
                    //grnLine.BudgetType = ;
                    grnLine.RefNumber = purchOrderLine.RefNumber;
                    //grnLine.FiscalYear = ;
                    grnLine.Month = purchOrderLine.Month;
                    //grnLine.BpType = ;
                    grnLine.Description = purchOrderLine.Description;
                    grnLine.SectionId = purchOrderLine.SectionId;
                    grnLine.BPGroup = purchOrderLine.BPGroup;
                    grnLine.BPGroupDescription = purchOrderLine.BPGroupDescription;
                    grnLine.POUnitPrice = purchOrderLine.UnitPrice;
                    grnLine.LineAmount = purchOrderLine.LineAmount;
                    grnLine.LineAmountTH = purchOrderLine.LineAmountTH;
                    grnLine.GRLineAmount = 0;
                    grnLine.GRLineAmountTH = 0;
                    grnLine.POId = purchTableId;
                    grnLine.SubSection = purchOrderLine.SubSection;
                    grnLine.WorkflowType = purchOrderLine.WorkflowType;

                    if (wfType == indirect)
                    {
                        grnLine.Source = indirect;
                    }
                    else
                    {
                        grnLine.Source = direct;
                    }
                    _grnLineService.CreateGRNLine(grnLine);
                }
                _grnLineService.SaveChanges();


                validate.Success = true;
                return validate;
            }
            catch (Exception ex)
            {
                validate.Success = false;
                validate.MessageError = ex.Message;
                return validate;
            }
        }
        //public decimal? GetReceived(long purchLineId)
        //{
        //    var grnmodel = _grnTableService.GetAll(m => m.PurchId == purchLineId && m.Status == "Completed");
        //    var poQty = (decimal?) 0.00;
        //     var received = (decimal?) 0.00;
        //     var remain = (decimal?) 0.00;
        //    foreach (var grn in grnmodel)
        //    {
        //        var grnlinemodel = _grnLineService.GetAll(m => m.ParentId == grn.GRNId);
        //        poQty += grnlinemodel.FirstOrDefault().POlineQty;
        //        received += grnlinemodel.Sum(m => m.Qty);
        //    }
        //    remain += poQty - received;
        //}

        public void GetReceived(long grnid)
        {
            try
            {
                var grnlinemodel = _grnLineService.GetAll( m => m.ParentId == grnid);
                foreach (var grnline in grnlinemodel)
                {
                    var model = from a in _grnLineService.GetAll(m => m.PurchLineId == grnline.PurchLineId).ToList()
                                join b in _grnTableService.GetAll().ToList() on a.ParentId equals
                                    b.GRNId
                                where b.Status == Resource.DocumentStatus.Completed
                                select new GRNLine()
                                {
                                    Qty = a.Qty,
                                    QTYRec = a.QTYRec,
                                    QTYAccrual = a.QTYAccrual,
                                    POlineQty = a.POlineQty
                                };

                    decimal? received = 0, remain = 0;
                    decimal? poqty = grnline.POlineQty;
                    if (model.Any())
                    {
                        received = model.Sum(m => m.Qty);
                        poqty = model.FirstOrDefault().POlineQty;
                        remain = poqty - received;
                    }
                    //else
                    //{
                    //        received = model.Sum(m => m.Qty);
                    //        poqty = model.FirstOrDefault().POlineQty;
                    //        remain = poqty - received;
                    //}
                    //decimal? received = model.Sum(m => m.Qty);
                    //decimal? poqty = model.FirstOrDefault().POlineQty;
                    //decimal? remain = poqty - received;

                    if (received == null || received == 0)
                    {
                        received = 0;
                    }

                    grnline.QTYAccrual = received;
                    grnline.QTYRec = remain;

                    _grnLineService.Update(grnline);
                }
                _grnLineService.SaveChanges();

            }
            catch (Exception exception)
            {
                throw new Exception(exception.Message);
            }
        }
        public Validate GetReceivedvalidate(long grnid)
        {
            try
            {
                var val = new Validate();
                var grnlinemodel = _grnLineService.GetAll(m => m.ParentId == grnid);
                foreach (var grnline in grnlinemodel)
                {
                    var model = from a in _grnLineService.GetAll(m => m.PurchLineId == grnline.PurchLineId).ToList()
                                join b in _grnTableService.GetAll().ToList() on a.ParentId equals
                                    b.GRNId
                                where b.Status == Resource.DocumentStatus.Completed
                                select new GRNLine()
                                {
                                    Qty = a.Qty,
                                    QTYRec = a.QTYRec,
                                    QTYAccrual = a.QTYAccrual,
                                    POlineQty = a.POlineQty
                                };

                    decimal? received = 0, remain = 0;
                    decimal? poqty = grnline.POlineQty;
                    if (model.Any())
                    {
                        received = model.Sum(m => m.Qty);
                        poqty = model.FirstOrDefault().POlineQty;
                        remain = poqty - received;
                    }
                    else
                    {
                        val.MessageError = "1";
                        val.Success = false;
                        return val;
                    }


                    val.MessageError += received;

                    if (received == null || received == 0)
                    {
                        received = 0;
                    }

                    grnline.QTYAccrual = received;
                    grnline.QTYRec = remain;

                    _grnLineService.Update(grnline);
                }
                _grnLineService.SaveChanges();
                return val;

            }
            catch (Exception exception)
            {
                throw new Exception(exception.Message);
            }
        }
        public decimal? GetSumReceiveQtyGRNLine(long purchLineId)
        {
            try
            {
                var model = from a in _grnLineService.GetAll(m => m.PurchLineId == purchLineId).ToList()
                            join b in _grnTableService.GetAll().ToList() on a.ParentId equals
                                b.GRNId
                            where b.Status == Resource.DocumentStatus.Completed
                            select new GRNLine()
                            {
                                Qty = a.Qty,
                                QTYRec = a.QTYAccrual,
                                QTYAccrual = a.QTYAccrual
                            };

                decimal? sum = model.Sum(m => m.Qty);
                return sum;

            }
            catch (Exception exception)
            {
                throw new Exception(exception.Message);
            }
        }

        public List<GRNTable> AllowUnlock(string userid)
        {
            try
            {
                var allowgr = new List<GRNTable>();
                var completed = DocumentStatusFlag.Completed.ToString();

                var receiptmodel = _departmentSetupService.GetReceiptSetupGroupByDepartmentByUserUnlock(userid).ToArray();

                if (receiptmodel.Any())
                {
                    var department = receiptmodel.Select(t => t.Key).ToList();

                    var grmodel = _grnTableService.GetAll(m => m.Status == completed).ToList();
                    var gr = grmodel.Where(m => department.Contains(m.DepartmentId)).ToList();
                    if (gr.Any())
                    {
                        return gr;
                    }
                }
                return allowgr;
            }
            catch (Exception exception)
            {
                throw new Exception(exception.Message);
            }
        }
        public List<GRNTable> GetAllGR()
        {
            try
            {

                return _grnTableService.GetAll().ToListSafe();
            }
            catch (Exception exception)
            {
                throw new Exception(exception.Message);
            }
        }
        public List<VendorMaster> GRNGetVendorByPO(long poid)
        {
            try
            {

                var vendmaster = new List<VendorMaster>();
                var vendgroup = _PurchLineService.GetAll(m => m.PurchId == poid).GroupBy(m => m.VendName);
                var polinemodel = _PurchLineService.GetAll(m => m.PurchId == poid).ToList();

                if (polinemodel.Any())
                {
                    foreach (var vend in vendgroup)
                    {
                        var venditem = new VendorMaster();
                        var val = polinemodel.FirstOrDefault(m => m.VendName == vend.Key);
                        venditem.VendorId = val.VendId;
                        venditem.VendorName = val.VendName;
                        vendmaster.Add(venditem);
                    }
                }

                return vendmaster;
            }
            catch (Exception exception)
            {
                throw new Exception(exception.Message);
            }
        }
        public List<string> GRNGetVendor(long poid)
        {
            try
            {
                var vendorlist = new List<string>();
                var polinemodel = _PurchLineService.GetAll(m => m.PurchId == poid).GroupBy(m => m.VendName).Select(m => m.Key).ToList();

                if (polinemodel.Any())
                {
                    vendorlist = polinemodel;
                }
                return vendorlist;
            }
            catch (Exception exception)
            {
                throw new Exception(exception.Message);
            }
        }
        public void DeletedList()
        {
            try{

                var grmodel = _grnTableService.GetAll(m => m.GRNExtId == null && m.CreateDate < DateTime.Today).ToListSafe();

            if (grmodel.Any())
                {
                    foreach (var gr in grmodel)
                    {
                        var gr1 = gr;
                        var grlinemodel = _grnLineService.GetAll(m => m.ParentId == gr1.GRNId).ToListSafe();

                        if (grlinemodel.Any())
                        {
                            foreach (var grnline in grlinemodel)
                            {
                                var grnline1 = grnline;
                                _grnLineService.Delete(m => m.Id == grnline1.Id);
                            }
                        }
                        _grnTableService.Delete(m => m.GRNId == gr1.GRNId);
                    }
                _grnLineService.SaveChanges();
                _grnTableService.SaveChanges();
                }

            }
            catch (Exception exception)
            {
                throw new Exception(exception.Message);
            }
        }
    }
}