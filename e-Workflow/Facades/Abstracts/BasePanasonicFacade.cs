﻿
using System;
using System.Data;
using System.Data.Entity;
using Microsoft.Ajax.Utilities;
using PanasonicProject.Models;

namespace PanasonicProject.Facades.Abstracts
{
    public abstract class BasePanasonicFacade
    {
        public Validate InitValidate(bool success, string message)
        {
            var validate = new Validate();
            validate.Success = success;
            validate.MessageError = message;
            return validate;
        }
    }
}