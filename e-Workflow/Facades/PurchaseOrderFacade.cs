﻿using System;
using System.Activities.Statements;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic;
using System.Web;
using System.Web.Mvc;
using System.Web.Mvc.Html;
using System.Web.Util;
using Microsoft.Ajax.Utilities;
using PanasonicProject.Models;
using PanasonicProject.Models.Enums;
using PanasonicProject.Models.eRequestSheet;
using PanasonicProject.Resource;
using PanasonicProject.Services;
using PanasonicProject.States;
using PanasonicProject.ViewModels;
using PanasonicProject.ViewModels.PurchaseOrder;
using PanasonicProject.ViewModels.Report;

namespace PanasonicProject.Facades
{
    public interface IPurchaseOrderFacade
    {

    }

    public class PurchaseOrderFacade
    {
        private readonly PurchTableService _purchTableService;
        private readonly PurchLineService _purchLineService;
        private readonly ValidationAmountSetupService _validationAmountSetupService;
        private readonly SectionTableService _sectionService;
        private readonly GRNTableService _gRNTableService;
        private readonly GRNLineService _gRNLineService;
        private readonly ApplicationFormService _applicationFormService;
        private readonly ApplFormBudgetService _applFormBudgetService;
        private readonly ReceiptSetupService _receiptSetupService;
        private readonly DepartmentSetupService _departmentSetupService;
        private readonly NumberSequenceTableService _numberSequenceTableService;

        public PurchaseOrderFacade(PurchLineService purchLineService, PurchTableService purchTableService, ValidationAmountSetupService validationAmountSetupService, SectionTableService sectionService, GRNTableService gRnTableService, GRNLineService gRnLineService, ApplicationFormService applicationFormService, ApplFormBudgetService applFormBudgetService, ReceiptSetupService receiptSetupService, DepartmentSetupService departmentSetupService, NumberSequenceTableService numberSequenceTableService = null)
        {
            _purchLineService = purchLineService;
            _purchTableService = purchTableService;
            _validationAmountSetupService = validationAmountSetupService;
            _sectionService = sectionService;
            _gRNTableService = gRnTableService;
            _gRNLineService = gRnLineService;
            _applicationFormService = applicationFormService;
            _applFormBudgetService = applFormBudgetService;
            _receiptSetupService = receiptSetupService;
            _departmentSetupService = departmentSetupService;
            _numberSequenceTableService = numberSequenceTableService;
        }

        public IEnumerable<PurchaseOrderIQueryViewModel> GetPurchLineFormCriteriaReport(BaseCriteriaReport criteria)
        {
            var modelHeads = _purchTableService.GetPOFormCriteraiReport(criteria);

            var modelLines = from line in _purchLineService.GetAll().ToListSafe()
                             join modelHead in modelHeads.ToListSafe()
                                 on line.PurchId equals modelHead.PurchId
                             select new PurchaseOrderIQueryViewModel { PurchId = line.PurchId, ApplId = line.ApplId, VendName = line.VendName, IsImport = modelHead.IsImport };

            return modelLines.ToListSafe();
        }



        public long CopyPurchaseOrder(long? primaryId, string documentnumber)
        {
            var modelCopyFrom = _purchTableService.Load(m => m.PurchId == primaryId);
            var modelCopyTo = new PurchTable();
            modelCopyTo = modelCopyFrom;
            modelCopyTo.PurchNum = documentnumber;
            modelCopyTo.ExtPurchDate = null;
            modelCopyTo.ExtPurchNum = null;
            modelCopyTo.CreatedDate = DateTime.Now;
            modelCopyTo.POStatus = DocumentStatus.Draft;
            modelCopyTo.DocumentStatus = "Creating";
            modelCopyTo.ReceiptStatus = "Open Order";
            modelCopyTo.IsCancel = null;
            modelCopyTo.CancelledDate = null;
            modelCopyTo.CancelDate = null;
            modelCopyTo.SubmitedDate = null;
            modelCopyTo.FinishedDate = null;
            modelCopyTo.SubmitedDate = null;
            modelCopyTo.FinishedDate = null;
            modelCopyTo.RemarkCancel = null;

            modelCopyTo = _purchTableService.Create(modelCopyTo);
            _purchTableService.SaveChanges();

            var modelLineCopyFrom = _purchLineService.GetpurchLineList(primaryId);

            foreach (var t in modelLineCopyFrom)
            {
                var modelLineCopyTo = new PurchLine();
                t.PurchId = modelCopyTo.PurchId;
                modelLineCopyTo = t;
                _purchLineService.Create(modelLineCopyTo);
            }

            _purchLineService.SaveChanges();

            return modelCopyTo.PurchId;
        }

        public Validate CheckBPBeforeSubmit(long primaryId)
        {
            var validatemsg = new Validate();
            var potable = _purchTableService.Load(m => m.PurchId == primaryId);
            var potableAppId = (long?)null;
            if (potable != null)
            {
                potableAppId = potable.ApplId;
            }

            var poline = _purchLineService.GetFirstOnly(m => m.PurchId == primaryId);
            if (poline != null)
            {
                validatemsg.Success = true;
                if (potableAppId == null && poline.ApplId != null)
                {
                    //validatemsg.MessageError = PECTHResource.ValidPOMustNotRefAPP;
                    validatemsg.MessageError = PECTHResource.ValidPONotMacth;
                }
                else if (potableAppId != null && poline.ApplId == null)
                {
                    // validatemsg.MessageError = PECTHResource.ValidPOMustRefAPP;
                    validatemsg.MessageError = PECTHResource.ValidPONotMacth;
                }
            }
            else
            {
                validatemsg.Success = false;
            }
            return validatemsg;
        }
        public List<ValidationAmountSetup> GetValidationAmountSetup(string workflowType)
        {
            return _validationAmountSetupService.GetValidationAmountSetupLineByWorkflowType(workflowType).ToList();
        }
        public Validate CheckPOBeforeSubmit(long primaryId)
        {
            var validatemsg = new Validate();

            var polinetable = _purchLineService.GetFirstOnly(m => m.PurchId == primaryId);
            var refApp = (bool?)false;
            if (polinetable != null)
            {
                refApp = polinetable.RefAPP;
            }

            var potable = _purchTableService.Load(m => m.PurchId == primaryId);
            if (potable != null)
            {
                if (!String.IsNullOrEmpty(potable.WorkflowType))
                {
                    validatemsg.Success = true;
                    if (refApp == false || refApp == null)
                    {
                        validatemsg.MessageError = GetMsgValidation(potable.WorkflowType, potable.TotalAmountTH,
                            potable.ApplId);
                    }
                    else if (refApp == true)
                    {
                        validatemsg.MessageError = GetMsgValidationRefAPP(potable.ApplId, potable.WorkflowType);
                    }
                }
            }
            else
            {
                validatemsg.Success = false;
            }
            return validatemsg;
        }
        public string GetMsgValidation(string workflowtype, decimal? totalamount, long? appid)
        {
            var msg = (string)null;
            if (String.IsNullOrEmpty(workflowtype) || totalamount == null)
            {
                return PECTHResource.ValidIsNull;
            }

            var amounttable = _validationAmountSetupService.GetValidationAmountSetupLineByWorkflowType(workflowtype).ToList();
            if (amounttable.Any())
            {
                var amountline =
                    amounttable.FirstOrDefault(m => m.TotalAmountEnd >= totalamount);
                if (amountline == null) return msg;
                if (amountline.RefAPP != null && (appid == null && (bool)amountline.RefAPP))
                {
                    msg = PECTHResource.ValidPOMustRefAPP;
                }
                else if (appid != null && (bool)!amountline.RefAPP)
                {
                    msg = PECTHResource.ValidPOMustNotRefAPP;
                }
            }
            return msg;
        }

        public string GetMsgValidationRefAPP(long? appid, string workflowtype)
        {
            var msg = (string)null;
            //todo enum
            if (appid == null && workflowtype == "FixAssets")
            {
                msg = PECTHResource.ValidPOMustRefAPP;
            }
            return msg;
        }
        public void UpdatePOBeforeSubmit(long primaryId)
        {
            var potable = _purchTableService.Load(m => m.PurchId == primaryId);
            if (potable == null) return;
            var poline = _purchLineService.GetFirstOnly(m => m.PurchId == primaryId);
            if (poline == null) return;
            var pototalamountth = _purchLineService.GetAll(m => m.PurchId == primaryId).Sum(m => m.LineAmountTH);
            if (pototalamountth == null) return;

            potable.WorkflowType = poline.WorkflowType;
            potable.SectionId = poline.SectionId;
            potable.TotalAmountTH = pototalamountth;
            _purchTableService.Update(potable);
            _purchTableService.SaveChanges();
        }

        public List<PurchTable> GetPOByUser(string userId)
        {
            var potable = new List<PurchTable>();

            var department = _sectionService.GetAll(m => m.Manager == userId).GroupBy(m => m.Department);

            var departmentByGM = _sectionService.GetAll(m => m.GM == userId).GroupBy(m => m.Department);
            var enumerable = department as IGrouping<string, SectionTable>[] ?? department.ToArray();
            var departmentByGm = departmentByGM as IGrouping<string, SectionTable>[] ?? departmentByGM.ToArray();
            if (!enumerable.Any() && !departmentByGm.Any())
            {
                return new List<PurchTable>();
            }
            if (departmentByGm.Any())
            {
                enumerable = departmentByGm;
            }
            foreach (var gm in enumerable)
            {
                var podepartment = _purchTableService.GetAll(m => m.DepartmentId == gm.Key);
                foreach (var poline in podepartment)
                {
                    potable.Add(poline);
                }
            }
            return potable;
        }
        public List<PurchTable> GetPOByDepartmentUser(string userId)
        {
            var potable = new List<PurchTable>();
            var department = _departmentSetupService.GetDepartmentSetupLineByUser(userId, true)
                              .GroupBy(m => m.DepartmentId);

            foreach (var line in department)
            {
                var podepartment = _purchTableService.GetAll(m => m.DepartmentId == line.Key);
                foreach (var poline in podepartment)
                {
                    potable.Add(poline);
                }
            }
            return potable;
        }
        public StateValidate StateValidateGetPOByUser(string userId)
        {
            var res = new StateValidate();
            var potable = new List<PurchTable>();


            var preparedepartment = _sectionService.GetAll(m => m.Manager == userId);
            res.Condition += preparedepartment.ToString();

            var department = preparedepartment.GroupBy(m => m.Department);
            res.StateName += department.ToString();


            var preparedepartmentByGM = _sectionService.GetAll(m => m.GM == userId);
            res.Condition += preparedepartment.ToString();

            var departmentByGM = preparedepartmentByGM.GroupBy(m => m.Department);
            res.StateName += departmentByGM.ToString();
            var departmentByGm = departmentByGM as IGrouping<string, SectionTable>[] ?? departmentByGM.ToArray();
            var enumerable = department as IGrouping<string, SectionTable>[] ?? department.ToArray();
            if (!enumerable.Any() && !departmentByGm.Any())
            {

                res.MessageError += "!department.Any() && !departmentByGM.Any()";
                return res;
            }
            if (departmentByGm.Any())
            {
                enumerable = departmentByGm;
                res.MessageError += "has departmentByGM";
            }
            foreach (var gm in enumerable)
            {
                res.MessageError += "has departmentonly";
                var podepartment = _purchTableService.GetAll(m => m.DepartmentId == gm.Key);
                foreach (var poline in podepartment)
                {
                    potable.Add(poline);
                    res.MessageError += "loop";
                }

            }
            res.MessageError += "finish";
            return res;
        }
        public decimal? CheckPOAmount(long? appid)
        {
            var stateservice = DependencyResolver.Current.GetService<AppBudgetState>();
            var poamount = (decimal?)0;
            var approved = DocumentStatusFlag.Approved.ToString();
            var po = _purchTableService.GetAll(m => m.POStatus == approved && m.ApplId == appid).ToList();
            var pojoinmodel = from line in _purchLineService.GetAll(m => m.ApplId == appid).ToList()
                              join modelHead in po.ToList()
                                      on line.PurchId equals modelHead.PurchId
                              select new ApplFormBudgetTable { BPNum = line.BPNum, BPReserve = line.LineAmountTH };


            if (pojoinmodel.Any())
            {
                //ext
                //decimal? poused = 0;
                //var appmodel = _applicationFormService.Load(m => m.ApplId == appid);
                //if (appmodel.IsClosed == approved)
                //{
                //    if
                //    foreach (var bp in pojoinmodel)
                //    {
                //        poused += stateservice.GetBPAmountApplFormBudget(bp.BPNum);
                //    }
                //}
                //else
                //{
                poamount = pojoinmodel.Sum(m => m.BPReserve);
                //}

            }
            return poamount;
        }
        public List<PurchTable> AllowReceiptByDepartment(string userid)
        {
            try
            {
                var allowpo = new List<PurchTable>();
                var approved = DocumentStatusFlag.Approved.ToString();
                var closed = ReceiptStatus.Closed.ToString();

                var receiptmodel = _departmentSetupService.GetReceiptSetupGroupByDepartmentByUserReceived(userid).ToArray();

                if (receiptmodel.Any())
                {
                    var department = receiptmodel.Select(t => t.Key).ToList();

                    var pomodel = _purchTableService.GetAll(m => m.POStatus == approved && m.ReceiptStatus != closed).ToList();
                    var po = pomodel.Where(m => department.Contains(m.DepartmentId)).ToList();
                    if (po.Any())
                    {
                        return po;
                    }
                }
                return allowpo;
            }
            catch (Exception exception)
            {
                throw new Exception(exception.Message);
            }
        }
        public List<PurchTable> AllowReceipt(string userid)
        {
            try
            {
                var allowpo = new List<PurchTable>();
                var approved = DocumentStatusFlag.Approved.ToString();
                var closed = ReceiptStatus.Closed.ToString();

                var receiptmodel = _receiptSetupService.GetReceiptSetupGroupByDepartmentByUser(userid).ToArray();

                if (receiptmodel.Any())
                {
                    var department = receiptmodel.Select(t => t.Key).ToList();

                    var pomodel = _purchTableService.GetAll(m => m.POStatus == approved && m.ReceiptStatus != closed).ToList();
                    var po = pomodel.Where(m => department.Contains(m.DepartmentId)).ToList();
                    if (po.Any())
                    {
                        return po;
                    }
                }
                return allowpo;
            }
            catch (Exception exception)
            {
                throw new Exception(exception.Message);
            }
        }
        public Validate UpdateStatusClosed(long purchId)
        {
            var val = new Validate();
            var pomodel = _purchTableService.Load(m => m.PurchId == purchId);
            if (pomodel != null)
            {
                if (pomodel.ApplId == null) // not ref app
                {
                    var bpgroup = _gRNLineService.GetAll(m => m.POId == purchId).GroupBy(m => m.BPNum);
                    val.MessageError += bpgroup;
                    foreach (var pgroup in bpgroup)
                    {
                        var grn = _gRNLineService.GetAll(m => m.BPNum == pgroup.Key && m.POId == purchId);
                        var totalused = grn.Sum(m => m.GRLineAmountTH);
                        var totalremain = grn.Sum(m => m.LineAmountTH);
                        val.MessageError += totalused;
                        val.MessageError += pgroup.Key;
                        var grnfirst = _gRNLineService.GetFirstOnly(m => m.BPNum == pgroup.Key);
                        val.MessageError += grnfirst;
                        var poline = _purchLineService.GetAll(m => m.PurchId == purchId && m.BPNum == grnfirst.BPNum).FirstOrDefault();
                        val.MessageError += poline;
                        if (poline == null) continue;
                        val.MessageError += "prepare";
                        poline.GRBudgetUsed = totalused;
                        poline.GRBudgetRemain = totalremain;
                        _purchLineService.Update(poline);
                        val.MessageError += "gg";
                    }
                    _purchLineService.SaveChanges();
                    val.Success = true;

                }
                //else // ref app
                //{
                //    var bpgroup = _gRNLineService.GetAll(m => m.POId == purchId).GroupBy(m => m.BPNum);
                //    val.MessageError += bpgroup;
                //    foreach (var pgroup in bpgroup)
                //    {
                //        var grn = _gRNLineService.GetAll(m => m.BPNum == pgroup.Key);
                //        var totalused = grn.Sum(m => m.GRLineAmountTH);
                //        var totalremain = grn.Sum(m => m.LineAmountTH);
                //        val.MessageError += totalused;
                //        val.MessageError += pgroup.Key;
                //        var grnfirst = _gRNLineService.GetFirstOnly(m => m.BPNum == pgroup.Key);
                //        val.MessageError += grnfirst;
                //        var appline = _applFormBudgetService.GetAll(m => m.ApplId == pomodel.ApplId && m.BPNum == grnfirst.BPNum).FirstOrDefault();
                //        if (appline == null) continue;
                //        val.MessageError += "prepare";
                //        appline.GRBudgetUsed = totalused;
                //        appline.GRBudgetRemain = totalremain;
                //        _applFormBudgetService.Update(appline);
                //        val.MessageError += "gg";
                //    }
                //    _applFormBudgetService.SaveChanges();
                //    val.Success = true;
                //}
                else // ref app
                {
                    var bpgroup = _gRNLineService.GetAll(m => m.POId == purchId).GroupBy(m => m.BPNum);
                    val.MessageError += bpgroup;
                    foreach (var pgroup in bpgroup)
                    {
                        var grn = _gRNLineService.GetAll(m => m.BPNum == pgroup.Key && m.POId == purchId);
                        var totalused = grn.Sum(m => m.GRLineAmountTH);
                        var totalremain = grn.Sum(m => m.LineAmountTH);
                        val.MessageError += totalused;
                        val.MessageError += pgroup.Key;
                        var grnfirst = _gRNLineService.GetFirstOnly(m => m.BPNum == pgroup.Key);
                        val.MessageError += grnfirst;
                        var appline = _applFormBudgetService.GetAll(m => m.ApplId == pomodel.ApplId && m.BPNum == grnfirst.BPNum).FirstOrDefault();
                        if (appline == null) continue;
                        val.MessageError += "prepare";
                        appline.GRBudgetUsed = totalused;
                        appline.GRBudgetRemain = totalremain;
                        _applFormBudgetService.Update(appline);
                        val.MessageError += "gg";
                    }
                    _applFormBudgetService.SaveChanges();
                    val.Success = true;
                }
            }
            return val;
        }


        public StateValidate CheckPOOnProcess(long poid)
        {
            var val = new StateValidate { Success = false };
            var pomodel = _purchTableService.Load(m => m.PurchId == poid);
            if (pomodel != null)
            {
                if (pomodel.ApplId != null)
                {
                    val.Success = true;
                    var appmodel = _applicationFormService.Load(m => m.ApplId == pomodel.ApplId);
                    if (appmodel.Status == "On process" || appmodel.Status == "On Process")
                    {
                        val.MessageError = string.Format("ไม่สามารถใช้เอกสาร (APP:{0}) ใบนี้ได้ เนื่องจากเอกสารใบนี้อยู่ในขั้นตอนอนุมัติแล้ว", appmodel.ApplNo);
                    }
                }

            }



            return val;
        }

        public string ConvertRequestSheetToPurchOrder(int id)
        {
            var requestService = new K2_eRequestSheetEntities();
            var draftPOTable = requestService.ERS_T_DraftPOHeader.FirstOrDefault(m => m.DraftPOHeaderID == id);
            var draftPOLines = requestService.ERS_T_DraftPODetail.Where(m => m.DraftPOHeaderID == id).ToListSafe();

            var applicationForm = new ApplicationForm();
            if (draftPOTable.AppID.HasValue)
            {
                applicationForm = _applicationFormService.Load(m => m.ApplId == draftPOTable.AppID);
            }

            List<int?> prIdLines = draftPOLines.Select(s => s.PRDetail_ID).ToListSafe();
            List<String> prNumbers = draftPOLines.Select(s => s.PRNo).ToListSafe().Distinct().ToListSafe();

            var prTables = requestService.ERS_T_RequestHeader.Where(m => prNumbers.Contains(m.PRNumber)).ToListSafe().Distinct().ToListSafe();

            var poTable = new PurchTable();
            poTable.SectionId = draftPOTable.Section;
            poTable.DepartmentId = draftPOTable.Department;
            poTable.FactoryId = draftPOTable.Division;
            poTable.FactoryName = draftPOTable.DivisionName;
            poTable.ApplId = draftPOTable.AppID;
            poTable.CreatedBy = draftPOTable.CreateBy;
            poTable.CreatedDate = DateTime.Now;
            poTable.POStatus = "Draft";
            poTable.CurrencyCode = draftPOTable.CurrencyCode;
            poTable.CurrencyRate = draftPOTable.CurrencyRate;

            poTable.ApplNo = applicationForm.ApplNo;
            poTable.PurchReqNum = "";
            poTable.Orderer = "";
            poTable.FromRequestSheet = true;

            poTable.PurchNum = _numberSequenceTableService.GetNewNumberDoc("PO");

            foreach (var prNumber in draftPOLines.Select(m => m.PRNo).ToListSafe().Distinct())
            {
                poTable.PurchReqNum = string.Format("{0},{1}", poTable.PurchReqNum, prNumber);
            }

            foreach (var createdBy in prTables.Select(m => m.CreateBy).ToListSafe().Distinct())
            {
                poTable.Orderer = string.Format("{0},{1}", poTable.Orderer, createdBy);
            }


            poTable = _purchTableService.Create(poTable);
            _purchTableService.SaveChanges();

            foreach (var poDraftLine in draftPOLines)
            {
                var poTableLine = new PurchLine();
                poTableLine.PurchId = poTable.PurchId;
                poTableLine.Items = poDraftLine.ItemCode;
                poTableLine.ItemName = poDraftLine.Description;
                poTableLine.Qty = poDraftLine.Qty;
                poTableLine.UnitPrice = poDraftLine.UnitPrice;
                poTableLine.LineAmountTH = poDraftLine.UnitPrice * poDraftLine.Qty;
                poTableLine.LineAmount = (poDraftLine.UnitPrice * draftPOTable.CurrencyRate) * poDraftLine.Qty;
                poTableLine.SectionId = draftPOTable.Section;
                poTableLine.CurrencyRate = draftPOTable.CurrencyRate;
                poTableLine.CurrencyId = draftPOTable.CurrencyCode;
                poTableLine.CurrencyFullName = draftPOTable.CurrencyFullName;
                poTableLine.ApplId = draftPOTable.AppID;

                _purchLineService.Create(poTableLine);
            }

            _purchLineService.SaveChanges();

            foreach (var draftPOLine in draftPOLines)
            {
                var requestUpdatePRService = new K2_eRequestSheetEntities();
                var line = requestUpdatePRService.ERS_T_RequestDetail.FirstOrDefault(m => m.RequestDTID == draftPOLine.PRDetail_ID);
                line.OpenPOAMT = line.OpenPOAMT + draftPOLine.Qty;
                requestUpdatePRService.SaveChanges();
            }

            return poTable.PurchNum;
        }

        public string ConvertRequestSheetToApplication(string id, string sectionId, string departmentId, string username, string divisionId, string budgetType)
        {
            var requestService = new K2_eRequestSheetEntities();

            var appNo = _numberSequenceTableService.GetNewNumberAcc("AP");

            var app = new ApplicationForm();
            app.SectionId = sectionId;
            app.DepartmentId = departmentId;
            app.RequesterBy = username;
            app.RequesterDate = DateTime.Now;
            app.CreatedName = username;
            app.CreatedDate = DateTime.Now;
            app.DivisionId = divisionId;
            app.ApplNo = appNo;
            app.ApplTypeSelect = budgetType;

            _applicationFormService.Create(app);
            _applicationFormService.SaveChanges();

            var ids = id.Contains(",") ? id.Split(',').ToListSafe() : new List<string>() { id };

            foreach (var prId in ids)
            {
                if (string.IsNullOrEmpty(prId))
                {
                    continue;
                }

                int k = Convert.ToInt16(prId);
                var prTable = requestService.ERS_T_RequestHeader.FirstOrDefault(m => m.RequestHDID == k);
                prTable.ApplicationID = (int)app.ApplId;
                requestService.SaveChanges();
            }

            return appNo;
        }

        public bool DeletePOLineWithPR(int id)
        {
            var poLine = _purchLineService.Load(m => m.PRDetailId == id);
            var requestUpdatePRService = new K2_eRequestSheetEntities();
            var line = requestUpdatePRService.ERS_T_RequestDetail.FirstOrDefault(m => m.RequestDTID == poLine.PRDetailId);
            line.OpenPOAMT = line.OpenPOAMT + poLine.Qty;
            requestUpdatePRService.SaveChanges();

            _purchLineService.DeletePurchLine(id);
            _purchLineService.SaveChanges();

            return true;
        }

        public bool DeletePOTableWithPR(int id)
        {
            var poTable = _purchTableService.Load(m => m.PurchId == id);
            var poLines = _purchLineService.GetAll(m => m.PurchId == id).ToListSafe();

            foreach(var poLine in poLines)
            {
                var requestUpdatePRService = new K2_eRequestSheetEntities();
                var line = requestUpdatePRService.ERS_T_RequestDetail.FirstOrDefault(m => m.RequestDTID == poLine.PRDetailId);
                line.OpenPOAMT = line.OpenPOAMT + poLine.Qty;
                requestUpdatePRService.SaveChanges();
            }

            _purchLineService.DeletePurchLine(id);
            _purchLineService.SaveChanges();

            _purchTableService.Delete(poTable);
            _purchTableService.SaveChanges();

            return true;
        }

        public ERS_T_RequestHeader GetPRTable(long? id)
        {
            if (!id.HasValue)
                return null;

            var requestUpdatePRService = new K2_eRequestSheetEntities();
            var result = requestUpdatePRService.ERS_T_RequestHeader.FirstOrDefault(m => m.RequestHDID == id);
            return result;
        }
    }
}