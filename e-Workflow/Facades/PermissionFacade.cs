﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using PanasonicProject.Facades.Abstracts;
using PanasonicProject.Models.Permission;
using PanasonicProject.Resource;
using PanasonicProject.Services.Permission;
using PanasonicProject.ViewModels;

namespace PanasonicProject.Facades
{
    public interface IPermissionFacade
    {

    }

    public class PermissionFacade : BasePanasonicFacade, IPermissionFacade
    {
        private readonly PermissionService _permissionService;
        private readonly PermissionMemberService _permissionMemberService;

        public PermissionFacade(PermissionService permissionService, PermissionMemberService permissionMemberService)
        {
            _permissionService = permissionService;
            _permissionMemberService = permissionMemberService;
        }

        public List<PermissionTable> GetPermissionTable()
        {
            try
            {
                return _permissionService.GetAll().ToList();
            }
            catch (Exception exception)
            {
                throw new Exception(exception.Message);
            }
        }

        public bool IsAuth(string module,string type, string username)
        {
            try
            {
                var permission = _permissionService.GetAll(m=>(m.Module == module && m.PermissionType == type) || (m.Module == module && m.PermissionType == PermissionResource.Type_ALL)).Select(s=>s.PermissionId).ToList();
                if (permission.Count() != 0)
                {
                    var permissionMember =
                    _permissionMemberService.Exists(m => m.FQN == username && permission.Contains(m.PermissionId));

                    return permissionMember;
                }

                return false;
            }
            catch (Exception exception)
            {
                throw new Exception(exception.Message);
            }
        }

        public List<AuthorizeViewModel> GetPermissionByUser(string username)
        {
            try
            {
               // throw new Exception("Hello");
                //throw new Exception(username);
                var userPermissionMember = _permissionMemberService.GetAll(m => m.FQN == username).ToList();
              //  throw new Exception(username);
                var model = new List<AuthorizeViewModel>();

                foreach (var u in userPermissionMember)
                {
                    var authModel = new AuthorizeViewModel();
                    
                    var permissionTable = _permissionService.GetFirstOnly(m => m.PermissionId == u.PermissionId);
                    
                    if (permissionTable != null)
                    {
                        authModel.Module = permissionTable.Module;
                        authModel.Type = permissionTable.PermissionType;
                        model.Add(authModel);
                    }
                }

                return model;
            }
            catch (Exception exception)
            {
                throw new Exception(exception.Message);
            }
        }
    }
}