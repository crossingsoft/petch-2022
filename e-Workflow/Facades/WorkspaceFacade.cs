﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using iTextSharp.text.pdf.qrcode;
using PanasonicProject.Services;

namespace PanasonicProject.Facades
{
    public class WorkspaceFacade : Abstracts.BasePanasonicFacade
    {
        private readonly ApplicationFormService _applicationFormService;
        private readonly PurchTableService _purchTableService;
        private readonly WorkflowService _WorkflowService;

        public WorkspaceFacade(ApplicationFormService applicationFormService, PurchTableService purchTableService, WorkflowService workflowService)
        {
            _applicationFormService = applicationFormService;
            _purchTableService = purchTableService;
            _WorkflowService = workflowService;
        }

        public void UpdateActiveBy(string sn)
        {
            var snSplit = sn.Split('_');
            if (snSplit.Any())
            {
                string procInst = snSplit.First();
                long procInstInt = Convert.ToInt64(procInst);

                if (_applicationFormService.Exists(m => m.ProcessInstance == procInst))
                {
                    var model = _applicationFormService.GetFirstOnly(m => m.ProcessInstance == procInst);
                    if (model != null)
                    {
                        var user = HttpContext.Current.User as PanasonicProject.Infrastructure.Security.IPECTHPrincipal;
                        if (user != null) model.ActiveBy = user.FullName;
                        _applicationFormService.SaveChanges();
                    }
                }
                else if (_purchTableService.Exists(m => m.ProcessInstance == procInstInt))
                {
                    var model = _purchTableService.GetFirstOnly(m => m.ProcessInstance == procInstInt);
                    if (model != null)
                    {
                        var user = HttpContext.Current.User as PanasonicProject.Infrastructure.Security.IPECTHPrincipal;
                        if (user != null) model.ActiveBy = user.FullName;
                        _purchTableService.SaveChanges();
                    }
                }

            }
            
        }

        public void ClearActiveBy(string sn)
        {
            var snSplit = sn.Split('_');
            if (snSplit.Any())
            {
                string procInst = snSplit.First();
                long procInstInt = Convert.ToInt64(procInst);

                if (_applicationFormService.Exists(m => m.ProcessInstance == procInst))
                {
                    var model = _applicationFormService.GetFirstOnly(m => m.ProcessInstance == procInst);
                    if (model != null)
                    {
                        var user = HttpContext.Current.User as PanasonicProject.Infrastructure.Security.IPECTHPrincipal;
                        if (user != null) model.ActiveBy = "";
                        _applicationFormService.SaveChanges();
                    }
                }
                else if (_purchTableService.Exists(m => m.ProcessInstance == procInstInt))
                {
                    var model = _purchTableService.GetFirstOnly(m => m.ProcessInstance == procInstInt);
                    if (model != null)
                    {
                        var user = HttpContext.Current.User as PanasonicProject.Infrastructure.Security.IPECTHPrincipal;
                        if (user != null) model.ActiveBy = "";
                        _purchTableService.SaveChanges();
                    }
                }

            }

        }


    }
}