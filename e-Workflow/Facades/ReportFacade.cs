﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.DynamicData;
using iTextSharp.text.pdf.qrcode;
using Microsoft.Reporting.WebForms;
using PanasonicProject.Models;
using PanasonicProject.Models.StoredProcedures;
using PanasonicProject.Resource;
using PanasonicProject.Services;
using PanasonicProject.Services.Report;
using PanasonicProject.ViewModels.Report;

namespace PanasonicProject.Facades
{
    public interface IReportFacade
    {
        BaseReportDataSource ReportSummary(DateTime? date);
        BaseReportDataSource ReportSummaryBPType(DateTime? date);
        BaseReportDataSource ReportSummaryMatrixSection(DateTime? date);
    }

    public class ReportFacade : Abstracts.BasePanasonicFacade
    {
        //private readonly IBusinessPlanLineService _businessPlanLineService;
        //private readonly ISummaryReportService _summaryReportService;
        //private readonly ISummaryReportBPTypeService _summaryReportBpTypeService;
        //private readonly ISummaryReportMatrixSection _summaryReportMatrixSection;

        private readonly ApplicationFormService _applicationFormService;
        private readonly EntityDataConnectionStoredProcedures _entityDataConnectionStored;
        private readonly PurchaseOrderFacade _purchaseOrderFacade;
        private readonly ADUserTableService _adUserTableService;
        private readonly MST_RequestReportNameService _mstRequestReportNameService;
        private readonly MST_SubRequestReportNameService _mstSubRequestReportNameService;
        private readonly PurchTableService _purchTableService;
        private readonly PurchLineService _PurchLineService;
        private readonly ApplicationApprovalHistoryService _applicationApprovalHistoryService;
        private readonly SectionTableService _sectionTableService;
        private readonly DepartmentTableService _departmentTableService;
        private readonly DivisionTableService _divisionTableService;
        //public ReportFacade(IBusinessPlanLineService businessPlanLineService, ISummaryReportService summaryReportService, ISummaryReportBPTypeService summaryReportBpTypeService, ISummaryReportMatrixSection summaryReportMatrixSection, IApplicationFormService applicationFormService, DbContext dbContext)
        //{
        //    _businessPlanLineService = businessPlanLineService;
        //    _summaryReportService = summaryReportService;
        //    _summaryReportBpTypeService = summaryReportBpTypeService;
        //    _summaryReportMatrixSection = summaryReportMatrixSection;
        //    _applicationFormService = applicationFormService;
        //    _dbContext = dbContext;
        //}
        public ReportFacade(ApplicationFormService applicationFormService, EntityDataConnectionStoredProcedures entityDataConnectionStored, PurchaseOrderFacade purchaseOrderFacade, ADUserTableService adUserTableService, MST_RequestReportNameService mstRequestReportNameService, MST_SubRequestReportNameService mstSubRequestReportNameService, PurchTableService purchTableService, PurchLineService purchLineService, ApplicationApprovalHistoryService applicationApprovalHistoryService, SectionTableService sectionTableService, DepartmentTableService departmentTableService, DivisionTableService divisionTableService)
        {
            _applicationFormService = applicationFormService;
            _entityDataConnectionStored = entityDataConnectionStored;
            _purchaseOrderFacade = purchaseOrderFacade;
            _adUserTableService = adUserTableService;
            _mstRequestReportNameService = mstRequestReportNameService;
            _mstSubRequestReportNameService = mstSubRequestReportNameService;
            _purchTableService = purchTableService;
            _PurchLineService = purchLineService;
            _applicationApprovalHistoryService = applicationApprovalHistoryService;
            _sectionTableService = sectionTableService;
            _departmentTableService = departmentTableService;
            _divisionTableService = divisionTableService;
        }
        //public BaseReportDataSource ReportSummary(DateTime? date)
        //{
        //    var businessPlanModel =_businessPlanLineService;
        //    var url = String.Empty;
        //    var filename = String.Empty;
        //    var datasource = new BaseReportDataSource
        //    {
        //        URL = url,
        //        FileName = filename,
        //        ReportDataSources = new ReportDataSource[2]
        //    };
        //    datasource.ReportDataSources[0] = new ReportDataSource("header", _summaryReportService.GetHeader());
        //    datasource.ReportDataSources[1] = new ReportDataSource("body", _summaryReportService.GetBody());
        //    return datasource;
        //}

        //public BaseReportDataSource ReportSummaryBPType(DateTime? date)
        //{
        //    var table = new List<BusinessPlanLine>();
        //    var url = String.Empty;
        //    var filename = String.Empty;
        //    var datasource = new BaseReportDataSource
        //    {
        //        URL = url,
        //        FileName = filename,
        //        ReportDataSources = new ReportDataSource[4]
        //    };
        //    datasource.ReportDataSources[0] = new ReportDataSource("header", _summaryReportBpTypeService.GetHeader());
        //    datasource.ReportDataSources[1] = new ReportDataSource("body_Expense", _summaryReportBpTypeService.GetBodyExpense(table));
        //    datasource.ReportDataSources[2] = new ReportDataSource("body_FixedAsset", _summaryReportBpTypeService.GetBodyFixedAsset(table));
        //    datasource.ReportDataSources[3] = new ReportDataSource("body_Repair", _summaryReportBpTypeService.GetBodyRepair(table));
        //    return datasource;
        //}

        //public BaseReportDataSource ReportSummaryMatrixSection(DateTime? date)
        //{
        //    var url = String.Empty;
        //    var filename = String.Empty;
        //    var datasource = new BaseReportDataSource
        //    {
        //        URL = url,
        //        FileName = filename,
        //        ReportDataSources = new ReportDataSource[2]
        //    };
        //    datasource.ReportDataSources[0] = new ReportDataSource("header", _summaryReportService.GetHeader());
        //    datasource.ReportDataSources[1] = new ReportDataSource("body", _summaryReportService.GetBody());
        //    return datasource;
        //}


        public List<byte[]> GetListPOAndFriendsFormReport(BaseCriteriaReport criteria)
        {
            var models = _purchaseOrderFacade.GetPurchLineFormCriteriaReport(criteria).ToListSafe();
            var groupByte = new List<byte[]>();
            var chkApplicationForm = new List<long>();
            var chkPOLineForm = new List<PurchLine>();

            int vendNo = 1;
            foreach (var t in models.OrderBy(m => m.ApplId))
            {
                if (!chkPOLineForm.Any(m => m.PurchId == t.PurchId && m.VendName == t.VendName))
                {
                    if (t.IsImport == null || t.IsImport == false)
                        groupByte.Add(LoadPOFormLocalReport(t.PurchId, t.VendName, false, "- "+vendNo,false));
                    else
                        groupByte.Add(LoadPOFormReport(t.PurchId, t.VendName, false, "- " + vendNo));

                    var poLineForm = new PurchLine();
                    poLineForm.PurchId = t.PurchId;
                    poLineForm.VendName = t.VendName;

                    var sumGroupeModel = models.Where(m => m.VendName == t.VendName).ToListSafe();
                    decimal sumInModel = sumGroupeModel.Count();
                    var sumGroupeCurrentLine = chkPOLineForm.Where(m => m.VendName == t.VendName);
                    decimal sumCurrentGroupe = sumGroupeCurrentLine.Count();

                    //if (chkPOLineForm.Any(m => m.PurchId == t.PurchId))
                    //{
                    //    vendNo++;
                    //}

                    if (sumCurrentGroupe <= sumInModel)
                    {
                        vendNo++;
                    }
                    else
                    {
                        vendNo = 1;
                    }
                    chkPOLineForm.Add(poLineForm);
                }

                var applId = Convert.ToInt64(t.ApplId);
                if (!chkApplicationForm.Any(m => m == applId))
                {
                    if (t.ApplId != null)
                    {
                        groupByte.Add(LoadApplicationFormReport(applId, false,false));
                        chkApplicationForm.Add(applId);
                    }
                }
                
            }

            return groupByte;
        }

        public List<byte[]> GetListPOFormReport(long primaryId,bool isPrePint)
        {
            //var modelHead = _purchTableService.Load(m => m.PurchId == primaryId);
            var modelLine = _PurchLineService.GetAll(m => m.PurchId == primaryId).ToListSafe();
            var groupByte = new List<byte[]>();
            var chkPOLineForm = new List<PurchLine>();
            int vendNo = 1;
            foreach (var t in modelLine.OrderBy(m => m.ApplId))
            {
                if (!chkPOLineForm.Any(m => m.PurchId == t.PurchId && m.VendName == t.VendName) || chkPOLineForm.Count == 0)
                {
                    //if (modelHead.IsImport == null || modelHead.IsImport == false)
                    //    groupByte.Add(LoadPOFormLocalReport(t.PurchId, t.VendName, isPrePint));
                    //else
                    //    groupByte.Add(LoadPOFormReport(t.PurchId, t.VendName, isPrePint));

                    groupByte.Add(LoadPOFormReport(t.PurchId, t.VendName, isPrePint, " - " + vendNo));

                    var poLineForm = new PurchLine();
                    poLineForm.PurchId = t.PurchId;
                    poLineForm.VendName = t.VendName;
                    //chkPOLineForm.Add(poLineForm);
                    //if (chkPOLineForm.Any(m => m.PurchId == t.PurchId))
                    //{
                    //    vendNo++;  
                    //}
                    //else
                    //{
                    //    vendNo = 1;
                    //}
                    var sumGroupeModel = modelLine.Where(m => m.VendName == t.VendName).ToListSafe();
                    decimal sumInModel = sumGroupeModel.Count();
                    var sumGroupeCurrentLine = chkPOLineForm.Where(m => m.VendName == t.VendName);
                    decimal sumCurrentGroupe = sumGroupeCurrentLine.Count();

                    if (sumCurrentGroupe <= sumInModel)
                    {
                        vendNo++;
                    }
                    else
                    {
                        vendNo = 1;
                    }
                    chkPOLineForm.Add(poLineForm);
                }
               
            }

            return groupByte;
        }

        public List<byte[]> GetListPOLocalFormReport(long primaryId,bool isPrePint,bool isModel600E)
        {
            //var modelHead = _purchTableService.Load(m => m.PurchId == primaryId);
            var modelLine = _PurchLineService.GetAll(m => m.PurchId == primaryId).ToListSafe();
            var groupByte = new List<byte[]>();
            var chkPOLineForm = new List<PurchLine>();
            int vendNo = 1;
            foreach (var t in modelLine.OrderBy(m => m.ApplId))
            {
                if (!chkPOLineForm.Any(m => m.PurchId == t.PurchId && m.VendName == t.VendName))
                {
                    //if (modelHead.IsImport == null || modelHead.IsImport == false)
                    //    groupByte.Add(LoadPOFormLocalReport(t.PurchId, t.VendName, isPrePint));
                    //else
                    //    groupByte.Add(LoadPOFormReport(t.PurchId, t.VendName, isPrePint));

                    groupByte.Add(LoadPOFormLocalReport(t.PurchId, t.VendName, isPrePint, "- " + vendNo, isModel600E));

                    var poLineForm = new PurchLine();
                    poLineForm.PurchId = t.PurchId;
                    poLineForm.VendName = t.VendName;

                    var sumGroupeModel = modelLine.Where(m => m.VendName == t.VendName).ToListSafe();
                    decimal sumInModel = sumGroupeModel.Count();
                    var sumGroupeCurrentLine = chkPOLineForm.Where(m => m.VendName == t.VendName);
                    decimal sumCurrentGroupe = sumGroupeCurrentLine.Count();

                    //if (chkPOLineForm.Any(m => m.PurchId == t.PurchId))
                    //{
                    //    vendNo++;
                    //}

                    if (sumCurrentGroupe <= sumInModel)
                    {
                        vendNo++;
                    }
                    else
                    {
                        vendNo = 1;
                    }
                    chkPOLineForm.Add(poLineForm);
                }
            }

            return groupByte;
        }

        public List<byte[]> GetListPOAndFriendsFormReport(long primaryId)
        {
            var modelHead = _purchTableService.Load(m=>m.PurchId == primaryId);
            var modelLine = _PurchLineService.GetAll(m => m.PurchId == primaryId).ToListSafe();
            var groupByte = new List<byte[]>();
            var chkApplicationForm = new List<long>();
            var chkPOLineForm = new List<PurchLine>();
            int vendNo = 1;
            foreach (var t in modelLine.OrderBy(m => m.ApplId))
            {
                if (!chkPOLineForm.Any(m => m.PurchId == t.PurchId && m.VendName == t.VendName))
                {
                    if (modelHead.IsImport == null || modelHead.IsImport == false)
                        groupByte.Add(LoadPOFormLocalReport(t.PurchId, t.VendName, false, "- " + vendNo,false));
                    else
                        groupByte.Add(LoadPOFormReport(t.PurchId, t.VendName, false, "- " + vendNo));

                    var poLineForm = new PurchLine();
                    poLineForm.PurchLineId = t.PurchLineId;
                    poLineForm.PurchId = t.PurchId;
                    poLineForm.VendName = t.VendName;
                    var sumGroupeModel = modelLine.Where(m => m.VendName == t.VendName).ToListSafe();
                    decimal sumInModel = sumGroupeModel.Count();
                    var sumGroupeCurrentLine = chkPOLineForm.Where(m => m.VendName == t.VendName);
                    decimal sumCurrentGroupe = sumGroupeCurrentLine.Count();

                    //if (chkPOLineForm.Any(m => m.PurchId == t.PurchId))
                    //{
                    //    vendNo++;
                    //}

                    if (sumCurrentGroupe <= sumInModel)
                    {
                        vendNo++;
                    }
                    else
                    {
                        vendNo = 1;
                    }
                    chkPOLineForm.Add(poLineForm);
                }

                var applId = Convert.ToInt64(t.ApplId);
                if (!chkApplicationForm.Any(m => m == applId))
                {
                    if (t.ApplId != null)
                    {
                        groupByte.Add(LoadApplicationFormReport(applId,false,false));
                        chkApplicationForm.Add(applId);
                    }
                }
                
            }

            return groupByte;
        }

        public List<byte[]> GetListPOFormReport(BaseCriteriaReport criteria)
        {
            var models = _purchaseOrderFacade.GetPurchLineFormCriteriaReport(criteria).ToListSafe();
            var groupByte = new List<byte[]>();
            var chkPOLineForm = new List<PurchLine>();
            int vendNo = 1;
            foreach (var t in models.OrderBy(m => m.ApplId))
            {
                if (!chkPOLineForm.Any(m => m.PurchId == t.PurchId && m.VendName == t.VendName))
                {
                    if (criteria.PrePintForm.HasValue)
                    {
                        if (criteria.IsImport)
                        {
                            groupByte.Add(LoadPOFormReport(t.PurchId, t.VendName, true, "- " + vendNo));
                        }
                        else
                        {
                            groupByte.Add(LoadPOFormLocalReport(t.PurchId, t.VendName, true, "- " + vendNo, false));
                        }
                    }
                    else
                    {
                        if (criteria.IsImport)
                        {
                            groupByte.Add(LoadPOFormReport(t.PurchId, t.VendName, false, "- " + vendNo));
                        }
                        else
                        {
                            groupByte.Add(LoadPOFormLocalReport(t.PurchId, t.VendName, false, "- " + vendNo,false));
                        }
                    }
                   
                    
                    var poLineForm = new PurchLine();
                    poLineForm.PurchId = t.PurchId;
                    poLineForm.VendName = t.VendName;
                    
                    if (chkPOLineForm.Any(m => m.PurchId == t.PurchId))
                    {
                        vendNo++;
                    }
                    else
                    {
                        vendNo = 1;
                    }
                    chkPOLineForm.Add(poLineForm);
                }
            }

            return groupByte;
        }


        public List<byte[]> GetListApplicationFormReport(BaseCriteriaReport criteria)
        {
            var models = _applicationFormService.GetListApplicationForm(criteria).ToListSafe();
            var groupByte = new List<byte[]>();
            foreach (var t in models)
            {
                if(criteria.PrePintForm.HasValue)
                    groupByte.Add(LoadApplicationFormReport(t.ApplId, true,false));
                else
                    groupByte.Add(LoadApplicationFormReport(t.ApplId, false,false));
            }
                

            return groupByte;
        }

        
        public List<byte[]> GetListApplicationFormReport(long primaryId,bool isPrePint,bool isModel600E)
        {
            var model = _applicationFormService.Load(m => m.ApplId == primaryId);
            var groupByte = new List<byte[]>();
            if (model != null)
            {
                groupByte.Add(LoadApplicationFormReport(model.ApplId, isPrePint, isModel600E));
                //if (isPrePint)
                //{
                    
                //}
                //else
                //{
                //    groupByte.Add(LoadApplicationFormReport(model.ApplId, false, isModel600E));
                //}
                
            }


            return groupByte;
        }

        public SummaryReportsViewModel InitSummaryReportsViewModel()
        {
            var model = new SummaryReportsViewModel();
            model.Criteria = InitBaseCriteriaReport();
            
            //model.ReportNames = model.ReportNames.OrderBy(m=>m).ToListSafe();

            return model;
        }

        
        public BaseCriteriaReport InitBaseCriteriaReport()
        {
            var model = new BaseCriteriaReport();
            return model;
        }

        public byte[] LoadApplicationFormReport(long id, bool isPrePint, bool isModel600E)
        {
            var data = _entityDataConnectionStored.ApplicationAppFormReport(id);
            var his = _applicationApprovalHistoryService.GetAll(m => m.PurchId == id && !string.IsNullOrEmpty(m.Comment)).OrderByDescending(m => m.ID).Take(3).ToListSafe();

            //comment MD : FD : GM
            //if(his.Any(m=>m.Activity))

            //int countComment = his.ToListSafe().Count;
            //while (countComment < 3)
            //{
            //    var modelComment = new ApplicationApprovalHistory();
            //    his.Add(modelComment);
            //    countComment++;
            //}

            var joinHiscomment = new ApplicationApprovalHistory();
            joinHiscomment.Comment = "";
            foreach (var c in his)
            {
                joinHiscomment.Comment = joinHiscomment.Comment + " [" + c.Actioner.Replace(@"TMBGROUP\", "") + "] - " + c.Comment + " \n";
            }
            his.Clear();
            his.Add(joinHiscomment);

            ReportDataSource datasource = new ReportDataSource("DataSet1", data.ToListSafe());
            ReportDataSource datasourceComment = new ReportDataSource("DataSet2", his.ToListSafe());

            List<ReportDataSource> datasources = new List<ReportDataSource>();
            datasources.Add(datasource);
            datasources.Add(datasourceComment);

            if (isPrePint)
            {
                if (isModel600E)
                {
                    return ConvertImageToByte("ApplicationForm" + DateTime.Now.ToString("ddMMyyyy"), "App/AppForm_600E.rdlc", datasources);
                }
                else
                {
                    return ConvertImageToByte("ApplicationForm" + DateTime.Now.ToString("ddMMyyyy"), "App/AppForm.rdlc", datasources);
                }
                
            }

            return ConvertImageToByte("ApplicationForm" + DateTime.Now.ToString("ddMMyyyy"), "App/AppForm_BlankPaper.rdlc", datasources);
        }

        public byte[] LoadPOFormReport(long id,string vendorName,bool isPrePint,string parmVendNo)
        {
            List<ReportParameter> rpParameter = new List<ReportParameter>();
            rpParameter.Add(new ReportParameter("ParmVendorNo", parmVendNo));

            var data = _entityDataConnectionStored.PurchaseOrderFormLocalReport(id, vendorName);
            ReportDataSource datasource = new ReportDataSource("DataSet1", data.ToListSafe());

            if (isPrePint)
            {
                return ConvertImageToByte("POForm" + DateTime.Now.ToString("ddMMyyyy"), "PO/POForm.rdlc", datasource, rpParameter);
            }

            return ConvertImageToByte("POForm" + DateTime.Now.ToString("ddMMyyyy"), "PO/POForm_BlankPaper.rdlc", datasource, rpParameter);
        }

        public byte[] LoadPOFormLocalReport(long id, string vendorName, bool isPrePint, string parmVendNo,bool isModel600E)
        {
            List<ReportParameter> rpParameter = new List<ReportParameter>();
            rpParameter.Add(new ReportParameter("ParmVendorNo", parmVendNo));

            var data = _entityDataConnectionStored.PurchaseOrderFormLocalReport(id, vendorName);
            ReportDataSource datasource = new ReportDataSource("DataSet1", data.ToListSafe());

            if (isPrePint)
            {
                if (isModel600E)
                {
                    return ConvertImageToByte("POForm" + DateTime.Now.ToString("ddMMyyyy"), "PO/POLocalForm_600E.rdlc", datasource, rpParameter);
                }
                else
                {
                    return ConvertImageToByte("POForm" + DateTime.Now.ToString("ddMMyyyy"), "PO/POLocalForm.rdlc", datasource, rpParameter);
                } 
            }

            return ConvertImageToByte("POForm" + DateTime.Now.ToString("ddMMyyyy"), "PO/POLocalForm_BlankPaper.rdlc", datasource, rpParameter);
        }

        public byte[] ConvertImageToByte(string fileName,string rdlcName, ReportDataSource reportDataSource)
        {
            Warning[] warnings;
            string[] streamIds;
            string mimeType = string.Empty;
            string encoding = string.Empty;
            string extension = string.Empty;

            ReportViewer viewer = new ReportViewer();
            viewer.ProcessingMode = ProcessingMode.Local;

            viewer.LocalReport.ReportPath = HttpContext.Current.Server.MapPath("~/Reports/" + rdlcName);
            viewer.LocalReport.DataSources.Clear();
            viewer.LocalReport.DataSources.Add(reportDataSource);
            //viewer.LocalReport.SetParameters(new ReportParameter("",""));

            byte[] bytes = viewer.LocalReport.Render("PDF", null, out mimeType, out encoding, out extension, out streamIds, out warnings);

            return bytes;
        }

        public byte[] ConvertImageToByte(string fileName, string rdlcName, ReportDataSource reportDataSource, List<ReportParameter> parameter)
        {
            Warning[] warnings;
            string[] streamIds;
            string mimeType = string.Empty;
            string encoding = string.Empty;
            string extension = string.Empty;

            ReportViewer viewer = new ReportViewer();
            viewer.ProcessingMode = ProcessingMode.Local;

            viewer.LocalReport.ReportPath = HttpContext.Current.Server.MapPath("~/Reports/" + rdlcName);
            viewer.LocalReport.DataSources.Clear();
            viewer.LocalReport.DataSources.Add(reportDataSource);
            viewer.LocalReport.SetParameters(parameter);
            //viewer.LocalReport.SetParameters(new ReportParameter("",""));

            byte[] bytes = viewer.LocalReport.Render("PDF", null, out mimeType, out encoding, out extension, out streamIds, out warnings);

            return bytes;
        }


        public byte[] ConvertImageToByte(string fileName, string rdlcName, List<ReportDataSource> reportDataSource)
        {
            Warning[] warnings;
            string[] streamIds;
            string mimeType = string.Empty;
            string encoding = string.Empty;
            string extension = string.Empty;

            ReportViewer viewer = new ReportViewer();
            viewer.ProcessingMode = ProcessingMode.Local;

            viewer.LocalReport.ReportPath = HttpContext.Current.Server.MapPath("~/Reports/" + rdlcName);
            viewer.LocalReport.DataSources.Clear();

            foreach (var t in reportDataSource)
            {
                viewer.LocalReport.DataSources.Add(t);
            }

            byte[] bytes = viewer.LocalReport.Render("PDF", null, out mimeType, out encoding, out extension, out streamIds, out warnings);

            return bytes;
        }

        public List<ADUserTable> GetListUserFilter(string username)
        {
            return _adUserTableService.GetListUserFilter(username);
        }
        public List<SectionTable> GetListSectionFilter(long? sectionid)
        {
            return _sectionTableService.GetListSectionFilter(sectionid);
        }
        public List<Division> GetListDivisionFilter(string divisionname)
        {
            return _divisionTableService.GetListDivisionFilter(divisionname);
        }
        public List<DepartmentTable> GetListDepartmentFilter(string departmentname)
        {
            return _departmentTableService.GetListDepartmentFilter(departmentname);
        }
        public string InitBaseParameter(BaseCriteriaReport model)
        {
            if (model.StartDate == null)
                model.StartDate = DateTime.Now.AddYears(-10);
            if (model.EndDate == null)
                model.EndDate = DateTime.Now;
            if (string.IsNullOrEmpty(model.CreateBy))
                model.CreateBy = " ";
            if(string.IsNullOrEmpty(model.PrimaryId))
                model.PrimaryId = "0";
            if (model.SectionId == null)
                model.SectionId = null;
            if (string.IsNullOrEmpty(model.DivisionId))
                model.DivisionId = " ";
            if (string.IsNullOrEmpty(model.DepartmentId))
                model.DepartmentId = " ";
            long primaryId = Convert.ToInt64(model.PrimaryId);
            string parameter = "&PrimaryId=" + primaryId + "&CreatedBy=" + model.CreateBy + "&StartDate=" +
                               model.StartDate.ToSSRSFormat() + "&EndDate=" + model.EndDate.ToSSRSFormat();
            return parameter;
        }

        public List<MST_RequestReportName> GetAllRequestReportName()
        {
            return _mstRequestReportNameService.GetAllRequestReportName();
        }

        public List<MST_SubRequestReportName> GetListSubRequestReportName(string requestToId)
        {
            return _mstSubRequestReportNameService.GetListSubReportName(requestToId);
        }
    }
}