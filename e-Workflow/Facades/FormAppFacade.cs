﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web.Mvc;
using iTextSharp.text;
using Microsoft.Reporting.WebForms;
using PanasonicProject.Models;
using PanasonicProject.Models.Enums;
using PanasonicProject.Resource;
using PanasonicProject.Services;
using PanasonicProject.Services.Report;
using PanasonicProject.States;
using PanasonicProject.ViewModels;
using PanasonicProject.ViewModels.Report;
using System.IO;
using OfficeOpenXml;
using System.Data;

namespace PanasonicProject.Facades
{
    public interface IFormAppFacade
    {
        BaseReportDataSource FormApplication(long id);
    }
    public class FormAppFacade : Abstracts.BasePanasonicFacade, IFormAppFacade
    {
        private readonly FormAplicationService _formAplicationService;
        private readonly ApplicationFormService _applicationFormService;
        private readonly ApplFormBudgetService _applFormBudgetService;
        private readonly PurchaseOrderFacade _purchaseOrderFacade;
        private readonly ApplBudgetExplanationService _applBudgetExplanationService;
        private readonly SectionTableService _sectionService;
        private readonly PurchLineService _purchLineService;
        private readonly PurchTableService _purchTableService;
        private readonly DepartmentSetupService _departmentSetupService;
        private readonly ADUserTableService _ADUserTableService;
        private readonly ApplFormMeetingAttachment _applFormMeetingAttachment;
        private readonly AttachmentSetupService _attachmentSetupService;
        private readonly BusinessPlanLineService _bpTableService;
        private readonly GLBalanceOracleService _glBalanceOracleService;
        private readonly GLBalanceExcelService _glBalanceExcelService;

        public FormAppFacade(FormAplicationService formAplicationService, ApplicationFormService applicationFormService, PurchaseOrderFacade purchaseOrderFacade, ApplFormBudgetService applFormBudgetService, ApplBudgetExplanationService applBudgetExplanationService, SectionTableService sectionService, PurchLineService purchLineService, DepartmentSetupService departmentSetupService, PurchTableService purchTableService, ApplFormMeetingAttachment applFormMeetingAttachment, AttachmentSetupService attachmentSetupService, BusinessPlanLineService bpTableService, GLBalanceOracleService glBalanceOracleService, GLBalanceExcelService glBalanceExcelService)
        {
            _formAplicationService = formAplicationService;
            _applicationFormService = applicationFormService;
            _purchaseOrderFacade = purchaseOrderFacade;
            _applFormBudgetService = applFormBudgetService;
            _applBudgetExplanationService = applBudgetExplanationService;
            _sectionService = sectionService;
            _purchLineService = purchLineService;
            _departmentSetupService = departmentSetupService;
            _purchTableService = purchTableService;
            _applFormMeetingAttachment = applFormMeetingAttachment;
            _attachmentSetupService = attachmentSetupService;
            _bpTableService = bpTableService;
            _glBalanceOracleService = glBalanceOracleService;
            _glBalanceExcelService = glBalanceExcelService;
        }

        public BaseReportDataSource FormApplication(long id)
        {
            var formAplicationModel = _formAplicationService;
            var url = String.Empty;
            var filename = String.Empty;
            var datasource = new BaseReportDataSource
            {
                URL = url,
                FileName = filename,
                ReportDataSources = new ReportDataSource[2]
            };
            var applicationTableModel = _applicationFormService.GetAppLine(id);
            var applFormBudgetTableModel = _applFormBudgetService.GetAppBudgetLineByApp(id);
            datasource.ReportDataSources[0] = new ReportDataSource("header", formAplicationModel.GetHeader(applicationTableModel));
            datasource.ReportDataSources[1] = new ReportDataSource("body", formAplicationModel.GetBody(applFormBudgetTableModel));
            return datasource;
        }

        public long CopyApplication(long? primaryId, string documentnumber)
        {
            var modelCopyFrom = _applicationFormService.Load(m => m.ApplId == primaryId);
            var modelCopyTo = new ApplicationForm();
            modelCopyTo = modelCopyFrom;
            modelCopyTo.ApplNo = documentnumber;
            modelCopyTo.IntApplId = null;
            modelCopyTo.IntDateRecord = null;
            modelCopyTo.CreatedDate = DateTime.Now;
            modelCopyTo.Status = DocumentStatus.Draft;
            modelCopyTo.DocumentStatus = "Creating";
            modelCopyTo.IsCancelling = null;
            modelCopyTo.CancelledDate = null;
            modelCopyTo.CancelDate = null;
            modelCopyTo.SubmitedDate = null;
            modelCopyTo.FinishedDate = null;
            modelCopyTo.SubmitedDate = null;
            modelCopyTo.FinishedDate = null;
            modelCopyTo.RemarkCancel = null;

            modelCopyTo = _applicationFormService.Create(modelCopyTo);
            _applicationFormService.SaveChanges();

            var modelLineCopyFrom = _applFormBudgetService.GetAll(m => m.ApplId == primaryId).ToListSafe();

            foreach (var t in modelLineCopyFrom)
            {
                var modelLineCopyTo = new ApplFormBudgetTable();
                t.ApplId = modelCopyTo.ApplId;
                modelLineCopyTo = t;
                _applFormBudgetService.Create(modelLineCopyTo);
            }

            var modelLineExpCopyFrom = _applBudgetExplanationService.GetAll(m => m.ApplId == primaryId).ToListSafe();

            foreach (var t in modelLineExpCopyFrom)
            {
                var modelLineExpCopyTo = new ApplBudgetExplanation();
                t.ApplId = modelCopyTo.ApplId;
                modelLineExpCopyTo = t;
                _applBudgetExplanationService.Create(modelLineExpCopyTo);
            }

            _applBudgetExplanationService.SaveChanges();
            _applFormBudgetService.SaveChanges();

            return modelCopyTo.ApplId;
        }
        public List<ApplicationForm> GetAppByDepartmentUser(string userId)
        {
            var apptable = new List<ApplicationForm>();
            var department = _departmentSetupService.GetDepartmentSetupLineByUser(userId, true)
                   .GroupBy(m => m.DepartmentId);

            foreach (var line in department)
            {
                var appdepartment = _applicationFormService.GetAll(m => m.DepartmentId == line.Key);
                foreach (var appline in appdepartment)
                {
                    apptable.Add(appline);
                }
            }
            return apptable;
        }
        public List<ApplicationForm> GetAppByUser(string userId)
        {
            var apptable = new List<ApplicationForm>();
            var department = _sectionService.GetAll(m => m.Manager == userId).GroupBy(m => m.Department);
            var departmentByGM = _sectionService.GetAll(m => m.GM == userId).GroupBy(m => m.Department);
            var enumerable = department as IGrouping<string, SectionTable>[] ?? department.ToArray();
            var departmentByGm = departmentByGM as IGrouping<string, SectionTable>[] ?? departmentByGM.ToArray();
            if (!enumerable.Any() && !departmentByGm.Any())
            {
                return new List<ApplicationForm>();
            }
            if (departmentByGm.Any())
            {
                enumerable = departmentByGm;
            }

            foreach (var gm in enumerable)
            {
                var appdepartment = _applicationFormService.GetAll(m => m.DepartmentId == gm.Key);
                foreach (var appline in appdepartment)
                {
                    apptable.Add(appline);
                }
            }
            return apptable;
        }
        public List<ADUserTable> GetPersonInChargeByDepartment(string departmentid)
        {
            var user = new List<ADUserTable>();
            var usermodel = from a in _departmentSetupService.GetAll(m => m.DepartmentId == departmentid && m.IsActive == true).ToList()
                            join b in _ADUserTableService.GetListUserFilter(null) on a.UserId equals
                            b.UserName
                            select new ADUserTable()
                            {
                                UserName = b.UserName,
                                DisplayName = b.DisplayName
                            };
            if (usermodel.Any())
            {
                return usermodel.ToList();
            }
            return user;
        }
        public List<DepartmentSetup> GetPersonInChargeByAll(string departmentid)
        {
            var department = new List<DepartmentSetup>();
            var departmentmodel = _departmentSetupService.GetAll(m => m.DepartmentId == departmentid && m.IsActive == true).ToList();
            if (departmentmodel.Any())
            {
                return departmentmodel;
            }
            return department;
        }

        public CheckBPViewModelstate Gettotalaval(long appid)
        {
            var checkmodel = new CheckBPViewModelstate();
            var groupbybp = new List<ApplFormBudgetTable>();
            decimal? used = 0;
            var budgetlinemodel = _applFormBudgetService.GetAll(m => m.ApplId == appid);
            if (budgetlinemodel.First().BudgetType == "Others")
            {
                checkmodel.BPAval = 0;
                checkmodel.BPRemain = 0;
                return checkmodel;
            }
            foreach (var bgline in budgetlinemodel)
            {
                used += GetBPRemainAndCheckBPAval(bgline.BPNum).BPRemain;
                checkmodel.State += used.ToString();
                if (
                    !groupbybp.Any(
                        m =>
                            m.SectionId == bgline.SectionId &&
                            m.FiscalYear == bgline.FiscalYear &&
                            m.BudgetType == bgline.BudgetType &&
                            m.RefNumber == bgline.RefNumber))
                {
                    var createbpline = new ApplFormBudgetTable();
                    createbpline = bgline;
                    groupbybp.Add(createbpline);
                }
            }
            checkmodel.State += used.ToString();
            checkmodel.State += groupbybp.ToString();
            decimal? total = 0;
            if (groupbybp.Any())
            {
                foreach (var budget in groupbybp)
                {
                    total += GetBPRemainAndCheckBP(budget.BPNum).BPAval;

                    checkmodel.State += total.ToString();
                }
            }


            checkmodel.BPAval = total - used;
            return checkmodel;
        }
        public CheckBPViewModel GetBPRemainAndCheckBP(string bpNumber)
        {
            var viewmodel = new CheckBPViewModel();
            var businessPlanLineService = DependencyResolver.Current.GetService<BusinessPlanLineService>();
            var businessPlanLineModel = businessPlanLineService.GetBPLine(bpNumber);

            var fisicalYearId = businessPlanLineModel.FisicalYearId;
            var sectionId = businessPlanLineModel.SectionId;
            var refnumber = businessPlanLineModel.RefNumber;
            var resultBPAval = CheckBPAval(fisicalYearId, sectionId, businessPlanLineModel.BusinessPlanType, refnumber);
            viewmodel.BPAval = resultBPAval;
            return viewmodel;
        }
        public CheckBPViewModel GetBPRemainAndCheckBPAval(string bpNumber)
        {
            var viewmodel = new CheckBPViewModel();
            var appBudgetState = DependencyResolver.Current.GetService<AppBudgetState>();
            decimal? bpUsed = 0;

            bpUsed += appBudgetState.GetBPAmountApplFormBudget(bpNumber);
            bpUsed += appBudgetState.GetBPAmountPurchLine(bpNumber);
            viewmodel.BPRemain = bpUsed;

            return viewmodel;
        }
        public decimal? CheckBPAval(string paramFiscalyear, string paramSectionId, string paramBPtype, string refnumber)
        {
            decimal? total = 0;
            var businessPlanLineService = DependencyResolver.Current.GetService<BusinessPlanLineService>();
            var bpmodel = businessPlanLineService.GetAll(
                m =>
                    m.FisicalYearId == paramFiscalyear && m.SectionId == paramSectionId &&
                    m.BusinessPlanType == paramBPtype && m.RefNumber == refnumber);

            if (bpmodel.Any())
            {
                total = bpmodel.Sum(m => m.BPAmount);
            }
            return total;
        }
        public Validate CloseApp(long appid)
        {
            var val = new Validate();
            var closed = ReceiptStatus.Closed.ToString();
            var draft = DocumentStatus.Draft.ToString(CultureInfo.InvariantCulture);
            var onprocess = DocumentStatus.OnProcess.ToString(CultureInfo.InvariantCulture);
            var rejected = DocumentStatus.Rejected.ToString(CultureInfo.InvariantCulture);
            var completed = DocumentStatus.Completed.ToString(CultureInfo.InvariantCulture);
            var approved = DocumentStatus.Approved.ToString(CultureInfo.InvariantCulture);
            var pomodel = _purchTableService.GetAll(m => m.ApplId == appid && (m.POStatus != approved || m.ReceiptStatus != closed)).Where(m => m.POStatus != rejected);
            if (pomodel.Any())
            {
                string po = null;
                foreach (var ponum in pomodel)
                {
                    po += ponum.PurchNum + " ";
                }
                val.MessageError += "Application can't closed, This application used to purchase order ( " + po + ")";
                return val;
            }


            var appmodel = _applicationFormService.Load(m => m.ApplId == appid);
            if (appmodel != null)
            {
                appmodel.IsClosed = true;
                appmodel.DocumentStatus = "Closed";
                _applicationFormService.Update(appmodel);
                _applicationFormService.SaveChanges();
                val.MessageError += "update success";
            }
            val.Success = true;
            return val;
        }

        public Validate UpdateApplBudgetAndRemoveExplanation(long appid)
        {
            var val = new Validate { Success = false };
            var appExpModel = _applBudgetExplanationService.GetAll(m => m.ApplId == appid);
            if (appExpModel.Any())
            {
                foreach (var expLine in appExpModel)
                {
                    if (string.IsNullOrEmpty(expLine.Description) && expLine.LineAmountTH == 0)
                    {
                        var line = expLine;
                        _applBudgetExplanationService.Delete(m => m.ID == line.ID);
                        val.MessageError += line.ID;
                    }
                }
                _applBudgetExplanationService.SaveChanges();
            }
            return val;
        }
        public StateValidate AttachmentMeeting(long appid)
        {
            var val = new StateValidate { Success = false };
            var appMeeting = _applFormMeetingAttachment.GetApplFormAttachmentTables(appid);
            var applFormAttachmentTables = appMeeting as ApplFormAttachmentTable[] ?? appMeeting.ToArray();
            if (applFormAttachmentTables.Any())
            {
                val.Condition = "appattachment not null";
                val.Success = true;
                if (applFormAttachmentTables.Any(m => m.Attachmentfile == null))
                {
                    val.MessageError += "Please completely fill in. (Attachments)";
                    val.StateName = "checkNullAttach";
                }
                else
                {
                    var attachMaster = _attachmentSetupService.GetAll();
                    var attachmentSetups = attachMaster as AttachmentSetup[] ?? attachMaster.ToArray();
                    if (attachmentSetups.Any())
                    {
                        //foreach (var meeting in applFormAttachmentTables)
                        //{
                        val.StateName = "beforeMustHaveAttach";
                        foreach (var setup in attachmentSetups)
                        {
                            if (!applFormAttachmentTables.Any(m => m.TypeId == setup.ID))
                            {
                                val.MessageError += "Please attach file of '" + setup.Type + "' at least 1 file. \r\n";
                            }
                        }
                        val.StateName = "Finish";
                        //}

                    }
                }
            }
            else
            {
                val.Condition = "appattachment null";
                var attachMaster = _attachmentSetupService.GetAll();
                var attachmentSetups = attachMaster as AttachmentSetup[] ?? attachMaster.ToArray();
                foreach (var setup in attachmentSetups)
                {

                    val.MessageError += "Please attach file of '" + setup.Type + "' at least 1 file. \r\n";

                }
                val.StateName = "Finish";
                val.Success = true;
            }

            return val;
        }

        public string UpdateBPTable()
        {
            try
            {
                var listBPTable = _bpTableService.GetAll().ToListSafe();
                var listGLBalance = _glBalanceOracleService.GetAll(m => m.PERIOD_DATE.Value.Month == DateTime.Now.Month && m.PERIOD_DATE.Value.Year == DateTime.Now.Year).ToListSafe();
                foreach (var t in listGLBalance)
                {
                    var codeId = "";
                    if (t.PERIOD_DATE.Value.Month >= 1 && t.PERIOD_DATE.Value.Month <= 3)
                    {
                        //codeId = (t.PERIOD_DATE.Value.Year - 1) + t.DEPARTMENT_CODE + t.ACCOUNT_CODE;
                        codeId = (t.PERIOD_DATE.Value.Year - 1) + "-" + t.DEPARTMENT_CODE + "-" + t.ACCOUNT_CODE;
                    }
                    else
                    {
                        //codeId = (t.PERIOD_DATE.Value.Year) + t.DEPARTMENT_CODE + t.ACCOUNT_CODE;
                        codeId = (t.PERIOD_DATE.Value.Year) + "-" + t.DEPARTMENT_CODE + "-" + t.ACCOUNT_CODE;
                    }
                    var findByCodeId = listBPTable.FirstOrDefault(m => m.BusinessPlanId == codeId);
                    if (findByCodeId != null)
                    {
                        findByCodeId.BPActualAvailable = findByCodeId.BPAmount - t.ENDING_BALANCE;
                        _bpTableService.SaveChanges();
                    }
                }
                return "Success";
            }
            catch (Exception exception)
            {
                throw new Exception(exception.Message);
            }
        }

        public string ReadData(string filePath)
        {
            if (!File.Exists(filePath)) throw new Exception("File is not found.");
            using (var destinationStream = File.OpenRead(filePath))
            {
                try
                {
                    List<GLBalanceExcel> list = new List<GLBalanceExcel>();
                    var listBPTable = _bpTableService.GetAll().ToListSafe();

                    if (!destinationStream.CanRead) throw new Exception("File can not read.");
                    var package = new ExcelPackage(new FileInfo(filePath));
                    var workSheet = package.Workbook.Worksheets[1];
                    var totalRows = workSheet.Dimension.End.Row;
                    var totalCols = workSheet.Dimension.End.Column;
                    var dt = new DataTable(workSheet.Name);
                    DataRow dr = null;

                    for (var i = 1; i <= totalRows; i++)
                    {
                        if (i > 1) dr = dt.Rows.Add();
                        for (var j = 1; j <= totalCols; j++)
                        {
                            if (i == 1)
                            {
                                if (workSheet.Cells[i, j] != null && workSheet.Cells[i, j].Value != null)
                                {
                                    dt.Columns.Add(workSheet.Cells[i, j].Value.ToString());
                                }
                            }
                            else
                            {
                                if (workSheet.Cells[i, j] != null && workSheet.Cells[i, j].Value != null)
                                {
                                    if (dr != null) dr[j - 1] = workSheet.Cells[i, j].Value.ToString();
                                }
                                if (j == totalCols)
                                {
                                    var excel = new GLBalanceExcel();
                                    excel.YearId = dr.ItemArray[0].ToString();
                                    excel.ProjNo = dr.ItemArray[1].ToString();
                                    excel.SectionCode = dr.ItemArray[2].ToString();
                                    excel.Item = dr.ItemArray[3].ToString();
                                    excel.ActaulAmount = Convert.ToInt32(dr.ItemArray[4]);
                                    excel.CreateDate = DateTime.Now;
                                    list.Add(excel);
                                    _glBalanceExcelService.Create(excel);
                                    _glBalanceExcelService.SaveChanges();
                                }
                            }
                        }
                    }
                    foreach (var t in list)
                    {
                        var codeId = "";
                        //if (Convert.ToInt32(t.ProjNo) <= 9 && Convert.ToInt32(t.ProjNo) >= 0)
                        //{
                        //    codeId = t.YearId + "-" + t.SectionCode + "-" + "0" + t.ProjNo;
                        //}
                        //else
                        //{
                        //    codeId = t.YearId + "-" + t.SectionCode + "-" + t.ProjNo;
                        //}

                        if (Convert.ToInt32(t.ProjNo) <= 9)
                        {
                            codeId = t.YearId + t.SectionCode + "0" + t.ProjNo;
                        }
                        else
                        {
                            codeId = t.YearId + t.SectionCode + t.ProjNo;
                        }

                        var findByCodeId = listBPTable.FirstOrDefault(m => m.BusinessPlanId == codeId);
                        if (findByCodeId != null)
                        {
                            findByCodeId.BPActualAvailable = findByCodeId.BPAmount - t.ActaulAmount;
                            findByCodeId.BPActualAmount = t.ActaulAmount;
                            _bpTableService.SaveChanges();
                        }
                    }
                    
                    return "Success";
                }
                catch (Exception exception)
                {
                    throw new Exception(exception.Message);
                }
            }
        }
        //public string UpdateApplBudget(long appid)
        //{
        //    string msg;


        //    return msg;
        //}
    }
}