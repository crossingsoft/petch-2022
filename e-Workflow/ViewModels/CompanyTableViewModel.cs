﻿
namespace PanasonicProject.ViewModels
{
    public class CompanyTableViewModel
    {
        public long? CompanyId { get; set; }
        public string CompanyNameTH { get; set; }
        public string CompanyNameEN { get; set; }
        public string Address { get; set; }
    }
}