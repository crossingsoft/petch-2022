﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PanasonicProject.ViewModels
{
    public class StateValidate
    {
        public bool Success { get; set; }
        public string StateName { get; set; }
        public string MessageError { get; set; }
        public string Condition { get; set; }
        public decimal? Value { get; set; }
        public bool allow { get; set; }
    }
}