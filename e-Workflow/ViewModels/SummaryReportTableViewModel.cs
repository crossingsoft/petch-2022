﻿using System;

namespace PanasonicProject.ViewModels
{
    public class SummaryReportTableViewModel : Abstracts.BaseTableViewModel
    {
        public DateTime? DateFrom { get; set; }
        public DateTime? DateTo { get; set; }
        //GrandTotalExpense
        public decimal? GrandTotalAmountExpenseBP { get; set; }
        public decimal? GrandTotalAmountExpenseActual { get; set; }
        public decimal? GrandTotalAmountExpenseBPActual { get; set; }
        //GrandTotalRepair
        public decimal? GrandTotalAmountRepairBP { get; set; }
        public decimal? GrandTotalAmountRepairActual { get; set; }
        public decimal? GrandTotalAmountRepairBPActual { get; set; }
        //GrandTotalAsset
        public decimal? GrandTotalAmountAssetBP { get; set; }
        public decimal? GrandTotalAmountAssetActual { get; set; }
        public decimal? GrandTotalAmountAssetBPActual { get; set; }
        //GrandTotalAmount
        public decimal? GrandTotalAmountAmountBP { get; set; }
        public decimal? GrandTotalAmountActual { get; set; }
        public decimal? GrandTotalAmountBPActual { get; set; }
    }
}