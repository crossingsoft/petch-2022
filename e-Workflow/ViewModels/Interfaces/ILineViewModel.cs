﻿namespace PanasonicProject.ViewModels.Interfaces
{
    public interface ILineViewModel
    {
        int Id { get; set; }
        int ParentId { get; set; }
        bool Deleted { get; set; }
    }
}