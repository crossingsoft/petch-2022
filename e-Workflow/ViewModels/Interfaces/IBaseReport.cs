﻿using System.Collections.Generic;
using Microsoft.Reporting.WebForms;

namespace PanasonicProject.ViewModels.Interfaces
{
    public interface IBaseReportViewModel
    {
        string URL { get; set; }
        string FileName { get; set; }
        string FileType { get; set; }
        int? Page { get; set; }
        ReportDataSource[] ReportDataSources { get; set; }
        string DefaultFileType();
        List<string> AllowFileType();
    }
}