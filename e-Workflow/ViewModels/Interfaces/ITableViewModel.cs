﻿using System;

namespace PanasonicProject.ViewModels.Interfaces
{
    public interface ITableViewModel
    {
        int Id { get; set; }
        string Title { get; set; }
        DateTime? CreatedDate { get; set; }
        DateTime? DocumentDate { get; set; }
        string CreatedBy { get; set; }
        string CreatedByName { get; set; }
        bool Deleted { get; set; }
    }
}