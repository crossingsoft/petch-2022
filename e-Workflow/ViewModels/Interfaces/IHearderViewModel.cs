﻿using System.Collections.Generic;

namespace PanasonicProject.ViewModels.Interfaces
{
    public interface IHearderViewModel
    {
        byte[] Logo { get; set; }
        Dictionary<string, string> SharedImage();
    }
}