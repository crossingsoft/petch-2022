﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PanasonicProject.ViewModels
{
    public class AuthorizeViewModel
    {
        public string Module { get; set; }
        public string Type { get; set; }
    }
}