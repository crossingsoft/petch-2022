﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PanasonicProject.ViewModels
{
    public class GRNCheckOverBudget
    {
        public decimal? UnitPrice { get; set; }
        public decimal? POUnitPrice { get; set; }
        public decimal? LineAmount { get; set; }
        public decimal? LineAmountTH { get; set; }
    }
}