﻿
namespace PanasonicProject.ViewModels
{
    public class SummaryReportLineViewModel : Abstracts.BaseLineViewModel
    {
        public string Department { get; set; }
        public string Section { get; set; }
        public string SectionName { get; set; }
        public string SubSection { get; set; }
        //Expense
        public decimal? ExpenseBP { get; set; }
        public decimal? ExpenseActual { get; set; }
        public decimal? ExpenseBPActual { get; set; }
        //Repair
        public decimal? RepairBP { get; set; }
        public decimal? RepairActual { get; set; }
        public decimal? RepairBPActual { get; set; }
        //Asset
        public decimal? AssetBP { get; set; }
        public decimal? AssetActual { get; set; }
        public decimal? AssetBPActual { get; set; }
        //Total
        public decimal? TotalBP { get; set; }
        public decimal? TotalActual { get; set; }
        public decimal? TotalBPActual { get; set; }
    }
}