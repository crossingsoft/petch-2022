﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using PanasonicProject.Models;

namespace PanasonicProject.ViewModels.PurchaseOrder
{
    public class PurchaseOrderIQueryViewModel
    {
        public long PurchId { get; set; }
        public long? ApplId { get; set; }
        public string VendName { get; set; }
        public bool? IsImport { get; set; }
    }
}