﻿namespace PanasonicProject.ViewModels
{
    public class SummaryReportMatrixSectionLineViewModel : Abstracts.BaseLineViewModel
    {
        public long Number { get; set; }
        public string Investment { get; set; }
        public string Type { get; set; }
        public decimal? Total { get; set; }
    }
}