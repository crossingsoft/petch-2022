﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using PanasonicProject.Models.Permission;

namespace PanasonicProject.ViewModels
{
    public class PermissionViewModel
    {
        public PermissionTable PermissionTable { get; set; }
        public PermissionMember PermissionMember { get; set; }
    }
}