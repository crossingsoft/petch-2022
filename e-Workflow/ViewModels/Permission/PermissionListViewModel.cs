﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using PanasonicProject.Models.Permission;

namespace PanasonicProject.ViewModels
{
    public class PermissionListViewModel
    {
        public List<PermissionViewModel> PermissionViewModels { get; set; } 
    }
}