﻿using System;
using System.Activities;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using PanasonicProject.ViewModels.Abstracts;

namespace PanasonicProject.ViewModels.Report
{
    public class SummaryReportsViewModel : BaseErrorMessageViewModel
    {
        public BaseCriteriaReport Criteria { get; set; }
        public List<string> ReportNames { get; set; }
        public List<string> SubReportNames { get; set; }
        public bool E600 { get; set; }
        public bool E600E { get; set; }
    }
}