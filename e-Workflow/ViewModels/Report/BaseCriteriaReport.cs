﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace PanasonicProject.ViewModels.Report
{
    public class BaseCriteriaReport
    {
        public DateTime? StartDate { get; set; }
        public DateTime? EndDate { get; set; }
        public String CreateBy { get; set; }
        [Required]
        public string ReportName { get; set; }
        public string SubReportName { get; set; }
        public string PrimaryId { get; set; }
        public string DocumentNumber { get; set; }

        public bool? PrePintForm { get; set; }
        public bool IsImport { get; set; }

        public string DivisionId { get; set; }
        public string DepartmentId { get; set; }
        public string SectionId { get; set; }
    }
}