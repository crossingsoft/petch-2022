﻿namespace PanasonicProject.ViewModels
{
    public class SummaryReportBPTypeViewModel 
    {
        public class Table : Abstracts.BaseTableViewModel
        {
            
        }
        public class Line : Abstracts.BaseLineViewModel
        {
            public decimal? BPAmount { get; set; }
            public decimal? ActualAmount { get; set; }
            public decimal? POAmount { get; set; }
        }
    }
}