﻿using System;

namespace PanasonicProject.ViewModels.Report
{
    public class FormAplicationViewModel
    {
        public class Table : Abstracts.BaseTableViewModel
        {
            public string ApplNo { get; set; }
            public string Subject { get; set; }
            public string SectionId { get; set; }
            public string CreatedName { get; set; }
            public DateTime? CreatedDate { get; set; }
            public DateTime? ReqDate { get; set; }
            public string RespPerson { get; set; }
            public string Purpose { get; set; }
            public string BudgetExp { get; set; }
            public string ActionPeriod { get; set; }
            public string FactoryAdv { get; set; }
            public decimal? BudgetAmount { get; set; }
            public decimal? BudgetRemain { get; set; }
            public string BudgetType { get; set; }
            public decimal? BPAmount { get; set; }
            public bool? AirPolution { get; set; }
            public bool? WaterPolution { get; set; }
            public bool? WasteMng { get; set; }
            public bool? ChemMng { get; set; }
            public bool? LawRegulation { get; set; }
            public bool? Bolier { get; set; }
            public bool? Safety { get; set; }
            public bool? Other { get; set; }
            public bool? RegController { get; set; }
            public bool? Controller { get; set; }
            public string IntApplId { get; set; }
            public DateTime? IntDateRecord { get; set; }
            public string RefApplId { get; set; }
            public string Status { get; set; }
        }
        public class Line : Abstracts.BaseLineViewModel
        {
            public string BPNum { get; set; }
            public string BudgetType { get; set; }
            public string RefNumber { get; set; }
            public string FiscalYear { get; set; }
            public string Month { get; set; }
            public string SectionId { get; set; }
            public decimal? BPAmount { get; set; }
            public decimal? BPAvailableAmount { get; set; }
            public decimal? BPReserve { get; set; }
            public string WorkflowType { get; set; }
        }
    }
}