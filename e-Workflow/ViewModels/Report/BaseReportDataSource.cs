﻿using Microsoft.Reporting.WebForms;

namespace PanasonicProject.ViewModels.Report
{
    public class BaseReportDataSource
    {
        public string URL { get; set; }
        public string FileName { get; set; }
        public ReportDataSource[] ReportDataSources { get; set; }
    }
}