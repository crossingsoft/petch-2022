﻿using System.Collections.Generic;
using PanasonicProject.Models;

namespace PanasonicProject.ViewModels
{
    public class WorklistViewModel
    {
        public List<WorklistItem> WorklistItems { get; set; }
    }
}