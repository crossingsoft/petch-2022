﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PanasonicProject.ViewModels
{
    public class CheckBPViewModelstate
    {
        public decimal? BPRemain { get; set; }
        public decimal? BPAval { get; set; }
        public string State { get; set; }
    }
}