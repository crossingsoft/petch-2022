﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PanasonicProject.ViewModels
{
    public class VendorMaster
    {
        public string VendorId { get; set; }
        public string VendorName { get; set; }
    }
}