﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PanasonicProject.ViewModels.Abstracts
{
    public class BaseErrorMessageViewModel
    {
        public bool IsErrorMessage { get; set; }
        public string ErrorMessage { get; set; }
        public string ErrorCssClass { get; set; }
    }
}