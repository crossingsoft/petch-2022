﻿using System.Collections.Generic;
using Microsoft.Reporting.WebForms;

namespace PanasonicProject.ViewModels.Abstracts
{
    public class IBaseReport : Interfaces.IBaseReportViewModel
    {
        public string URL { get; set; }
        public string FileName { get; set; }
        public string FileType { get; set; }
        public int? Page { get; set; }
        public ReportDataSource[] ReportDataSources { get; set; }
        public string DefaultFileType()
        {
            return "pdf";
        }

        public List<string> AllowFileType()
        {
            return new List<string> {"pdf","xls"};
        }
    }
}