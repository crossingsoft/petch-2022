﻿using System;
using System.Collections.Generic;

namespace PanasonicProject.ViewModels.Abstracts
{
    public abstract class BaseTableViewModel : Interfaces.ITableViewModel, Interfaces.IHearderViewModel
    {
        public int Id { get; set; }
        public string Title { get; set; }
        public DateTime? DateFrom { get; set; }
        public DateTime? DateTo { get; set; }
        public DateTime? CreatedDate { get; set; }
        public DateTime? DocumentDate { get; set; }
        public string CreatedBy { get; set; }
        public string CreatedByName { get; set; }
        public bool Deleted { get; set; }
        public byte[] Logo { get; set; }
        public Dictionary<string, string> SharedImage()
        {
            var image = new Dictionary<string, string>();
            image.Add("CheckBoxIsTure",
                "iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAYAAAAf8/9hAAAAuklEQVR42qWScQfFIBTF27dNzIxMxtREkpjE7Nu+98q7U+96T0/nj3Vv55wfsYF0akgfpdTj3+KrM1QAKWVzed93DNi2rRmgtcaAdV1/lowxBDJpRgAhxNeytTafkEk7AizLkk3nHIEZdlCZQQDOeTa99/lMO8ywg9I9AszznM3jONATwAOlDAJM03QHQgj3XN6XPgKM41iFYozk8670EIAx1vgXEHKeJwZQSpsB13VhQHP7rQrQo27AE+MRcBFOD9LhAAAAAElFTkSuQmCC");
            image.Add("CheckBoxIsFalse",
    "iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAYAAAAf8/9hAAAAuklEQVR42qWScQfFIBTF27dNzIxMxtREkpjE7Nu+98q7U+96T0/nj3Vv55wfsYF0akgfpdTj3+KrM1QAKWVzed93DNi2rRmgtcaAdV1/lowxBDJpRgAhxNeytTafkEk7AizLkk3nHIEZdlCZQQDOeTa99/lMO8ywg9I9AszznM3jONATwAOlDAJM03QHQgj3XN6XPgKM41iFYozk8670EIAx1vgXEHKeJwZQSpsB13VhQHP7rQrQo27AE+MRcBFOD9LhAAAAAElFTkSuQmCC");
            return new Dictionary<string, string>();
        }


    }
}