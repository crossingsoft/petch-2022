﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PanasonicProject.ViewModels.Abstracts
{
    public class BaseSmartFormViewModel
    {
        public string Url { get; set; }
        public string DescProcessName { get; set; }
        public string ActivityName { get; set; }
    }
}