﻿namespace PanasonicProject.ViewModels
{
    public static class ParamViewModel
    {
        public static long? ParamId { get; set; }
        public static string Activity { get; set; }
    }
    public static class ParamMenu
    {
        public static string Name { get; set; }
    }
}