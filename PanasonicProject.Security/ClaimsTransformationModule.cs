﻿using System.IdentityModel.Services;
using System.Security.Claims;
using System.Web;
using System.Web.Security;

namespace PanasonicProject
{
    public class ClaimsTransformationModule : ClaimsAuthenticationManager
    {
        public override ClaimsPrincipal Authenticate(string resourceName, ClaimsPrincipal incomingPrincipal)
        {

            if (incomingPrincipal != null && incomingPrincipal.Identity.IsAuthenticated == true)
            {
                var identity = ((ClaimsIdentity)incomingPrincipal.Identity);
                identity.AddClaim(new Claim(ClaimTypes.Role, "Admin"));
                identity.AddClaim(new Claim(ClaimTypes.Role, "IRAdmin"));
            }

            return incomingPrincipal;
        }

        public void SignOut()
        { 
            FederatedAuthentication.SessionAuthenticationModule.CookieHandler.Delete();
            FederatedAuthentication.SessionAuthenticationModule.DeleteSessionTokenCookie();
            FederatedAuthentication.SessionAuthenticationModule.SignOut();
            FederatedAuthentication.WSFederationAuthenticationModule.SignOut(false);
            FormsAuthentication.SignOut();
        }

    }
}