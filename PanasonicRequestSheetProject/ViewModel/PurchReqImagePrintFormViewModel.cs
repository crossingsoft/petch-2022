﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PanasonicRequestSheetProject.ViewModel
{
    public class PurchReqImagePrintFormViewModel
    {
        public int Row { get; set; }
        public string Data { get; set; }
        public string Description { get; set; }
        public string  ItemNo { get; set; }
    }
}