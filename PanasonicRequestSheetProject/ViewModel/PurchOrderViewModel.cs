﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PanasonicRequestSheetProject.ViewModel
{
    public class PurchOrderViewModel
    {
        public int? ReferId { get; set; }
        public string Description { get; set; }
        public decimal? Qty { get; set; }
        public string Unit { get; set; }
        public int OpenPO { get; set; }
        public decimal? POAmount { get; set; }
    }
}