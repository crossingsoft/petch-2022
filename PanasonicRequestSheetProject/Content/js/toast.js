﻿function displayMessage(message, messageType) {
    $("#messagewrapper").html('<div class="messagebox ' + messageType.toLowerCase() + '"></div>');
    $("#messagewrapper .messagebox").html(message+"<i class=\"toast_onhovertext\">&nbsp;(Click to dismiss)</i>");
    displayMessages();
}

function displayMessages() {
    if ($("#messagewrapper").children().length > 0) {
        $("#messagewrapper").show();
//        $(document).click(function () {
//            clearMessages();
        //        });
        $('#messagewrapper').click(function () {
            clearMessages();
        });
    }
    else {
        $("#messagewrapper").hide();
    }
}

function clearMessages() {
    $("#messagewrapper").fadeOut(500, function () {
        $("#messagewrapper").empty();
    });
}

function handleAjaxMessages() {
    $(document).ajaxSuccess(function (event, request) {
        checkAndHandleMessageFromHeader(request);
    }).ajaxError(function (event, request) {
        if (request.responseText) {
            if (request.responseText.length <= 150) {
                displayMessage(request.responseText, "error");
            } else {
                var s = request.responseText.indexOf('<title>');
                var e = request.responseText.indexOf('</title>');
                if (s >= 0 && (e >= 0 && e >= s)) {
                    displayMessage(request.responseText.substr(s + 7, e - (s + 7)), "error");
                } else {
                    displayMessage(request.status + ' ' + request.statusText, "error");
                }
            }
        }
        //displayMessage(request.status + ' ' + request.statusText, "error");
        //displayMessage(request.responseText, "error");
    });
}

function checkAndHandleMessageFromHeader(request) {
    var msg = request.getResponseHeader('X-Message');
    if (msg) {
        displayMessage(msg, request.getResponseHeader('X-Message-Type'));
    }
}	