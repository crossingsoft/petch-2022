﻿
function initFileUpload(url, guid, deleteUrl, hiddenName, textboxName) {
    if (hiddenName == undefined)
        hiddenName = 'fileNames';
    if (textboxName == undefined)
        textboxName = 'fileDescriptions';
    var addIndex = 0;
    var doneIndex = 0;
    $(function () {
        $('#fileupload').fileupload({
            url: url + '?Guid=' + guid,
            dataType: 'json',
            maxFileSize: 500000000,
            singleFileUploads: false,
            add: function (e, data) {
                $.each(data.files, function (index, file) {
                    addIndex = addIndex + 1;
                    $('table#file-list').append(
                        '<tr id="file' + addIndex + '"><td>Uploading ' + file.name + '</td>'
                        + '<td class="file-description">'
                        + '<input type="hidden" name="'+ hiddenName + '" value="'+ file.name +'" />'
                        + '<input type="text" name="'+ textboxName + '" />'
                        + '</td>'
                        //+ '<td class="progress">'
                        //+ '<div><div class="bar" style="width: 0%;"></div></div>'
                        //+ '</td>'
                        + '<td class="delete">'
                        + 'uploading'
                        + '</td></tr>'
                        );
                });
                data.submit();
            },
            done: function (e, data) {
                $.each(data.files, function (index, file) {
                    doneIndex = doneIndex + 1;
                    $('tr#file' + doneIndex).children('td:first').html(file.name);
                    $('tr#file' + doneIndex).children('td.delete').hide().html('<a class="file_delete" href="#" data-file-name="' + file.name + '" data-type="DELETE">Delete</a>').fadeIn('fast');
                });
            },
            fail: function (e, data) {
                $.each(data.files, function (index, file) {
                    doneIndex = doneIndex + 1;
                    $('tr#file' + doneIndex).children('td:first').html(file.name);
                    $('tr#file' + doneIndex).children('td.delete').html('Error');
                });
            }
        });
        $(document).off('click', '.file_delete');
        $(document).on('click', '.file_delete', function () {

            $.ajax({
                context: $(this),
                type: 'DELETE',
                url: deleteUrl + '?f=' + $(this).attr('data-file-name') + '&Guid=' + guid
            }).success(function () {
                $(this).parents('tr').fadeOut('fast', function () {
                    $(this).remove();
                });
            });
            return false;
        });
    });
}