﻿var tipco = { };
function fadeToggle(target, bodyselector ) {
    if (bodyselector === undefined) bodyselector = '.employee-detail';
    $(target).parent().parent().siblings(bodyselector).fadeToggle();
    return false;
}

function slideToggle(target) {
    $(target).parents('.collapsible-header:first').nextAll('div.collapsible-body:first').slideToggle(function () {
        $(target).parents('.collapsible:first').toggleClass('collapsed');
    });
    $(target).children('i.ui-icon:first').toggleClass('ui-icon-triangle-1-e').toggleClass('ui-icon-triangle-1-s');
//    if (isLessThanIE9()) {
//        $(target).children('i').toggleClass('icon-caret-down');
//        $(target).children('i').toggleClass('icon-caret-right');
//    }
    return false;
}

function isLessThanIE8() {
    var html = $("html");
    if (html.hasClass("ie7") || html.hasClass("ie6"))
        return true;
    return false;
}

function isLessThanIE9() {
    if (isLessThanIE8())
        return true;
    var html = $("html");
    if (html.hasClass("ie8"))
        return true;
    return false;
}

function cascadingDropDown(parent, child, url, func) {
    $(document).off('change', parent);
    $(document).on('change', parent, function () {
        for (var i = 0; i < child.length; i++) {
            var $child = $(child[i]);
            $child.empty();
            $child.parent().addClass('ui-autocomplete-loading');
            var val = $(this).val();
            if (val == '') {
                $child.parent().removeClass('ui-autocomplete-loading');
                return;
            }
            $.ajax({
                url: url[i],
                ajaxI: i, // Keep i
                type: 'GET',
                cache: false,
                data: { parm1: val },
                success: function (result) {
                    i = this.ajaxI; // Reinstate the correct value for 'i'.
                    $(result).each(function () {
                        $(child[i]).parent().removeClass('ui-autocomplete-loading');
                        $(child[i]).append(
                            $('<option></option>').val(this.Value).html(this.Text)
                        );
                    });
                }
            });
            if (func !== undefined && func !== null) {
                if (func[i] !== undefined) {
                    func[i](val);
                }
            }
        }
    });
}

function initDatePicker(selector, option) {
    if (option === undefined || option === null) {
        $(selector).datepicker({
            dateFormat: 'dd/mm/yy',
            changeMonth: true,
            changeYear: true,
            showOn: 'both'
        }).next('button').text('Pick a date').button({
            icons: {
                primary: 'ui-icon-calendar'
            },
            text: false
        });
    } else {
        $(selector).datepicker(option).next('button').text('Pick a date').button({
            icons: {
                primary: 'ui-icon-calendar'
            },
            text: false
        });
    }
}

function bindButtonEvent(button, event, targetdiv, url, loadingHtml, data, httpMethod, refreshValidator, confirmationMessage) {
    if (httpMethod === undefined || httpMethod === null)
        httpMethod = 'GET';
    if (refreshValidator === undefined || refreshValidator === null)
        refreshValidator = false;
    if (confirmationMessage !== undefined && confirmationMessage !== null && confirmationMessage != '') {
        $(document).off(event, button);
        $(document).on(event, button, function (e) {
            e.preventDefault();
            if (confirm(confirmationMessage)) {
                $(targetdiv).html(loadingHtml);
                $.ajax({
                    url: url,
                    type: httpMethod,
                    cache: false,
                    data: data,
                    success: function (result) {
                        $(targetdiv).hide();
                        $(targetdiv).html(result);
                        if (refreshValidator == true) {
                            $('form').removeData('validator');
                            $('form').removeData('unobtrusiveValidation');
                            $.validator.unobtrusive.parse('form');
                        }
                        $(targetdiv).fadeIn();
                    }
                });
            }
            return false;
        });
    } else {
        $(document).off(event, button);
        $(document).on(event, button, function (e) {
            e.preventDefault();
            $(targetdiv).html(loadingHtml);
            $.ajax({
                url: url,
                type: httpMethod,
                cache: false,
                data: data,
                success: function (result) {
                    $(targetdiv).hide();
                    $(targetdiv).html(result);
                    if (refreshValidator == true) {
                        $('form').removeData('validator');
                        $('form').removeData('unobtrusiveValidation');
                        $.validator.unobtrusive.parse('form');
                    }
                    $(targetdiv).fadeIn();
                }
            });
            return false;
        });
    }
}

function bindButtonEventFunc(button, event, targetdiv, url, loadingHtml, func, httpMethod, refreshValidator, confirmationMessage, executeFuncBeforeClear, funcSuccess) {
    if (httpMethod === undefined)
        httpMethod = 'GET';
    if (executeFuncBeforeClear === undefined)
        executeFuncBeforeClear = true;
    
    // I SWITCHED FUNCBEFORE DIRECTION!! WATCH OUT!!
    if (confirmationMessage !== undefined && confirmationMessage !== null && confirmationMessage != '') {
        $(document).off(event, button);
        $(document).on(event, button, function (e) {
            e.preventDefault();
            if (confirm(confirmationMessage)) {
                var val = $(button).val();
                if (!executeFuncBeforeClear)
                    $(targetdiv).html(loadingHtml);
                $.ajax({
                    url: url,
                    type: httpMethod,
                    cache: false,
                    data: func(e, val),
                    success: function (result) {
                        $(targetdiv).hide();
                        $(targetdiv).html(result);
                        if (refreshValidator == true) {
                            $('form').removeData('validator');
                            $('form').removeData('unobtrusiveValidation');
                            $.validator.unobtrusive.parse('form');
                        }
                        $(targetdiv).fadeIn();
                        if (funcSuccess !== undefined && funcSuccess !== null) {
                            funcSuccess();
                        }
                    }
                });
                if (executeFuncBeforeClear)
                    $(targetdiv).html(loadingHtml);
            }
            return false;
        });
    } else {
        $(document).off(event, button);
        $(document).on(event, button, function (e) {
            e.preventDefault();
            var val = $(button).val();
            if (!executeFuncBeforeClear)
                $(targetdiv).html(loadingHtml);
            $.ajax({
                url: url,
                type: httpMethod,
                cache: false,
                data: func(e, val),
                success: function (result) {
                    //$(targetdiv).hide();
                    $(targetdiv).html(result);
                    if (refreshValidator == true) {
                        $('form').removeData('validator');
                        $('form').removeData('unobtrusiveValidation');
                        $.validator.unobtrusive.parse('form');
                    }
                    $(targetdiv).fadeIn();
                    if (funcSuccess !== undefined && funcSuccess !== null) {
                        funcSuccess();
                    }
                }
            });
            if (executeFuncBeforeClear)
                $(targetdiv).html(loadingHtml);
            return false;
        });
    }
}

function dialogViewHierarchy(selector, url, requestorSelector) {
    $(selector).click(function (e) {
        e.preventDefault();
        var url2 = url;
        if (requestorSelector !== undefined && requestorSelector !== null) {
            var val = $(requestorSelector).val();
            if (val.indexOf("\\", 0) < 0) {
                $(requestorSelector).catcomplete('search', val);
                return false;
            }
            url2 = url.replace('__id__', val).replace('\\', '!');
        }
        var $dialog = $('<div>' + ajaxLoadingAnimation + '</div>').dialog({
            autoOpen: true,
            title: 'Hierarchy',
            modal: true,
            width: 400,
            height: 367,
            open: function () {
                $.ajax({
                    url: url2,
                    cache: false,
                    success: function (result) {
                        $dialog.html(result);
                    }
                });
            },
            close: function () {
                $dialog.remove();
            }
        });
        //        var $dialog = $('<div></div>')
        //			.load(url)
        //			.dialog({
        //			    autoOpen: true,
        //			    title: 'Hierarchy',
        //			    modal: true,
        //			    width: 400,
        //			    height: 367
        //			});
    });
}
//function bindDropDownEvent(source, event, targetArray, jsonUrl, httpMethod, delegate) {

//    $(source).off('change');
//    $(source).on('change', function () {
//        var $this = $(source);
//        var val = $this.val();
//        if (val == '') {
//            return null;
//        }
//        for (var i = 0; i < targetArray.length; i++) {
//            $(targetArray[i]).parent().addClass('ui-autocomplete-loading');
//        }
//        $.ajax({
//            url: jsonUrl,
//            type: httpMethod,
//            cache: false,
//            data: { parm1: val },
//            success: function (result) {
//                for (var j = 0; j < targetArray.length; j++) {
//                    $(targetArray[j]).parent().removeClass('ui-autocomplete-loading');
//                }
//                if (delegate !== undefined && delegate !== null) {
//                    if (delegate['success']) {
//                        delegate['success'](result);
//                    }
//                }
//                //                $division_text.parent().removeClass('ui-autocomplete-loading');
//                //                $division_text.html(result[1]);
//                //                $department_text.parent().removeClass('ui-autocomplete-loading');
//                //                $department_text.html(result[2]);
//                //                $section_text.parent().removeClass('ui-autocomplete-loading');
//                //                $section_text.html(result[3]);
//                //                $unitofwork_text.parent().removeClass('ui-autocomplete-loading');
//                //                $unitofwork_text.html(result[4]);
//            }
//        });
//        return true;
//    });
//}

function copyDeliveryToFinalDest() {
    //$('#finaldestname_dropdown').val($('#deliveryto_dropdown').val());
    //$('#finaldestaddress_dropdown').val($('#deliveryaddress_dropdown').val());
    $('#finaldestname_textbox').val($('#deliveryname_textbox').val());
    $('#finaldestaddress_textbox').val($('#deliveryaddress_textbox').val());
}

function precise_round(value, decPlaces){
    var val = value * Math.pow(10, decPlaces);
    var fraction = (Math.round((val-parseInt(val))*10)/10);

    //this line is for consistency with .NET Decimal.Round behavior
    // -342.055 => -342.06
    if(fraction == -0.5) fraction = -0.6;

    val = Math.round(parseInt(val) + fraction) / Math.pow(10, decPlaces);
    return val;
}

function executeFunctionByName(functionName, context /*, args */) {
    var args = Array.prototype.slice.call(arguments).splice(2);
    var namespaces = functionName.split(".");
    var func = namespaces.pop();
    for (var i = 0; i < namespaces.length; i++) {
        context = context[namespaces[i]];
    }
    return context[func].apply(this, args);
}

function isString(s) {
    if (typeof s == 'string' || s instanceof String)
        return true;

    return false;
}
function prettifyLookupButton() {
    $(".lookuptextbox-button").not('.ui-button').html('Lookup').button({ icons: {
        primary: "ui-icon-search"
    },
        text: false
    });
}
function split(val) {
    return val.split(/;\s*/);
}
function extractLast(term) {
    return split(term).pop();
}
function _emplLookupAjaxCall(context, jsonUrl, data, response) {
    $.ajax({
        dataType: "json",
        url: jsonUrl,
        data: data,
        element: context,
        success: response,
        error: function () {
            $(this.element.element).removeClass('ui-autocomplete-loading').addClass('ui-autocomplete-loading-error');
        }
    });
}

function buttonTriggered(selector, allowEmpty, t) {
    if ($(t).attr('disabled') != null)
        return false;
    var val = selector.val();
    if (val != '') {
        if (val.length < 2) // Default minimum 2 characters, override it
            val = val + ' ';
        //selector.catcomplete("enable"); // In case of performance woe, disable autocomplete.
        //selector.catcomplete('search', val);
        if (selector.hasClass('customdatacomplete')) {
            selector.customdatacomplete('search', val);
        } else {
            selector.catcomplete('search', val);
        }
        selector.focus();
    } else {
        if (allowEmpty == true || allowEmpty == undefined) {
            //selector.catcomplete("enable"); // In case of performance woe, disable autocomplete.
            if (selector.hasClass('customdatacomplete')) {
                selector.customdatacomplete('search', '  ');
            } else {
                selector.catcomplete('search', '  ');
            }
            selector.focus();
        }
    }
    return false;
}

function endsWith(str, suffix) {
    return str.indexOf(suffix, str.length - suffix.length) !== -1;
}