﻿$.widget("custom.catcomplete", $.ui.autocomplete, {
    _renderMenu: function (ul, items) {
        var that = this, currentCategory = "";
        $.each(items, function (index, item) {
            if (item.category != currentCategory) {
                ul.append("<li class='ui-autocomplete-category'>" + item.category + "</li>");
                currentCategory = item.category;
            }
            that._renderItemData(ul, item);
        });
    }
});

function initEmplLookupMulti(selector, jsonUrl, data) {
    $(selector)
    // don't navigate away from the field on tab when selecting an item
    .bind("keydown", function (event) {
        if (event.keyCode === $.ui.keyCode.TAB &&
            $(this).data("customCatcomplete").menu.active) {
            event.preventDefault();
        }
    })
    .catcomplete({
        minLength: 2,
        source: function (request, response) {
            if (data !== undefined && data !== null) {
                $.extend(data, { term: extractLast(request.term) });
            }
            else {
                data = { term: extractLast(request.term) };
            }
            _emplLookupAjaxCall(this, jsonUrl, data, response);
        },
        search: function () {
            $(this).removeClass('ui-autocomplete-loading-error');
            // custom minLength
            if (this.value == '') // activated by button
                return true;
            var term = extractLast(this.value);
            if (term.length < 2) {
                return false;
            }
            return true;
        },
        focus: function () {
            // prevent value inserted on focus
            return false;
        },
        select: function (event, ui) {
            var terms = split(this.value);
            // remove the current input
            terms.pop();
            // add the selected item
            terms.push(ui.item.value);
            // add placeholder to get the comma-and-space at the end
            terms.push("");
            this.value = terms.join(";");
            return false;
        }
    });
    prettifyLookupButton();
}
function initEmplLookupSingle(selector, jsonUrl, data) {
    $(selector)
    // don't navigate away from the field on tab when selecting an item
    .bind("keydown", function (event) {
        if (event.keyCode === $.ui.keyCode.TAB &&
            $(this).data("customCatcomplete").menu.active) {
            event.preventDefault();
        }
    })
    .catcomplete({
        minLength: 2,
        delay: 1500, // 1.5 seconds delay
        source: function (request, response) {
            if (data !== undefined && data !== null) {
                $.extend(data, { term: extractLast(request.term) });
            }
            else {
                data = { term: extractLast(request.term) };
            }
            _emplLookupAjaxCall(this, jsonUrl, data, response);
            //            $.getJSON(jsonUrl, data, response)
            //                .fail(function (jqxhr, textStatus, error) {
            //                    $(selector).removeClass('ui-autocomplete-loading');
            //                });
        },
        search: function () {
            $(this).removeClass('ui-autocomplete-loading-error');
            return true;
        },
        focus: function () {
            // prevent value inserted on focus
            return false;
        },
        close: function () {
            // In case of performance woe, disable autocomplete.
            //$(selector).catcomplete("disable");
        },
        select: function (event, ui) {
            $(this).removeClass('ui-autocomplete-loading');
        }
    });         // .catcomplete("disable"); // In case of performance woe, disable autocomplete.
    prettifyLookupButton();
}

function initEmplLookupSingleFunc(selector, jsonUrl, datafunc, validatefunc, selectfunc) {
    $(selector)
    // don't navigate away from the field on tab when selecting an item
    .bind("keydown", function (event) {
        if (event.keyCode === $.ui.keyCode.TAB &&
            $(this).data("customCatcomplete").menu.active) {
            event.preventDefault();
        }
    })
    .catcomplete({
        minLength: 2,
        delay: 1500, // 1.5 seconds delay
        source: function (request, response) {
            var data;
            if (isString(datafunc)) {
                data = executeFunctionByName(datafunc, window, this);
            } else {
                data = datafunc(this);
            }
            if (data != null) {
                $.extend(data, { term: extractLast(request.term) });
            }
            else {
                data = { term: extractLast(request.term) };
            }
            _emplLookupAjaxCall(this, jsonUrl, data, response);
        },
        search: function () {
            $(this).removeClass('ui-autocomplete-loading-error');
            if (validatefunc != undefined && validatefunc != null) {
                var r = true;
                if (isString(validatefunc)) {
                    r = executeFunctionByName(validatefunc, window, this);
                } else {
                    r = validatefunc(this);
                }
                if (!r) {
                    $(this).addClass('ui-autocomplete-loading-error');
                }
                return r;
            }
        },
        focus: function () {
            // prevent value inserted on focus
            return false;
        },
        close: function () {
            // In case of performance woe, disable autocomplete.
            //$(selector).catcomplete("disable");
        },
        select: function (event, ui) {
            if (selectfunc != undefined && selectfunc != null & selectfunc != '') {
                return executeFunctionByName(selectfunc, window, this, ui);
            }
        }
    });             // .catcomplete("disable"); // In case of performance woe, disable autocomplete.
    prettifyLookupButton();
}