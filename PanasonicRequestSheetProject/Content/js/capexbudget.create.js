﻿function CapexBudget_Create_Init() {
    var $tb = $('input[name$="selectlist_textbox"]');
    //initCustomDataLookupSingleFunc('#' + $tb.attr('id'), $tb.data('lookupUrl'), $tb.data('datafunc'), $tb.data('validatefunc'));
    initEmplLookupSingleFunc('#' + $tb.attr('id'), $tb.data('lookupUrl'), $tb.data('datafunc'), $tb.data('validatefunc'), $tb.data('selectfunc'));
    $(".btnAddToListBox").button({
        icons: {
            primary: "ui-icon-plus"
        }
    }).click(function (e) {
        e.preventDefault();
        var $p = $(this).parents('.selectlist-wrapper');
        listbox_add($p.find('.selectlist_listbox').attr('id'), $p.find('.selectlist_textbox'), $p.find('.selectlist_hidden'));
    });
    $(".btnRemoveListBox").button({
        icons: {
            primary: "ui-icon-close"
        }
    }).click(function (e) {
        e.preventDefault();
        var $p = $(this).parents('.selectlist-wrapper');
        listbox_remove($p.find('.selectlist_listbox').attr('id'), $p.find('.selectlist_hidden'));
    });
    $(".btnShiftUp").button({
        icons: {
            primary: "ui-icon-arrow-1-n"
        },
        text: false
    }).click(function (e) {
        e.preventDefault();
        var $p = $(this).parents('.selectlist-wrapper');
        listbox_moveup($p.find('.selectlist_listbox').attr('id'), $p.find('.selectlist_hidden'));
    });
    $(".btnShiftDown").button({
        icons: {
            primary: "ui-icon-arrow-1-s"
        },
        text: false
    }).click(function (e) {
        e.preventDefault();
        var $p = $(this).parents('.selectlist-wrapper');
        listbox_movedown($p.find('.selectlist_listbox').attr('id'), $p.find('.selectlist_hidden'));
    });
    $('.selectlist-buttons-wrapper').buttonset();
}
$(function () {
    //CapexBudget_Create_Init();
});
function budgetLookup_Validate(e) {
    var company = $('#company_dropdown').val();
    if (company == '') {
        displayMessage('Please select company.', 'error');
        return false;
    }
    var costcenter = $('#costcenter_textbox').val();
    if (costcenter == '') {
        displayMessage('Please select cost center.', 'error');
        return false;
    }
    var requestorposition = $('#position_dropdown').val();
    if (requestorposition == '') {
        displayMessage('Please select position', 'error');
        return false;
    }
    return true;
}
function budgetLookup_Data(e) {
    var company = $('#company_dropdown').val();
    var costcenter = $('#costcenter_textbox').val();
    var requestorposition = $('#position_dropdown').val();
    var o = { company: company, costcenter: costcenter, requestorposition: requestorposition };
    return o;
}
function budgetLookup_Select(e, ui) {
    $(e).val(ui.item.label);
    $(e).data('label', ui.item.label);
    $(e).data('id', ui.item.value);
    return false;
}
function listbox_add(listId, $t, $h) {
    var label = $t.data('label');
    if (label == undefined || label == '' || label != $t.val()) return false;
    var newId = $t.data('id');
    var vs = $h.val().split(',');
    for (var i = 0; i < vs.length; i++) {
        if (vs[i] == newId) {
            displayMessage('Duplicated Capex Budget No.', 'error');
            return false;
        }
    }
    $('#' + listId).append($('<option>').attr('value', newId).attr('title', label).html(label));
    //$('#' + listId).append($('<option>').attr('value', newId).html(label));
    var v = $h.val();
    if (v != undefined && v != '' && !endsWith(v, ',')) {
        v = v + ',';
    }
    v = v + newId;
    $h.val(v);
    $t.val('');
    $t.data('id', '');
    $t.data('label', '');
}
function listbox_remove(listId, $h) {
    var tmp = [];
    $('#' + listId + ' option:selected').each(function (i, o) {
        tmp.push(o.index);
        $(this).remove();
    });
    if (tmp.length > 0) {
        var vs = $h.val().split(',');
        var len = tmp.length;
        while (len--) {
            vs.splice(tmp[len], 1);
        }
        $h.val(vs.join(','));
    }
}
function listbox_moveup(listId, $h) {
    var tmp = [];
    $('#' + listId + ' option:selected').each(function (i, o) {
        if (o.index == 0) return false;
        tmp.push(o.index);
        $(o).insertBefore($(o).prev());
    });
    if (tmp.length > 0) {
        var vs = $h.val().split(',');
        for (var j = 0; j < tmp.length; j++) {
            var l = vs[tmp[j]-1];
            vs[tmp[j] - 1] = vs[tmp[j]];
            vs[tmp[j]] = l;
        }
        $h.val(vs.join(','));
    }
}
function listbox_movedown(listId, $h) {
    var tmp = [];
    var itemsCount = $('#' + listId + ' option').length;
    $($('#' + listId + ' option:selected').get().reverse()).each(function (i, o) {
        if (o.index + 1 == itemsCount) return false;
        tmp.push(o.index);
        $(o).insertAfter($(o).next());
    });
    if (tmp.length > 0) {
        var vs = $h.val().split(',');
        for (var j = 0; j < tmp.length; j++) {
            var l = vs[tmp[j] + 1];
            vs[tmp[j] + 1] = vs[tmp[j]];
            vs[tmp[j]] = l;
        }
        $h.val(vs.join(','));
    }
}

function OnBudget_onChanged(e, t) {

  
    //console.log($(t).val());
    var val = $(t).val();
    if (val == "Budget") {
        //$('#unplannedbudget_textbox').val('');
        $('#unplannedbudget_textbox').attr('disabled', 'disabled');
        $('.selectlist-wrapper').find('button,input,a,select').removeAttr('disabled');
    } else {
        $('#unplannedbudget_textbox').removeAttr('disabled');
        $('.selectlist-wrapper').find('button,input,a,select').attr('disabled', 'disabled');
    }
}