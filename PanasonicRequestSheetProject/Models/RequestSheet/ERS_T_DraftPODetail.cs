//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace PanasonicRequestSheetProject.Models.RequestSheet
{
    using System;
    using System.Collections.Generic;
    
    public partial class ERS_T_DraftPODetail
    {
        public int DraftPOID { get; set; }
        public Nullable<int> DraftPOHeaderID { get; set; }
        public Nullable<int> PRDetail_ID { get; set; }
        public string PRNo { get; set; }
        public Nullable<int> SeqNo { get; set; }
        public Nullable<int> ItemId { get; set; }
        public string ItemCode { get; set; }
        public string ItemName { get; set; }
        public string Description { get; set; }
        public Nullable<decimal> Qty { get; set; }
        public Nullable<decimal> UnitPrice { get; set; }
        public Nullable<bool> OpenPOComplete { get; set; }
        public Nullable<decimal> AlreadyPOAMT { get; set; }
        public Nullable<decimal> OpenPOAmt { get; set; }
        public string Unit { get; set; }
        public Nullable<decimal> Amount { get; set; }
        public string Remark { get; set; }
        public Nullable<System.DateTime> CreateDate { get; set; }
        public string CreateBy { get; set; }
        public Nullable<System.DateTime> UpdateDate { get; set; }
        public string UpdateBy { get; set; }
    }
}
