//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace PanasonicRequestSheetProject.Models.RequestSheet
{
    using System;
    
    public partial class SP_ERS_DraftPO_Result
    {
        public Nullable<int> RequestHDID { get; set; }
        public int RequestDTID { get; set; }
        public string PRNumber { get; set; }
        public string ItemCode { get; set; }
        public string Description { get; set; }
        public Nullable<decimal> Qty { get; set; }
        public string Unit { get; set; }
        public Nullable<decimal> OpenPOAMT { get; set; }
        public string Remark { get; set; }
        public Nullable<decimal> UnitPrice { get; set; }
    }
}
