﻿using PanasonicRequestSheetProject.Models.RequestSheet;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Configuration;

namespace PanasonicRequestSheetProject.Services
{
    public class AttachmentService
    {
        private object _lock = new object();
        public bool SyncAttachFileInfo(int id, HttpPostedFileBase file)
        {
            string filePath = "";

            try
            {
                var serviceAttach = new RequestSheetEntities();

                var attachment = serviceAttach.ERS_T_AttachFileTable.FirstOrDefault(m => m.AttachID == id);

                if (attachment == null || file == null)
                    return true;


                string subFloder = attachment.RequestHDRefer.ToString();
            
                string apppicationId = WebConfigurationManager.AppSettings["AttachmentPath"] + @"\AttachFileInfo\" + subFloder;


                var service = new RequestSheetEntities();

                var updateAttachment = service.ERS_T_AttachFileTable.FirstOrDefault(m => m.AttachID == attachment.AttachID);
                var date = DateTime.Now;

                string path = @apppicationId;
                var extension = Path.GetExtension(file.FileName);

                if (extension.ToLower().Contains("jpg") || extension.ToLower().Contains("png"))
                {
                    filePath = Path.Combine(path, "Ori" + file.FileName);
                }
                else
                {
                    filePath = Path.Combine(path, file.FileName);
                }

                if (!Directory.Exists(path))
                {
                    Directory.CreateDirectory(path);
                }

                if (File.Exists(filePath))
                {
                    File.Delete(filePath);
                }

                file.SaveAs(filePath);

                if (extension.ToLower().Contains("jpg") || extension.ToLower().Contains("png"))
                {
                    var ext = extension.Replace(".", "");
                    Resize(filePath, file.FileName, file.InputStream.Length, ext);
                }

                var urlWebApp = HttpContext.Current.Request.Url.AbsoluteUri.Replace(HttpContext.Current.Request.Url.AbsolutePath, "");
                urlWebApp = String.Format("{0}/attachment/downloadattachmentinfo?id={1}", urlWebApp, updateAttachment.AttachID);

                updateAttachment.Path = filePath.Replace("Ori","");
                updateAttachment.OriginFileName = file.FileName;
                service.SaveChanges();

                return true;
            }
            catch (Exception exception)
            {
                throw new Exception(exception.Message);
            }
            finally
            {
                if (filePath.Contains("Ori"))
                {
                    File.Delete(filePath);
                }
            }
        }

        private void Resize(string path, string fileName, long originalSize, string fileExtension)
        {
            System.Drawing.Image img = System.Drawing.Image.FromFile(path);
            var result = ResizeImage(img, 640, 480);
            path = path.Replace("Ori", "");
            result.Save(path, ImageFormat.Png);
            img.Dispose();
        }

        //image resize method
        public static Bitmap ResizeImage(Image image, int width, int height)
        {
            var destRect = new Rectangle(0, 0, width, height);
            //create new destImage object
            var destImage = new Bitmap(width, height);

            //maintains DPI regardless of physical size
            destImage.SetResolution(image.HorizontalResolution, image.VerticalResolution);

            using (var graphics = Graphics.FromImage(destImage))
            {
                //determines whether pixels from a source image overwrite or are combined with background pixels.
                graphics.CompositingMode = CompositingMode.SourceCopy;
                //determines the rendering quality level of layered images.
                graphics.CompositingQuality = CompositingQuality.HighQuality;
                // determines how intermediate values between two endpoints are calculated
                graphics.InterpolationMode = InterpolationMode.HighQualityBicubic;
                //specifies whether lines, curves, and the edges of filled areas use smoothing 
                graphics.SmoothingMode = SmoothingMode.HighQuality;
                //affects rendering quality when drawing the new image
                graphics.PixelOffsetMode = PixelOffsetMode.HighQuality;

                using (var wrapMode = new ImageAttributes())
                {
                    //prevents ghosting around the image borders
                    wrapMode.SetWrapMode(WrapMode.TileFlipXY);
                    graphics.DrawImage(image, destRect, 0, 0, image.Width, image.Height, GraphicsUnit.Pixel, wrapMode);
                }
            }

            return destImage;
        }

    }
}