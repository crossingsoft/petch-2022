﻿using PanasonicRequestSheetProject.Models.K2;
using PanasonicRequestSheetProject.Models.RequestSheet;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PanasonicRequestSheetProject.Facades
{
    public class RequestSheetFacade
    {
        public string ConvertRequestSheetToPurchOrder(string documentNumber, int id)
        {
            var k2Service = new PETCHEntities();

            var requestService = new RequestSheetEntities();
            var draftPOTable = requestService.ERS_T_DraftPOHeader.FirstOrDefault(m => m.DraftPOHeaderID == id);
            var draftPOLines = requestService.ERS_T_DraftPODetail.Where(m => m.DraftPOHeaderID == id).ToListSafe();

            var applicationForm = new ApplicationForm();
            if (draftPOTable.AppID.HasValue)
            {
                applicationForm = k2Service.ApplicationForms.FirstOrDefault(m => m.ApplId == draftPOTable.AppID);
            }

            List<int?> prIdLines = draftPOLines.Select(s => s.PRDetail_ID).ToListSafe();
            List<String> prNumbers = draftPOLines.Select(s => s.PRNo).ToListSafe().Distinct().ToListSafe();

            var prTables = requestService.ERS_T_RequestHeader.Where(m => prNumbers.Contains(m.PRNumber)).ToListSafe().Distinct().ToListSafe();

            var poTable = new PurchTable();
            poTable.SectionId = draftPOTable.Section;
            poTable.DepartmentId = draftPOTable.Department;
            poTable.FactoryId = draftPOTable.Division;
            poTable.FactoryName = draftPOTable.DivisionName;
            poTable.ApplId = draftPOTable.AppID;
            poTable.CreatedBy = draftPOTable.CreateBy;
            poTable.CreatedDate = DateTime.Now;
            poTable.POStatus = "Draft";
            poTable.CurrencyCode = draftPOTable.CurrencyCode;
            poTable.CurrencyRate = draftPOTable.CurrencyRate;

            poTable.ApplNo = applicationForm.ApplNo;
            poTable.PurchReqNum = "";
            poTable.Orderer = "";
            poTable.FromRequestSheet = true;
            poTable.PurchNum = "";

            foreach (var prNumber in draftPOLines.Select(m => m.PRNo).ToListSafe().Distinct())
            {
                poTable.PurchReqNum = string.Format("{0},{1}", poTable.PurchReqNum, prNumber);
                poTable.PurchReqNum = poTable.PurchReqNum.Trim(',');
            }

            foreach (var createdBy in prTables.Select(m => m.CreateBy).ToListSafe().Distinct())
            {
                poTable.Orderer = string.Format("{0},{1}", poTable.Orderer, createdBy);
                poTable.Orderer = poTable.Orderer.Trim(',');
            }

            var k2POService = new PETCHEntities();
            poTable = k2POService.PurchTables.Add(poTable);
            k2POService.SaveChanges();

            var k2POLineService = new PETCHEntities();
            foreach (var poDraftLine in draftPOLines)
            {
                var poTableLine = new PurchLine();
                poTableLine.PurchId = poTable.PurchId;
                poTableLine.Items = poDraftLine.ItemId.HasValue ? poDraftLine.ItemId.ToString() : "";
                poTableLine.ItemName = poDraftLine.ItemName;
                poTableLine.ItemCode = poDraftLine.ItemCode;
                poTableLine.Qty = poDraftLine.Qty;
                poTableLine.UnitPrice = poDraftLine.UnitPrice;
                poTableLine.LineAmountTH = poDraftLine.UnitPrice * poDraftLine.Qty;
                poTableLine.LineAmount = (poDraftLine.UnitPrice * draftPOTable.CurrencyRate) * poDraftLine.Qty;
                poTableLine.SectionId = draftPOTable.Section;
                poTableLine.CurrencyRate = draftPOTable.CurrencyRate;
                poTableLine.CurrencyId = draftPOTable.CurrencyCode;
                poTableLine.CurrencyFullName = draftPOTable.CurrencyFullName;
                poTableLine.ApplId = draftPOTable.AppID;
                poTableLine.ItemNameDescription = poDraftLine.Description;
                poTableLine.PRDetailID = poDraftLine.PRDetail_ID;
                poTableLine.PRDocumentNumber = poDraftLine.PRNo;

                k2POLineService.PurchLines.Add(poTableLine);
            }

            k2POLineService.SaveChanges();

            foreach (var draftPOLine in draftPOLines)
            {
                var requestUpdatePRService = new RequestSheetEntities();
                var line = requestUpdatePRService.ERS_T_RequestDetail.FirstOrDefault(m => m.RequestDTID == draftPOLine.PRDetail_ID);
                line.OpenPOAMT = line.OpenPOAMT + draftPOLine.Qty;
                requestUpdatePRService.SaveChanges();

                var checkStatus = requestUpdatePRService.ERS_T_RequestDetail.All(m => m.OpenPOAMT >= m.Qty && m.RequestHDRefer == line.RequestHDRefer);
                if (checkStatus)
                {
                    var header = requestUpdatePRService.ERS_T_RequestHeader.FirstOrDefault(m => m.RequestHDID == line.RequestHDRefer);
                    header.OpenPOStatus = "Completed";
                    header.POCompleteDate = DateTime.Now;
                    requestUpdatePRService.SaveChanges();
                }
            }

            var serviceUpdate = new PETCHEntities();
            var modelUpdate = serviceUpdate.PurchTables.FirstOrDefault(m => m.PurchId == poTable.PurchId);
            modelUpdate.PurchNum = "DP"+poTable.PurchId.ToString().PadLeft(6,'0');
            serviceUpdate.SaveChanges();

            return modelUpdate.PurchNum;
        }

        public string ConvertRequestSheetToApplication(string documentNumber, string id, string sectionId, string departmentId, string username, string divisionId, string budgetType)
        {
            var k2Service = new PETCHEntities();
            var requestService = new RequestSheetEntities();

            var app = new ApplicationForm();
            app.SectionId = sectionId;
            app.DepartmentId = departmentId;
            app.RequesterBy = username;
            app.RequesterDate = DateTime.Now;
            app.CreatedName = username;
            app.CreatedDate = DateTime.Now;
            app.DivisionId = divisionId;
            app.ApplTypeSelect = budgetType;
            app.ApplNo = documentNumber;
            app.Status = "Draft";
            app.DocumentStatus = "Draft";

            k2Service.ApplicationForms.Add(app);
            k2Service.SaveChanges();

            var ids = id.Contains(",") ? id.Split(',').ToListSafe() : new List<string>() { id };

            foreach (var prId in ids)
            {
                if (string.IsNullOrEmpty(prId))
                {
                    continue;
                }

                int k = Convert.ToInt16(prId);
                var prTable = requestService.ERS_T_RequestHeader.FirstOrDefault(m => m.RequestHDID == k);
                prTable.ApplicationID = (int)app.ApplId;
                requestService.SaveChanges();
            }

            var serviceUpdate = new PETCHEntities();
            var modelUpdate = serviceUpdate.ApplicationForms.FirstOrDefault(m => m.ApplId == app.ApplId);
            modelUpdate.ApplNo = "DA" + app.ApplId.ToString().PadLeft(6, '0');
            serviceUpdate.SaveChanges();

            return modelUpdate.ApplNo; 
        }

        public bool DeletePOLineWithPR(int id)
        {
            var k2Service = new PETCHEntities();
            var poLine = k2Service.PurchLines.FirstOrDefault(m => m.PurchLineId == id);
            
            var requestUpdatePRService = new RequestSheetEntities();
            var line = requestUpdatePRService.ERS_T_RequestDetail.FirstOrDefault(m => m.RequestDTID == poLine.PRDetailID);
            line.OpenPOAMT = line.OpenPOAMT - poLine.Qty;
            requestUpdatePRService.SaveChanges();

            k2Service.PurchLines.Remove(poLine);
            k2Service.SaveChanges();

            return true;
        }

        public bool DeletePOTableWithPR(int id)
        {
            var k2Service = new PETCHEntities();

            var poTable = k2Service.PurchTables.FirstOrDefault(m => m.PurchId == id);
            var poLines = k2Service.PurchLines.Where(m => m.PurchId == id).ToListSafe();

            foreach (var poLine in poLines)
            {
                var requestUpdatePRService = new RequestSheetEntities();
                var line = requestUpdatePRService.ERS_T_RequestDetail.FirstOrDefault(m => m.RequestDTID == poLine.PRDetailID);
                line.OpenPOAMT = line.OpenPOAMT - poLine.Qty;
                requestUpdatePRService.SaveChanges();
            }

            foreach (var poLine in poLines)
            {
                k2Service.PurchLines.Remove(poLine);
            }

            k2Service.SaveChanges();

            return true;
        }

        public bool ReceivePurchOrder(int id)
        {
            var requestUpdatePRService = new RequestSheetEntities();
            var summary = requestUpdatePRService.ERS_T_POSummaryLine.Where(m => m.PurchTableID == id && m.ReceiveQty.HasValue).ToListSafe();

            foreach (var row in summary)
            {
                var k2Service = new PETCHEntities();
                var poLines = k2Service.PurchLines.Where(m => m.ItemCode == row.ItemCode && m.PurchId == id).ToListSafe().OrderBy(m=>m.PRDetailID);

                var receive = row.ReceiveQty;
                foreach (var poLine in poLines)
                {
                    if(receive <= 0)
                    {
                        continue;
                    }

                    if(poLine.Qty > receive)
                    {
                        var requestUpdatePRLineService = new RequestSheetEntities();
                        var prLine = requestUpdatePRLineService.ERS_T_RequestDetail.FirstOrDefault(m => m.RequestDTID == poLine.PRDetailID);

                        prLine.ReceiveAMT = prLine.ReceiveAMT + receive;
                        prLine.OpenPOAMT = prLine.OpenPOAMT - receive;

                        receive = 0;
                        requestUpdatePRLineService.SaveChanges();

                        var all = requestUpdatePRLineService.ERS_T_RequestDetail.All(m => m.Qty == m.ReceiveAMT && m.RequestHDRefer == prLine.RequestHDRefer);
                        if (all)
                        {
                            var requestUpdatePRHeaderService = new RequestSheetEntities();
                            var table = requestUpdatePRHeaderService.ERS_T_RequestHeader.First(m => m.RequestHDID == prLine.RequestHDRefer);
                            table.ItemReceiveStatus = "Completed";
                            requestUpdatePRHeaderService.SaveChanges();
                        }
                    }
                    else
                    {
                        var requestUpdatePRLineService = new RequestSheetEntities();
                        var prLine = requestUpdatePRLineService.ERS_T_RequestDetail.FirstOrDefault(m => m.RequestDTID == poLine.PRDetailID);

                        var returnQty = receive - (receive - poLine.Qty);
                        prLine.ReceiveAMT = returnQty;

                        receive = receive - poLine.Qty;
                        requestUpdatePRLineService.SaveChanges();

                        var all = requestUpdatePRLineService.ERS_T_RequestDetail.All(m => m.Qty == m.ReceiveAMT && m.RequestHDRefer == prLine.RequestHDRefer);
                        if (all)
                        {
                            var requestUpdatePRHeaderService = new RequestSheetEntities();
                            var table = requestUpdatePRHeaderService.ERS_T_RequestHeader.First(m => m.RequestHDID == prLine.RequestHDRefer);
                            table.ItemReceiveStatus = "Completed";
                            requestUpdatePRHeaderService.SaveChanges();
                        }
                    }
                }
            }

            return true;
        }

    }
}