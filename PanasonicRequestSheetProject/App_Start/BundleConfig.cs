﻿using System.Web;
using System.Web.Optimization;

namespace PanasonicRequestSheetProject
{
    public class BundleConfig
    {
        // For more information on bundling, visit https://go.microsoft.com/fwlink/?LinkId=301862
        public static void RegisterBundles(BundleCollection bundles)
        {

            bundles.Add(new ScriptBundle("~/bundles/jquery").Include(
                        "~/Scripts/jquery.min.js",
                        "~/Content/js/jquery.exists.js"));

            bundles.Add(new ScriptBundle("~/bundles/jqueryui").Include(
              "~/Scripts/jquery-ui-{version}.js",
              "~/Scripts/jquery.global.js",
              "~/Scripts/jquery.blockUI.js",
              "~/Scripts/jquery-ui-11.js"));

            bundles.Add(new ScriptBundle("~/bundles/jqueryplugins").Include(
               "~/Scripts/jquery.hoverIntent.js",
               "~/Scripts/superfish.js",
               "~/Scripts/supersubs.js",
               "~/Scripts/jquery.placeholder-enhanced.js",
               "~/Scripts/jquery.tablesorter.js",
               "~/Content/js/toast.js",
               "~/Content/js/form.js"));

            bundles.Add(new ScriptBundle("~/bundles/fileupload").Include(
                        "~/Scripts/jquery.iframe-transport.js",
                        "~/Scripts/jquery.fileupload.js"
                        ));

            bundles.Add(new ScriptBundle("~/bundles/jqueryval").Include(
                        "~/Scripts/jquery.validate*"));

            bundles.Add(new ScriptBundle("~/bundles/modernizr").Include(
                        "~/Scripts/modernizr-*"));

            bundles.Add(new ScriptBundle("~/bundles/capexbudget.create").Include(
               "~/Content/js/capexbudget.create.js"));

            bundles.Add(new ScriptBundle("~/bundles/bootstrap").Include(
                      "~/Scripts/bootstrap.js",
                      "~/Scripts/kendo.all.min.js",
                        "~/Scripts/kendo.aspnetmvc.min.js",
                        "~/Scripts/kendo.timezones.min.js",
                        "~/Scripts/photo.js"));

            bundles.Add(new StyleBundle("~/Content/css").Include(
                      "~/Content/site.css"));

            bundles.Add(new StyleBundle("~/Content/css/css").Include(
                "~/Content/bootstrap.css",
               "~/Content/css/cssreset.css",
               "~/Content/skeleton/skeleton.css",
               "~/Content/css/superfish.css",
               "~/Content/css/Site.css",
               "~/Content/skeleton/layout.css",
               "~/Content/web/kendo.common.min.css",
               "~/Content/web/kendo.default.min.css",
                "~/Content/css/photo.css"));


            bundles.Add(new StyleBundle("~/Content/css/viewImg").Include(
               "~/Content/bootstrap.css",
               "~/Content/css/photo.css"));


            bundles.Add(new ScriptBundle("~/bundles/viewImg").Include(
                         "~/Scripts/jquery-ui-11.js",
                      "~/Scripts/photo.js"));

            bundles.IgnoreList.Clear();
        }
    }
}
