﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Xml.Linq;

namespace PanasonicRequestSheetProject
{
    public static class OverrideHelper
    {
        public static List<TSource> ToListSafe<TSource>(this IEnumerable<TSource> source)
        {
            try
            {
                if (source == null)
                {

                    return new List<TSource>();
                }
                else
                {
                    return source.ToList();
                }
            }
            catch (Exception exception)
            {
                throw new Exception();
            }
        }

        public static string ToImagePrintForm(this string data)
        {
            if (string.IsNullOrEmpty(data))
            {
                return "";
            }

            if (data.Contains("<content>"))
            {
                XDocument doc = XDocument.Parse(data);

                var content = "";
                foreach (XElement element in doc.Descendants("content"))
                {
                    content = element.FirstNode.ToString();
                }

                return content;
            }

            return data;
        }
    }
}