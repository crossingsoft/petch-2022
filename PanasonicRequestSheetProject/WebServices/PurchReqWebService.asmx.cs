﻿using PanasonicRequestSheetProject.Facades;
using PanasonicRequestSheetProject.Models.RequestSheet;
using PanasonicRequestSheetProject.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;

namespace PanasonicRequestSheetProject.WebServices
{
    /// <summary>
    /// Summary description for PurchReqWebService
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    // [System.Web.Script.Services.ScriptService]
    public class PurchReqWebService : System.Web.Services.WebService
    {

        [WebMethod]
        public List<PurchOrderViewModel> PurchReqList(int referId)
        {
            var service = new RequestSheetEntities();
            var results = new List<PurchOrderViewModel>();
            var models = service.ERS_T_RequestHeader.FirstOrDefault(m => m.RequestHDID == referId);
            var lines = service.ERS_T_RequestDetail.Where(m => m.RequestHDRefer == referId);

            if (models == null)
            {
                return results;
            }

            foreach (var line in lines.ToList())
            {
                var result = new PurchOrderViewModel();
                result.ReferId = line.RequestHDRefer;
                result.Description = line.Description;
                result.Qty = line.Qty;
                result.Unit = line.Unit;
                result.OpenPO = 0;//w8 PO Line Table
                result.POAmount = Math.Abs((int)(0 - line.Qty));
                
                results.Add(result);
            }

            return results;
        }

        [WebMethod]
        public string ConvertRequestSheetToPurchOrder(string documentNumber,int id)
        {
            var facade = new RequestSheetFacade();
            return facade.ConvertRequestSheetToPurchOrder(documentNumber, id);
        }

        [WebMethod]
        public string ConvertRequestSheetToApplication(string documentNumber,string id, string sectionId, string departmentId, string username, string divisionId, string budgetType)
        {
            var facade = new RequestSheetFacade();
            return facade.ConvertRequestSheetToApplication(documentNumber, id, sectionId, departmentId, username, divisionId, budgetType);
        }

        [WebMethod]
        public bool DeletePOLineWithPR(int id)
        {
            var facade = new RequestSheetFacade();
            return facade.DeletePOLineWithPR(id);
        }

        [WebMethod]
        public bool DeletePOTableWithPR(int id)
        {
            var facade = new RequestSheetFacade();
            return facade.DeletePOTableWithPR(id);
        }

        [WebMethod]
        public bool ReceivePurchOrder(int id)
        {
            var facade = new RequestSheetFacade();
            return facade.ReceivePurchOrder(id);
        }
    }
}
