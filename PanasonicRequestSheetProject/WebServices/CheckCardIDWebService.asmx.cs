﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;

namespace PanasonicRequestSheetProject.WebServices
{
    /// <summary>
    /// Summary description for CheckCardIDWebService
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    // [System.Web.Script.Services.ScriptService]
    public class CheckCardIDWebService : System.Web.Services.WebService
    {

        [WebMethod]
        public bool CheckCardID(string cardId)
        {
            try
            {
                bool check = true;
                string card12Char = cardId.Remove(cardId.Length-1);
                int sum = 0;
                if(card12Char.Length == 12)
                {
                    for(int i =0; i< card12Char.Length; i++)
                    {
                        int cardNo = Int32.Parse(card12Char[i].ToString());
                        sum = sum + ((13 - i) * cardNo);
                    }
                    int mod = sum % 11;
                    int subtractMod = 11 - mod;
                    int Digit = subtractMod % 10;

                    card12Char = card12Char + Digit.ToString();
                    if (card12Char.Equals(cardId))
                    {
                        check = true;
                    }
                    else
                    {
                        check = false;
                    }
                }
                else
                {
                    check = false;
                }
                return check;
                //return card12Char + " " + cardId;
            }
            catch (Exception exception)
            {
                throw new Exception(exception.Message);
            }
            
        }
    }
}
