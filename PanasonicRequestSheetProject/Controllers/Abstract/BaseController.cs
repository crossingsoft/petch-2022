﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Data.Entity.Validation;
using System.Drawing.Imaging;
using System.Drawing.Printing;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Web;
using System.Web.Mvc;
using Microsoft.Reporting.WebForms;

namespace PanasonicRequestSheetProject.Controllers.Abstract
{
    public abstract class BaseController : Controller
    {
       
        protected static string FormatMessage(Exception exception, string errorMessage)
        {

            var msg = new StringBuilder();
            if (!string.IsNullOrEmpty(errorMessage))
                msg.AppendLine(errorMessage);
            if (!string.IsNullOrEmpty(exception.Message))
                msg.Append(exception.Message);
            msg.Replace(Environment.NewLine, "<br/>");

            return msg.ToString();
        }
        protected string HandleDbEntityValidationException(DbEntityValidationException dbEx, string separator = "<br/>")
        {
            var list = new List<string>();
            foreach (var validationErrors in dbEx.EntityValidationErrors)
            {
                foreach (var validationError in validationErrors.ValidationErrors)
                {
                    ModelState.AddModelError(string.Empty, validationError.ErrorMessage);
                    list.Add(validationError.ErrorMessage);
                }
            }
            return String.Join(separator, list);
        }
        protected ActionResult Report(string reportName, HttpRequestBase httpRequestBase, ReportDataSource[] reportDataSources,ReportParameter[] reportParameters, string fileNameStr, string format = "PDF", int page = 0)
        {
            if (format == null || format.Equals("web", StringComparison.InvariantCultureIgnoreCase))
            {
                var url = string.Format(@"~/Reports/{0}.aspx", reportName);
                if (httpRequestBase.RequestContext.RouteData.Values.ContainsKey("id"))
                {
                    var id = httpRequestBase.RequestContext.RouteData.Values["id"];
                    url += string.Format(@"?{0}={1}", "id", id);
                }



                var allKey = httpRequestBase.QueryString.AllKeys;
                if (allKey.Any())
                {
                    url += "?";
                }

                for (int i = 0; i < httpRequestBase.QueryString.Count; i++)
                {
                    url += string.Format("{0}={1}", allKey[i], httpRequestBase.QueryString[i]);
                    if (i != httpRequestBase.QueryString.Count - 1)
                        url += "&";
                }
                Response.Redirect(url);
                return null;
            }
            else
            {

                var zipping = false;

                var localReport = new LocalReport();

                localReport.ReportPath = Server.MapPath(string.Format(@"~/Reports/{0}.rdlc", reportName));

                foreach (var reportDataSource in reportDataSources)
                {
                    localReport.DataSources.Add(reportDataSource);
                }

                localReport.SetParameters(reportParameters);


                string reportType;
                string outputFormat;
                if (format.Equals("jpeg", StringComparison.InvariantCultureIgnoreCase))
                {
                    reportType = "image";
                    outputFormat = "jpeg";
                    if (page == 0)
                    {
                        zipping = true;
                    }
                }
                else if (format.Equals("png", StringComparison.InvariantCultureIgnoreCase))
                {
                    reportType = "image";
                    outputFormat = "png";
                    if (page == 0)
                    {
                        zipping = true;
                    }
                }
                else if (format.Equals("tiff", StringComparison.InvariantCultureIgnoreCase))
                {
                    reportType = "image";
                    outputFormat = "tiff";
                    page = 0;
                }
                else if (format.Equals("excel", StringComparison.InvariantCultureIgnoreCase))
                {
                    reportType = "Excel";
                    outputFormat = "xls";
                    page = 0;
                }
                else if (format.Equals("word", StringComparison.InvariantCultureIgnoreCase))
                {
                    reportType = "Word";
                    outputFormat = "doc";
                    page = 0;
                }
                else
                {
                    reportType = "PDF";
                    outputFormat = "PDF";
                    page = 0;
                }
                string mimeType = string.Empty;
                string encoding;
                string fileNameExtension = null;
                if (zipping == false)
                {
                    string deviceInfo = string.Format(
                        "<DeviceInfo>" +
                        "  <OutputFormat>{0}</OutputFormat>" +
                        "  <StartPage>{1}</StartPage>" +
                        "</DeviceInfo>", outputFormat, page);

                    Warning[] warnings;
                    string[] streams;
                    byte[] renderedBytes;

                    var paramReport = localReport.GetParameters();
                    var paramDic = paramReport.ToDictionary(info => info.Name);
                    if (paramDic.ContainsKey("UserID"))
                    {
                        localReport.SetParameters(new ReportParameter("UserID", HttpContext.User.Identity.Name));
                    }

                    //Render the report
                    renderedBytes = localReport.Render(
                        reportType,
                        deviceInfo,
                        out mimeType,
                        out encoding,
                        out fileNameExtension,
                        out streams,
                        out warnings);


                    string fileName = string.Format("{0}.{1}", fileNameStr,
                                                    fileNameExtension);
                    Response.AppendHeader("Content-Disposition", string.Format("inline; filename={0}", fileName));
                    return File(renderedBytes, mimeType);
                }
                else
                {
                    var files = new List<byte[]>();
                    for (int i = 1; i <= 99; i++)
                    {
                        string deviceInfo = string.Format(
                        "<DeviceInfo>" +
                        "  <OutputFormat>{0}</OutputFormat>" +
                        "  <StartPage>{1}</StartPage>" +
                        "</DeviceInfo>", outputFormat, i);

                        Warning[] warnings;
                        string[] streams;
                        byte[] renderedBytes;

                        //Render the report
                        renderedBytes = localReport.Render(
                            reportType,
                            deviceInfo,
                            out mimeType,
                            out encoding,
                            out fileNameExtension,
                            out streams,
                            out warnings);
                        if (renderedBytes.Length == 0)
                            break;

                        files.Add(renderedBytes);
                    }
                    if (files.Any())
                    {
                        if (files.Count == 1)
                        {
                            string fileName = string.Format("{0}.{1}", fileNameStr,
                                                    fileNameExtension);
                            Response.AppendHeader("Content-Disposition",
                                                          string.Format("inline; filename={0}", fileName));
                            return File(files[0], mimeType);
                        }
                        //else
                        //{
                        //    using (var zip = new Ionic.Zip.ZipFile())
                        //    {
                        //        var guid = Guid.NewGuid();

                        //        if (!System.IO.Directory.Exists(Upload.UploadHandler.TemporaryStorageRoot))
                        //            System.IO.Directory.CreateDirectory(Upload.UploadHandler.TemporaryStorageRoot);

                        //        var filePath = UploadHandler.TemporaryStorageRoot + guid + "/";

                        //        if (!System.IO.Directory.Exists(filePath))
                        //            System.IO.Directory.CreateDirectory(filePath);

                        //        int i = 1;
                        //        foreach (var file in files)
                        //        {
                        //            string fileName = string.Format("{0}_{1}.{2}", fileNameStr, i, outputFormat);
                        //            System.IO.File.WriteAllBytes(filePath + fileName, file);

                        //            if (System.IO.File.Exists(filePath + fileName))
                        //            {
                        //                zip.AddFile(filePath + fileName, string.Empty);
                        //            }
                        //            i++;
                        //        }
                        //        string zipName = string.Format("{0}.zip", fileNameStr);
                        //        zip.Save(filePath + zipName);
                        //        if (!System.IO.Directory.Exists(filePath + zipName))
                        //        {
                        //            Response.AppendHeader("Content-Disposition",
                        //                                  string.Format("attachment; filename={0}", zipName));
                        //            return File(filePath + zipName, "application/octet-stream");
                        //        }
                        //    }
                        //}
                    }
                    return null;
                }
            }
        }


        protected ActionResult Report(string reportName, HttpRequestBase httpRequestBase, ReportDataSource[] reportDataSources, string fileNameStr, string format = "PDF", int page = 0)
        {
            if (format == null || format.Equals("web", StringComparison.InvariantCultureIgnoreCase))
            {
                var url = string.Format(@"~/Reports/{0}.aspx", reportName);
                if (httpRequestBase.RequestContext.RouteData.Values.ContainsKey("id"))
                {
                    var id = httpRequestBase.RequestContext.RouteData.Values["id"];
                    url += string.Format(@"?{0}={1}", "id", id);
                }



                var allKey = httpRequestBase.QueryString.AllKeys;
                if (allKey.Any())
                {
                    url += "?";
                }

                for (int i = 0; i < httpRequestBase.QueryString.Count; i++)
                {
                    url += string.Format("{0}={1}", allKey[i], httpRequestBase.QueryString[i]);
                    if (i != httpRequestBase.QueryString.Count - 1)
                        url += "&";
                }
                Response.Redirect(url);
                return null;
            }
            else
            {

                var zipping = false;

                var localReport = new LocalReport();

                localReport.ReportPath = Server.MapPath(string.Format(@"~/Reports/{0}.rdlc", reportName));

                foreach (var reportDataSource in reportDataSources)
                {
                    localReport.DataSources.Add(reportDataSource);
                }


                string reportType;
                string outputFormat;
                if (format.Equals("jpeg", StringComparison.InvariantCultureIgnoreCase))
                {
                    reportType = "image";
                    outputFormat = "jpeg";
                    if (page == 0)
                    {
                        zipping = true;
                    }
                }
                else if (format.Equals("png", StringComparison.InvariantCultureIgnoreCase))
                {
                    reportType = "image";
                    outputFormat = "png";
                    if (page == 0)
                    {
                        zipping = true;
                    }
                }
                else if (format.Equals("tiff", StringComparison.InvariantCultureIgnoreCase))
                {
                    reportType = "image";
                    outputFormat = "tiff";
                    page = 0;
                }
                else if (format.Equals("excel", StringComparison.InvariantCultureIgnoreCase))
                {
                    reportType = "Excel";
                    outputFormat = "xls";
                    page = 0;
                }
                else if (format.Equals("word", StringComparison.InvariantCultureIgnoreCase))
                {
                    reportType = "Word";
                    outputFormat = "doc";
                    page = 0;
                }
                else
                {
                    reportType = "PDF";
                    outputFormat = "PDF";
                    page = 0;
                }
                string mimeType = string.Empty;
                string encoding;
                string fileNameExtension = null;
                if (zipping == false)
                {
                    string deviceInfo = string.Format(
                        "<DeviceInfo>" +
                        "  <OutputFormat>{0}</OutputFormat>" +
                        "  <StartPage>{1}</StartPage>" +
                        "</DeviceInfo>", outputFormat, page);

                    Warning[] warnings;
                    string[] streams;
                    byte[] renderedBytes;

                    var paramReport = localReport.GetParameters();
                    var paramDic = paramReport.ToDictionary(info => info.Name);
                    if (paramDic.ContainsKey("UserID"))
                    {
                        localReport.SetParameters(new ReportParameter("UserID", HttpContext.User.Identity.Name));
                    }

                    //Render the report
                    renderedBytes = localReport.Render(
                        reportType,
                        deviceInfo,
                        out mimeType,
                        out encoding,
                        out fileNameExtension,
                        out streams,
                        out warnings);


                    string fileName = string.Format("{0}.{1}", fileNameStr,
                                                    fileNameExtension);
                    Response.AppendHeader("Content-Disposition", string.Format("inline; filename={0}", fileName));
                    return File(renderedBytes, mimeType);
                }
                else
                {
                    var files = new List<byte[]>();
                    for (int i = 1; i <= 99; i++)
                    {
                        string deviceInfo = string.Format(
                        "<DeviceInfo>" +
                        "  <OutputFormat>{0}</OutputFormat>" +
                        "  <StartPage>{1}</StartPage>" +
                        "</DeviceInfo>", outputFormat, i);

                        Warning[] warnings;
                        string[] streams;
                        byte[] renderedBytes;

                        //Render the report
                        renderedBytes = localReport.Render(
                            reportType,
                            deviceInfo,
                            out mimeType,
                            out encoding,
                            out fileNameExtension,
                            out streams,
                            out warnings);
                        if (renderedBytes.Length == 0)
                            break;

                        files.Add(renderedBytes);
                    }
                    if (files.Any())
                    {
                        if (files.Count == 1)
                        {
                            string fileName = string.Format("{0}.{1}", fileNameStr,
                                                    fileNameExtension);
                            Response.AppendHeader("Content-Disposition",
                                                          string.Format("inline; filename={0}", fileName));
                            return File(files[0], mimeType);
                        }
                        //else
                        //{
                        //    using (var zip = new Ionic.Zip.ZipFile())
                        //    {
                        //        var guid = Guid.NewGuid();

                        //        if (!System.IO.Directory.Exists(Upload.UploadHandler.TemporaryStorageRoot))
                        //            System.IO.Directory.CreateDirectory(Upload.UploadHandler.TemporaryStorageRoot);

                        //        var filePath = UploadHandler.TemporaryStorageRoot + guid + "/";

                        //        if (!System.IO.Directory.Exists(filePath))
                        //            System.IO.Directory.CreateDirectory(filePath);

                        //        int i = 1;
                        //        foreach (var file in files)
                        //        {
                        //            string fileName = string.Format("{0}_{1}.{2}", fileNameStr, i, outputFormat);
                        //            System.IO.File.WriteAllBytes(filePath + fileName, file);

                        //            if (System.IO.File.Exists(filePath + fileName))
                        //            {
                        //                zip.AddFile(filePath + fileName, string.Empty);
                        //            }
                        //            i++;
                        //        }
                        //        string zipName = string.Format("{0}.zip", fileNameStr);
                        //        zip.Save(filePath + zipName);
                        //        if (!System.IO.Directory.Exists(filePath + zipName))
                        //        {
                        //            Response.AppendHeader("Content-Disposition",
                        //                                  string.Format("attachment; filename={0}", zipName));
                        //            return File(filePath + zipName, "application/octet-stream");
                        //        }
                        //    }
                        //}
                    }
                    return null;
                }
            }
        }

        protected ActionResult Report(string reportName, HttpRequestBase httpRequestBase, ReportDataSource reportDataSource, string fileNameStr, string format = "PDF", int page = 0)
        {
            return Report(reportName, httpRequestBase,
                          new[] { reportDataSource }, fileNameStr,
                          format, page);

        }
    }
}