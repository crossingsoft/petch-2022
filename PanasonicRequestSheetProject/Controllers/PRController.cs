﻿using Microsoft.Reporting.WebForms;
using PanasonicRequestSheetProject.Controllers.Abstract;
using PanasonicRequestSheetProject.Models.RequestSheet;
using PanasonicRequestSheetProject.Services;
using PanasonicRequestSheetProject.ViewModel;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace PanasonicRequestSheetProject.Controllers
{
    public class PRController : BaseController
    {
        [HttpGet]
        public ActionResult AttachFileInfo(int parentId)
        {
            var model = new ERS_T_AttachFileTable();
            model.RequestHDRefer = parentId;

            return View(model);
        }

        [HttpGet]
        public void DownloadAttachmentInfo(int id)
        {
            var service = new RequestSheetEntities();
            var model = service.ERS_T_AttachFileTable.FirstOrDefault(m => m.AttachID == id);
            if (model == null)
                throw new Exception("Row not found.");

            Response.ContentType = "application/octet-stream";
            Response.AppendHeader("Content-Disposition", "attachment; filename=" + Path.GetFileName(model.Path));
            Response.WriteFile(model.Path);
            Response.End();
        }

        [HttpPost]
        public ActionResult AttachFileInfoSaveDetail(ERS_T_AttachFileTable model, HttpPostedFileBase file)
        {
            var service = new RequestSheetEntities();

            var date = DateTime.Now;
            model.Active = true;
            model.CreateDate = date;
            model = service.ERS_T_AttachFileTable.Add(model);

            service.SaveChanges();
            var attachmentService = new AttachmentService();
            attachmentService.SyncAttachFileInfo(model.AttachID, file);

            return View("SmartFormSuccess");
        }

        [HttpGet]
        public ActionResult PrintForm(int id)
        {
            var service = new RequestSheetEntities();
            var dsData = service.GetPurchReqForm(id).ToListSafe();
            var dsImages = service.ERS_T_AttachFileTable.Where(m => m.RequestHDRefer == id && (m.OriginFileName.Contains(".png") || m.OriginFileName.Contains(".jpg"))).ToListSafe();
            var dsImageViewModels = new List<PurchReqImagePrintFormViewModel>();


            decimal countDsData = ((decimal)dsData.Count() / 7);
            decimal countDsImages = ((decimal)dsImages.Count() / 4);

            int countDsDataInt = (int)Math.Ceiling(countDsData);
            int countDsImagesInt = (int)Math.Ceiling(countDsImages);

            var maxPage = countDsDataInt > countDsImagesInt ? countDsDataInt : countDsImagesInt;

            var realPageDsInt = 7 * maxPage;
            var addRowDsInt = realPageDsInt - dsData.Count();

            for (var r = 1; r <= addRowDsInt; r++)
            {
                dsData.Add(new GetPurchReqForm_Result());
            }


            var realPageDsImageInt = 4 * maxPage;
            var addRowDsImageInt = realPageDsImageInt - dsImages.Count();

            for (var r = 1; r <= addRowDsImageInt; r++)
            {
                dsImages.Add(new ERS_T_AttachFileTable());
            }

            int i = 1;
            foreach (var row in dsData)
            {
                row.Row = i;
                row.SignatureApp1 = row.SignatureApp1.ToImagePrintForm();
                row.SignatureApp2 = row.SignatureApp2.ToImagePrintForm();
                row.SignatureApp3 = row.SignatureApp3.ToImagePrintForm();
                row.SignatureApp4 = row.SignatureApp4.ToImagePrintForm();
                i++;
            }

            i = 1;
            foreach (var row in dsImages)
            {
                if (!string.IsNullOrEmpty(row.OriginFileName) && !string.IsNullOrEmpty(row.Path))
                {
                    var originalFileName = row.OriginFileName.ToLower();
                    if (originalFileName.Contains(".png") || originalFileName.Contains(".jpg"))
                    {
                        byte[] imageArray = System.IO.File.ReadAllBytes(row.Path);
                        string base64ImageRepresentation = Convert.ToBase64String(imageArray);

                        var dsImageViewModel = new PurchReqImagePrintFormViewModel();
                        dsImageViewModel.Data = base64ImageRepresentation;
                        dsImageViewModel.Description = row.Description;
                        dsImageViewModel.ItemNo = "";
                        dsImageViewModel.Row = i;
                        dsImageViewModels.Add(dsImageViewModel);
                        i++;
                    }
                }
            }

            ReportDataSource[] reportDataSources = null;

            reportDataSources = new ReportDataSource[2];
            reportDataSources[0] = new ReportDataSource("DataSet", dsData);
            reportDataSources[1] = new ReportDataSource("DataSetImage", dsImageViewModels);

            return Report("PRForm", Request, reportDataSources, string.Format("PRForm"), "pdf");

        }
    }
}